/**
  ******************************************************************************
  * @file    MX25L128.h
  * @author  MCD Application Team
  * @brief   This file contains all the description of the  QSPI memory.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2016 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef ___H
#define ___H
//#include "stdint.h"

#ifdef __cplusplus
 extern "C" {
#endif 

/* Includes ------------------------------------------------------------------*/

/** @addtogroup BSP
  * @{
  */ 

/** @addtogroup Components
  * @{
  */ 
  
/** @addtogroup 
  * @{
  */

/** @defgroup _Exported_Types
  * @{
  */
	 
/* MX25L128 Component Error codes *********************************************/
#define MX25L128_OK                           0
#define MX25L128_ERROR_INIT                  -1
#define MX25L128_ERROR_COMMAND               -2
#define MX25L128_ERROR_TRANSMIT              -3
#define MX25L128_ERROR_RECEIVE               -4
#define MX25L128_ERROR_AUTOPOLLING           -5
#define MX25L128_ERROR_MEMORYMAPPED          -6
/**exported type **/
	 
/******************MX25L128_Info_t**********************/
typedef struct
{
  uint32_t FlashSize;          /*!< Size of the flash */
  uint32_t EraseSectorSize;    /*!< Size of sectors for the erase operation */
  uint32_t EraseSectorsNumber; /*!< Number of sectors for the erase operation */
  uint32_t ProgPageSize;       /*!< Size of pages for the program operation */
  uint32_t ProgPagesNumber;    /*!< Number of pages for the program operation */
} MX25L128_Info_t;



/******************MX25L128_Transfer_t**********************/
typedef enum
{
  MX25L128_SPI_MODE = 0,                 /*!< 1-1-1 commands, Power on H/W default setting */
  MX25L128_SPI_2IO_MODE,                 /*!< 1-1-2, 1-2-2 read commands                   */
  MX25L128_SPI_4IO_MODE,                 /*!< 1-1-4, 1-4-4 read commands                   */
  MX25L128_QPI_MODE                      /*!< 4-4-4 commands                               */
} MX25L128_Interface_t;

/******************MX25L128_Transfer_t**********************/

typedef enum
{
  MX25L128_STR_TRANSFER = 0,             /* Single Transfer Rate */
  MX25L128_DTR_TRANSFER                  /* Double Transfer Rate */
} MX25L128_Transfer_t;


/******************MX25L128_DualFlash_t**********************/


typedef enum
{
  MX25L128_DUALFLASH_DISABLE = QSPI_DUALFLASH_DISABLE, /*!<  Single flash mode              */
  MX25L128_DUALFLASH_ENABLE  =QSPI_DUALFLASH_ENABLE
} MX25L128_DualFlash_t;



/******************MX25L128_Erase_t**********************/


typedef enum
{
  MX25L128_ERASE_4K = 0,                 /*!< 4K size Sector erase */
  MX25L128_ERASE_32K,                    /*!< 32K size Block erase */
  MX25L128_ERASE_64K,                    /*!< 64K size Block erase */
  MX25L128_ERASE_CHIP                    /*!< Whole chip erase     */
} MX25L128_Erase_t;

   
/**
  * @}
  */ 

/** @defgroup _Exported_Constants
  * @{
  */
   
/** 
  * @brief   Configuration  
  */  

   
/** 
  * @brief   Registers  
  */ 
/**
  * @}

  */
  
///** @defgroup _Exported_Functions
//  * @{
//  */ 
//
//	int32_t MX25L128_GetFlashInfo			(MX25L128_Info_t *pInfo);
//	int32_t MX25L128_AutoPollingMemReady	(QSPI_HandleTypeDef *Ctx, MX25L128_Interface_t Mode);
//	int32_t MX25L128_Enter4BytesAddressMode	(QSPI_HandleTypeDef *Ctx, MX25L128_Interface_t Mode);
//	int32_t MX25L128_ReadSTR				(QSPI_HandleTypeDef *Ctx, MX25L128_Interface_t Mode, uint8_t *pData, uint32_t ReadAddr, uint32_t Size);
//	int32_t MX25L128_ReadDTR				(QSPI_HandleTypeDef *Ctx, MX25L128_Interface_t Mode, uint8_t *pData, uint32_t ReadAddr, uint32_t Size);
//	int32_t MX25L128_WriteEnable			(QSPI_HandleTypeDef *Ctx, MX25L128_Interface_t Mode);
//	int32_t MX25L128_PageProgram			(QSPI_HandleTypeDef *Ctx, MX25L128_Interface_t Mode, uint8_t *pData, uint32_t WriteAddr, uint32_t Size);
//	int32_t MX25L128_ResetEnable			(QSPI_HandleTypeDef *Ctx, MX25L128_Interface_t Mode);
//	int32_t MX25L128_ResetMemory			(QSPI_HandleTypeDef *Ctx, MX25L128_Interface_t Mode);
//
///**
//  * @}
//  */ 
      
#ifdef __cplusplus
}
#endif

#endif /* ___H */

/**
  * @}
  */ 

/**
  * @}
  */ 

/**
  * @}
  */
  
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
