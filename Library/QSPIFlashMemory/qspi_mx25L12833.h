#ifndef __QPI_MX25L12833_H
#define __QPI_MX25L12833_H
/**
 * \file            qspi_mx25L12833.h
 * \brief           --
 * CREATED: 		31/03/2020, by luciano Ciaperoni
 * @attention
 *
 * All rights reserved.</center></h2>
 *
 * @verbatim
 *
 * @endverbatim
*/
	#include "MX25L128_conf.h"

	#define MX25L128_FLASH_SIZE                  	0x8000000	/* 128 MBits 	=> 		16MBytes */
	#define MX25L128_SECTOR_SIZE                 	0x10000   	/* 256 sectors of 		64KBytes */
	#define MX25L128_SUBSECTOR_SIZE              	0x1000   	/* 16384 subsectors of 	4kBytes */
	#define MX25L128_PAGE_SIZE                   	0x100     	/* 65536 pages of 		256 bytes */

	#define MX25L128_DUMMY_CYCLES_READ_QUAD      	3
	#define MX25L128_DUMMY_CYCLES_READ_QUAD_IO   	10
	#define MX25L128_DUMMY_CYCLES_READ_DTR       	6
	#define MX25L128_DUMMY_CYCLES_READ_QUAD_DTR  	8

	#define MX25L128_BULK_ERASE_MAX_TIME         	600000
	#define MX25L128_SECTOR_ERASE_MAX_TIME       	2000
	#define MX25L128_SUBSECTOR_ERASE_MAX_TIME    	800
	//
	// ------------------------------------------------------------------------
	//		COMMAND
	//
	/* Reset Operations */
	#define RESET_ENABLE_CMD                    	0x66
	#define RESET_MEMORY_CMD                     	0x99

	/* Identification Operations */
	#define READ_ID_CMD                          	0x9F
	#define MULTIPLE_IO_READ_ID_CMD              	0xAB
	#define READ_SERIAL_FLASH_DISCO_PARAM_CMD    	0x5A	//RDSFDP
	#define READ_QPIID							 	0xAF

	/* Read Operations */
	#define READ_CMD                             	0x03	//READ

	#define FAST_READ_CMD                        	0x0B	//

	#define DUAL_OUT_FAST_READ_CMD               	0x3B	//

	#define QUAD_OUT_FAST_READ_CMD               	0x6B	//

	#define QUAD_INOUT_FAST_READ_CMD             	0xEB	//

	/* Write Operations */
	#define WRITE_ENABLE_CMD                     	0x06	//
	#define WRITE_DISABLE_CMD                    	0x04	//

	/* Register Operations */
	#define READ_STATUS_REG_CMD                  	0x05	//RDSR
	#define READ_CFG_REG_CMD                     	0x15   	//RDCR
	#define WRITE_STATUS_CFG_REG_CMD             	0x01	//

	#define READ_LOCK_REG_CMD                    	0x2B	//
	#define WRITE_LOCK_REG_CMD                   	0x2F	//

	/* Program Operations */
	#define PAGE_PROG_CMD                        	0x02	//

	#define QUAD_IN_FAST_PROG_CMD                	0x38	//

	/* Erase Operations */
	#define SUBSECTOR_ERASE_CMD                  	0x20	//
 	#define SECTOR32K_ERASE_CMD             		0x52	//
	#define SECTOR64K_ERASE_CMD             		0xD8	//
	   
	#define SECTOR_ERASE_CMD                     	0xD8	//

	#define BULK_ERASE_CMD                       	0xC7	// si pu� usare anche 0x60

	#define PROG_ERASE_RESUME_CMD                	0x30	//
	#define PROG_ERASE_SUSPEND_CMD               	0xB0	//

	/* 4-byte Address Mode Operations */
	#define ENTER_4_BYTE_ADDR_MODE_CMD           	0xB7
	#define EXIT_4_BYTE_ADDR_MODE_CMD            	0xE9

	/* Quad Operations */
	#define ENTER_QUAD_CMD                       	0x35	//EQIO
	#define EXIT_QUAD_CMD                        	0xF5	//RSTQIO
	  
	/* Added for compatibility */ 
	#define QPI_READ_4_BYTE_ADDR_CMD             QUAD_INOUT_FAST_READ_CMD
	#define QPI_PAGE_PROG_4_BYTE_ADDR_CMD        QSPI_PAGE_PROG_4_BYTE_ADDR_CMD

	// ------------------------------------------------------------------------

	/* Status Register */
	typedef struct{
		
		unsigned short wip		:1;	//!< 0 not in write operation = 1 write operation
		unsigned short wel		:1;	//!< 0 NOT write enable	= 1 write enable
		unsigned short bp		:4;
		unsigned short qe		:1;	//!< 0 not quad operation = 1 quad operation
		unsigned short srwd		:1;	//!< 0 status register write enable = 1 write disable
		
	}_MX25L128_SR;

	#define MX25L128_SR_WIP                      ((uint8_t)0x01)    /*!< Write in progress */
	#define MX25L128_SR_WREN                     ((uint8_t)0x02)    /*!< Write enable latch */
	#define MX25L128_SR_BLOCKPR                  ((uint8_t)0x5C)    /*!< Block protected against program and erase operations */
	#define MX25L128_SR_PRBOTTOM                 ((uint8_t)0x20)    /*!< Protected memory area defined by BLOCKPR starts from top or bottom */
	#define MX25L128_SR_QUADEN                   ((uint8_t)0x40)    /*!< Quad IO mode enabled if =1 */
	#define MX25L128_SR_SRWREN                   ((uint8_t)0x80)    /*!< Status register write enable/disable */

	/* Configuration Register */
	typedef struct{
		
		unsigned short ods		:3;	//!< output driver strngth
		unsigned short tb		:1;	//!< 0 Top area protect = 1 bottom area protect
		unsigned short rs		:2;
		unsigned short dc		:2;	//!< dummy cycle
		
	}_MX25L128_CR;


	#define MX25L128_CR_ODS                      ((uint8_t)0x07)    /*!< Output driver strength */
	#define MX25L128_CR_ODS_30                   ((uint8_t)0x07)    /*!< Output driver strength 30 ohms (default)*/
	#define MX25L128_CR_ODS_15                   ((uint8_t)0x06)    /*!< Output driver strength 15 ohms */
	#define MX25L128_CR_ODS_20                   ((uint8_t)0x05)    /*!< Output driver strength 20 ohms */
	#define MX25L128_CR_ODS_45                   ((uint8_t)0x03)    /*!< Output driver strength 45 ohms */
	#define MX25L128_CR_ODS_60                   ((uint8_t)0x02)    /*!< Output driver strength 60 ohms */
	#define MX25L128_CR_ODS_90                   ((uint8_t)0x01)    /*!< Output driver strength 90 ohms */
	#define MX25L128_CR_TB                       ((uint8_t)0x08)    /*!< Top/Bottom bit used to configure the block protect area */
	#define MX25L128_CR_PBE                      ((uint8_t)0x10)    /*!< Preamble Bit Enable */
	#define MX25L128_CR_4BYTE                    ((uint8_t)0x20)    /*!< 3-bytes or 4-bytes addressing */
	#define MX25L128_CR_NB_DUMMY                 ((uint8_t)0x80)    /*!< Number of dummy clock cycles */

	#define MX25L128_MANUFACTURER_ID             ((uint8_t)0xC2)
	#define MX25L128_DEVICE_ID_MEM_TYPE          ((uint8_t)0x20)
	#define MX25L128_DEVICE_ID_MEM_CAPACITY      ((uint8_t)0x1A)
	#define MX25L128_UNIQUE_ID_DATA_LENGTH       ((uint8_t)0x10)  

	// secure register
	typedef struct{
		
		unsigned short otp		:1;	//!< = 0 non factory lock		= 1 factory lock
		unsigned short ldso		:1;	//!< = 0 not lock down			= 1 lock down cannot program
		unsigned short psb		:1;	//!< = 0 program not suspended	= 1 program suspended
		unsigned short esb		:1;	//!< = 0 erase not suspended	= 1 erase suspended
		unsigned short reserved	:1;
		unsigned short p_fail	:1;	//!< = 0 program ok				= 1 program fail
		unsigned short e_fail	:1;	//!< = 0 Erase ok				= 1 Erase fail
		unsigned short wpsel	:1;	//!< = 0 Block lock  			= 1 Individual sector protection mode
		
	}_MX25L128_SER;
	#define MX25L128_SER_WPSEL		((uint8_t)0x80) //!< = 0 Block lock  			= 1 Individual sector protection mode
	#define MX25L128_SER_E_FAIL		((uint8_t)0x40) //!< = 0 Erase ok				= 1 Erase fail
	#define MX25L128_SER_P_FAIL		((uint8_t)0x20) //!< = 0 program ok				= 1 program fail
	#define MX25L128_SER_ESB		((uint8_t)0x08) //!< = 0 erase not suspended	= 1 erase suspended
	#define MX25L128_SER_PSB		((uint8_t)0x04) //!< = 0 program not suspended	= 1 program suspended
	#define MX25L128_SER_LDSO		((uint8_t)0x02) //!< = 0 not lock down			= 1 lock down cannot program
	#define MX25L128_SER_OTP		((uint8_t)0x01) //!< = 0 non factory lock		= 1 factory lock

	typedef struct{
		unsigned char Manufacture;
		unsigned char Type;			//memory type
		unsigned char density;
	}_MEMORY_TYPE;

	/* QSPI Error codes */
	#define QSPI_OK            ((uint8_t)0x00)
	#define QSPI_ERROR         ((uint8_t)0x01)
	#define QSPI_BUSY          ((uint8_t)0x02)
	#define QSPI_NOT_SUPPORTED ((uint8_t)0x04)
	#define QSPI_SUSPENDED     ((uint8_t)0x08)
	#define QSPI_ERASE_FAIL    ((uint8_t)0x10)
	#define QSPI_TIMEOUT	   ((uint8_t)0x20)
	#define QSPI_INIT_NO	   ((uint8_t)0x30)
	#define QSPI_INIT_OK	   ((uint8_t)0x35)

	/* QSPI Info */
	typedef struct {
		uint32_t FlashSize;          	/*!< Size of the flash */
		uint32_t EraseSectorSize;    	/*!< Size of sectors for the erase operation */
		uint32_t EraseSectorsNumber; 	/*!< Number of sectors for the erase operation */
		uint32_t ProgPageSize;       	/*!< Size of pages for the program operation */
		uint32_t ProgPagesNumber;    	/*!< Number of pages for the program operation */
		_MEMORY_TYPE	memory;
		_MX25L128_CR  *config_reg;		//!< configuration register
		_MX25L128_SR  *status_reg;
		_MX25L128_SER *security_reg;	
		unsigned int init; 				//!< indica se il driver � correttamente inizializzato
	} QSPI_Info;

	uint8_t QSPI_Init       	( void );
	uint8_t QSPI_DeInit     	( void );
	uint8_t QSPI_Read       	( uint8_t* pData, uint32_t ReadAddr, uint32_t Size );
	uint8_t QSPI_Write      	( uint8_t* pData, uint32_t WriteAddr, uint32_t Size );
	uint8_t QSPI_GetInfo    	( QSPI_Info* pInfo );
	uint8_t QSPI_GetDeviceId	( void );
	int32_t SpiReadID			( uint8_t *ID );
	uint8_t QSPI_Erase_Sector	( uint32_t Address );
	uint8_t QSPI_Erase_Block32K ( uint32_t Address );
	uint8_t QSPI_Erase_Block64K ( uint32_t Address );
	uint8_t QSPI_Erase_Chip 	( void );

	uint8_t QSPI_EnableMemoryMappedMode( void );

#endif 
