#ifndef MX25L128_CONF_H
#define MX25L128_CONF_H
/**
 * \file            MX25L128_conf.h
 * \brief           --
 * CREATED: 		01/04/2020, by luciano Ciaperoni
 * @attention
 *
 * All rights reserved.</center></h2>
 *
 * @verbatim
 *
 * @endverbatim
*/

	#include <stdbool.h>
	#include "m_error.h"
	#include "mtype.h"
	#include "stm32h7xx.h"
	#include "stm32h7xx_hal.h"
	#include "qspi_mx25l12833.h"
	#include "memory.h"



#ifdef __cplusplus
 extern "C" {
#endif


/** @addtogroup MX25L12833
  * @{
  */

	#define QSPI_CLOCKPRESCALER				4	//
	#define QSPI_FIFO_THRESHOLD				4
	#define QSPI_DUMMY_CYCLES				2	// = 6 cilcli dummy, vedi datasheet
	#define MX25L128_DUMMY_CYCLES_READ     	8
	  
//-----------------------------------------------------------------------------
//		RADAR
//
//	#define QSPI_CLK_ENABLE()              	__HAL_RCC_QSPI_CLK_ENABLE()
//	#define QSPI_CLK_DISABLE()             	__HAL_RCC_QSPI_CLK_DISABLE()
//	  
//	#define QSPI_CLK_GPIO_CLK_ENABLE()     	__HAL_RCC_GPIOB_CLK_ENABLE()
//	  
//	#define QSPI_CS_GPIO_CLK_ENABLE()  		__HAL_RCC_GPIOB_CLK_ENABLE()
//	  
//	#define QSPI_D0_GPIO_CLK_ENABLE()  		__HAL_RCC_GPIOC_CLK_ENABLE()
//	#define QSPI_D1_GPIO_CLK_ENABLE()  		__HAL_RCC_GPIOC_CLK_ENABLE()
//	#define QSPI_D2_GPIO_CLK_ENABLE()  		__HAL_RCC_GPIOE_CLK_ENABLE()
//	#define QSPI_D3_GPIO_CLK_ENABLE()  		__HAL_RCC_GPIOA_CLK_ENABLE()
//
//	#define QSPI_FORCE_RESET()         		__HAL_RCC_QSPI_FORCE_RESET()
//	#define QSPI_RELEASE_RESET()       		__HAL_RCC_QSPI_RELEASE_RESET()
//
//	/* Definition for QSPI Pins */
//	#define QSPI_CLK_PIN               		GPIO_PIN_2
//	#define QSPI_CLK_GPIO_PORT         		GPIOB
//	#define QSPI_CLK_PIN_AF            		GPIO_AF9_QUADSPI
//	/* Bank 1 */
//	#define QSPI_CS_PIN            			GPIO_PIN_6
//	#define QSPI_CS_GPIO_PORT      			GPIOB
//	#define QSPI_CS_PIN_AF             		GPIO_AF10_QUADSPI
//		  
//	#define QSPI_D0_PIN            			GPIO_PIN_9
//	#define QSPI_D0_GPIO_PORT      			GPIOC
//	#define QSPI_D0_PIN_AF             		GPIO_AF9_QUADSPI
//	#define QSPI_D1_PIN            			GPIO_PIN_10
//	#define QSPI_D1_GPIO_PORT      			GPIOC
//	#define QSPI_D1_PIN_AF             		GPIO_AF9_QUADSPI
//		  
//	#define QSPI_D2_PIN            			GPIO_PIN_2
//	#define QSPI_D2_GPIO_PORT      			GPIOE
//	#define QSPI_D2_PIN_AF             		GPIO_AF9_QUADSPI
//		  
//	#define QSPI_D3_PIN		            	GPIO_PIN_1
//	#define QSPI_D3_GPIO_PORT      			GPIOA
//	#define QSPI_D3_PIN_AF             		GPIO_AF9_QUADSPI

	#define QSPI_CLK_ENABLE()           __HAL_RCC_QSPI_CLK_ENABLE()
	#define QSPI_CLK_DISABLE()          __HAL_RCC_QSPI_CLK_DISABLE()
	#define QSPI_CLK_GPIO_CLK_ENABLE()  __HAL_RCC_GPIOB_CLK_ENABLE()
	#define QSPI_CS_GPIO_CLK_ENABLE()	__HAL_RCC_GPIOB_CLK_ENABLE()
	  
	#define QSPI_D0_GPIO_CLK_ENABLE()  	__HAL_RCC_GPIOD_CLK_ENABLE()
	#define QSPI_D1_GPIO_CLK_ENABLE()	__HAL_RCC_GPIOD_CLK_ENABLE()
	#define QSPI_D2_GPIO_CLK_ENABLE()  	__HAL_RCC_GPIOE_CLK_ENABLE()
	#define QSPI_D3_GPIO_CLK_ENABLE()  	__HAL_RCC_GPIOD_CLK_ENABLE()

	#define QSPI_FORCE_RESET()         	__HAL_RCC_QSPI_FORCE_RESET()
	#define QSPI_RELEASE_RESET()       	__HAL_RCC_QSPI_RELEASE_RESET()

	/* Definition for QSPI Pins */
	#define QSPI_CLK_PIN               	GPIO_PIN_2
	#define QSPI_CLK_GPIO_PORT         	GPIOB
	#define QSPI_CLK_PIN_AF            	GPIO_AF9_QUADSPI
	/* Bank 1 */
	#define QSPI_CS_PIN            		GPIO_PIN_10
	#define QSPI_CS_GPIO_PORT      		GPIOB
	#define QSPI_CS_PIN_AF             	GPIO_AF10_QUADSPI
		  
	#define QSPI_D0_PIN            		GPIO_PIN_11
	#define QSPI_D0_GPIO_PORT      		GPIOD
	#define QSPI_D0_PIN_AF             	GPIO_AF9_QUADSPI
	
	#define QSPI_D1_PIN            		GPIO_PIN_12
	#define QSPI_D1_GPIO_PORT      		GPIOD
	#define QSPI_D1_PIN_AF             	GPIO_AF9_QUADSPI
		  
	#define QSPI_D2_PIN            		GPIO_PIN_2
	#define QSPI_D2_GPIO_PORT      		GPIOE
	#define QSPI_D2_PIN_AF             	GPIO_AF9_QUADSPI
		  
	#define QSPI_D3_PIN		            GPIO_PIN_13
	#define QSPI_D3_GPIO_PORT      		GPIOD
	#define QSPI_D3_PIN_AF             	GPIO_AF9_QUADSPI


/**
  * @}
  */
	  
#ifdef __cplusplus
}
#endif

#endif 
