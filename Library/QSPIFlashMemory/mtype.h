#ifndef __MTYPE_H
#define __MTYPE_H

	typedef enum	{
		OFF,
		ON
	}_on_off;

	#ifndef false
		#define false	0
	#endif

	#ifndef true
		#define true 1
	#endif

	#ifndef FALSE
		#define FALSE	0
	#endif

	#ifndef TRUE
		#define TRUE 1
	#endif

	typedef union _data {
		float f;
		char  s[sizeof(float)];
	} float_byte;

	#define SHORT_LOWB(x)		((unsigned char)((unsigned short)x))
	#define SHORT_HIGHT(x)		((unsigned char)(((unsigned short)x)>>8))
	#define _reverse(x)			((SHORT_LOWB(x) << 8) | SHORT_HIGHT(x))
	#define countof(a)   		(sizeof(a) / sizeof(*(a)))
	
	#define __STR2__(x) #x
	#define __STR1__(x) __STR2__(x)
	#define MAX_RESOLUTION_14B	(16383)
	#define MAX_RESOLUTION_13B	( 8191)
	#define MAX_RESOLUTION_12B	( 4095)

/* Exported macro ------------------------------------------------------------*/
#define COUNTOF(__BUFFER__)   (sizeof(__BUFFER__) / sizeof(*(__BUFFER__)))
	
	typedef enum	{
		M_OK	= 0,
		MERR_PERIFER = -1	// errore su inizializzazione periferica
	}_m_error;

#endif