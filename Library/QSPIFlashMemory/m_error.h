/**
  ******************************************************************************
  * @file           : m_errno.h
  * @brief          : Error Code
  ******************************************************************************
  ******************************************************************************
*/

#ifndef __M_ERRNO_H
#define __M_ERRNO_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Common Error codes */
typedef enum	{
	ERROR_NONE              		= 0,
	ERROR_NO_INIT                	= -1,
	ERROR_WRONG_PARAM            	= -2,
	ERROR_BUSY                   	= -3,
	ERROR_PERIPH_FAILURE         	= -4,
	ERROR_COMPONENT_FAILURE      	= -5,
	ERROR_UNKNOWN_FAILURE        	= -6,
	ERROR_UNKNOWN_COMPONENT      	= -7,
	ERROR_BUS_FAILURE            	= -8,
	ERROR_CLOCK_FAILURE          	= -9,
	ERROR_MSP_FAILURE            	= -10,
	ERROR_FEATURE_NOT_SUPPORTED   	= -11,
	ERROR_FLASH_WRITE_PROTECT   	= -100
}m_error; 
   
/* BUS error codes */

#define ERROR_BUS_TRANSACTION_FAILURE    -100
#define ERROR_BUS_ARBITRATION_LOSS       -101
#define ERROR_BUS_ACKNOWLEDGE_FAILURE    -102
#define ERROR_BUS_PROTOCOL_FAILURE       -103

#define ERROR_BUS_MODE_FAULT             -104
#define ERROR_BUS_FRAME_ERROR            -105
#define ERROR_BUS_CRC_ERROR              -106
#define ERROR_BUS_DMA_FAILURE            -107

#endif /*__M_ERRNO_H */

