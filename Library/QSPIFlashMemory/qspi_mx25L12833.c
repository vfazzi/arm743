		/**
 * \file            qspi_mx25L12833.c
 * \brief           --
 * CREATED: 		20/03/2020, luciano Ciaperoni
 * @attention
 *
 * All rights reserved.</center></h2>
 *
 * @verbatim
 *
 * @endverbatim
*/
//#include "system.h"
#include "msg.h"
#include "qspi_mx25l12833.h"

// PROTOTIPI FUNZIONE ESTERNE -------------------------------------------------

extern void QSPI_MspInit	( QSPI_HandleTypeDef *hqspi, void *Params );
extern void QSPI_MspDeInit	( QSPI_HandleTypeDef *hqspi, void *Params );

// VARIABILI ESTERNE ----------------------------------------------------------
// MACRO PRIVATE --------------------------------------------------------------
// VARIABILI GLOBALI ----------------------------------------------------------
// VARIABILI PRIVATE ----------------------------------------------------------

static unsigned int qmem_wait = 20; // tempo di attesa per cancellazione
QSPI_HandleTypeDef QSPIHandle;
unsigned char qmem_status 	= 0;
unsigned char qmem_config 	= 0;
unsigned char qmem_security	= 0;
QSPI_Info 	qmem;
#if QSPI_TIME_MEASURE == 1
	__elapsed_time tprog;
#else
	#define sys_get_time(x)
	#define sys_elapsedTime(x)
#endif
// PROTOTIPI FUNZIONE PRIVATE -------------------------------------------------

static uint8_t QSPI_ResetMemory				( QSPI_HandleTypeDef *hqspi );
static uint8_t QSPI_DummyCyclesCfg			( QSPI_HandleTypeDef *hqspi );
static uint8_t QSPI_EnterMemory_QPI			( QSPI_HandleTypeDef *hqspi );
static uint8_t QSPI_ExitMemory_QPI			( QSPI_HandleTypeDef *hqspi );
static uint8_t QSPI_OutDrvStrengthCfg		( QSPI_HandleTypeDef *hqspi );
static uint8_t QSPI_WriteEnable				( QSPI_HandleTypeDef *hqspi );
static uint8_t QSPI_AutoPollingMemReady  	( QSPI_HandleTypeDef *hqspi, uint32_t Timeout );
static uint8_t GetSecurityRegister			( void );
static uint8_t GetConfigurationRegister		( void );
static uint8_t GetStatusRegister			( void );
static uint8_t SPI_GetStatusRegister		( void );
static uint8_t GetStatusAndSecurityRegister	( void );
static uint8_t waitBusy						( unsigned int timout );
static uint8_t QSPI_Erase					( uint32_t Address, unsigned char cmd );
static uint8_t QuadGetDeviceId				( void );
HAL_StatusTypeDef QSPI_Command				( QSPI_HandleTypeDef *hqspi, QSPI_CommandTypeDef *s_command, uint32_t Timeout );
HAL_StatusTypeDef QSPI_Receive				( QSPI_HandleTypeDef *hqspi, uint8_t *pData, uint32_t Timeout );
static uint8_t QSPI_SetBlokProtection		( QSPI_HandleTypeDef *hqspi, unsigned char block  );

// ============================================================================
// 				FUNZIONI PUBBLICHE
// ============================================================================

// Inizializzazione 
//
//	@retval QSPI memory status
//
uint8_t QSPI_Init( void )
{ 
	RCC_PeriphCLKInitTypeDef 	m_clock;
	PLL2_ClocksTypeDef			pll2;
 
  	QSPIHandle.Instance = QUADSPI;
  	qmem.init = QSPI_INIT_NO;
	// deinizializza 
	if (HAL_QSPI_DeInit(&QSPIHandle) != HAL_OK)
		return QSPI_ERROR;
	
	HAL_RCCEx_GetPeriphCLKConfig( &m_clock );
	HAL_RCCEx_GetPLL2ClockFreq(&pll2);
	
	if ( pll2.PLL2_R_Frequency > 160000000 )	{
		msg_info("Frequenza in ingresso qspi troppo alta %f Mhz\rClockPrescaler %d\r",pll2.PLL2_R_Frequency, QSPI_CLOCKPRESCALER);
		return QSPI_ERROR;
	}

	// imposta il puntatore alla variabili locali dei registri
	qmem.security_reg					= (_MX25L128_SER *)&qmem_security;	
	qmem.status_reg 					= (_MX25L128_SR *)&qmem_status;
	qmem.config_reg						= (_MX25L128_CR *)&qmem_config;
	//
	// System level initialization 
	//
	QSPI_MspInit(&QSPIHandle, NULL);
	qmem.FlashSize          			= MX25L128_FLASH_SIZE;
	qmem.EraseSectorSize    			= MX25L128_SUBSECTOR_SIZE;
	qmem.EraseSectorsNumber 			= (MX25L128_FLASH_SIZE/MX25L128_SUBSECTOR_SIZE);
	qmem.ProgPageSize       			= MX25L128_PAGE_SIZE;
	qmem.ProgPagesNumber    			= (MX25L128_FLASH_SIZE/MX25L128_PAGE_SIZE);
  
	QSPIHandle.Init.ClockPrescaler     	= QSPI_CLOCKPRESCALER;   
	QSPIHandle.Init.FifoThreshold      	= QSPI_FIFO_THRESHOLD;
//	QSPIHandle.Init.SampleShifting     	= QSPI_SAMPLE_SHIFTING_HALFCYCLE;
	QSPIHandle.Init.SampleShifting     	= QSPI_SAMPLE_SHIFTING_NONE;
	QSPIHandle.Init.FlashSize          	= POSITION_VAL(MX25L128_FLASH_SIZE) - 1;
	QSPIHandle.Init.ChipSelectHighTime 	= QSPI_CS_HIGH_TIME_6_CYCLE; /* Min 30ns for nonRead */
	QSPIHandle.Init.ClockMode          	= QSPI_CLOCK_MODE_0;
	QSPIHandle.Init.FlashID            	= QSPI_FLASH_ID_1;
	QSPIHandle.Init.DualFlash          	= QSPI_DUALFLASH_DISABLE;
	//
//	QSPI_MspInit(&QSPIHandle, NULL);

	if (HAL_QSPI_Init(&QSPIHandle) != HAL_OK)	
		return QSPI_ERROR;

	// reset 
	if (QSPI_ResetMemory( &QSPIHandle ) != QSPI_OK)
		return QSPI_NOT_SUPPORTED;

	// funzionamento in QSPI
	if( QSPI_EnterMemory_QPI( &QSPIHandle )!=QSPI_OK )
		return QSPI_NOT_SUPPORTED;

	// Configuration of the dummy cycles on QSPI memory side 
	if (QSPI_DummyCyclesCfg( &QSPIHandle ) != QSPI_OK)
		return QSPI_NOT_SUPPORTED;

	// Configuration of the Output driver strength on memory side
	if( QSPI_OutDrvStrengthCfg( &QSPIHandle ) != QSPI_OK )
		return QSPI_NOT_SUPPORTED;
	//
	if ( GetStatusRegister() != QSPI_OK)
		return QSPI_ERROR;
  
	// si controlla se c'� stato un errore 
	if ( GetSecurityRegister() == QSPI_ERROR )
		return QSPI_ERROR;
	// leggo l'id della flash
	QuadGetDeviceId();
	// inizializzazione completata
	return QSPI_OK;
}
// ----------------------------------------------------------------------------
//	deinizializza la flash
//
//	@retval QSPI memory status
//
uint8_t QSPI_DeInit(void)
{ 
	QSPIHandle.Instance = QUADSPI;

	qmem.init = QSPI_INIT_NO;

	/* Put QSPI memory in SPI mode */
	if( QSPI_ExitMemory_QPI( &QSPIHandle )!=QSPI_OK )
		return QSPI_NOT_SUPPORTED;

	/* Call the DeInit function to reset the driver */
	if (HAL_QSPI_DeInit(&QSPIHandle) != HAL_OK)
		return QSPI_ERROR;
		
	/* System level De-initialization */
	QSPI_MspDeInit(&QSPIHandle, NULL);

	return QSPI_OK;
}
// ----------------------------------------------------------------------------
// 	Lettura device ID flash
//
//	@retval QSPI memory status
//
uint8_t QSPI_GetDeviceId( void )
{
	if ( qmem.init != QSPI_INIT_OK )
		return QSPI_INIT_OK;
	if ( qmem.status_reg->qe == 1 )	{
		// quad spi attivo
		QuadGetDeviceId();
	}
	else {
		// funzionamento in spi da implementare
		return QSPI_NOT_SUPPORTED;
	}
	return QSPI_OK;
}
// ----------------------------------------------------------------------------
//
//
uint8_t BSP_QSPI_GetInfo(QSPI_Info* pInfo)
{
	pInfo->FlashSize          = qmem.FlashSize;
	pInfo->EraseSectorSize    = qmem.EraseSectorSize ;
	pInfo->EraseSectorsNumber = qmem.EraseSectorsNumber;
	pInfo->ProgPageSize       = qmem.ProgPageSize;
	pInfo->ProgPagesNumber    = qmem.ProgPagesNumber;
  
  return QSPI_OK;
}
// ----------------------------------------------------------------------------
/**  @brief scrittura a pagine di una qualsiasi zona di memoria

  	* @param  pData: Pointer to data to be written
	* @param  WriteAddr: Write start address
	* @param  Size: Size of data to write    
	* @retval QSPI memory status
*/

uint8_t QSPI_Write(uint8_t* pData, uint32_t WriteAddr, uint32_t Size)
{
	QSPI_CommandTypeDef s_command;
	uint32_t end_addr, current_size, current_addr;

	// calcolo della dimensione della prima pagina da scrivere
	current_size = MX25L128_PAGE_SIZE - (WriteAddr % MX25L128_PAGE_SIZE);

	// controlla se i dati totali eccedono la dimensione della pagina
	if (current_size > Size)	{
		current_size = Size;
	}
	/* Initialize the address variables */
	current_addr 	= WriteAddr;
	end_addr 		= WriteAddr + Size;

	/* Initialize the program command */
	s_command.InstructionMode   = QSPI_INSTRUCTION_4_LINES;
	s_command.Instruction       = 0x02;
	s_command.AddressMode       = QSPI_ADDRESS_4_LINES;
	s_command.AddressSize       = QSPI_ADDRESS_24_BITS;
	s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	s_command.DataMode          = QSPI_DATA_4_LINES;
	s_command.DummyCycles       = 0;
	s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
	s_command.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
	s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

	// scrivo le pagine
	do
	{
		s_command.Address = current_addr;
		s_command.NbData  = current_size;

		// � necessaria ogni volta 
		if (QSPI_WriteEnable(&QSPIHandle) != QSPI_OK)	
		  return QSPI_ERROR;

		// command 
		if (HAL_QSPI_Command(&QSPIHandle, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)	{
		  return QSPI_ERROR;
		}
		// Transmission of the data 
		if (HAL_QSPI_Transmit(&QSPIHandle, pData, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)	{
		  return QSPI_ERROR;
		}
		// attesa scrittura
		if ( waitBusy( 1500 ) == QSPI_ERROR )
			return QSPI_ERROR;

		// controllo se scrittura ok
		if ( GetStatusAndSecurityRegister() == QSPI_ERROR )
			return QSPI_ERROR;
		
		if ( qmem.status_reg->wel == 1 )	
		  return QSPI_ERROR;
		
		if ( qmem.security_reg->e_fail )
			return QSPI_ERASE_FAIL;
		//		
		// incremento indirizzo per le eventuali pagine successive
		current_addr += current_size;
		pData += current_size;
		current_size = ((current_addr + MX25L128_PAGE_SIZE) > end_addr) ? (end_addr - current_addr) : MX25L128_PAGE_SIZE;
	} while (current_addr < end_addr);

	return QSPI_OK;
}
// ----------------------------------------------------------------------------
// 	Cancellazione totale chip, pu� essere necessario fino a 60 secondi
//
uint8_t QSPI_Erase_Chip( void )
{
	QSPI_CommandTypeDef s_command;

	/* Initialize the erase command */
	s_command.InstructionMode   = QSPI_INSTRUCTION_4_LINES;
	s_command.Instruction       = BULK_ERASE_CMD;
	s_command.AddressMode       = QSPI_ADDRESS_NONE;
	s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	s_command.DataMode          = QSPI_DATA_NONE;
	s_command.DummyCycles       = 0;
	s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
	s_command.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
	s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

	/* Enable write operations */
	if (QSPI_WriteEnable(&QSPIHandle) != QSPI_OK)	{
		return QSPI_ERROR;
	}

	/* Send the command */
	if (HAL_QSPI_Command(&QSPIHandle, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)	{
		return QSPI_ERROR;
	}
	// la cancellazione pu� impiegare fino a MX25L128_BULK_ERASE_MAX_TIME ( 60sec)
	
	if ( waitBusy( MX25L128_BULK_ERASE_MAX_TIME ) == QSPI_ERROR )
		return QSPI_ERROR;

	// controllo se cancellazione ok
	if ( GetStatusAndSecurityRegister() == QSPI_ERROR )
		return QSPI_ERROR;
	
	if ( qmem.status_reg->wel == 1 )	
	  return QSPI_ERROR;
	
	if ( qmem.security_reg->e_fail )
		return QSPI_ERASE_FAIL;
	
	return QSPI_OK;
}
// ---------------------------------------------------------------------------
//	Cancellazione di un settore da 32K
//  @brief  Cancella un settore qualsiasi da 32K nella flash. 
//  @param  Address: sector address to erase  
//  @retval : QSPI_OK  o QSPI_ERROR o QSPI_ERASE_FAIL
//
uint8_t QSPI_Erase_Block32K(uint32_t Address)
{
	qmem_wait = 120;
	return QSPI_Erase( Address, SECTOR32K_ERASE_CMD );
}
// ---------------------------------------------------------------------------
//	Cancellazione di un settore da 64K
//  @brief  Cancella un settore qualsiasi da 64K nella flash. 
//  @param  Address: sector address to erase  
//  @retval : QSPI_OK  o QSPI_ERROR o QSPI_ERASE_FAIL
//
uint8_t QSPI_Erase_Block64K(uint32_t Address)
{
	qmem_wait = 220;
	return QSPI_Erase( Address, SECTOR64K_ERASE_CMD );
}
// ---------------------------------------------------------------------------
//	Cancellazione di un settore da 4K
//  @brief  Cancella un settore qualsiasi da 4K nella flash. 
//  @param  Address: sector address to erase  
//  @retval : QSPI_OK  o QSPI_ERROR o QSPI_ERASE_FAIL
//
uint8_t QSPI_Erase_Sector( uint32_t Address)
{
	qmem_wait = 500; // era 50 ma andava in timeout
	return QSPI_Erase( Address, SUBSECTOR_ERASE_CMD );
}
/**
    ---------------------------------------------------------------------------
 * @brief  legge una qualsiasi quantit� di flash.
 * @param  pData: Pointer to data to be read
 * @param  ReadAddr: Read start address
 * @param  Size: Size of data to read    
 * @retval QSPI memory status
  */
uint8_t QSPI_Read(uint8_t* pData, uint32_t ReadAddr, uint32_t Size)
{
	QSPI_CommandTypeDef s_command;

	/* Initialize the read command */
	s_command.InstructionMode   = QSPI_INSTRUCTION_4_LINES;	
	s_command.Instruction       = 0xEb;
	s_command.AddressMode       = QSPI_ADDRESS_4_LINES;
	s_command.AddressSize       = QSPI_ADDRESS_24_BITS;
	s_command.Address           = ReadAddr;
	s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	s_command.DataMode          = QSPI_DATA_4_LINES;
	s_command.DummyCycles       = MX25L128_DUMMY_CYCLES_READ;
	s_command.NbData            = Size;
	s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
	s_command.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
	s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

	/* Configure the command */
	if ( HAL_QSPI_Command( &QSPIHandle, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE ) != HAL_OK)
		return QSPI_ERROR;

	//  /* Set S# timing for Read command */
	MODIFY_REG(QSPIHandle.Instance->DCR, QUADSPI_DCR_CSHT, QSPI_CS_HIGH_TIME_4_CYCLE);

	/* Reception of the data */
	if (HAL_QSPI_Receive(&QSPIHandle, pData, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
		return QSPI_ERROR;
  
//  /* Restore S# timing for nonRead commands */
	MODIFY_REG(QSPIHandle.Instance->DCR, QUADSPI_DCR_CSHT, QSPI_CS_HIGH_TIME_4_CYCLE);

  return QSPI_OK;
}

	
/** ---------------------------------------------------------------------------
  * @brief   effettua un test di cancellazione scrittura e lettura di un blocco di memoria
  * @param  puntatore al buffer dei dati per il test
  * @param  indirizzo appartenente al blocco in cui scrivere
  * @retval : QSPI_OK  o QSPI_ERROR 
  */
uint8_t QSPI_test_flash( unsigned char * qbuf, unsigned int add, unsigned int size )	
{
	uint8_t status;
	
	status = QSPI_Init( );
	
	if ( status != QSPI_OK )
		return QSPI_ERROR;

	status = QSPI_Erase_Sector( add );
	if ( status != QSPI_OK )
		return QSPI_ERROR;
	
	memset( qbuf, 0, size );

	status = QSPI_Read( qbuf, add, size );
	if ( status != QSPI_OK )
		return QSPI_ERROR;
			
	for ( int i =0; i < size; i++)	{
		qbuf[i] = (unsigned char )(i+127);
	}
	
	status = QSPI_Write( qbuf, add, size );
	if ( status != QSPI_OK )
		return QSPI_ERROR;

	memset( qbuf, 0, size );
	
	status = QSPI_Read( qbuf, add, size );

	return status;
}

uint8_t QSPI_test_Erase_write_read( unsigned char * qbuf, unsigned int add, unsigned int size )	
{
	uint8_t status;

	status = QSPI_Erase_Sector( add );
	if ( status != QSPI_OK )
		return QSPI_ERROR;
	
	memset( qbuf, 0, size );	

	status = QSPI_Read( qbuf, add, size );
	if ( status != QSPI_OK )
		return QSPI_ERROR;
			
	for ( int i =0; i < size; i++)
	{
		qbuf[i] = (unsigned char )(i);
	}
	
	status = QSPI_Write( qbuf, add, size );
	if ( status != QSPI_OK )
		return QSPI_ERROR;

	memset( qbuf, 0, size );
	//QSPI_ENTER_CRITICAL_SECTION();
	status = QSPI_Read( qbuf, add, size );
	//QSPI_EXIT_CRITICAL_SECTION();
		
	for ( int i =0; i < size; i++)
	{
		if ( qbuf[i] != (unsigned char) (i) )
		{
			return QSPI_ERROR;
		}
	}
	return status;

}
// ----------------------------------------------------------------------------
//
//
static uint8_t QSPI_SetBlokProtection( QSPI_HandleTypeDef *hqspi, unsigned char block  )
{
	QSPI_CommandTypeDef s_command;
	uint8_t reg[2];

	/* Initialize the reading of status register */
	s_command.InstructionMode   = QSPI_INSTRUCTION_4_LINES;
	s_command.Instruction       = READ_STATUS_REG_CMD;
	s_command.AddressMode       = QSPI_ADDRESS_NONE;
	s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	s_command.DataMode          = QSPI_DATA_4_LINES;
	s_command.DummyCycles       = 0;
	s_command.NbData            = 1;
	s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
	s_command.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
	s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

	/* Configure the command */
	if (HAL_QSPI_Command(hqspi, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
		return QSPI_ERROR;

	/* Reception of the data */
	if (HAL_QSPI_Receive(hqspi, &(reg[0]), HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
		return QSPI_ERROR;

	qmem_status = reg[0];

	/* Initialize the reading of configuration register */
	s_command.InstructionMode   = QSPI_INSTRUCTION_4_LINES;
	s_command.Instruction       = READ_CFG_REG_CMD;
	s_command.AddressMode       = QSPI_ADDRESS_NONE;
	s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	s_command.DataMode          = QSPI_DATA_4_LINES;
	s_command.DummyCycles       = 0;
	s_command.NbData            = 1;
	s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
	s_command.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
	s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

	/* Configure the command */
	if (HAL_QSPI_Command(hqspi, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
		return QSPI_ERROR;

	/* Reception of the data */
	if (HAL_QSPI_Receive(hqspi, &(reg[1]), HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
		return QSPI_ERROR;

	qmem_config = reg[1];

	/* Enable write operations */
	if (QSPI_WriteEnable(hqspi) != QSPI_OK)
		return QSPI_ERROR;

	/* Update the configuration register with new dummy cycles */
	s_command.InstructionMode   = QSPI_INSTRUCTION_4_LINES;
	s_command.Instruction       = WRITE_STATUS_CFG_REG_CMD;
	s_command.AddressMode       = QSPI_ADDRESS_NONE;
	s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	s_command.DataMode          = QSPI_DATA_4_LINES;
	s_command.DummyCycles       = 0;
	s_command.NbData            = 2;
	s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
	s_command.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
	s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;
  	
	qmem.status_reg->bp = (block & MX25L128_SR_BLOCKPR);	
	reg[0] = qmem_status;
	
	/* Configure the write volatile configuration register command */
	if (HAL_QSPI_Command(hqspi, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
		return QSPI_ERROR;
  
	/* Transmission of the data */
	if (HAL_QSPI_Transmit(hqspi, &(reg[0]), HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
		return QSPI_ERROR;

	/* 40ms  Write Status/Configuration Register Cycle Time */
	HAL_Delay( 50 );  
	if ( GetConfigurationRegister() != QSPI_OK)
		return QSPI_ERROR;

	return QSPI_OK;
}
QSPI_Info * GetQmemContext( void )
{
	return &qmem;
}
// ============================================================================
//			FUNZIONI PRIVATE
// ============================================================================

/**
  * @brief  This function reset the QSPI memory.
  * @param  hqspi: QSPI handle
  * @retval None
  */
static uint8_t QSPI_ResetMemory(QSPI_HandleTypeDef *hqspi)
{
  QSPI_CommandTypeDef      s_command;
  QSPI_AutoPollingTypeDef  s_config;
  uint8_t                  reg;

  /* Send command RESET command in QPI mode (QUAD I/Os) */
  /* Initialize the reset enable command */
  s_command.InstructionMode   = QSPI_INSTRUCTION_4_LINES;
  s_command.Instruction       = RESET_ENABLE_CMD;
  s_command.AddressMode       = QSPI_ADDRESS_NONE;
  s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
  s_command.DataMode          = QSPI_DATA_NONE;
  s_command.DummyCycles       = 0;
  s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
  s_command.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
  s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;
  /* Send the command */
  if (HAL_QSPI_Command(hqspi, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return QSPI_ERROR;
  }
  /* Send the reset memory command */
  s_command.Instruction = RESET_MEMORY_CMD;
  if (HAL_QSPI_Command(hqspi, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return QSPI_ERROR;
  }  

  /* Send command RESET command in SPI mode */
  /* Initialize the reset enable command */
  s_command.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
  s_command.Instruction       = RESET_ENABLE_CMD;
  /* Send the command */
  if (HAL_QSPI_Command(hqspi, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return QSPI_ERROR;
  }
  /* Send the reset memory command */
  s_command.Instruction = RESET_MEMORY_CMD;
  if (HAL_QSPI_Command(hqspi, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return QSPI_ERROR;
  }

  /* After reset CMD, 1000ms requested if QSPI memory SWReset occured during full chip erase operation */
  HAL_Delay( 1000 );

  /* Configure automatic polling mode to wait the WIP bit=0 */
  s_config.Match           = 0;
  s_config.Mask            = MX25L128_SR_WIP;
  s_config.MatchMode       = QSPI_MATCH_MODE_AND;
  s_config.StatusBytesSize = 1;
  s_config.Interval        = 0x10;
  s_config.AutomaticStop   = QSPI_AUTOMATIC_STOP_ENABLE;

  s_command.InstructionMode = QSPI_INSTRUCTION_1_LINE;
  s_command.Instruction     = READ_STATUS_REG_CMD;
  s_command.DataMode        = QSPI_DATA_1_LINE;
  int rc = 0;

  if (( rc = HAL_QSPI_AutoPolling(hqspi, &s_command, &s_config, HAL_QPSI_TIMEOUT_DEFAULT_VALUE)) != HAL_OK)
  {
    return QSPI_ERROR;
  }


  /* Initialize the reading of status register */
  s_command.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
  s_command.Instruction       = READ_STATUS_REG_CMD;
  s_command.AddressMode       = QSPI_ADDRESS_NONE;
  s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
  s_command.DataMode          = QSPI_DATA_1_LINE;
  s_command.DummyCycles       = 0;
  s_command.NbData            = 1;
  s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
  s_command.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
  s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

  /* Configure the command */
  if (HAL_QSPI_Command(hqspi, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return QSPI_ERROR;
  }

  /* Reception of the data */
  if (HAL_QSPI_Receive(hqspi, &reg, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return QSPI_ERROR;
  }

  /* Enable write operations, command in 1 bit */
  /* Enable write operations */
  s_command.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
  s_command.Instruction       = WRITE_ENABLE_CMD;
  s_command.AddressMode       = QSPI_ADDRESS_NONE;
  s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
  s_command.DataMode          = QSPI_DATA_NONE;
  s_command.DummyCycles       = 0;
  s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
  s_command.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
  s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

  if (HAL_QSPI_Command(hqspi, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return QSPI_ERROR;
  }
  
  /* Configure automatic polling mode to wait for write enabling */  
  s_config.Match           = MX25L128_SR_WREN;
  s_config.Mask            = MX25L128_SR_WREN;
  s_config.MatchMode       = QSPI_MATCH_MODE_AND;
  s_config.StatusBytesSize = 1;
  s_config.Interval        = 0x10;
  s_config.AutomaticStop   = QSPI_AUTOMATIC_STOP_ENABLE;

  s_command.Instruction    = READ_STATUS_REG_CMD;
  s_command.DataMode       = QSPI_DATA_1_LINE;

  if (( rc = HAL_QSPI_AutoPolling(hqspi, &s_command, &s_config, HAL_QPSI_TIMEOUT_DEFAULT_VALUE)) != HAL_OK)
  {
    return QSPI_ERROR;
  }


  /* Update the configuration register with new dummy cycles */
  s_command.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
  s_command.Instruction       = WRITE_STATUS_CFG_REG_CMD;
  s_command.AddressMode       = QSPI_ADDRESS_NONE;
  s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
  s_command.DataMode          = QSPI_DATA_1_LINE;
  s_command.DummyCycles       = 0;
  s_command.NbData            = 1;
  s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
  s_command.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
  s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

  /* Enable the Quad IO on the QSPI memory (Non-volatile bit) */
  reg |= MX25L128_SR_QUADEN;

  /* Configure the command */
  if (HAL_QSPI_Command(hqspi, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return QSPI_ERROR;
  }
  
  /* Transmission of the data */
  if (HAL_QSPI_Transmit(hqspi, &reg, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return QSPI_ERROR;
  }
  
  /* 40ms  Write Status/Configuration Register Cycle Time */
  HAL_Delay( 40 );  

  return QSPI_OK;
}
//
// ---------------------------------------------------------------------------
//
/**
  * @brief  This function configure the dummy cycles on memory side.
  * @param  hqspi: QSPI handle
  * @retval None
  */
static uint8_t QSPI_DummyCyclesCfg(QSPI_HandleTypeDef *hqspi)
{
	QSPI_CommandTypeDef s_command;
	uint8_t reg[2];

	/* Initialize the reading of status register */
	s_command.InstructionMode   = QSPI_INSTRUCTION_4_LINES;
	s_command.Instruction       = READ_STATUS_REG_CMD;
	s_command.AddressMode       = QSPI_ADDRESS_NONE;
	s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	s_command.DataMode          = QSPI_DATA_4_LINES;
	s_command.DummyCycles       = 0;
	s_command.NbData            = 1;
	s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
	s_command.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
	s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

	/* Configure the command */
	if (HAL_QSPI_Command(hqspi, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
		return QSPI_ERROR;

	/* Reception of the data */
	if (HAL_QSPI_Receive(hqspi, &(reg[0]), HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
		return QSPI_ERROR;

	qmem_status = reg[0];

	/* Initialize the reading of configuration register */
	s_command.InstructionMode   = QSPI_INSTRUCTION_4_LINES;
	s_command.Instruction       = READ_CFG_REG_CMD;
	s_command.AddressMode       = QSPI_ADDRESS_NONE;
	s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	s_command.DataMode          = QSPI_DATA_4_LINES;
	s_command.DummyCycles       = 0;
	s_command.NbData            = 1;
	s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
	s_command.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
	s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

	/* Configure the command */
	if (HAL_QSPI_Command(hqspi, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
		return QSPI_ERROR;

	/* Reception of the data */
	if (HAL_QSPI_Receive(hqspi, &(reg[1]), HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
		return QSPI_ERROR;

	qmem_config = reg[1];

	/* Enable write operations */
	if (QSPI_WriteEnable(hqspi) != QSPI_OK)
		return QSPI_ERROR;

	/* Update the configuration register with new dummy cycles */
	s_command.InstructionMode   = QSPI_INSTRUCTION_4_LINES;
	s_command.Instruction       = WRITE_STATUS_CFG_REG_CMD;
	s_command.AddressMode       = QSPI_ADDRESS_NONE;
	s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	s_command.DataMode          = QSPI_DATA_4_LINES;
	s_command.DummyCycles       = 0;
	s_command.NbData            = 2;
	s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
	s_command.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
	s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;
  
	/* MX25L128_DUMMY_CYCLES_READ_QUAD = 3 for 10 cycles in QPI mode */
	MODIFY_REG( reg[1], MX25L128_CR_NB_DUMMY, (MX25L128_DUMMY_CYCLES_READ_QUAD << POSITION_VAL(MX25L128_CR_NB_DUMMY)));
	qmem.config_reg->dc = QSPI_DUMMY_CYCLES;	// Output driver strength on memory side 
	reg[1] = qmem_config;
	/* Configure the write volatile configuration register command */
	if (HAL_QSPI_Command(hqspi, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
		return QSPI_ERROR;
  
	/* Transmission of the data */
	if (HAL_QSPI_Transmit(hqspi, &(reg[0]), HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
		return QSPI_ERROR;

	/* 40ms  Write Status/Configuration Register Cycle Time */
	HAL_Delay( 50 );  
	if ( GetConfigurationRegister() != QSPI_OK)
		return QSPI_ERROR;

	return QSPI_OK;
}
//
// ---------------------------------------------------------------------------
//
/**
  * @brief  This function put QSPI memory in QPI mode (quad I/O).
  * @param  hqspi: QSPI handle
  * @retval None
  */
static uint8_t QSPI_EnterMemory_QPI( QSPI_HandleTypeDef *hqspi )
{
  QSPI_CommandTypeDef      s_command;
  QSPI_AutoPollingTypeDef  s_config;

  /* Initialize the QPI enable command */
  /* QSPI memory is supported to be in SPI mode, so CMD on 1 LINE */
  s_command.InstructionMode   = QSPI_INSTRUCTION_1_LINE;  
  s_command.Instruction       = ENTER_QUAD_CMD;
  s_command.AddressMode       = QSPI_ADDRESS_NONE;
  s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
  s_command.DataMode          = QSPI_DATA_NONE;
  s_command.DummyCycles       = 0;
  s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
  s_command.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
  s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

  /* Send the command */
  if (HAL_QSPI_Command(hqspi, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return QSPI_ERROR;
  }

  /* Configure automatic polling mode to wait the QUADEN bit=1 and WIP bit=0 */
  s_config.Match           = MX25L128_SR_QUADEN;
  s_config.Mask            = MX25L128_SR_QUADEN|MX25L128_SR_WIP;
  s_config.MatchMode       = QSPI_MATCH_MODE_AND;
  s_config.StatusBytesSize = 1;
  s_config.Interval        = 0x10;
  s_config.AutomaticStop   = QSPI_AUTOMATIC_STOP_ENABLE;

  s_command.InstructionMode   = QSPI_INSTRUCTION_4_LINES;
  s_command.Instruction       = READ_STATUS_REG_CMD;
  s_command.DataMode          = QSPI_DATA_4_LINES;

  if (HAL_QSPI_AutoPolling(hqspi, &s_command, &s_config, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return QSPI_ERROR;
  }

  return QSPI_OK;
}
//
// ---------------------------------------------------------------------------
//
/**
  * @brief  This function put QSPI memory in SPI mode.
  * @param  hqspi: QSPI handle
  * @retval None
  */
static uint8_t QSPI_ExitMemory_QPI( QSPI_HandleTypeDef *hqspi)
{
	QSPI_CommandTypeDef      s_command;

	/* Initialize the QPI enable command */
	/* QSPI memory is supported to be in QPI mode, so CMD on 4 LINES */
	s_command.InstructionMode   = QSPI_INSTRUCTION_4_LINES;  
	s_command.Instruction       = EXIT_QUAD_CMD;
	s_command.AddressMode       = QSPI_ADDRESS_NONE;
	s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	s_command.DataMode          = QSPI_DATA_NONE;
	s_command.DummyCycles       = 0;
	s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
	s_command.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
	s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

	/* Send the command */
	if (HAL_QSPI_Command(hqspi, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
		return QSPI_ERROR;

	return QSPI_OK;
}
//
// ---------------------------------------------------------------------------
//
/**
  * @brief  This function configure the Output driver strength on memory side.
  * @param  hqspi: QSPI handle
  * @retval None
  */
static uint8_t QSPI_OutDrvStrengthCfg( QSPI_HandleTypeDef *hqspi )
{
	QSPI_CommandTypeDef s_command;
	uint8_t reg[2];

	/* Initialize the reading of status register */
	s_command.InstructionMode   = QSPI_INSTRUCTION_4_LINES;
	s_command.Instruction       = READ_STATUS_REG_CMD;
	s_command.AddressMode       = QSPI_ADDRESS_NONE;
	s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	s_command.DataMode          = QSPI_DATA_4_LINES;
	s_command.DummyCycles       = 0;
	s_command.NbData            = 1;
	s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
	s_command.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
	s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

	/* Configure the command */
	if (HAL_QSPI_Command(hqspi, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
	{
		return QSPI_ERROR;
	}

	/* Reception of the data */
	if (HAL_QSPI_Receive(hqspi, &(reg[0]), HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
	{
		return QSPI_ERROR;
	}

	/* Initialize the reading of configuration register */
	s_command.InstructionMode   = QSPI_INSTRUCTION_4_LINES;
	s_command.Instruction       = READ_CFG_REG_CMD;
	s_command.AddressMode       = QSPI_ADDRESS_NONE;
	s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	s_command.DataMode          = QSPI_DATA_4_LINES;
	s_command.DummyCycles       = 0;
	s_command.NbData            = 1;
	s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
	s_command.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
	s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

	/* Configure the command */
	if (HAL_QSPI_Command(hqspi, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
	{
		return QSPI_ERROR;
	}

	/* Reception of the data */
	if (HAL_QSPI_Receive(hqspi, &(reg[1]), HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
	{
		return QSPI_ERROR;
	}
	qmem_config = reg[1];
	/* Enable write operations */
	if (QSPI_WriteEnable(&QSPIHandle) != QSPI_OK)
	{
		return QSPI_ERROR;
	}

	/* Update the configuration register with new output driver strength */
	s_command.InstructionMode   = QSPI_INSTRUCTION_4_LINES;
	s_command.Instruction       = WRITE_STATUS_CFG_REG_CMD;
	s_command.AddressMode       = QSPI_ADDRESS_NONE;
	s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	s_command.DataMode          = QSPI_DATA_4_LINES;
	s_command.DummyCycles       = 0;
	s_command.NbData            = 2;
	s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
	s_command.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
	s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

	/* Set Output Strength of the QSPI memory 15 ohms */
	//MODIFY_REG( reg[1], MX25L128_CR_ODS, (MX25L128_CR_ODS_15 << POSITION_VAL(MX25L128_CR_ODS)));
	qmem.config_reg->ods = 7;
	reg[1] = qmem_config;

	/* Configure the write volatile configuration register command */
	if (HAL_QSPI_Command(hqspi, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
	{
		return QSPI_ERROR;
	}

	/* Transmission of the data */
	if (HAL_QSPI_Transmit(hqspi, &(reg[0]), HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
	{
		return QSPI_ERROR;
	}
	/* 40ms  Write Status/Configuration Register Cycle Time */
	HAL_Delay( 50 );  
	
	if ( GetConfigurationRegister() != QSPI_OK)
		return QSPI_ERROR;

	return QSPI_OK;
}
//
// ---------------------------------------------------------------------------
//
/**
  * @brief  This function send a Write Enable and wait it is effective.
  * @param  hqspi: QSPI handle
  * @retval None
  */
static uint8_t QSPI_WriteEnable(QSPI_HandleTypeDef *hqspi)
{
	QSPI_CommandTypeDef     s_command;
	QSPI_AutoPollingTypeDef s_config;

	/* Enable write operations */
	s_command.InstructionMode   = QSPI_INSTRUCTION_4_LINES;
	s_command.Instruction       = WRITE_ENABLE_CMD;
	s_command.AddressMode       = QSPI_ADDRESS_NONE;
	s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	s_command.DataMode          = QSPI_DATA_NONE;
	s_command.DummyCycles       = 0;
	s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
	s_command.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
	s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

  
	if ( QSPI_Command(hqspi, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)	{
		return QSPI_ERROR;
	}
	
  	HAL_Delay( 1000 );
	if (  waitBusy( 1500 ) != QSPI_OK )
		return QSPI_ERROR;

//
//	/* Configure automatic polling mode to wait for write enabling */  
	s_config.Match           = MX25L128_SR_WREN;
	s_config.Mask            = MX25L128_SR_WREN;
	s_config.MatchMode       = QSPI_MATCH_MODE_AND;
	s_config.StatusBytesSize = 1;
	s_config.Interval        = 0x10;
	s_config.AutomaticStop   = QSPI_AUTOMATIC_STOP_ENABLE;
//
	s_command.Instruction    = READ_STATUS_REG_CMD;
	s_command.DataMode       = QSPI_DATA_4_LINES;
//
	if (HAL_QSPI_AutoPolling(hqspi, &s_command, &s_config, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
	{
		return QSPI_ERROR;
  	}
  
	return QSPI_OK;
}
//
// ---------------------------------------------------------------------------
//
/**
  * @brief  This function read the SR of the memory and wait the EOP.
  * @param  hqspi: QSPI handle
  * @param  Timeout
  * @retval None
  */
static uint8_t QSPI_AutoPollingMemReady(QSPI_HandleTypeDef *hqspi, uint32_t Timeout)
{
  QSPI_CommandTypeDef     s_command;
  QSPI_AutoPollingTypeDef s_config;

  /* Configure automatic polling mode to wait for memory ready */  
  s_command.InstructionMode   = QSPI_INSTRUCTION_4_LINES;
  s_command.Instruction       = READ_STATUS_REG_CMD;
  s_command.AddressMode       = QSPI_ADDRESS_NONE;
  s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
  s_command.DataMode          = QSPI_DATA_4_LINES;
  s_command.DummyCycles       = 0;
  s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
  s_command.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
  s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

  s_config.Match           = 0;
  s_config.Mask            = MX25L128_SR_WIP;
  s_config.MatchMode       = QSPI_MATCH_MODE_AND;
  s_config.StatusBytesSize = 1;
  s_config.Interval        = 0x10;
  s_config.AutomaticStop   = QSPI_AUTOMATIC_STOP_ENABLE;

  if (HAL_QSPI_AutoPolling(hqspi, &s_command, &s_config, Timeout) != HAL_OK)
  {
    return QSPI_ERROR;
  }

  return QSPI_OK;
}
//
// ---------------------------------------------------------------------------
//
/**
  * @brief QSPI MSP Initialization
  *        This function configures the hardware resources used in this example:
  *           - Peripheral's clock enable
  *           - Peripheral's GPIO Configuration
  *           - NVIC configuration for QSPI interrupt
  * @retval None
  */
__weak void QSPI_MspInit(QSPI_HandleTypeDef *hqspi, void *Params)
{
  GPIO_InitTypeDef gpio_init_structure;

  /*##-1- Enable peripherals and GPIO Clocks #################################*/
  /* Enable the QuadSPI memory interface clock */
  QSPI_CLK_ENABLE();
  /* Reset the QuadSPI memory interface */
  QSPI_FORCE_RESET();
  QSPI_RELEASE_RESET();
  /* Enable GPIO clocks */
  QSPI_CS_GPIO_CLK_ENABLE();
  QSPI_CLK_GPIO_CLK_ENABLE();
  QSPI_D0_GPIO_CLK_ENABLE();
  QSPI_D1_GPIO_CLK_ENABLE();
  QSPI_D2_GPIO_CLK_ENABLE();
  QSPI_D3_GPIO_CLK_ENABLE();

  /*##-2- Configure peripheral GPIO ##########################################*/
  /* QSPI CS GPIO pin configuration  */
  gpio_init_structure.Pin       = QSPI_CS_PIN;
  gpio_init_structure.Alternate = QSPI_CS_PIN_AF;
  gpio_init_structure.Mode      = GPIO_MODE_AF_PP;
  gpio_init_structure.Pull      = GPIO_PULLUP;
  gpio_init_structure.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
  HAL_GPIO_Init(QSPI_CS_GPIO_PORT, &gpio_init_structure);
  /* QSPI CLK GPIO pin configuration  */
  gpio_init_structure.Pin       = QSPI_CLK_PIN;
  gpio_init_structure.Alternate = QSPI_CLK_PIN_AF;
  gpio_init_structure.Pull      = GPIO_NOPULL;
  HAL_GPIO_Init(QSPI_CLK_GPIO_PORT, &gpio_init_structure);
  /* QSPI D0 GPIO pin configuration  */
  gpio_init_structure.Pin       = QSPI_D0_PIN;
  gpio_init_structure.Alternate = QSPI_D0_PIN_AF;
  HAL_GPIO_Init(QSPI_D0_GPIO_PORT, &gpio_init_structure);
  /* QSPI D1 GPIO pin configuration  */
  gpio_init_structure.Pin       = QSPI_D1_PIN;
  gpio_init_structure.Alternate = QSPI_D1_PIN_AF;
  HAL_GPIO_Init(QSPI_D1_GPIO_PORT, &gpio_init_structure);
  /* QSPI D2 GPIO pin configuration  */
  gpio_init_structure.Pin       = QSPI_D2_PIN;
  gpio_init_structure.Alternate = QSPI_D2_PIN_AF;
  HAL_GPIO_Init(QSPI_D2_GPIO_PORT, &gpio_init_structure);
  /* QSPI D3 GPIO pin configuration  */
  gpio_init_structure.Pin       = QSPI_D3_PIN;
  gpio_init_structure.Alternate = QSPI_D3_PIN_AF;
  HAL_GPIO_Init(QSPI_D3_GPIO_PORT, &gpio_init_structure);

  /*##-3- Configure the NVIC for QSPI #########################################*/
  /* NVIC configuration for QSPI interrupt */
  HAL_NVIC_SetPriority(QUADSPI_IRQn, 0x0F, 0);
  HAL_NVIC_EnableIRQ(QUADSPI_IRQn);
}
/**
  * @brief QSPI MSP De-Initialization
  *        This function frees the hardware resources used in this example:
  *          - Disable the Peripheral's clock
  *          - Revert GPIO and NVIC configuration to their default state
  * @retval None
  */
__weak void QSPI_MspDeInit(QSPI_HandleTypeDef *hqspi, void *Params)
{
  /*##-1- Disable the NVIC for QSPI ###########################################*/
  HAL_NVIC_DisableIRQ(QUADSPI_IRQn);

  /*##-2- Disable peripherals and GPIO Clocks ################################*/
  /* De-Configure QSPI pins */
  HAL_GPIO_DeInit(QSPI_CS_GPIO_PORT, QSPI_CS_PIN);
  HAL_GPIO_DeInit(QSPI_CLK_GPIO_PORT, QSPI_CLK_PIN);
  HAL_GPIO_DeInit(QSPI_D0_GPIO_PORT, QSPI_D0_PIN);
  HAL_GPIO_DeInit(QSPI_D1_GPIO_PORT, QSPI_D1_PIN);
  HAL_GPIO_DeInit(QSPI_D2_GPIO_PORT, QSPI_D2_PIN);
  HAL_GPIO_DeInit(QSPI_D3_GPIO_PORT, QSPI_D3_PIN);

  /*##-3- Reset peripherals ##################################################*/
  /* Reset the QuadSPI memory interface */
  QSPI_FORCE_RESET();
  QSPI_RELEASE_RESET();

  /* Disable the QuadSPI memory interface clock */
  QSPI_CLK_DISABLE();
}
//
// ---------------------------------------------------------------------------
//
/**
  * @brief  Reads current status of the QSPI memory.
  * @retval QSPI memory status
  */
static uint8_t GetSecurityRegister(void)
{
	QSPI_CommandTypeDef s_command;
	uint8_t reg;

	/* Initialize the read flag status register command */
	s_command.InstructionMode   = QSPI_INSTRUCTION_4_LINES;
	s_command.Instruction       = 0x2b;
	s_command.AddressMode       = QSPI_ADDRESS_NONE;
	s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	s_command.DataMode          = QSPI_DATA_4_LINES;
	s_command.DummyCycles       = 0;
	s_command.NbData            = 1;
	s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
	s_command.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
	s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

	/* Configure the command */
	if (HAL_QSPI_Command(&QSPIHandle, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
		return QSPI_ERROR;

	/* Reception of the data */
	if (HAL_QSPI_Receive(&QSPIHandle, &reg, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
		return QSPI_ERROR;

	qmem_security = reg;

	return QSPI_OK;
  
}
// ----------------------------------------------------------------------------
//
//
static uint8_t GetConfigurationRegister( void )
{
	QSPI_CommandTypeDef s_command;

	/* Initialize the read flag status register command */
	s_command.InstructionMode   = QSPI_INSTRUCTION_4_LINES;
	s_command.Instruction       = READ_CFG_REG_CMD;
	s_command.AddressMode       = QSPI_ADDRESS_NONE;
	s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	s_command.DataMode          = QSPI_DATA_4_LINES;
	s_command.DummyCycles       = 0;
	s_command.NbData            = 1;
	s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
	s_command.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
	s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

	/* Configure the command */
	if (HAL_QSPI_Command(&QSPIHandle, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
		return QSPI_ERROR;

	/* Reception of the data */
	if (HAL_QSPI_Receive(&QSPIHandle, &qmem_config, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
		return QSPI_ERROR;

	return QSPI_OK;
  
}
// ----------------------------------------------------------------------------
//
//
static uint8_t GetStatusAndSecurityRegister(void)
{
	QSPI_CommandTypeDef s_command;
	uint8_t reg;

	/* Initialize the read flag status register command */
	s_command.InstructionMode   = QSPI_INSTRUCTION_4_LINES;
	s_command.Instruction       = READ_STATUS_REG_CMD;
	s_command.AddressMode       = QSPI_ADDRESS_NONE;
	s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	s_command.DataMode          = QSPI_DATA_4_LINES;
	s_command.DummyCycles       = 0;
	s_command.NbData            = 1;
	s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
	s_command.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
	s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

	/* Configure the command */
	if (HAL_QSPI_Command(&QSPIHandle, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
		return QSPI_ERROR;

	/* Reception of the data */
	if (HAL_QSPI_Receive(&QSPIHandle, &reg, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
		return QSPI_ERROR;

	qmem_status = reg;

	/* Initialize the read flag status register command */
	s_command.Instruction       = 0x2b;

	/* Configure the command */
	if (HAL_QSPI_Command(&QSPIHandle, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
		return QSPI_ERROR;

	/* Reception of the data */
	if (HAL_QSPI_Receive(&QSPIHandle, &reg, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
		return QSPI_ERROR;

	qmem_security = reg;

	return QSPI_OK;
}
// ----------------------------------------------------------------------------
//
//
static uint8_t GetStatusRegister(void)
{
	QSPI_CommandTypeDef s_command;
	uint8_t reg;

	/* Initialize the read flag status register command */
	s_command.InstructionMode   = QSPI_INSTRUCTION_4_LINES;
	s_command.Instruction       = READ_STATUS_REG_CMD;
	s_command.AddressMode       = QSPI_ADDRESS_NONE;
	s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	s_command.DataMode          = QSPI_DATA_4_LINES;
	s_command.DummyCycles       = 0;
	s_command.NbData            = 1;
	s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
	s_command.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
	s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

	/* Configure the command */
	if (HAL_QSPI_Command(&QSPIHandle, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
		return QSPI_ERROR;

	/* Reception of the data */
	if (HAL_QSPI_Receive(&QSPIHandle, &reg, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
		return QSPI_ERROR;

	qmem_status = reg;
	/* Check the value of the register*/ 
	if ((reg & MX25L128_SR_WIP) == 0)
		return QSPI_OK;
	else
		return QSPI_BUSY;
}

// ----------------------------------------------------------------------------
//	lettura del registro di stato in modalit� SPI
//
uint8_t SPI_GetStatusRegister(void)
{
	QSPI_CommandTypeDef s_command;
	uint8_t reg;

	/* Initialize the read flag status register command */
	s_command.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
	s_command.Instruction       = READ_STATUS_REG_CMD;
	s_command.AddressMode       = QSPI_ADDRESS_NONE;
	s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	s_command.DataMode          = QSPI_DATA_1_LINE;
	s_command.DummyCycles       = 0;
	s_command.NbData            = 1;
	s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
	s_command.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
	s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

	/* Configure the command */
	if (HAL_QSPI_Command(&QSPIHandle, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
		return QSPI_ERROR;

	/* Reception of the data */
	if (HAL_QSPI_Receive(&QSPIHandle, &reg, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
		return QSPI_ERROR;
	qmem_status = reg;
	/* Check the value of the register*/ 
	if ((reg & MX25L128_SR_WIP) == 0)
		return QSPI_OK;
	else
		return QSPI_BUSY;
}
// ----------------------------------------------------------------------------
//	resta in attesa fin quando la flash non � libera
//
static uint8_t waitBusy( unsigned int timout )
{

	unsigned int status;
	unsigned int time = HAL_GetTick() + timout;
	do {
		status =  GetStatusRegister( );
		if ( status == QSPI_ERROR )
			return QSPI_ERROR;
		else if ( time < HAL_GetTick())
			return QSPI_TIMEOUT;
		
	}while ( status == QSPI_BUSY );

	return QSPI_OK;

}
// ----------------------------------------------------------------------------
//	codic comune alle varie funzioni di cancellazione blocchi
//
static uint8_t QSPI_Erase(uint32_t Address, unsigned char cmd)
{
	QSPI_CommandTypeDef s_command;
	uint8_t status;

	/* Initialize the erase command */
	s_command.InstructionMode   = QSPI_INSTRUCTION_4_LINES;
	s_command.Instruction       = cmd;
	s_command.AddressMode       = QSPI_ADDRESS_4_LINES;
	s_command.AddressSize       = QSPI_ADDRESS_24_BITS;
	s_command.Address           = Address;
	s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	s_command.DataMode          = QSPI_DATA_NONE;
	s_command.DummyCycles       = 0;
	s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
	s_command.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
	s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

	/* Enable write operations */
	if (QSPI_WriteEnable(&QSPIHandle) != QSPI_OK)	{
		return QSPI_ERROR;
	}

	/* Send the command */
	if (HAL_QSPI_Command(&QSPIHandle, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)	{
		return QSPI_ERROR;
	}
 	sys_get_time( &tprog );
	
	status = waitBusy( qmem_wait );
	if (  status != QSPI_OK )	{
		GetStatusRegister();
		return status;
	}
	
	sys_elapsedTime( &tprog );
	
	// controllo se cancellazione ok
	if ( GetStatusAndSecurityRegister() == QSPI_ERROR )
		return QSPI_ERROR;
	
	if ( qmem.status_reg->wel == 1 )	
	  return QSPI_ERROR;
	
	if ( qmem.security_reg->e_fail )
		return QSPI_ERASE_FAIL;

	return QSPI_OK;
}
// -----------------------------------------------------------------------------
// ritorna l'id della memoria, 3 byte
// lettura solo in quad SPI
//
static uint8_t QuadGetDeviceId( void )
{
	QSPI_CommandTypeDef s_command;


	/* Initialize the read flag status register command */
	s_command.InstructionMode   = QSPI_INSTRUCTION_4_LINES;
	s_command.Instruction       = READ_QPIID;
	s_command.AddressMode       = QSPI_ADDRESS_NONE;
	s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	s_command.DataMode          = QSPI_DATA_4_LINES;
	s_command.DummyCycles       = 0;
	s_command.NbData            = 3;
	s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
	s_command.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
	s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

	/* Configure the command */
	if (HAL_QSPI_Command(&QSPIHandle, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
		return QSPI_ERROR;

	/* Reception of the data */
	if (HAL_QSPI_Receive(&QSPIHandle, (unsigned char *)&qmem.memory , HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
		return QSPI_ERROR;

	return QSPI_OK;
  
}
// ----------------------------------------------------------------------------
//
//
HAL_StatusTypeDef QSPI_Command(QSPI_HandleTypeDef *hqspi, QSPI_CommandTypeDef *s_command, uint32_t Timeout)
{
	HAL_StatusTypeDef status;
	
	
//	QSPI_ENTER_CRITICAL_SECTION();
	/* Configure the command */
	status = HAL_QSPI_Command(&QSPIHandle, s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE);

//	QSPI_EXIT_CRITICAL_SECTION();
	
	if ( status != HAL_OK)	{
		return HAL_ERROR;
	}
	return HAL_OK;

}
// ----------------------------------------------------------------------------
//
//
HAL_StatusTypeDef QSPI_Receive(QSPI_HandleTypeDef *hqspi, uint8_t *pData, uint32_t Timeout)
{
	HAL_StatusTypeDef status;
	
	
//	QSPI_ENTER_CRITICAL_SECTION();
	/* Reception of the data */
	status = HAL_QSPI_Receive(&QSPIHandle, pData , HAL_QPSI_TIMEOUT_DEFAULT_VALUE);

//	QSPI_EXIT_CRITICAL_SECTION();

	if ( status != HAL_OK)	{
		return HAL_ERROR;
	}
	return HAL_OK;

}

/**
  * @brief This function handles QUADSPI global interrupt.
  */
/*
void QUADSPI_IRQHandler(void)
{
  HAL_QSPI_IRQHandler(&QSPIHandle);
}
DA RIVEDERE !!!!!
*/
