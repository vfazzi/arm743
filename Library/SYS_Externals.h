/********************************************************************************************/
/*	SYS_Externals.h				- Definizione variabili e funzioni di sistema esterne		*/

/********************************************************************************************/

/*------------------------------------------------------------------------------------------*/
/*	Funzioni e handles USB																	*/
/*------------------------------------------------------------------------------------------*/
extern uint8_t CDC_Transmit_FS(uint8_t* Buf, uint16_t Len);
extern int8_t CDC_USBIsReady ( void );
extern USBD_HandleTypeDef hUsbDeviceFS;

/*------------------------------------------------------------------------------------------*/
/*	Buffers di comunicazione per la porta USB e il terminale								*/
/*------------------------------------------------------------------------------------------*/
extern uint8_t 	UserRxBufferFS[APP_RX_DATA_SIZE];
extern uint8_t 	UserTxBufferFS[APP_TX_DATA_SIZE];
extern _queue	pTermbRxBuf;
extern _queue	pTermTxBuf;
extern unsigned char usb_rx_buf[TERMINAL_BUFFER_RX_SIZE];
extern unsigned char usb_tx_buf[TERMINAL_BUFFER_TX_SIZE];

/*------------------------------------------------------------------------------------------*/
/*	Variabili e strutture reti 485															*/
/*------------------------------------------------------------------------------------------*/
extern UART_HandleTypeDef 	huart6;
extern UART_HandleTypeDef 	huart3;
extern bool 				flgLoadConfig;

/*------------------------------------------------------------------------------------------*/
/*	Handle comunicazione CAN																*/
/*------------------------------------------------------------------------------------------*/
extern FDCAN_HandleTypeDef 	hfdcan1;


/*------------------------------------------------------------------------------------------*/
/*	Variabili e strutture sistema I/O														*/
/*------------------------------------------------------------------------------------------*/
extern systemIO _sysIO;

/********************************************************************************************/
