/********************************************************************************************/
/*	WEB_Server.d				- Server web												*/

/********************************************************************************************/

#include "SYS_Include.h"
#include "APP_Include.h"
#include "TCP_Server.h"

#ifdef WEB_SERVER_ENABLED

/********************************************************************************************/
/*	Dichiarazioni e variabili																*/
/********************************************************************************************/
//	Handle del server
TCPServerHandle	p_httpServer;
hWebPage*		p_pageList = NULL;
uint8_t			p_pageListCount = 0;
uint8_t			p_pageListSize = 0;
char 			p_webServerRxBuffer [ RX_BUFFER_SIZE ];
int				p_webServerRxWritePtr;
uint8_t			p_currentPage = 0;
char* 			p_pageDataStack [ 16 ];
int				p_pageDataStackPtr;
hWebRes*		p_webResList = NULL;
int16_t			p_webResCount = 0;
int16_t			p_webResListSize = 0;
static char 	paramBuffer [ GET_PARAM_BUFFER_SIZE ];
static int 		toTimer;

/********************************************************************************************/
/*	Funzioni private																		*/
/********************************************************************************************/
static void WSRV_ProcessGET ( char* pGet );
static void WSRV_ProcessPOST ( char* pPost );
static void WSRV_ProcessGETDATA ( char* pGetData );
static void WSRV_ProcessPOSTDATA ( char* pPostData );
static void WSRV_ProcessPostValues ( char* pPost);
static void WSRV_SendString ( char* pString );
static bool WSRV_ChecUser ( char* pData );
static void WSRV_SendPage ( int nPage );
static void WSRV_SendPage2 ( char* pDataPtr );
static hWebRes* WSRV_FindResource ( resType rType, int16_t rId );
void WSRV_processPostField (char* pSrc );
static uint8_t WSRV_hexCharToVal ( char hChar );

/********************************************************************************************/
/*	Tipi interni																			*/
/********************************************************************************************/
//	Conversione LONG<->BYTE[]
union conv
 {
 	unsigned int l;
 	struct bytes
 	{
 		char b1;
 		char b2;
 		char b3;
 		char b4;
 	} b;
 };

/********************************************************************************************/
/*	Risposte predefinite del server															*/
/********************************************************************************************/
//	Pagina non autorizzata - Richiesta login
const char respUnauthorized [] = "HTTP/1.1 401 Unauthorized\r\n"
		 	   	   	 	 	 	 "WWW-Authenticate: Basic realm="
								 "\x22" "Access to site" "\x22" "\r\n\r\n";
//	Pagina non trovata
const char respPageNotFound [] = "HTTP/1.1 404 Page not found\r\n\r\n";
//	Richiesta non valida
const char respInvalidRequest [] = "HTTP/1.1 400 Invalid request\r\n\r\n";
//	Richiesta accettata (segue pagina HTML)
const char respOK [] = 	"HTTP/1.1 200 OK\r\n"
						"Content-Type: text/html\r\n"
						"Content-Language: it\r\n"
						"\r\n";
const char imageTag [] = "<img src=";

/********************************************************************************************/
/*	Funzioni del modulo																		*/
/*																							*/
/*	WSRV_Start ( )					- Crea e avvia il web server							*/
/*	WSRV_RegisterWebPage ( )		- Registra una nuova pagina nel server					*/
/*	WSRV_RegisterResource ( )		- Registra una risorsa nel server						*/
/*	WSRV_OnConnect ( )				- Inizio connessione 									*/
/*	WSRV_OnDisconnect ( )			- Termine connessione									*/
/*	WSRV_OnReceiveData ( )			- Ricezione dati										*/
/*	WSRV_OnSystemTimer ( )			- Timer di sistema (gestione timeout connessione)		*/
/********************************************************************************************/
/*------------------------------------------------------------------------------------------*/
/*	WSRV_Start ( )					- Crea e avvia il web server							*/
/*------------------------------------------------------------------------------------------*/
bool WSRV_Start ( void )
{
	//	Server in modalità bufferizzata
	p_httpServer.connPort = 80;
	p_httpServer.txBufferSize = 1450;
	toTimer = 0;
	if ( SRV_StartServer (&p_httpServer) )
	{
		//	Inizializza la lista delle pagine web
		p_pageList = (hWebPage*)malloc ( WPAGES_INIT_BUFFER_SIZE * sizeof(hWebPage));
		p_pageListSize = WPAGES_INIT_BUFFER_SIZE;
		p_pageListCount = 0;
		//	Registra le pagine
		WS_RegisterWebPages ( );
		//	Inizializza la lista delle risorse
		p_webResList = (hWebRes*)malloc ( WRES_INIT_BUFFER_SIZE * sizeof(hWebRes));
		p_webResListSize = WRES_INIT_BUFFER_SIZE;
		p_webResCount = 0;
		//	Registra le risorse
		WS_RegisterWebResources ( );

		return ( true );
	}
	return ( false );
}
/*------------------------------------------------------------------------------------------*/
/*	WSRV_RegisterWebPage ( )		- Registra una nuova pagina nel server					*/
/*	newPage			= puntatore alla struttura di pagina da registrare						*/
/*------------------------------------------------------------------------------------------*/
bool WSRV_RegisterWebPage ( hWebPage* newPage )
{
	//	Crea lo spazio nella lista di registrazione delle pagine se necessario
	if ( p_pageListCount >= p_pageListSize )
	{
		p_pageList = realloc ( p_pageList, ( p_pageListSize + 1 ) * sizeof(hWebPage));
		p_pageListSize ++;
	}
	//	Salva i dati nella lista
	memcpy ( &p_pageList [ p_pageListCount ], newPage, sizeof( hWebPage));
	p_pageListCount ++;
	return ( true );
}
/*------------------------------------------------------------------------------------------*/
/*	WSRV_RegisterResource ( )		- Registra una risorsa nel server						*/
/*	rType		= tipo di risorsa															*/
/*	resId		= identificatore della risorsa												*/
/*	res			= puntatore alla risorsa													*/
/*------------------------------------------------------------------------------------------*/
bool WSRV_RegisterResource ( resType rType, int16_t resId, char* res )
{
	//	Crea lo spazio nella lista di registrazione delle pagine se necessario
	if ( p_webResCount >= p_webResListSize )
	{
		p_webResList = realloc ( p_webResList, ( p_webResListSize + 1 ) * sizeof(hWebRes));
		p_webResListSize ++;
	}
	//	Salva i dati nella lista
	p_webResList [ p_webResCount ].p_resType = rType;
	p_webResList [ p_webResCount ].p_resIndex = resId;
	p_webResList [ p_webResCount ].p_resPtr = res;
	p_webResCount ++;
	return ( true );
}


/*------------------------------------------------------------------------------------------*/
/*	WSRV_OnConnect ( )				- Inizio connessione 									*/
/*------------------------------------------------------------------------------------------*/
void WSRV_OnConnect ( void )
{
	p_webServerRxWritePtr = 0;
	memset ( p_webServerRxBuffer, 0, RX_BUFFER_SIZE );
}
/*------------------------------------------------------------------------------------------*/
/*	WSRV_OnDisconnect ( )			- Termine connessione									*/
/*------------------------------------------------------------------------------------------*/
void WSRV_OnDisconnect ( void )
{
	p_webServerRxWritePtr = 0;
	memset ( p_webServerRxBuffer, 0, RX_BUFFER_SIZE );
}
/*------------------------------------------------------------------------------------------*/
/*	WSRV_OnReceiveData ( )			- Ricezione dati										*/
/*------------------------------------------------------------------------------------------*/
bool WSRV_OnReceiveData ( uint8_t* pData, int dataSize )
{
	char*	pCmd;

	//	Controllo spazio nel buffer di ricezione
	if ( ( p_webServerRxWritePtr + dataSize ) >= RX_BUFFER_SIZE )
	{
		//	Ricezione annullata per sovraccarico del buffer interno
		p_webServerRxWritePtr = 0;
		p_webServerRxBuffer[0] = 0;
		return ( true );
	}

	//	Copia i dati
	memcpy ( p_webServerRxBuffer + p_webServerRxWritePtr, pData, dataSize );
	p_webServerRxWritePtr += dataSize;
	p_webServerRxBuffer [ p_webServerRxWritePtr ] = 0;
	//	Se a fine buffer non c'è il terminatore significa che il pacchetto è solo parziale
	//	e attende il resto.
	if ( strstr ( p_webServerRxBuffer, "\r\n\r\n") != NULL )
	{
		//	Debug
		#ifdef TCP_SERVER_DEBUG
			printf ( "WEB SERVER-Rx data %s\r\n", p_webServerRxBuffer );
		#endif
		//	Richiesta ricevuta - processa il comando
		//	Comando GET
		p_webServerRxWritePtr = 0;
		pCmd = strstr ( p_webServerRxBuffer, "GET / HTTP" );
		if ( pCmd == NULL )
		{
			pCmd = strstr ( p_webServerRxBuffer, "GET /Pag" );
		}
		if ( pCmd != NULL )
		{
			WSRV_ProcessGET ( pCmd );
			return ( true );
		}
		//	Comando POST
		pCmd = strstr ( p_webServerRxBuffer, "POST /" );
		if ( pCmd != NULL )
		{
			WSRV_ProcessPOST ( pCmd );
			return ( true );
		}
		//	Comando GETDATA
		pCmd = strstr ( p_webServerRxBuffer, "GETDATA /" );
		if ( pCmd != NULL )
		{
			WSRV_ProcessGETDATA ( pCmd );
			return ( true );
		}
		//	Comando POSTDATA
		pCmd = strstr ( p_webServerRxBuffer, "POSTDATA /" );
		if ( pCmd != NULL )
		{
			WSRV_ProcessPOSTDATA ( pCmd );
			return ( true );
		}
		//	Fine comandi riconosciuti
		//	Invia un errore 400 (richiesta non valida)
		WSRV_SendString ( (char*)respInvalidRequest );
		return ( true );
	}
	return ( false );
}
/*------------------------------------------------------------------------------------------*/
/*	WSRV_OnSystemTimer ( )			- Timer di sistema (gestione timeout connessione)		*/
/*------------------------------------------------------------------------------------------*/
void WSRV_OnSystemTimer ( void )
{
	if ( p_httpServer.isConnected )
	{
		if ( toTimer >= 5000 )
		{
			p_httpServer.bCmdClose = true;
			#ifdef TCP_SERVER_DEBUG
				printf ( "WEB SERVER TIMEOUT\r\n" );
			#endif
		}
		else toTimer ++;
	}
	else toTimer = 0;
}


/********************************************************************************************/
/*	Funzioni interne																		*/
/*																							*/
/*	WSRV_ProcessGET ( )				- Gestione comando GET									*/
/*	WSRV_ProcessGETDATA ( )			- Gestione comando GETDATA								*/
/*	WSRV_ProcessPOST ( )			- Gestione comando POST									*/
/*	WSRV_ProcessPOSTDATA ( )		- Gestione comando POSTDATA								*/
/*	WSRV_ProcessPostValues ( )		- Elaborazione lista valori di POST o POSTDATA			*/
/*	WSRV_ChecUser ( ) 				- Verifica le credenziali dell'utente					*/
/*	WSRV_SendPage ( )				- Invio di una pagina completa							*/
/*	WSRV_SendPage2 ( )				- Invio di una pagina o sezione di pagina				*/
/*	WSRV_FindResource ( )			- Estrazione risorsa dalla lista interna				*/
/*	WSRV_SendString ( )				- Trasmissione stringa									*/
/* 	WSRV_hexCharToVal ( ) 			- Valore di un carattere ASCII HEX 						*/
/*	WSRV_processPostField ( )		- Elabora un campo POST per gestire i caratteri speciali*/
/********************************************************************************************/
/*------------------------------------------------------------------------------------------*/
/*	WSRV_ProcessGET ( )				- Gestione comando GET									*/
/*	pGet		= puntatore alla richiesta GET												*/
/*------------------------------------------------------------------------------------------*/
static void WSRV_ProcessGET ( char* pGet )
{
	int 	reqPage = -1;
	int 	tempReqPage = -1;
	bool	bAuthorized = true;
	char* 	tempPtr = NULL;

	//	Sposta il data pointer dopo il backslash
	pGet += 5;
	//	Controlla se è stato specificato un indirizzo di pagina
	if ( strncmpi ( pGet, "Pag", 5 ))
	{
		//	Indirizzo specificato - Legge il numero della pagina richiesta
		tempReqPage = atoi ( pGet + 3);
	}
	else tempReqPage = 0;
	//	La pagina 999 è una pagina fittizia che provoca il reset del sistema
	if ( tempReqPage == 999 )
	{
		HAL_NVIC_SystemReset ( );
	}
	//	Scan nella lista delle pagine registrate alla ricerca di una pagina con
	//	l'ID richiesto
	for ( int n = 0; n < p_pageListCount; n ++ )
	{
		if ( p_pageList [ n ].p_pageId == tempReqPage )
		{
			reqPage = n;
			break;
		}
	}
	//	reqPage contiene il numero di pagina richiesto oppure -1 se pagina non trovata
	if ( reqPage == -1 )
	{
		//	Invia errore 404 - Page not found
		WSRV_SendString ( (char*)respPageNotFound );
		return;
	}
	//	La pagina è stata trovata, reqPage contiene il suo indice nella lista
	//	Controlla se la pagina richiede autenticazione
	if ( p_pageList [ reqPage ].p_pageAuthUserOnly )
	{
		//	Al momento la pagina non è autorizzata
		bAuthorized = false;
		//	Cerca il token "Authorization"
		tempPtr = strstr ( pGet, "Authorization: Basic");
		if ( tempPtr != NULL )
		{
			//	Verifica le credenziali
			bAuthorized = WSRV_ChecUser ( tempPtr );
		}
	}
	//	Invia risposta "Unauthorized" e attende una nuova richiesta autenticata
	if ( ! bAuthorized )
	{
		WSRV_SendString ( (char*)respUnauthorized );
		return;
	}
	//	Invio pagina autorizzato
	p_currentPage = reqPage;
	//	Notifica apertura pagina
	if ( p_pageList [ reqPage ].p_pageOpenCallback )
	{
		p_pageList [ reqPage ].p_pageOpenCallback ( reqPage );
	}
	//	Invia la pagina richiesta
	WSRV_SendPage ( reqPage );
}
/*------------------------------------------------------------------------------------------*/
/*	WSRV_ProcessGETDATA ( )			- Gestione comando GETDATA								*/
/*	pGetData	= puntatore alla richiesta GETDATA											*/
/*------------------------------------------------------------------------------------------*/
static void WSRV_ProcessGETDATA ( char* pGetData )
{
	#define MAX_RESP_BUFFER 128

	int nParam;
	char respBuffer [ MAX_RESP_BUFFER ];
	char* pPtr = respBuffer;
	//	Scan parametri richiesta
	memset ( respBuffer, 0, MAX_RESP_BUFFER );
	while ( *pGetData )
	{
		if ( *pGetData == 'F')
		{
			pGetData ++;
			nParam = atoi(pGetData);
			//	Richiede il valore del campo
			memset ( paramBuffer, 0, GET_PARAM_BUFFER_SIZE );
			//	I parametri con ID < 1000 sono specifici delle singole pagine
			if ( nParam < 1000 )
			{
				if ( p_pageList [ p_currentPage ].p_pageGetParamCallback )
				{
					p_pageList [ p_currentPage ].p_pageGetParamCallback (p_currentPage, nParam, paramBuffer );
				}
			}
			//	I parametri con ID >= 1000 sono globali
			else
			{
				WS_GetGlobalParamCallback (p_currentPage, nParam, paramBuffer );
			}
			int chCount = sprintf ( pPtr, "F%d=%s;", nParam, paramBuffer );
			pPtr += chCount;
		}
		pGetData ++;
	}
	WSRV_SendString ( (char*)respOK );
	osDelay ( 10 );
	WSRV_SendString ( (char*)respBuffer );
}
/*------------------------------------------------------------------------------------------*/
/*	WSRV_ProcessPOST ( )			- Gestione comando POST									*/
/*	pPost		= puntatore alla richeista POST												*/
/*------------------------------------------------------------------------------------------*/
static void WSRV_ProcessPOST ( char* pPost )
{
	//	Cerca il marker 0x0D-0x0A-0x0D-0x0A
	char* pScanPtr = strstr ( pPost, "\r\n\r\n");
	//	Se l'area parametri ha dimensione non nulla mette alla fine un separatore di
	//	parametro e sposta il terminatore di stringa alla posizione successiva
	if ( pScanPtr != NULL )
	{
		WSRV_ProcessPostValues ( pScanPtr );
	}
	//	Notifica il POST della pagina (numero parametro e valore = NULL )
	//	Utile per eventuale salvataggio dei parametri
	if ( p_pageList [ p_currentPage ].p_pagePostParamCallback )
	{
		p_pageList [ p_currentPage ].p_pagePostParamCallback (p_currentPage, 0, NULL );
	}
	//	Reinvia la pagima al browser
	WSRV_SendPage ( p_currentPage );
}
/*------------------------------------------------------------------------------------------*/
/*	WSRV_ProcessPOSTDATA ( )		- Gestione comando POSTDATA								*/
/*	pPostData	= puntatore alla richeista POSTDATA											*/
/*------------------------------------------------------------------------------------------*/
static void WSRV_ProcessPOSTDATA ( char* pPostData )
{
	WSRV_ProcessPostValues ( pPostData );
	osDelay ( 10 );
	WSRV_SendString ( (char*)respOK );
}
/*------------------------------------------------------------------------------------------*/
/*	WSRV_ProcessPostValues ( )		- Elaborazione lista valori di POST o POSTDATA			*/
/*	pPost		= puntatore alla richeista POST												*/
/*------------------------------------------------------------------------------------------*/
static void WSRV_ProcessPostValues ( char* pPost)
{
	uint16_t 	paramId;
	uint8_t 	writePtr;
	uint16_t	n;
	//	Cerca il marker 0x0D-0x0A-0x0D-0x0A
	char* pScanPtr = pPost;
	if ( ( n = strlen ( pScanPtr ) ) > 4 )
	{
		pScanPtr [ n++ ] = '&';
		pScanPtr [ n ] = 0;
		printf ( pScanPtr );
		//	Ogni parametro è nella forma  Fx=value&
		//	F = marker nome di parametro
		//	x = ID del parametro
		//	value = valore del parametro in formato ASCII con caratteri speciali codificati
		//	& = carattere di separazione tra parametri
		//	Ciclo di scansione dell'area parametri - procede fino al carattere nullo
		uint8_t parScanStatus = 0;
		while ( *pScanPtr )
		{
			switch ( parScanStatus )
			{
				//	Attesa marker F
				case 0:
					if ( *pScanPtr == 'F') parScanStatus = 1;
				break;
				//	ID del parametro
				case 1:
					paramId = atoi(pScanPtr);
					parScanStatus = 2;
					writePtr = 0;
					memset ( paramBuffer, 0, GET_PARAM_BUFFER_SIZE );
				break;
				//	Segno =
				case 2:
					if ( *pScanPtr == '=' ) parScanStatus = 3;
				break;
				//	Valore del parametro e terminatore
				case 3:
					if ( *pScanPtr == '&')
					{
						//	Reset stato del decoder
						parScanStatus = 0;
						//	Converte i caratteri speciali da formato HEX ad ASCII
						WSRV_processPostField ( paramBuffer );
						//	Notifica il POST del parametro
						if ( p_pageList [ p_currentPage ].p_pagePostParamCallback )
						{
							p_pageList [ p_currentPage ].p_pagePostParamCallback (p_currentPage, paramId, paramBuffer );
						}
					}
					else
					{
						paramBuffer [ writePtr ++ ] = *pScanPtr;
					}
				break;
			}
			//	Controllo buffer overrun
			if ( writePtr >= GET_PARAM_BUFFER_SIZE )
			{
				writePtr = 0;
				parScanStatus = 0;
			}
			//	Carattere successivo
			pScanPtr ++;
		}
	}
}
/*------------------------------------------------------------------------------------------*/
/*	WSRV_ChecUser ( ) 				- Verifica le credenziali dell'utente					*/
/*	pData		= puntatore ai dati della pagina											*/
/*------------------------------------------------------------------------------------------*/
static bool WSRV_ChecUser ( char* pData )
{
	const char b64Table [] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	char userData [ 32 ];
	char udBase64 [ 48 ];
	//	Compone username e password nella stringa da codificare in base64
	strcpy ( userData, sysCfg.m_userName );
	strcat ( userData, ":");
	strcat ( userData, sysCfg.m_userPwd );
	//	Codifica in base64 nome utente e password
	int pEncoder = 0;
	int udSize = strlen ( userData );
	for ( int i = 0; i < udSize; i += 3)
	{
		union conv src;
		src.l = 0;
		src.b.b3 = userData [ i ];
		src.b.b2 = userData [ i + 1 ];
		src.b.b1 = userData [ i + 2 ];
		udBase64 [pEncoder + 3] = b64Table [ src.b.b1 & 0x3F ];
		src.l >>= 6;
		udBase64 [pEncoder + 2] = b64Table [ src.b.b1 & 0x3F ];
		src.l >>= 6;
		udBase64 [pEncoder + 1] = b64Table [ src.b.b1 & 0x3F ];
		src.l >>= 6;
		udBase64 [pEncoder] = b64Table [ src.b.b1 & 0x3F ];
		pEncoder += 4;
	}
	udBase64 [pEncoder]=0;
	//	Padding
	if ( ( udSize % 3 ) == 1 )
	{
		udBase64 [pEncoder-2]='=';
		udBase64 [pEncoder-1]='=';
	}
	else if ( ( udSize % 3 ) == 2 )
	{
		udBase64 [pEncoder-1]='=';
	}
	//	Confronto credenziali codificate
	if ( memcmp ( pData + 21, udBase64, strlen ( udBase64 ) ) == 0 )
	{
		return ( true );
	}
	//	Confronta le due stringhe codificate
	return ( false );
}
/*------------------------------------------------------------------------------------------*/
/*	WSRV_SendPage ( )				- Invio di una pagina completa							*/
/*	nPage		= numero della pagina														*/
/*------------------------------------------------------------------------------------------*/
static void WSRV_SendPage ( int nPage )
{
	//	Inizializza lo stack
	p_pageDataStackPtr = 0;
	//	Invio header
	WSRV_SendString ( (char*)respOK );
	osDelay ( 10 );
	WSRV_SendPage2 ( p_pageList [ nPage ].p_pagePtr );
	//	Trasmette i dati eventualmente rimasti nel buffer
	SRV_FlushBuffer ( &p_httpServer );
}
/*------------------------------------------------------------------------------------------*/
/*	WSRV_SendPage2 ( )				- Invio di una pagina o sezione di pagina				*/
/*	pDataPtr	= puntatore ai dati della pagina											*/
/*------------------------------------------------------------------------------------------*/
static void WSRV_SendPage2 ( char* pDataPtr )
{
	hWebRes* tag;
	//	Scansione del blocco dati fino al terminatore 0x00
	while ( *pDataPtr )
	{
		//	Controllo tag speciali - Iniziano con <#
		if ( ( *pDataPtr == '<' ) && ( *( pDataPtr + 1 ) == '#' ) )
		{
			//	Il carattere successivo è il tipo di tag
			uint8_t tagType = *( pDataPtr + 2 );
			//	Segue il numero del tag
			int16_t tagId = atoi ( pDataPtr + 3 );
			//	Processa il tag
			switch ( tagType )
			{
				//	Immagine - Preceduta da tag immagine
				case RES_IMAGE:
					if ( ( tag = WSRV_FindResource ( tagType, tagId ) ) != NULL )
					{
						SRV_BufferedSendString (&p_httpServer,  (char*)imageTag );
						WSRV_SendPage2 ( tag->p_resPtr );
						SRV_BufferedSendByte ( &p_httpServer, '>', false );
					};
				break;
				//	Campo dati
				case RES_FIELD:
					//	Richiede il valore del campo
					memset ( paramBuffer, 0, GET_PARAM_BUFFER_SIZE );
					//	I parametri con ID < 1000 sono specifici delle singole pagine
					if ( tagId < 1000 )
					{
						if ( p_pageList [ p_currentPage ].p_pageGetParamCallback )
						{
							p_pageList [ p_currentPage ].p_pageGetParamCallback (p_currentPage, tagId, paramBuffer );
						}
					}
					//	I parametri con ID >= 1000 sono globali
					else
					{
						WS_GetGlobalParamCallback (p_currentPage, tagId, paramBuffer );
					}
					//	Invia il campo al server
					SRV_BufferedSendByte ( &p_httpServer, 0x22, false );
					SRV_BufferedSendString ( &p_httpServer, paramBuffer );
					SRV_BufferedSendByte ( &p_httpServer, 0x22, false );
				break;
				//	Altri tipi di tag
				case RES_SCRIPT:
				case RES_HTML:
				case RES_RESOURCE:
					if ( ( tag = WSRV_FindResource ( tagType, tagId ) ) != NULL )
					{
						WSRV_SendPage2 ( tag->p_resPtr );
					}
				break;
			}
			while ( ( *pDataPtr != '>' ) && ( *pDataPtr != 0 ) )
			{
				pDataPtr ++;
			}
		}
		else
		{
			SRV_BufferedSendByte ( &p_httpServer, *pDataPtr, false );
		}
		//	Incremento del data pointer
		if ( *pDataPtr != 0 ) pDataPtr ++;
	}
}
/*------------------------------------------------------------------------------------------*/
/*	WSRV_FindResource ( )			- Estrazione risorsa dalla lista interna				*/
/*	rType		= tipo di risorsa															*/
/*	rId			= ID della risorsa															*/
/*------------------------------------------------------------------------------------------*/
static hWebRes* WSRV_FindResource ( resType rType, int16_t rId )
{
	for ( int i = 0; i < p_webResCount; i ++ )
	{
		if ( ( p_webResList [ i].p_resType == rType ) &&
			 ( p_webResList [ i].p_resIndex == rId ) )
		{
			return ( &p_webResList[i]);
		}
	}
	return(NULL);
}
/*------------------------------------------------------------------------------------------*/
/*	WSRV_SendString ( )				- Trasmissione stringa									*/
/*------------------------------------------------------------------------------------------*/
static void WSRV_SendString ( char* pString )
{
	SRV_SendData ( &p_httpServer, (uint8_t*)pString, strlen(pString));
}
/*------------------------------------------------------------------------------------------*/
/* WSRV_hexCharToVal ( ) 			- Valore di un carattere ASCII HEX 						*/
/*  hChar = carattere ASCII HEX																*/
/*  Valore di ritormo																		*/
/*  Il valore numerico, 0 se non è un carattere HEX valido									*/
/*------------------------------------------------------------------------------------------*/
static uint8_t WSRV_hexCharToVal ( char hChar )
{
	if ( ( hChar >= '0' ) && ( hChar <= '9' ) ) return ( hChar - '0' );
	hChar &= 0xDF;
	if ( ( hChar >= 'A' ) && ( hChar <= 'F' ) ) return ( hChar - 'A' + 10 );
	return ( 0 );
}
/*------------------------------------------------------------------------------------------*/
/*	WSRV_processPostField ( )		- Elabora un campo POST per gestire i caratteri speciali*/
/*	pSrc	= campo da processare															*/
/*------------------------------------------------------------------------------------------*/
void WSRV_processPostField (char* pSrc )
{
	// Puntatori a sorgente e destinazione nello stesso buffer
	char* pDest = pSrc;
	char c;
	// Scansione del buffer
	while ( ( c = *pSrc ) != 0 )
	{
		// Spazio sostituito da +
		if ( c == '+' )
		{
			*pDest = ' ';
		}
		// Carattere trasmesso con %XX (XX=codice ASCII in esadecimale)
		else if ( c == '%' )
		{
			pSrc ++;
			c = WSRV_hexCharToVal ( *pSrc ) * 16;
			pSrc ++;
			*pDest = WSRV_hexCharToVal ( *pSrc ) + c;
		}
		// Carattere standard da non elaborare
		else *pDest = c;
		// Incremento puntatori
		pSrc ++;
		pDest ++;
	}
	// Terminatore
	*pDest = 0;
}

#endif

/********************************************************************************************/
