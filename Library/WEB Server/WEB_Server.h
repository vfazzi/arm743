/********************************************************************************************/
/*	WEB_Server.h				- Server web												*/

/********************************************************************************************/


#ifndef __WEB_SERVER_DEFINED
#define __WEB_SERVER_DEFINED


/********************************************************************************************/
/*	Configurazione interna del server														*/
/********************************************************************************************/
//	Dimensione iniziale della lista delle pagine
#define	WPAGES_INIT_BUFFER_SIZE		10
//	Dimensione iniziale della lista delle risorse
#define	WRES_INIT_BUFFER_SIZE		64
//	Buffer di ricezione (contiene i comandi GET e POST)
#define	RX_BUFFER_SIZE				(1024*3)
//	Dimensione del buffer di parametro GET o POST
#define	GET_PARAM_BUFFER_SIZE		64


/********************************************************************************************/
/*	Strutture e tipi di dati																*/
/********************************************************************************************/
//	Tipi di Risorse del sito
typedef enum	{
	RES_IMAGE		= 'I',				//	Immagine codificata in base64 (I)
	RES_SCRIPT		= 'S',				//	Script (S)
	RES_HTML		= 'H',				//	Blocco HTML (H)
	RES_FIELD		= 'F',				//	Campo dati (F)
	RES_RESOURCE	= 'R',				//	Altro tipo di risorsa (R)
} resType;

//	Struttura di definizione pagina web
typedef struct {
	uint8_t		p_pageId;			//	Identificatore della pagina
	bool		p_pageAuthUserOnly;	//	Riservata a utenti autenticati
	char*		p_pagePtr;			//	Puntatore ai dati della pagina
	void (*p_pageOpenCallback)(uint8_t);						//	Callback apertura pagina
	void (*p_pageGetParamCallback)(uint8_t, uint16_t, char*);	//	Callback lettura parametri
	void (*p_pagePostParamCallback)(uint8_t, uint16_t, char*);	//	Callback salvataggio parametri
} hWebPage;


//	Struttura di definizione risorsa
typedef struct {
	uint8_t		p_resType;
	uint16_t	p_resIndex;
	char*		p_resPtr;
} hWebRes;


/********************************************************************************************/
/*	Funzioni del modulo																		*/
/********************************************************************************************/
bool WSRV_Start 			( void );
bool WSRV_RegisterWebPage 	( hWebPage* newPage );
bool WSRV_RegisterResource 	( resType rType, int16_t resId, char* res );
void WSRV_OnConnect 		( void );
void WSRV_OnDisconnect 		( void );
bool WSRV_OnReceiveData 	( uint8_t* pData, int dataSize );
void WSRV_OnSystemTimer 	( void );


#endif


/********************************************************************************************/
