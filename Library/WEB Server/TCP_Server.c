/********************************************************************************************/
/*	TCP_Server.c				- Server TCP/IP												*/

/********************************************************************************************/

#include "SYS_Include.h"
#include "TCP_Server.h"


/********************************************************************************************/
/*	Dichiarazioni e variabili																*/
/********************************************************************************************/
//	Variabili interne
TCPServerHandle* 	p_srvPool [ TCP_MAX_SERVER ];
uint8_t				p_srvCount = 0;

//	Dichiarazione funzioni interne
void static srvThreadProc ( void* arg );

//	Variabili esterne
extern struct netif gnetif;

/********************************************************************************************/
/*	Funzioni del modulo																		*/
/*																							*/
/*	SRV_StartServer ( )				- Inizializzazione server TCP/IP						*/
/*	SRV_SendData ( )				- Trasmissione blocco dati								*/
/*	SRV_BufferedSendByte ( )		- Trasmissione di un byte in modo bufferizzato			*/
/*	SRV_FlushBuffer ( )				- Trasmette il contenuto del buffer 					*/
/*	SRV_Disconnect ( )				- Chiusura connessione al client						*/
/*	SRV_EthernetUnlink ( )			- Reset connessioni in caso di cavo rete scollegato		*/
/*	SRV_IsConnected ( )				- Indica se il server ha un client collegato			*/
/*	SRV_IsEthCableConnected ( )		- Stato di connessione cavo ethernet					*/
/*	SRV_GetServerCount ( )			- Numero di server attivi								*/
/*	SRV_GetServerHandle ( )			- Recupero handle di un server 							*/
/********************************************************************************************/
/*------------------------------------------------------------------------------------------*/
/*	SRV_StartServer ( )				- Inizializzazione server TCP/IP						*/
/*	maxConn = numero massimo di connessioni attive											*/
/*------------------------------------------------------------------------------------------*/
bool SRV_StartServer ( TCPServerHandle* hServer )
{
	//	Controlla se c'è spazio libero nel pool
	if ( p_srvCount < TCP_MAX_SERVER )
	{
		//	Inizializza la struttura di definizione
		hServer->lstnConn = NULL;
		hServer->newConn = NULL;
		hServer->bCmdClose = false;
		hServer->isConnected = false;
		hServer->rxBuffer = NULL;
		//	Il thread del server ha nome Srv + numero della porta [es. Srv80]
		sprintf ( hServer->thrName, "Srv%d", hServer->connPort );
		//	Avvia il thread del server
	    if ( sys_thread_new ( hServer->thrName,  srvThreadProc, hServer, DEFAULT_THREAD_STACKSIZE, osPriorityLow) )
	    {
	    	//	Crea il buffer di trasmissione se richiesto
	    	if ( hServer->txBufferSize > 0 )
	    	{
	    		hServer->txBuffer = malloc ( hServer->txBufferSize );
	    		hServer->txBufferPtr = 0;
	    	}
	    	//	Aggiunge il server al pool dei server
	    	p_srvPool [ p_srvCount ] = hServer;
	    	p_srvCount ++;
	    	return (true);
	    }
	}
	//	Server pool pieno
	return ( false );
}
/*------------------------------------------------------------------------------------------*/
/*	SRV_SendData ( )				- Trasmissione blocco dati								*/
/*	hServer		= handle del server															*/
/*	pData		= buffer dati																*/
/*	dataSize	= dimensione buffer dati													*/
/*------------------------------------------------------------------------------------------*/
bool SRV_SendData ( TCPServerHandle* hServer, unsigned char* pData, unsigned int dataSize )
{
	if ( hServer->newConn != NULL )
	{
		if ( netconn_write(hServer->newConn, pData, dataSize, NETCONN_COPY) == ERR_OK )
		{
			return ( true );
		}
	}
	return ( false );
}
/*------------------------------------------------------------------------------------------*/
/*	SRV_BufferedSendByte ( )		- Trasmissione di un byte in modo bufferizzato			*/
/*	hServer		= handle del server															*/
/*	ch			= carattere da inserire nel buffer											*/
/*	forceSend	= forza la trasmissione e lo svuotamento del buffer							*/
/*------------------------------------------------------------------------------------------*/
bool SRV_BufferedSendByte ( TCPServerHandle* hServer, uint8_t ch, bool forceSend )
{
	//	Modo bufferizzato non attivo
	if ( hServer->txBuffer == NULL ) return ( false );
	//	Inserisce il carattere nel buffer di trasmissione
	hServer->txBuffer [ hServer->txBufferPtr ] = ch;
	//	Incrementa il write pointer
	hServer->txBufferPtr ++;
	//	Trasmissione per buffer pieno o forzata
	if ( ( hServer->txBufferPtr == hServer->txBufferSize ) || forceSend )
	{
		//	Trasmissione dati
		if ( netconn_write(hServer->newConn, hServer->txBuffer, hServer->txBufferPtr, NETCONN_COPY) == ERR_OK )
		{
			hServer->txBufferPtr = 0;
			osDelay ( 100 );
			return ( true );
		}
		//	Errore di trasmissione - Reset buffer (dati persi)
		hServer->txBufferPtr = 0;
		return ( false );
	}
	return ( true );
}
/*------------------------------------------------------------------------------------------*/
/*	SRV_BufferedSendString ( )		- Trasmissione di una stringa in modo bufferizzato		*/
/*------------------------------------------------------------------------------------------*/
void SRV_BufferedSendString ( TCPServerHandle* hServer, char* pString )
{
	while ( *pString )
	{
		SRV_BufferedSendByte ( hServer, *pString, false );
		pString ++;
	}
}
/*------------------------------------------------------------------------------------------*/
/*	SRV_FlushBuffer ( )				- Trasmette il contenuto del buffer 					*/
/*	hServer		= handle del server															*/
/*------------------------------------------------------------------------------------------*/
void SRV_FlushBuffer ( TCPServerHandle* hServer )
{
	//	Controlla se ci sono dati nel buffer
	if ( hServer->txBufferPtr > 0 )
	{
		//	Trasmissione dati
		if ( netconn_write(hServer->newConn, hServer->txBuffer, hServer->txBufferPtr, NETCONN_COPY) == ERR_OK )
		{
			hServer->txBufferPtr = 0;
			osDelay ( 100 );
		}
	}
}
/*------------------------------------------------------------------------------------------*/
/*	SRV_Disconnect ( )				- Chiusura connessione al client						*/
/*	hServer		= handle del server															*/
/*------------------------------------------------------------------------------------------*/
void SRV_Disconnect ( TCPServerHandle* hServer )
{
	if ( hServer->isConnected )
	{
		hServer->bCmdClose = true;
	}
}
/*------------------------------------------------------------------------------------------*/
/*	SRV_EthernetUnlink ( )			- Reset connessioni in caso di cavo rete scollegato		*/
/*------------------------------------------------------------------------------------------*/
void SRV_EthernetUnlink ( void )
{
	for ( uint8_t i = 0; i < p_srvCount; i ++ )
	{
		if ( p_srvPool [ i ]->newConn != NULL )
		{
			p_srvPool [ i ]->bCmdClose = true;
		}
	}
}
/*------------------------------------------------------------------------------------------*/
/*	SRV_IsConnected ( )				- Indica se il server ha un client collegato			*/
/*	hServer		= handle del server															*/
/*------------------------------------------------------------------------------------------*/
bool inline SRV_IsConnected ( TCPServerHandle* hServer )
{
	return ( hServer->isConnected );
}
/*------------------------------------------------------------------------------------------*/
/*	SRV_IsEthCableConnected ( )		- Stato di connessione cavo ethernet			*/
/*------------------------------------------------------------------------------------------*/
bool inline SRV_IsEthCableConnected ( void )
{
	return ( netif_is_up(&gnetif) );
}
/*------------------------------------------------------------------------------------------*/
/*	SRV_GetServerCount ( )			- Numero di server attivi								*/
/*------------------------------------------------------------------------------------------*/
uint8_t inline SRV_GetServerCount ( void )
{
	return ( p_srvCount );
}
/*------------------------------------------------------------------------------------------*/
/*	SRV_GetServerHandle ( )			- Recupero handle di un server 							*/
/*	nServer	= numero del server																*/
/*------------------------------------------------------------------------------------------*/
TCPServerHandle* SRV_GetServerHandle ( uint8_t nServer )
{
	return (  ( nServer < p_srvCount ) ? p_srvPool [ nServer ] : NULL );
}


/********************************************************************************************/
/*	Funzioni interne																		*/
/*																							*/
/*	srvThreadProc ( )				- Thread del server										*/
/********************************************************************************************/
/*------------------------------------------------------------------------------------------*/
/*	srvThreadProc ( )				- Thread del server										*/
/*------------------------------------------------------------------------------------------*/
void static srvThreadProc ( void* arg )
{
	TCPServerHandle* hThisServer = (TCPServerHandle*)arg;
	err_t connError;
	bool bRcv = true;
	//	Crea la connessione in ascolto del server
	hThisServer->lstnConn = netconn_new ( NETCONN_TCP );
	if ( hThisServer->lstnConn != NULL )
	{
		//	Definisce la porta di ascolto del server
		if ( netconn_bind ( hThisServer->lstnConn, IP4_ADDR_ANY, hThisServer->connPort  ) == ERR_OK )
		{
			//	Mette la connessione in ascolto
			netconn_listen ( hThisServer->lstnConn );
			//	Attende il collegamento di un client
			while ( 1 )
			{
				//	Accetta la connessione
				if ( netconn_accept(hThisServer->lstnConn, &hThisServer->newConn) == ERR_OK )
				{
					//	Messaggi di debug
					#ifdef TCP_SERVER_DEBUG
						printf ( "TCP SERVER-Aperta connessione su porta %d\r\n", hThisServer->connPort );
					#endif
					//	Inizializza la connessione
					hThisServer->bCmdClose = false;
					hThisServer->rxBuffer = NULL;
					hThisServer->newConn->recv_timeout = 2000;
					//hThisServer->newConn->send_timeout = 2000;
					hThisServer->isConnected = true;
					//	Notifica connessione al web server
					#ifdef WEB_SERVER_ENABLED
						if ( hThisServer->connPort == HTTP_PORT )
						{
							WSRV_OnConnect ( );
						}
					#endif
					//	Notifica connessione all'APP
					APP_TcpServerConnected ( hThisServer );
					//	Attesa chiusura connessione
					while ( ! hThisServer->bCmdClose )
					{
						//	Ricezione dati
						connError = netconn_recv ( hThisServer->newConn, &hThisServer->rxBuffer );
						if ( connError == ERR_OK )
						{
							bRcv = true;
							#ifdef WEB_SERVER_ENABLED
								if ( hThisServer->connPort == HTTP_PORT )
								{
									bRcv = false;
									hThisServer->bCmdClose = WSRV_OnReceiveData ( hThisServer->rxBuffer->ptr->payload,
											  	  	  	 	 	 	 	 	 	  hThisServer->rxBuffer->ptr->len );
								}
							#endif
							//	Notifica ricezione dati
							if ( bRcv )
							{
								hThisServer->bCmdClose = APP_TcpReceiveCallback ( hThisServer,
																				  hThisServer->rxBuffer->ptr->payload,
																				  hThisServer->rxBuffer->ptr->len );
							}
							//	Cancella il buffer di ricezione
							netbuf_delete ( hThisServer->rxBuffer );
							hThisServer->rxBuffer = NULL;
						}
						//	Client disconnesso
						else if ( connError != ERR_TIMEOUT )
						{
							hThisServer->bCmdClose = true;
						}
					}
					//osDelay ( 10 );
					//netconn_shutdown ( hThisServer->newConn, 0x01, 0x01 );
					osDelay ( 10 );
					netconn_close ( hThisServer->newConn );
					netconn_delete ( hThisServer->newConn );
					hThisServer->newConn = NULL;
					hThisServer->isConnected = false;
					//	Notifica disconnessione al web server
					#ifdef WEB_SERVER_ENABLED
						if ( hThisServer->connPort == HTTP_PORT )
						{
							WSRV_OnDisconnect ( );
						}
					#endif
					//	Notifica discnnessione all'APP
					APP_TcpServerDisconnected ( hThisServer );
					#ifdef TCP_SERVER_DEBUG
						printf ( "TCP SERVER-Chiusa connessione su porta %d\r\n", hThisServer->connPort );
					#endif
				}
				else
				{
					netconn_delete ( hThisServer->newConn );
					hThisServer->newConn = NULL;
				}
			}

		}
	}

}

/********************************************************************************************/
