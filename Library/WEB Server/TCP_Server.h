/********************************************************************************************/
/*	TCP_Server.h				- Server TCP/IP												*/

/********************************************************************************************/

#include "lwip.h"
#include "lwip/opt.h"
#include "lwip/arch.h"
#include "lwip/api.h"
#include "lwip/apps/fs.h"
#include "arch.h"

#ifndef __TCP_SERVER_INCLUDE
#define __TCP_SERVER_INCLUDE

/********************************************************************************************/
/*	Tipi e strutture																		*/
/********************************************************************************************/
//	Struttura di definizione del server
typedef struct
{
	//	Parametri di configurazione
	int connPort;					//	Porta del server
	int txBufferSize;				//	Dimensione del buffer di trasmissione (se attivo)
	//	Parametri interni
	bool isConnected;				//	Stato connessione a un client
	bool bCmdClose;					//	Comando di chiusura connessione
	char thrName [10];				//	Nome del thread del server
	struct netconn *lstnConn;		//	Connessione in ascolto
	struct netconn *newConn;		//	Connessione attiva
	struct netbuf *rxBuffer;		//	Buffer di ricezione
	uint8_t* txBuffer;				//	Buffer di trasmissione
	int txBufferPtr;				//	Puntatore di scrittura nel buffer di trasmissione
} TCPServerHandle;


/********************************************************************************************/
/*	Funzioni del modulo																		*/
/********************************************************************************************/
bool SRV_StartServer ( TCPServerHandle* hServer );
bool SRV_SendData ( TCPServerHandle* hServer, unsigned char* pData, unsigned int dataSize );
void SRV_BufferedSendString ( TCPServerHandle* hServer, char* pString );
bool SRV_BufferedSendByte ( TCPServerHandle* hServer, uint8_t ch, bool forceSend );
void SRV_FlushBuffer ( TCPServerHandle* hServer );
void SRV_Disconnect ( TCPServerHandle* hServer );
void SRV_EthernetUnlink ( void );
bool SRV_IsEthCableConnected ( void );
bool SRV_IsConnected ( TCPServerHandle* hServer );
uint8_t SRV_GetServerCount ( void );
TCPServerHandle* SRV_GetServerHandle ( uint8_t nServer );

//	Callback esterne
extern bool APP_TcpReceiveCallback ( TCPServerHandle* hServer, char* pData, uint16_t dataLen );
extern void APP_TcpServerConnected ( TCPServerHandle* hServer );
extern void APP_TcpServerDisconnected ( TCPServerHandle* hServer );

#endif


/********************************************************************************************/
