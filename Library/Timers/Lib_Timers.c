/********************************************************************************************/
/* 	LIB_Timers.c - Gestione timers	 														*/

/********************************************************************************************/

#include "SYS_Include.h"

//	Definizione variabili interne
volatile int32_t appTimers [APP_MAX_TIMER];
volatile int32_t sysTimers [SYS_MAX_TIMER];
volatile int32_t appLongTimers [APP_MAX_LTIMER];
volatile int32_t nLongTimerTick;

volatile uint64_t nLongTickCount = 0;

/********************************************************************************************/
/*	Funziobi pubbliche																		*/
/*																							*/
/*	LIB_TimerInit ( )		- Inizializzazione timers										*/
/*	LIB_TimerTickHandler ( )- Funzione di aggiornamento timers								*/
/* 	ShortTimerSet ( ) 		- Inizalizza un timer di applicazione							*/
/*	ShortTimerSetOff ( )			- Disattiva un timer di applicazione							*/
/* 	ShortTimerIsExpired ( )	- Controllo scadenza di un timer di applicazione				*/
/*	ShortTimerIsOff ( )		- Controlla se un timer di applicazione è disattivato			*/
/*	ShortTimerIsOn ( )		- Controlla se un timer di applicazione è attivo				*/
/* 	ShortTimerGetValue ( )	- Lettura valore di un timer di applicazione					*/
/*																							*/
/* 	LongTimerSet ( ) 	- Inizalizza un long timer di applicazione						*/
/*	LongTimerSetOff ( )		- Disattiva un long timer di applicazione						*/
/* 	LongTimerIsExpired ( )	- Controllo scadenza di un long timer di applicazione		*/
/*	LongTimerIsOff ( )	- Controlla se un long timer di applicazione è disattivato		*/
/*	LongTimerIsOn ( )	- Controlla se un long timer di applicazione è attivo			*/
/* 	LongTimerGetValue ( )	- Lettura valore di un timer di applicazione				*/
/*																							*/
/* 	SysTimerSet ( ) 		- Inizalizza un timer di sistema								*/
/*	SysTimerOff ( )			- Disattiva un timer di sistema									*/
/* 	isSysTimerExpired ( )	- Controllo scadenza di un timer di sistema						*/
/*	isSysTimerOff ( )		- Controlla se un timer di sistema è disattivato				*/
/*	isSysTimerOn ( )		- Controlla se un timer di sistema è attivo						*/
/* 	SysTimerGetValue ( )	- Lettura valore di un timer di sistema							*/
/*																							*/
/*	LongTCGetValue ( )		- Lettura del long tick counter									*/
/* 	LongElapsedMs ( )		- Intervallo trascorso da un long tick di riferimento			*/

/********************************************************************************************/
/*------------------------------------------------------------------------------------------*/
/*	LIB_TimerInit ( )	- Inizializzazione timers											*/
/*------------------------------------------------------------------------------------------*/
void LIB_TimerInit(void)
{
	memset ( (void*)appTimers, 0xFF, APP_MAX_TIMER * sizeof (int));
	memset ( (void*)sysTimers, 0xFF, SYS_MAX_TIMER * sizeof (int));
	nLongTickCount = 0;
	memset ( (void*)appLongTimers, 0xFF, APP_MAX_LTIMER * sizeof (int));
	nLongTimerTick = 0;
}
/*------------------------------------------------------------------------------------------*/
/* 	ShortTimerSet ( ) 		- Inizalizza un timer di applicazione							*/
/*	nTimer		= identificatore del timer													*/
/*	timerTime	= valore di inizializzazione (durata in ms)									*/
/*------------------------------------------------------------------------------------------*/
void ShortTimerSet ( enum AppTimers nTimer, unsigned int timerTime )
{
	appTimers[nTimer] = timerTime;
}
/*------------------------------------------------------------------------------------------*/
/*	ShortTimerSetOff ( )			- Disattiva un timer di applicazione							*/
/*	nTimer		= identificatore del timer													*/
/*------------------------------------------------------------------------------------------*/
void ShortTimerSetOff ( enum AppTimers nTimer )
{
	appTimers[nTimer] = NONSIGN_32;
}
/*------------------------------------------------------------------------------------------*/
/* 	ShortTimerIsExpired ( )	- Controllo scadenza di un timer di applicazione				*/
/*	nTimer		= identificatore del timer													*/
/*------------------------------------------------------------------------------------------*/
bool ShortTimerIsExpired ( enum AppTimers nTimer )
{
	return (bool)( appTimers[nTimer] == SCADUTO );
}
/*------------------------------------------------------------------------------------------*/
/*	ShortTimerIsOff ( )		- Controlla se un timer di applicazione è disattivato			*/
/*	nTimer		= identificatore del timer													*/
/*------------------------------------------------------------------------------------------*/
bool ShortTimerIsOff ( enum AppTimers nTimer )
{
	return ( appTimers[nTimer] == NONSIGN_32 );
}
/*------------------------------------------------------------------------------------------*/
/*	ShortTimerIsOn ( )		- Controlla se un timer di applicazione è attivo				*/
/*	nTimer		= identificatore del timer													*/
/*------------------------------------------------------------------------------------------*/
bool ShortTimerIsOn ( enum AppTimers nTimer )
{
	return ( appTimers[nTimer] > 0 );
}
/*------------------------------------------------------------------------------------------*/
/* 	ShortTimerGetValue ( )	- Lettura valore di un timer di applicazione					*/
/*------------------------------------------------------------------------------------------*/
int32_t ShortTimerGetValue ( enum AppTimers nTimer )
{
	return ( appTimers[nTimer] );
}

/*------------------------------------------------------------------------------------------*/
/* 	LongTimerSet ( ) 	- Inizalizza un long timer di applicazione						*/
/*	nTimer		= identificatore del timer													*/
/*	timerTime	= valore di inizializzazione (durata in ms)									*/
/*------------------------------------------------------------------------------------------*/
void LongTimerSet ( enum AppLongTimers nTimer, unsigned int timerTime )
{
	appLongTimers[nTimer] = timerTime;
}
/*------------------------------------------------------------------------------------------*/
/*	LongTimerSetOff ( )		- Disattiva un long timer di applicazione						*/
/*	nTimer		= identificatore del timer													*/
/*------------------------------------------------------------------------------------------*/
void LongTimerSetOff ( enum AppLongTimers nTimer )
{
	appLongTimers[nTimer] = NONSIGN_32;
}
/*------------------------------------------------------------------------------------------*/
/* 	LongTimerIsExpired ( )	- Controllo scadenza di un long timer di applicazione		*/
/*	nTimer		= identificatore del timer													*/
/*------------------------------------------------------------------------------------------*/
bool LongTimerIsExpired ( enum AppLongTimers nTimer )
{
	return (bool)( appLongTimers[nTimer] == SCADUTO );
}
/*------------------------------------------------------------------------------------------*/
/*	LongTimerIsOff ( )	- Controlla se un long timer di applicazione è disattivato		*/
/*	nTimer		= identificatore del timer													*/
/*------------------------------------------------------------------------------------------*/
bool LongTimerIsOff ( enum AppLongTimers nTimer )
{
	return ( appLongTimers[nTimer] == NONSIGN_32 );
}
/*------------------------------------------------------------------------------------------*/
/*	LongTimerIsOn ( )	- Controlla se un long timer di applicazione è attivo			*/
/*	nTimer		= identificatore del timer													*/
/*------------------------------------------------------------------------------------------*/
bool LongTimerIsOn ( enum AppLongTimers nTimer )
{
	return ( appLongTimers[nTimer]>0 && appLongTimers[nTimer]!=NONSIGN_32);
}
/*------------------------------------------------------------------------------------------*/
/* 	LongTimerGetValue ( )	- Lettura valore di un timer di applicazione				*/
/*------------------------------------------------------------------------------------------*/
int32_t LongTimerGetValue ( enum AppLongTimers nTimer )
{
	return ( appLongTimers[nTimer] );
}

/*------------------------------------------------------------------------------------------*/
/* 	SysTimerSet ( ) 		- Inizalizza un timer di sistema								*/
/*	nTimer		= identificatore del timer													*/
/*	timerTime	= valore di inizializzazione (durata in ms)									*/
/*------------------------------------------------------------------------------------------*/
void SysTimerSet ( enum sysTimers nTimer, unsigned int timerTime )
{
	sysTimers[nTimer] = timerTime;
}
/*------------------------------------------------------------------------------------------*/
/*	SysTimerOff ( )			- Disattiva un timer di sistema									*/
/*	nTimer		= identificatore del timer													*/
/*------------------------------------------------------------------------------------------*/
void SysTimerOff ( enum sysTimers nTimer )
{
	sysTimers[nTimer] = NONSIGN_32;
}
/*------------------------------------------------------------------------------------------*/
/* 	isSysTimerExpired ( )	- Controllo scadenza di un timer di sistema						*/
/*	nTimer		= identificatore del timer													*/
/*------------------------------------------------------------------------------------------*/
bool isSysTimerExpired ( enum sysTimers nTimer )
{
	return (bool)( sysTimers[nTimer] == SCADUTO );
}
/*------------------------------------------------------------------------------------------*/
/*	isSysTimerOff ( )		- Controlla se un timer di sistema è disattivato				*/
/*	nTimer		= identificatore del timer													*/
/*------------------------------------------------------------------------------------------*/
bool isSysTimerOff ( enum sysTimers nTimer )
{
	return ( sysTimers[nTimer] == NONSIGN_32 );
}
/*------------------------------------------------------------------------------------------*/
/*	isSysTimerOn ( )		- Controlla se un timer di sistema è attivo						*/
/*	nTimer		= identificatore del timer													*/
/*------------------------------------------------------------------------------------------*/
bool isSysTimerOn ( enum sysTimers nTimer )
{
	return ( sysTimers[nTimer] > 0 );
}
/*------------------------------------------------------------------------------------------*/
/* 	SysTimerGetValue ( )	- Lettura valore di un timer di sistema							*/
/*------------------------------------------------------------------------------------------*/
int32_t SysTimerGetValue ( enum sysTimers nTimer )
{
	return ( sysTimers[nTimer] );
}


/*------------------------------------------------------------------------------------------*/
/*	LongTCGetValue ( )		- Lettura del long tick counter									*/
/*------------------------------------------------------------------------------------------*/
int64_t LongTCGetValue ( void )
{
	return ( nLongTickCount );
}
/*------------------------------------------------------------------------------------------*/
/* 	LongElapsedMs ( )		- Intervallo trascorso da un long tick di riferimento			*/
/*------------------------------------------------------------------------------------------*/
int64_t LongElapsedMs ( int64_t refTickCount )
{
	return ( nLongTickCount - refTickCount );
}
/*------------------------------------------------------------------------------------------*/
/*	LIB_TimerTickHandler ( )- Funzione di aggiornamento timers							*/
/*------------------------------------------------------------------------------------------*/
inline void LIB_TimerTickHandler(void)
{
	nLongTickCount ++;
	// Ciclo di decremento dei timers
	for ( int i = 0; i < APP_MAX_TIMER; i ++ )
	{
		if ( appTimers [ i ] > 0 ) appTimers [ i ] --;
	}
	for ( int i = 0; i < SYS_MAX_TIMER; i ++ )
	{
		if ( sysTimers [ i ] > 0 ) sysTimers [ i ] --;
	}
	//	Gestione long timers
	nLongTimerTick ++;
	if ( nLongTimerTick == 100 )
	{
		nLongTimerTick = 0;
		for ( int i = 0; i < APP_MAX_LTIMER; i ++ )
		{
			if ( appLongTimers [ i ] > 0 ) appLongTimers [ i ] --;
		}
	}
}

