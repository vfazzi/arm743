/********************************************************************************************/
/* 	LIB_Timers.h - Gestione timers	 														*/

/********************************************************************************************/


#ifndef TIMER_H
#define TIMER_H

#include <stdbool.h>
#include "stdint.h"
#include "SYS_Include.h"

#define CLICK		1	// 1mS
#define SCADUTO		0
#define NONSIGN_32	(unsigned int)0xFFFFFFFF
#define mSEC(x)			(x/CLICK)
#define SEC(x)			(x*1000/CLICK)
#define SYSTIMER_PERIOD		1000
//#define TIMEOUT_POLLING		Msec80
#define DEFINED_TIMERS		FTIMER


//	Funzioni aggiunte in revisione RTOS
// 	Controllo timer 1 ms
void 	LIB_TimerInit			( void );
void 	LIB_TimerTickHandler	( void );

void 	ShortTimerSet				( enum AppTimers nTimer, unsigned int timerTime );
void 	ShortTimerSetOff				( enum AppTimers nTimer );
bool 	ShortTimerIsExpired 		( enum AppTimers nTimer );
bool 	ShortTimerIsOff		 	( enum AppTimers nTimer );
bool 	ShortTimerIsOn		 	( enum AppTimers nTimer );
int32_t ShortTimerGetValue 		( enum AppTimers nTimer );

void 	LongTimerSet			( enum AppLongTimers nTimer, unsigned int timerTime );
void 	LongTimerSetOff			( enum AppLongTimers nTimer );
bool 	LongTimerIsExpired 	( enum AppLongTimers nTimer );
bool 	LongTimerIsOff		( enum AppLongTimers nTimer );
bool 	LongTimerIsOn		( enum AppLongTimers nTimer );
int32_t LongTimerGetValue 	( enum AppLongTimers nTimer );

void 	SysTimerSet				( enum sysTimers nTimer, unsigned int timerTime );
void 	SysTimerOff				( enum sysTimers nTimer );
bool 	isSysTimerExpired 		( enum sysTimers nTimer );
bool 	isSysTimerOff		 	( enum sysTimers nTimer );
bool 	isSysTimerOn		 	( enum sysTimers nTimer );
int32_t SysTimerGetValue 		( enum sysTimers nTimer );


int64_t LongTCGetValue 			( void );
int64_t LongElapsedMs 			( int64_t refTickCount );

extern volatile uint64_t nLongTickCount;

#endif
