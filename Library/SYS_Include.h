/********************************************************************************************/
/*	SYS_Include.h		- Dichiarazioni ausiliarie di sistema								*/

/********************************************************************************************/
#include "stdint.h"
#include <stdbool.h>
#include <stdint.h>
#include "string.h"
#include "ctype.h"
#include <limits.h>

#include "APP_CodeConfig.h"
#include "stm32h7xx_hal.h"
#include "cmsis_os.h"
#include "adc.h"
#include "comp.h"
#include "fdcan.h"
#include "lwip.h"
#include "quadspi.h"
#include "rtc.h"
#include "spi.h"
#include "usart.h"
#include "usb_device.h"
#include "gpio.h"
#include <NET485/NET485_Include.h>
#include <NET485/stm32h7xx_ll_usart.h>
#include "LIB_USB.h"
#include "LIB_Term.h"
#include "usbd_cdc.h"
#include "usbd_cdc_if.h"
#include "qspi_mx25L12833.h"
#include "Lib_Timers.h"
#include "stm32h7xx_hal_uart.h"
#include "canbus.h"
#include "LIB_Can.h"
#include "LIB_IO.h"
#include "WS_Include.h"
#include "TCP_server.h"
#include "WEB_Server.h"
#include "SYS_Externals.h"
#include "LIB_DateTime.h"
#include "FWUpdater.h"

#ifndef	__SYS_INCLUDE_DEFINED
#define __SYS_INCLUDE_DEFINED


/*------------------------------------------------------------------------------------------*/
/*	Strutture e tipi di dati																*/
/*------------------------------------------------------------------------------------------*/



/*------------------------------------------------------------------------------------------*/
/*	Funzioni di timer e loop di sistema														*/
/*------------------------------------------------------------------------------------------*/
void 	SYS_SystemTimerTick ( void );
void 	SYS_SystemLoopInit ( void );
void 	SYS_SystemLoopCallback ( void );

/*------------------------------------------------------------------------------------------*/
/*	Macro controllo I/O																		*/
/*------------------------------------------------------------------------------------------*/
// 	Uscita RSTO
#define RSTO_ON				HAL_GPIO_WritePin	(RSTO_GPIO_Port, RSTO_Pin, GPIO_PIN_RESET)
#define RSTO_OFF			HAL_GPIO_WritePin	(RSTO_GPIO_Port, RSTO_Pin, GPIO_PIN_SET)
#define RSTO_FALLING_EDGE	RSTO_ON;RSTO_ON;RSTO_OFF
// Uscita PLO
#define PLO_OFF				HAL_GPIO_WritePin	(PLO_GPIO_Port, PLO_Pin, GPIO_PIN_RESET)
#define PLO_ON				HAL_GPIO_WritePin	(PLO_GPIO_Port, PLO_Pin, GPIO_PIN_SET)
#define PLO_LATCH			PLO_OFF;PLO_ON
//	Uscita PLI
#define PLI_OFF				HAL_GPIO_WritePin	(PLI_GPIO_Port, PLI_Pin, GPIO_PIN_RESET)
#define PLI_ON				HAL_GPIO_WritePin	(PLI_GPIO_Port, PLI_Pin, GPIO_PIN_SET)
#define PLI_LATCH			PLI_OFF;PLI_OFF;PLI_ON
//	DE-RE esterno
#define DEREEXT_ON 			HAL_GPIO_WritePin(DERE_EXT_GPIO_Port, DERE_EXT_Pin, GPIO_PIN_SET);
#define DEREEXT_OFF 		HAL_GPIO_WritePin(DERE_EXT_GPIO_Port, DERE_EXT_Pin, GPIO_PIN_RESET);
//	DE-RE esterno
#define DEREINT_ON 			HAL_GPIO_WritePin(DERE_INT_GPIO_Port, DERE_INT_Pin, GPIO_PIN_SET);
#define DEREINT_OFF 		HAL_GPIO_WritePin(DERE_INT_GPIO_Port, DERE_INT_Pin, GPIO_PIN_RESET);
// Uscita EN_OUT
#define EN_OUT_OFF	  		HAL_GPIO_WritePin(EN_OUT_GPIO_Port, EN_OUT_Pin, GPIO_PIN_SET)
#define EN_OUT_ON			HAL_GPIO_WritePin(EN_OUT_GPIO_Port, EN_OUT_Pin, GPIO_PIN_RESET)

/*------------------------------------------------------------------------------------------*/
/*	Varie																					*/
/*------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------*/
/* 	Definzione dei timer di sistema interni													*/
/*	Elencare nella enum i nomi dei timers dell'applicazione	e chiudere con FTIMER			*/
/* 	P.es. definire due tumers a 32 bit chiamati TIMER_0 e TIMER_!							*/
/* 	enum _sysTimers {																		*/
/*		TIMER_0,																			*/
/*		TIMER_1,																			*/
/*		SYS_MAX_TIMER																		*/
/*	};																						*/
/*------------------------------------------------------------------------------------------*/
enum sysTimers
{
	#ifdef	CON_USB_BUFFERED
		SYS_TIMER_USB_POLLING,
	#endif
	#ifdef ASCII_TERM_ENABLED
		SYS_TIMER_TERM_USB,
	#endif
	#ifdef USART6_INT_485_ENABLED
		SYS_TIMER_485INT,
		SYS_TIMER_485INT_RXTIMEOUT,
	#endif
	#ifdef CAN1_ENABLED
	#endif
		SYS_MAX_TIMER
};

#endif

/********************************************************************************************/
