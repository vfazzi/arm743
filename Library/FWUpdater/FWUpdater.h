/********************************************************************************************/
/*	FWUpdater.h					- Sistema di aggiornamento firmware via rete				*/

/********************************************************************************************/

#ifndef	__FWU_UPDATER_DEFINED__
#define __FWU_UPDATER_DEFINED__

//	Struttura messaggio
typedef struct
{
	uint32_t	structSize;				//	Dimensione in bytes della struttura
	uint32_t	structVersion;			//	Versione
	uint8_t		pwd [ 16];				//	Password/key
	uint32_t	cmd;					//	Comando
	uint32_t	dwParam [4];			//	Parametri
	uint32_t	dataSize;				//	Dimensione blocco dati
	uint32_t	dataAddress;			//	Indirizzo di destinazione dati
	uint64_t	dataChecksum;			//	Checksum blocco dati
	uint8_t 	dataBlock [512];		//	Dati
	uint64_t 	EOP;					//	End-Of-Packet
	uint64_t	packChecksum;
} FWU_Message;
#endif

//	Costanti messaggio (identificatore di versione e codice di accesso)
#define		FWU_VERSION		100
#define		FWU_EOP			0x55AA55AA55AAAA55
#define		FWU_PASSWORD	"01234$$%%&&56789"

//	Codici dei messaggi
#define		FWU_CMD_REQ_INFO	1
#define		FWU_MSG_INFO_ACK	2
#define		FWU_CMD_DATA_BLOCK	3
#define		FWU_MSG_DATA_ACK	4
#define		FWU_MSG_PWD_ERROR	5
#define		FWU_MSG_CLEAR_FLASH	6
#define		FWU_MSG_CLEAR_ACK	7
#define		FWU_MSG_CHECKUPLOAD	8
#define		FWU_MSG_CHECK_ACK	9
#define		FWU_MSG_CLEAR_ERR	10
#define		FWU_MSG_CHECK_ERR	11
#define		FWU_MSG_DATA_ERR	12
#define		FWU_MSG_BANK_SWITCH	13
#define		FWU_MSG_SWITCH_ACK	14
#define		FWU_MSG_SWITCH_ERR	15
#define		FWU_MSG_BREAKUPLOAD	16
#define		FWU_MSG_BREAK_ACK	17

//	Funzioni del modulo
void FWU_StartSystem ( void );
void FWU_RcvData ( unsigned char* pData, uint16_t dataLen );
void FWU_TimerTick ( void );

/********************************************************************************************/
