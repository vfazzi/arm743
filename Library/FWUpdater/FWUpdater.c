/********************************************************************************************/
/*	FWUpdater.c					- Sistema di aggiornamento firmware via rete				*/

/********************************************************************************************/
// Include di sistema e applicazione
#include "SYS_Include.h"
#include "APP_Include.h"

TCPServerHandle	p_fwuServer;			//	Server TCP
FWU_Message		p_fwuRxMsg;				//	Messaggio ricevuto
FWU_Message		p_fwuTxMsg;				//	Messaggio trasmesso
unsigned char* 	p_rcvPtr;				//	DecOder - puntatore di scrittura dati
unsigned int 	p_rcvCount;				//	Decoder - contatore caratteri ricevuti
unsigned char	p_rcvStatus = 0;		//	Decoder - Stato interno
bool			p_rcvFlag;				//	Decoder - Messaggio ricevuto
int				p_fwuTimeoutTimer = -1;	//	Controllo timeout
const char 		p_firmwareMarker [] = "FW_524_A55%-$722i_SIGNATURE";
//	Struttura di definizione del thread
osThreadId_t fwuSystemTaskHandle;;
const osThreadAttr_t fwuSystemTask_attributes = {
  .name = "fwuSystemTask",
  .stack_size = 256 * 4,
  .priority = (osPriority_t) osPriorityNormal,
};


//	Funzioni interne del modulo
static void FWU_SystemTask(void *argument);
static bool FWU_msgValidate ( FWU_Message* msg );
static void FWU_msgInit ( FWU_Message* msg );
static void FWU_msgCalcChecksum ( FWU_Message* msg );
static void FWU_msgSend ( FWU_Message* msg );
static void FWU_msgInfo ( FWU_Message *msg );
static bool FWU_ClearUpperMemory ( void );
static bool FWU_FlashBlock ( FWU_Message *msg );
static bool FWU_VerifyData ( FWU_Message *msg );
static bool FWU_SwitchFlashBank ( void );
static bool FWU_VerifiyFw ( void );

/********************************************************************************************/
/*	Funzioni del modulo																		*/
/*																							*/
/*	APP_TcpStartAppServer ( )		- Avvia i server TCP specifici dell'applicazione		*/
/*	FWU_RcvData ( )					- Ricezione dati remoti e decodifica pacchetto			*/
/*	FWU_TimerTick ( )				- Timer interni	(chiamata dal timer di sistema)			*/
/********************************************************************************************/
/*------------------------------------------------------------------------------------------*/
/*	APP_TcpStartAppServer ( )		- Avvia i server TCP specifici dell'applicazione		*/
/*	N.B. il server HTTP e il server di programmazione remota sono già attivati nel SYS		*/
/*------------------------------------------------------------------------------------------*/
void FWU_StartSystem ( void )
{
	//	Timeout timer
	p_fwuTimeoutTimer = -1;
	//	Serve per obbligare il compilatore ad inserire nel file la stringa
	strcpy ( (char*)p_fwuRxMsg.dataAddress, p_firmwareMarker );
	//	Attiva il server
	p_fwuServer.connPort = FWUPDATE_PORT;
	p_fwuServer.txBufferSize = 1024;
	SRV_StartServer (&p_fwuServer);
	//	Attiva il thread del sistema
	fwuSystemTaskHandle = osThreadNew(FWU_SystemTask, NULL, &fwuSystemTask_attributes);
}
/*------------------------------------------------------------------------------------------*/
/*	FWU_RcvData ( )					- Ricezione dati remoti e decodifica pacchetto			*/
/*------------------------------------------------------------------------------------------*/
void FWU_RcvData ( unsigned char* pData, uint16_t dataLen )
{

	//	Analisi flusso dati ricevuti
	for ( int i = 0; i < dataLen; i ++ )
	{
		unsigned char b = pData [i];
		//	Macchina a stati decoder
		switch ( p_rcvStatus )
		{
			//	Stato di attesa
			case 0:
				if ( b == STX )
				{
					p_rcvFlag = false;
					p_rcvStatus = 1;
					p_rcvCount = 0;
					memset ( &p_fwuRxMsg, 0, sizeof ( FWU_Message ) );
					p_rcvPtr = (unsigned char*)&p_fwuRxMsg;
				}
			break;
			//	Stato di ricezione
			case 1:
				//	Eliminatore DLE
				if ( b == DLE )
				{
					p_rcvStatus = 10;
				}
				//	ETX - Fine messaggio
				else if ( b == ETX )
				{
					//	Controllo correttezza messaggio
					if ( FWU_msgValidate ( &p_fwuRxMsg ) )
					{
						//	Messaggio valido - Blocca la ricezione fino a che non è stato
						//	processato dal thread apposito
						p_rcvFlag = true;
						p_rcvStatus = 100;
					}
					//	Ripristino ricezione
					else p_rcvStatus = 0;
				}
				//	Errore
				else if ( b == STX )
				{
					p_rcvStatus = 0;
					p_rcvCount = 0;
				}
				//	Salva carattere generico
				else
				{
					*p_rcvPtr = b;
					p_rcvPtr ++;
					p_rcvCount ++;
				}
			break;
			//	Carattere successivo al DLE
			case 10:
				//	Accetta solo STX, ETX o DLE
				if ( ( b == STX ) || ( b == DLE ) || ( b == ETX ) )
				{
					*p_rcvPtr = b;
					p_rcvPtr ++;
					p_rcvCount ++;
					p_rcvStatus = 1;
				}
				//	Altri caratteri = ERRORE di sequenza
				else
				{
					p_rcvStatus = 0;
					p_rcvCount = 0;
				}
			break;
		}
		//	Controllo buffer overrun
		if ( p_rcvCount > sizeof ( FWU_Message ) )
		{
			p_rcvStatus = 0;
			p_rcvCount = 0;
		}
	}
}
/*------------------------------------------------------------------------------------------*/
/*	FWU_TimerTick ( )				- Timer interni	(chiamata dal timer di sistema)			*/
/*------------------------------------------------------------------------------------------*/
void FWU_TimerTick ( void )
{
	//	Decrementa il timer di controllo della ricezione dei pacchetti dati
	if ( p_fwuTimeoutTimer > 0 )
	{
		p_fwuTimeoutTimer --;
	}
	//	Timeout - Comunicazione interrotta
	else if ( p_fwuTimeoutTimer == 0 )
	{
		//	Effettua un reset completo perchè il server TCP resta bloccato
		#ifdef FWUPDATE_DEBUG_MSG
		printf ( "Reset per timeout del firmware updater\r\n");
		#endif
		FWU_ClearUpperMemory ( );
		SYSTEM_RESET;
	}
}

/********************************************************************************************/
/*	Funzioni private																		*/
/*																							*/
/*	FWU_SystemTask ( )				- Thread principale del sistema di aggiornamento		*/
/*	FWU_VerifiyFw ( )				- Verifica firma del codice caricato					*/
/*	FWU_SwitchFlashBank ( )			- Switch banchi di flash								*/
/*	FWU_VerifyData ( )				- Verifica finale dei dati caricati						*/
/*	FWU_FlashBlock ( )				- Scrive un blocco nella flash bank II					*/
/*	FWU_ClearUpperMemory ( )		- Cancella la memoria da 0x08100000 a 0x081FFFFF		*/
/*	FWU_msgInfo ( )					- Messaggio INFO										*/
/*	FWU_msgSend ( )					- Codifica e trasmissione di un messaggio				*/
/*	FWU_msgInit ( )					- Inizializza la struttura di un messaggio				*/
/*	FWU_msgCalcChecksum ( )			- Calcola le checksum di un messaggio					*/
/*	FWU_msgValidate ( )				- Verifica la validità di un messaggio					*/
/********************************************************************************************/
/*------------------------------------------------------------------------------------------*/
/*	FWU_SystemTask ( )				- Thread principale del sistema di aggiornamento		*/
/*------------------------------------------------------------------------------------------*/
static void FWU_SystemTask ( void* arg )
{
	while ( 1 )
	{
		//	Controlla se ha ricevuto un messaggio
		if ( p_rcvFlag )
		{
			//	Inizializza il messaggio di risposta
			FWU_msgInit ( &p_fwuTxMsg);
			//	Controllo password
			if ( ! memcmp ( p_fwuRxMsg.pwd, FWU_PASSWORD, 16 ) )
			{
				//	Aggiunge un ritardo nella risposta per evitare intasamenti della rete
				osDelay ( 50);
				//	Gestione comandi
				#ifdef FWUPDATE_DEBUG_MSG
					printf ( "Ricevuto messaggio - Codice %d\r\n", p_fwuRxMsg.cmd );
				#endif
				switch ( p_fwuRxMsg.cmd )
				{
					//	Richiesta info scheda e connessione
					case FWU_CMD_REQ_INFO:
						FWU_msgInfo ( &p_fwuTxMsg );
						FWU_msgSend ( &p_fwuTxMsg );
					break;
					//	Blocco dati
					case FWU_CMD_DATA_BLOCK:
						p_fwuTimeoutTimer = FWUPDATE_TIMEOUT_MS;
						#ifdef FWUPDATE_DEBUG_MSG
						printf ( "BLK ADDR=%lx SZ=%ld\r\n", p_fwuRxMsg.dataAddress, p_fwuRxMsg.dataSize );
						printf ( "BLK Num.%ld su %ld\r\n", p_fwuRxMsg.dwParam[0], p_fwuRxMsg.dwParam[1]);
						#endif
						p_fwuTxMsg.cmd = FWU_FlashBlock ( &p_fwuRxMsg ) ? FWU_MSG_DATA_ACK : FWU_MSG_DATA_ERR;
						p_fwuTxMsg.dwParam[0] = p_fwuRxMsg.dwParam[0];
						FWU_msgSend ( &p_fwuTxMsg );
					break;
					//	Comando cancellazione flash
					case FWU_MSG_CLEAR_FLASH:
						p_fwuTxMsg.cmd = FWU_ClearUpperMemory ( ) ? FWU_MSG_CLEAR_ACK : FWU_MSG_CLEAR_ERR;
						FWU_msgSend ( &p_fwuTxMsg );
					break;
					//	Comando verifica dati caricati
					case FWU_MSG_CHECKUPLOAD:
						p_fwuTimeoutTimer = -1;
						p_fwuTxMsg.dwParam[0] = p_fwuRxMsg.dwParam[0];
						p_fwuTxMsg.cmd = FWU_VerifyData ( &p_fwuRxMsg ) ? FWU_MSG_CHECK_ACK : FWU_MSG_CHECK_ERR;
						//	Se il firmware non è verificato, lo cancella
						if ( p_fwuTxMsg.cmd == 0 )
						{
							FWU_ClearUpperMemory ( );
						}
						FWU_msgSend ( &p_fwuTxMsg );
					break;
					//	Comando bank switch
					case FWU_MSG_BANK_SWITCH:
						if ( FWU_VerifiyFw ( ) )
						{
							p_fwuTxMsg.cmd = FWU_MSG_SWITCH_ACK;
							FWU_msgSend ( &p_fwuTxMsg );
							osDelay ( 50 );
							FWU_SwitchFlashBank ( );
						}
						else
						{
							p_fwuTxMsg.cmd = FWU_MSG_SWITCH_ERR;
							FWU_msgSend ( &p_fwuTxMsg );
						}
					break;
					//	Comando BREAK UPLOAD
					case FWU_MSG_BREAKUPLOAD:
						p_fwuTimeoutTimer = -1;
						p_fwuTxMsg.cmd = FWU_MSG_BREAK_ACK;
						FWU_msgSend ( &p_fwuTxMsg );
						FWU_ClearUpperMemory ( );
						#ifdef FWUPDATE_DEBUG_MSG
						printf ( "Interruzione upload\r\n");
						#endif
					break;
				}
			}
			//	Chiave di accesso errata
			else
			{
				osDelay ( 50);
				p_fwuTxMsg.cmd = FWU_MSG_PWD_ERROR;
				FWU_msgSend ( &p_fwuTxMsg );
			}
			//	Abilita la ricezione di un nuovo messaggio
			memset ( &p_fwuRxMsg, 0, sizeof(FWU_Message) );
			p_rcvFlag = false;
			p_rcvStatus = 0;
		}
		osDelay ( 1 );
	}
}
/*------------------------------------------------------------------------------------------*/
/*	FWU_VerifiyFw ( )				- Verifica firma del codice caricato					*/
/*------------------------------------------------------------------------------------------*/
static inline bool FWU_VerifiyFw ( void )
{
	//	Verifica la firma del contenuto della flash
	char*	ptr = (char*)( 0x08100000);
	for ( int i = 0; i < 0x0FFFE0; i ++ )
	{
		if ( *ptr == p_firmwareMarker [0] )
		{
			if ( strcmp ( ptr, p_firmwareMarker ) == 0 )
			{
				return ( true );
			}
		}
		ptr ++;
	}
	//	Firma non trovata
	return ( false );
}
/*------------------------------------------------------------------------------------------*/
/*	FWU_SwitchFlashBank ( )			- Switch banchi di flash								*/
/*------------------------------------------------------------------------------------------*/
static inline bool FWU_SwitchFlashBank ( void )
{
	FLASH_OBProgramInitTypeDef OBInit;

	//	Abilita l'accesso agli option bytes
	HAL_FLASH_OB_Unlock();
	//	Legge la configurazione corrente
	HAL_FLASHEx_OBGetConfig(&OBInit);
    OBInit.Banks     = FLASH_BANK_1;
    HAL_FLASHEx_OBGetConfig(&OBInit);
    //	Scambia il banco attivo: se 1 passa a 2, e viceversa
    if ((OBInit.USERConfig & OB_SWAP_BANK_ENABLE) == OB_SWAP_BANK_DISABLE)
    {
    	//	Da banco 1 a banco 2
    	OBInit.OptionType = OPTIONBYTE_USER;
    	OBInit.USERType   = OB_USER_SWAP_BANK;
    	OBInit.USERConfig = OB_SWAP_BANK_ENABLE;
    }
    else
    {
    	//	Da banco 2 a banco 1
    	OBInit.OptionType = OPTIONBYTE_USER;
    	OBInit.USERType = OB_USER_SWAP_BANK;
    	OBInit.USERConfig = OB_SWAP_BANK_DISABLE;
    }
	//	Salva l'option bit
    HAL_FLASHEx_OBProgram(&OBInit);
	HAL_FLASH_OB_Launch();
	//	Invalida la cache del processore e fa il reset della scheda
	SCB_InvalidateICache();
	HAL_FLASH_OB_Lock();
	HAL_NVIC_SystemReset ( );
	return ( true );
}
/*------------------------------------------------------------------------------------------*/
/*	FWU_VerifyData ( )				- Verifica finale dei dati caricati						*/
/*------------------------------------------------------------------------------------------*/
static inline bool FWU_VerifyData ( FWU_Message *msg )
{
	bool rc = true;
	//	Calcolo checksum globale
	uint64_t 	chk = 0;
	uint8_t*	ptr = (uint8_t*)( 0x08100000);
	uint64_t	rChk = *((uint64_t*)&msg->dataBlock[0]);
	for ( int32_t i = 0; i < msg->dataSize; i ++ )
	{
		chk += *ptr;
		ptr ++;
	}
	if ( chk != rChk) rc = false;
	//	Eventualmente aggiungere il controllo delle checksum parziali

	return ( rc );
}
/*------------------------------------------------------------------------------------------*/
/*	FWU_FlashBlock ( )				- Scrive un blocco nella flash bank II					*/
/*------------------------------------------------------------------------------------------*/
static inline bool FWU_FlashBlock ( FWU_Message *msg )
{
	bool		rc = true;
	uint32_t	destAddr = 0x08100000 + msg->dataAddress;
	uint32_t	srcAddr = (uint32_t)&msg->dataBlock [0];
	//	Scrive i dati nella flash
	HAL_FLASH_Unlock ( );
	for ( int i = 0; i < 512; i += 32 )
	{
		if ( HAL_FLASH_Program ( FLASH_TYPEPROGRAM_FLASHWORD, destAddr, srcAddr ) != HAL_OK )
		{
			rc = false;
		}
		destAddr += 32;
		srcAddr += 32;
	}
	HAL_FLASH_Lock ( );
	//	Verifica la scrittura
	uint8_t*	pFlash = (uint8_t*)0x08100000 + msg->dataAddress;
	for ( int i = 0; i < 512; i ++ )
	{
		if ( msg->dataBlock[i] != *pFlash ) rc = false;
		pFlash ++;
	}
	return ( rc );
}
/*------------------------------------------------------------------------------------------*/
/*	FWU_ClearUpperMemory ( )		- Cancella la memoria da 0x08100000 a 0x081FFFFF		*/
/*------------------------------------------------------------------------------------------*/
static inline bool FWU_ClearUpperMemory ( void )
{
	FLASH_EraseInitTypeDef f;
	uint32_t sectErr = 0;
	bool rc = false;
	//	Cancella 8 settori (128Kx8 = 1MB) del banco 2 della flash
	f.TypeErase = FLASH_TYPEERASE_MASSERASE;
	f.Banks = FLASH_BANK_2;
	f.NbSectors = 8;
	f.Sector = FLASH_SECTOR_0;
	f.VoltageRange = FLASH_VOLTAGE_RANGE_3;
	HAL_FLASH_Unlock ( );
	rc = ( HAL_FLASHEx_Erase ( &f, &sectErr ) == HAL_OK );
	HAL_FLASH_Lock ( );
	return ( rc );
}
/*------------------------------------------------------------------------------------------*/
/*	FWU_msgInfo ( )					- Messaggio INFO										*/
/*------------------------------------------------------------------------------------------*/
static inline void FWU_msgInfo ( FWU_Message *msg )
{
	//	Prepara il messaggio di risposta
	msg->cmd = FWU_MSG_INFO_ACK;
	//	Dati
	strcpy ( (char*)msg->dataBlock, _CODE_PROG );
	strcpy ( (char*)msg->dataBlock + 16, sysCfg.m_boardID );
	strcpy ( (char*)msg->dataBlock + 32, __DATE__ );
	strcpy ( (char*)msg->dataBlock + 48, __TIME__ );
	strcpy ( (char*)msg->dataBlock + 64, "SAIMA Sicurezza S.p.a. Loc.Indicatore Arezzo");
	msg->dataSize = 128;
}
/*------------------------------------------------------------------------------------------*/
/*	FWU_msgSend ( )					- Codifica e trasmissione di un messaggio				*/
/*------------------------------------------------------------------------------------------*/
static void FWU_msgSend ( FWU_Message* msg )
{
	//	Prepara la checksum del messaggio
	FWU_msgCalcChecksum ( msg );
	//	Codifica e inserisce il messaggio nel buffer di trasmissione
	uint8_t* pData = (uint8_t*)msg;
	//	STX
	SRV_BufferedSendByte ( &p_fwuServer, STX, false );
	//	Dati
	for ( int i = 0; i < sizeof ( FWU_Message ); i ++, pData ++ )
	{
		uint8_t b = *pData;
		//	Inserimento DLE
		if ( ( b == STX ) || ( b == DLE ) || ( b == ETX ) )
		{
			SRV_BufferedSendByte ( &p_fwuServer, DLE, false );
		}
		SRV_BufferedSendByte ( &p_fwuServer, b, false );
	}
	//	ETX e trasmissione
	SRV_BufferedSendByte ( &p_fwuServer, ETX, false );
	SRV_FlushBuffer ( &p_fwuServer );
}
/*------------------------------------------------------------------------------------------*/
/*	FWU_msgInit ( )					- Inizializza la struttura di un messaggio				*/
/*------------------------------------------------------------------------------------------*/
static void FWU_msgInit ( FWU_Message* msg )
{
	//	Inizializza la struttura a 0
	memset ( msg, 0, sizeof(FWU_Message));
	//	Campi fissi
	memcpy ( msg->pwd, FWU_PASSWORD, 16 );
	msg->structSize = sizeof(FWU_Message);
	msg->structVersion = FWU_VERSION;
	msg->EOP = FWU_EOP;
}
/*------------------------------------------------------------------------------------------*/
/*	FWU_msgCalcChecksum ( )			- Calcola le checksum di un messaggio					*/
/*------------------------------------------------------------------------------------------*/
static void FWU_msgCalcChecksum ( FWU_Message* msg )
{
	msg->dataChecksum = 0;
	msg->packChecksum = 0;
	uint8_t* pPtr = (uint8_t*)msg;
	//	Calcolo checksum dati
	for ( int i = 0; i < 512; i ++ ) msg->dataChecksum += msg->dataBlock [i];
	//	Calcolo checksum pacchetto
	for ( int i = 0; i < sizeof(FWU_Message) - 8; i ++, pPtr ++ ) msg->packChecksum += *pPtr;
}
/*------------------------------------------------------------------------------------------*/
/*	FWU_msgValidate ( )				- Verifica la validità di un messaggio					*/
/*------------------------------------------------------------------------------------------*/
static bool FWU_msgValidate ( FWU_Message* msg )
{
	//	Controllo dimensione della struttura e dei dati ricevuti
	if ( msg->structSize == sizeof ( FWU_Message ) && ( p_rcvCount  == sizeof ( FWU_Message ) ))
	{
		//	Versione della struttura
		if ( msg->structVersion == FWU_VERSION )
		{
			//	EOP (marker di fine pacchetto)
			if ( msg->EOP == FWU_EOP )
			{
				uint64_t calcChecksum = 0;
				uint8_t* pScan = (uint8_t*)msg;
				//	Checksum globale
				for ( int i = 0; i < sizeof ( FWU_Message ) - 8; i ++, pScan ++ )
				{
					calcChecksum += *pScan;
				}
				if ( calcChecksum == msg->packChecksum )
				{
					//	Controllo checksum dati
					calcChecksum = 0;
					for ( int i = 0; i < 512; i ++ )
					{
						calcChecksum += msg->dataBlock [i];
					}
					if ( calcChecksum == msg->dataChecksum )
					{
						return ( true );
					}
				}
			}
		}
	}
	return ( false );
}

/********************************************************************************************/
