/********************************************************************************************/
/*	LIB_DateTime.c				- Gestione data e ora										*/

/********************************************************************************************/

#include "SYS_Include.h"
#include "APP_Include.h"

/********************************************************************************************/
/*	Funzioni del modulo																		*/
/*																							*/
/*	TM_GetSystemDate ( )			-	Lettura data di sistema da RTC						*/
/*	TM_GetSystemTime ( )			-	Lettura ora di sistema da RTC						*/
/*	TM_SetSystemDate ( )			-	Impostazione data di sistema in RTC					*/
/*	TM_SetSystemTime ( )			-	Impostazione ora di sistema in RTC					*/
/*	TM_GetSystemDateString ( )		- Data di sistema in formato stringa GG-MM-AAAA			*/
/*	TM_GetSystemTimeString ( )		- Ora di sistema in formato stringa hh:mm:ss			*/
/*	TM_SetSystemDateString ( )		- Imposta la data di sistema da stringa GG-MM-AAAA		*/
/*	TM_SetSystemTimeString ( )		- Imposta l'ora di sistema da stringa hh:mm:ss			*/
/********************************************************************************************/
/*------------------------------------------------------------------------------------------*/
/*	TM_GetSystemDate ( )			-	Lettura data di sistema da RTC						*/
/*	dt		= struttura di ritorno dati														*/
/*	TM_GetSystemTimeString ( )		- Ora di sistema in formato stringa hh:mm:ss			*/
/*------------------------------------------------------------------------------------------*/
void TM_GetSystemDate ( sysDate* dt )
{
	//	Legge la data
	RTC_DateTypeDef GetDate;
	HAL_RTC_GetDate(&hrtc,&GetDate,RTC_FORMAT_BIN);
	dt->dt_year = GetDate.Year + 2000;
	dt->dt_month = GetDate.Month;
	dt->dt_day = GetDate.Date;
}
/*------------------------------------------------------------------------------------------*/
/*	TM_GetSystemTime ( )			-	Lettura ora di sistema da RTC						*/
/*	tm		= struttura di ritorno dati														*/
/*------------------------------------------------------------------------------------------*/
void TM_GetSystemTime ( sysTime* tm )
{
	RTC_TimeTypeDef GetTime;
	HAL_RTC_GetTime(&hrtc,&GetTime,RTC_FORMAT_BIN);
	RTC_DateTypeDef GetDate;
	HAL_RTC_GetDate(&hrtc,&GetDate,RTC_FORMAT_BIN);
	tm->tm_hour = GetTime.Hours;
	tm->tm_min = GetTime.Minutes;
	tm->tm_Sec = GetTime.Seconds;
}
/*------------------------------------------------------------------------------------------*/
/*	TM_SetSystemDate ( )			-	Impostazione data di sistema in RTC					*/
/*	dt		= struttura nuova data															*/
/*------------------------------------------------------------------------------------------*/
bool TM_SetSystemDate ( sysDate* dt )
{
	RTC_DateTypeDef sDate = {0};
	sDate.Date = dt->dt_day;
	sDate.Month = dt->dt_month;
	sDate.Year = dt->dt_year - 2000;
	sDate.WeekDay = 1;
	return ((bool)( HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BIN) == HAL_OK ));
}
/*------------------------------------------------------------------------------------------*/
/*	TM_SetSystemTime ( )			-	Impostazione ora di sistema in RTC					*/
/*	tm		= struttura nuovo orario														*/
/*------------------------------------------------------------------------------------------*/
bool TM_SetSystemTime ( sysTime* tm )
{
	RTC_TimeTypeDef sTime = {0};
	sTime.Hours = tm->tm_hour;
	sTime.Minutes = tm->tm_min;
	sTime.Seconds = tm->tm_Sec;
	sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
	sTime.StoreOperation = RTC_STOREOPERATION_SET;
	return ((bool)( HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BIN) == HAL_OK ));
}
/*------------------------------------------------------------------------------------------*/
/*	TM_GetSystemDateString ( )		- Data di sistema in formato stringa GG-MM-AAAA			*/
/*	strDate	= buffer di ritorno (min.11 char)												*/
/*------------------------------------------------------------------------------------------*/
void TM_GetSystemDateString ( char* strDate )
{
	sysDate	dt;
	TM_GetSystemDate ( &dt );
	sprintf(strDate, "%02d-%02d-%04d", dt.dt_day, dt.dt_month, dt.dt_year );
}
/*------------------------------------------------------------------------------------------*/
/*	TM_GetSystemTimeString ( )		- Ora di sistema in formato stringa hh:mm:ss			*/
/*	strTime	= buffer di ritorno (min.9 char)												*/
/*------------------------------------------------------------------------------------------*/
void TM_GetSystemTimeString ( char* strTime )
{
	sysTime			tm;
	TM_GetSystemTime ( &tm );
	sprintf(strTime, "%02d:%02d:%02d", tm.tm_hour, tm.tm_min, tm.tm_Sec );
}
/*------------------------------------------------------------------------------------------*/
/*	TM_SetSystemDateString ( )		- Imposta la data di sistema da stringa GG-MM-AAAA		*/
/*	strDate	= data																			*/
/*------------------------------------------------------------------------------------------*/
bool TM_SetSystemDateString ( char* strDate )
{
	if ( strlen ( strDate ) == 10 )
	{
		sysDate date;
		sscanf ( strDate, "%02d-%02d-%04d", &date.dt_day, &date.dt_month, &date.dt_year );
		return ( APP_SetSystemDate ( &date ) );
	}
	return ( false );
}
/*------------------------------------------------------------------------------------------*/
/*	TM_SetSystemTimeString ( )		- Imposta l'ora di sistema da stringa hh:mm:ss			*/
/*	strTime	= ora																			*/
/*------------------------------------------------------------------------------------------*/
bool TM_SetSystemTimeString ( char* strTime )
{
	if ( strlen ( strTime ) == 8 )
	{
		sysTime	time;
		sscanf ( strTime, "%02d:%02d:%02d", &time.tm_hour, &time.tm_min, &time.tm_Sec );
		return ( APP_SetSystemTime ( &time ) );
	}
	return ( false );
}



/********************************************************************************************/

