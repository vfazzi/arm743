/********************************************************************************************/
/*	LIB_DateTime.h				- Gestione data e ora										*/

/********************************************************************************************/

#ifndef	__DATETIME_INCLUDE_DEFINED
#define __DATETIME_INCLUDE_DEFINED

/********************************************************************************************/
/*	Strutture e tipi																		*/
/********************************************************************************************/
typedef struct
{
	int tm_hour;
	int tm_min;
	int tm_Sec;
}  sysTime;

typedef struct
{
	int  dt_year;
	int dt_month;
	int dt_day;
} sysDate;
#endif

/********************************************************************************************/
/*	Funzioni																				*/
/********************************************************************************************/
void TM_GetSystemDate ( sysDate* dt );
void TM_GetSystemTime ( sysTime* tm );
bool TM_SetSystemDate ( sysDate* dt );
bool TM_SetSystemTime ( sysTime* tm );
void TM_GetSystemDateString ( char* dt );
void TM_GetSystemTimeString ( char* dt );
bool TM_SetSystemDateString ( char* dt );
bool TM_SetSystemTimeString ( char* dt );


/********************************************************************************************/
