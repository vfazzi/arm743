#ifndef CANBUS_H
#define CANBUS_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct can_packet
{
  unsigned sid : 11;
  unsigned dlc : 4;
  uint8_t data[8];
} can_packet;



void canbus_initialize(
  void *hcan,
  uint16_t filter,
  uint16_t mask);

void canbus_send(
  void *hcan,
  can_packet packet);

void canbus_receive_callback(
  void *hcan,
  can_packet packet);

void canbus_error_callback(void *hcan);

void CAN_Init ( uint16_t devAddress );

m_error can_init	( void );
m_error can_deinit	( void );
void can_DeInit		(FDCAN_HandleTypeDef * hcan);
void can_enable		(FDCAN_HandleTypeDef * hcan);
void can_disable	(FDCAN_HandleTypeDef * hcan);
void can_send_packet( uint32_t addr, uint8_t* TxData );

#ifdef __cplusplus
}
#endif

#endif
