/********************************************************************************************/
/*	LIB_Can.c					- Funzioni gestione comunicazione tramite CAN				*/
/********************************************************************************************/
// Include di sistema e applicazione
#include "SYS_Include.h"
#include "APP_Include.h"


/********************************************************************************************/
/*	Variabili interne																		*/
/********************************************************************************************/
#ifdef CAN1_ENABLED

/********************************************************************************************/
/*	Funzioni pubbliche																		*/
/*																							*/
/*	SYS_CanInit ( )						- Inizializzazione CAN								*/
/*	SYS_CanReceiveCallback ( )			- Ricezione da CAN									*/
/*	SYS_CanPollingProc ( )				- Polling messaggi CAN								*/
/********************************************************************************************/
/*------------------------------------------------------------------------------------------*/
/*	SYS_CanInit ( )						- Inizializzazione CAN								*/
/*------------------------------------------------------------------------------------------*/
void inline SYS_CanInit ( bool isMaster )
{
	#if defined DEBUG_TX_CAN || defined DEBUG_RX_CAN
		dbg_printf(isMaster ? "CAN master\r\n" : "CAN slave\r\n");
	#endif
	CAN_Init ( isMaster ? CAN_ADDR_MASTER : CAN_ADDR_SLAVE );
	ShortTimerSet ( TIM_SLAVE_ALIVE, CAN_SLAVE_ALIVE_TIME );
	ShortTimerSet ( TIM_CAN, CAN_POLLING_TIME );

	#ifdef	CAN_LED_ENABLED
		LEDV_OFF;
		LEDR_OFF;
	#endif
}

/*------------------------------------------------------------------------------------------*/
/*	SYS_CanReceiveCallback ( )			- Ricezione da CAN									*/
/*------------------------------------------------------------------------------------------*/
void inline SYS_CanReceiveCallback ( void *hcan, can_packet packet )
{
	#ifdef	CAN_LED_ENABLED
		LEDV_TOGGLE;
	#endif
	if(isMaster) // Messaggio dallo slave
		VARCO_CanReceiveCallbackMaster ( hcan, packet );
	else
		VARCO_CanReceiveCallbackSlave ( hcan, packet );

	#ifdef DEBUG_RX_CAN
		dbg_printf("\r\nReceived via CAN: ");
		for(int ind=0;ind<packet.dlc;ind++)
		{
			dbg_printf("%d ",packet.data[ind]);
		}
		dbg_printf("\r\n");
	#endif
}

/*	SYS_CanPollingProc ( )				- Polling messaggi CAN								*/
/*------------------------------------------------------------------------------------------*/
void inline SYS_CanPollingProc ( void )
{
	can_packet packet;

	if(isMaster)
		VARCO_CanSendCallbackMaster ( &packet );
	else
		VARCO_CanSendCallbackSlave ( &packet );
	
	canbus_send( (void*)&hfdcan1, packet);
	ShortTimerSet ( TIM_CAN, CAN_POLLING_TIME );
	
	#ifdef DEBUG__TX_CAN
		dbg_printf("\r\nSent via CAN: ");
		for(int ind=0;ind<packet.dlc;ind++)
		{
			dbg_printf("%d ",packet.data[ind]);
		}
		dbg_printf("\r\n");
	#endif
}
#endif


/********************************************************************************************/
