/********************************************************************************************/
/*	LIB_Can.h					- Funzioni gestione comunicazione tramite CAN				*/

/********************************************************************************************/

#ifndef	__LIBCAN_INCLUDED
#define __LIBCAN_INCLUDED

//	Dichiarazione costanti
enum enmCom_CAN{COM_CAN_STATUS,COM_CAN_PARAM_1,COM_CAN_PARAM_2};

//	Funzioni
void SYS_CanInit 			( bool isMaster );
void SYS_CanReceiveCallback ( void *hcan, can_packet packet );
void SYS_CanPollingProc 	( void );
#endif
