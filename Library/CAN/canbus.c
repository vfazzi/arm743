#include "SYS_Include.h"
#include "APP_Include.h"




void CAN_Init ( uint16_t devAddress )
{
	can_init();
	can_enable(&hfdcan1);
	canbus_initialize((void*)&hfdcan1, devAddress, CAN_SLAVE_MASK);
}

uint32_t encode_dlc(uint8_t length)
{
  switch(length)
  {
  case 0: return FDCAN_DLC_BYTES_0;
  case 1: return FDCAN_DLC_BYTES_1;
  case 2: return FDCAN_DLC_BYTES_2;
  case 3: return FDCAN_DLC_BYTES_3;
  case 4: return FDCAN_DLC_BYTES_4;
  case 5: return FDCAN_DLC_BYTES_5;
  case 6: return FDCAN_DLC_BYTES_6;
  case 7: return FDCAN_DLC_BYTES_7;
  case 8: return FDCAN_DLC_BYTES_8;
  default: return 0;
  }
}

uint8_t decode_dlc(uint32_t code)
{
  switch(code)
  {
  case FDCAN_DLC_BYTES_0: return 0;
  case FDCAN_DLC_BYTES_1: return 1;
  case FDCAN_DLC_BYTES_2: return 2;
  case FDCAN_DLC_BYTES_3: return 3;
  case FDCAN_DLC_BYTES_4: return 4;
  case FDCAN_DLC_BYTES_5: return 5;
  case FDCAN_DLC_BYTES_6: return 6;
  case FDCAN_DLC_BYTES_7: return 7;
  case FDCAN_DLC_BYTES_8: return 8;
  default: return 0;
  }
}


void canbus_initialize( void *hcan, uint16_t filter, uint16_t mask)
{
  FDCAN_FilterTypeDef filter_config = { 0 };
  filter_config.IdType = FDCAN_STANDARD_ID;
  filter_config.FilterIndex = 0;
  filter_config.FilterType = FDCAN_FILTER_MASK;
  filter_config.FilterConfig = FDCAN_FILTER_TO_RXFIFO0;
  filter_config.FilterID1 = filter;
  filter_config.FilterID2 = mask;
  HAL_FDCAN_ConfigFilter(hcan, &filter_config);
  HAL_FDCAN_ConfigGlobalFilter(
    hcan,
    FDCAN_REJECT,
    FDCAN_REJECT,
    FDCAN_REJECT_REMOTE,
    FDCAN_REJECT_REMOTE);
  HAL_FDCAN_ConfigFifoWatermark(hcan, FDCAN_CFG_RX_FIFO0, 1);
  HAL_FDCAN_ActivateNotification(hcan, FDCAN_IT_RX_FIFO0_WATERMARK, 0);
  HAL_FDCAN_ActivateNotification(hcan, FDCAN_IT_TX_FIFO_EMPTY, 0);
  HAL_FDCAN_ActivateNotification(
    hcan,
    FDCAN_IT_RAM_ACCESS_FAILURE |
    FDCAN_IT_ERROR_LOGGING_OVERFLOW |
    FDCAN_IT_RAM_WATCHDOG |
    FDCAN_IT_ARB_PROTOCOL_ERROR |
    FDCAN_IT_DATA_PROTOCOL_ERROR |
    FDCAN_IT_RESERVED_ADDRESS_ACCESS |
    FDCAN_IT_ERROR_PASSIVE |
    FDCAN_IT_ERROR_WARNING |
    FDCAN_IT_BUS_OFF,
    0);
  HAL_FDCAN_Start(hcan);
}

HAL_StatusTypeDef errore;

void canbus_send(void *hcan, can_packet packet )
{
  FDCAN_TxHeaderTypeDef header;
  header.Identifier = packet.sid;
  header.IdType = FDCAN_STANDARD_ID;
  header.TxFrameType = FDCAN_DATA_FRAME;
  header.DataLength = encode_dlc(packet.dlc);
  header.ErrorStateIndicator = FDCAN_ESI_ACTIVE;
  header.BitRateSwitch = FDCAN_BRS_OFF;
  header.FDFormat = FDCAN_FRAME_CLASSIC;
  header.TxEventFifoControl = FDCAN_NO_TX_EVENTS;
  header.MessageMarker = 0;
  if ( (((FDCAN_HandleTypeDef*)hcan)->Instance->TXFQS & FDCAN_TXFQS_TFQF) == 0U)
  	errore = HAL_FDCAN_AddMessageToTxFifoQ(hcan, &header, packet.data);
  else 
	  return;
  if( errore != HAL_OK)
  {
    canbus_error_callback(hcan);
  }
}



void can_enable(FDCAN_HandleTypeDef * hcan)
{
  HAL_FDCAN_Start( hcan );
}
/** ---------------------------------------------------------------------------
  * @brief  disabilitazione
  * @param handle
  * @retval -
  */
void can_disable(FDCAN_HandleTypeDef * hcan)
{
  HAL_FDCAN_Stop( hcan );
}


/* HAL callbacks.
*/
void canbus_get_rx( FDCAN_HandleTypeDef *hfdcan )
{
  while(HAL_FDCAN_GetRxFifoFillLevel(hfdcan, FDCAN_RX_FIFO0) > 0)
  {
    can_packet packet = { 0 };
    FDCAN_RxHeaderTypeDef header = { 0 };
    HAL_FDCAN_GetRxMessage(hfdcan, FDCAN_RX_FIFO0, &header, packet.data);
    packet.dlc = decode_dlc(header.DataLength);
    packet.sid = header.Identifier;
  }
}


#ifdef CAN1_ENABLED
void HAL_FDCAN_RxFifo0Callback(FDCAN_HandleTypeDef *hfdcan, uint32_t RxFifo0ITs)
{
  while(HAL_FDCAN_GetRxFifoFillLevel(hfdcan, FDCAN_RX_FIFO0) > 0)
  {
    can_packet packet = { 0 };
    FDCAN_RxHeaderTypeDef header = { 0 };
    HAL_FDCAN_GetRxMessage(hfdcan, FDCAN_RX_FIFO0, &header, packet.data);
    packet.dlc = decode_dlc(header.DataLength);
    packet.sid = header.Identifier;
    //	Callback di sistema
    SYS_CanReceiveCallback (hfdcan, packet);
  }
}
#endif

void HAL_FDCAN_TxFifoEmptyCallback(FDCAN_HandleTypeDef *hfdcan)
{
}

void HAL_FDCAN_ErrorCallback(FDCAN_HandleTypeDef *hfdcan)
{
  hfdcan->ErrorCode = HAL_FDCAN_ERROR_NONE;
  canbus_get_rx( hfdcan );
}
unsigned int error_status=0;
void HAL_FDCAN_ErrorStatusCallback(FDCAN_HandleTypeDef *hfdcan, uint32_t ErrorStatusITs)
{

	error_status = ErrorStatusITs;
  if(ErrorStatusITs & FDCAN_IT_BUS_OFF)
  {
    canbus_error_callback(hfdcan);
  }
  canbus_get_rx( hfdcan );
}

	  
void FDCAN1_IT0_IRQHandler(void)
{
	HAL_FDCAN_IRQHandler(&hfdcan1);
}

void FDCAN1_IT1_IRQHandler(void)
{
	HAL_FDCAN_IRQHandler(&hfdcan1);
}
	  


m_error can_init(void)
{
	FDCAN_FilterTypeDef     sFilterConfig;
	
	hfdcan1.Instance 				= FDCAN1;
	hfdcan1.Init.FrameFormat 		= FDCAN_FRAME_CLASSIC;
	hfdcan1.Init.Mode 				= FDCAN_MODE_NORMAL;
	hfdcan1.Init.AutoRetransmission = DISABLE;
	hfdcan1.Init.TransmitPause 		= DISABLE;
	hfdcan1.Init.ProtocolException 	= ENABLE;
	hfdcan1.Init.NominalPrescaler 	= 20;
	hfdcan1.Init.NominalSyncJumpWidth = 1;
	hfdcan1.Init.NominalTimeSeg1 	= 13;
	hfdcan1.Init.NominalTimeSeg2 	= 2;
	hfdcan1.Init.DataPrescaler 		= 1;
	hfdcan1.Init.DataSyncJumpWidth 	= 1;
	hfdcan1.Init.DataTimeSeg1 		= 1;
	hfdcan1.Init.DataTimeSeg2 		= 1;
	hfdcan1.Init.MessageRAMOffset 	= 0;
	hfdcan1.Init.StdFiltersNbr 		= 1;
	hfdcan1.Init.ExtFiltersNbr 		= 0;
	hfdcan1.Init.RxFifo0ElmtsNbr 	= 64;
	hfdcan1.Init.RxFifo0ElmtSize 	= FDCAN_DATA_BYTES_8;
	hfdcan1.Init.RxFifo1ElmtsNbr 	= 0;
	hfdcan1.Init.RxFifo1ElmtSize 	= FDCAN_DATA_BYTES_8;
	hfdcan1.Init.RxBuffersNbr 		= 0;
	hfdcan1.Init.RxBufferSize 		= FDCAN_DATA_BYTES_8;
	hfdcan1.Init.TxEventsNbr 		= 0;
	hfdcan1.Init.TxBuffersNbr 		= 0;
	hfdcan1.Init.TxFifoQueueElmtsNbr= 32;
	hfdcan1.Init.TxFifoQueueMode 	= FDCAN_TX_FIFO_OPERATION;
	hfdcan1.Init.TxElmtSize 		= FDCAN_DATA_BYTES_8;	
	
	if (HAL_FDCAN_Init(&hfdcan1) != HAL_OK)
	{
		Error_Handler();
	}
	/* Configure Rx filter */
	sFilterConfig.IdType = FDCAN_EXTENDED_ID;
	sFilterConfig.FilterIndex = 0;
	sFilterConfig.FilterType = FDCAN_FILTER_RANGE;
	sFilterConfig.FilterConfig = FDCAN_FILTER_TO_RXFIFO0;
	sFilterConfig.FilterID1 = 0x00000000;
	sFilterConfig.FilterID2 = 0x1FFFFFFF;
	//        sFilterConfig.RxBufferIndex = 0;
	HAL_FDCAN_ConfigFilter(&hfdcan1, &sFilterConfig);
	
	/* Configure global filter to reject all non-matching frames */
	HAL_FDCAN_ConfigGlobalFilter(&hfdcan1, FDCAN_REJECT, FDCAN_REJECT, 
								 FDCAN_REJECT_REMOTE, FDCAN_REJECT_REMOTE);
	
	/* Configure Rx FIFO 0 watermark to 2 */
	HAL_FDCAN_ConfigFifoWatermark(&hfdcan1, FDCAN_CFG_RX_FIFO0, 1);
	
	/* Activate Rx FIFO 0 watermark notification */
	HAL_FDCAN_ActivateNotification(&hfdcan1, FDCAN_IT_RX_FIFO0_WATERMARK, 0);
	
	return ERROR_NONE;
}


void canbus_error_callback(void *hcan)
{
	HAL_FDCAN_Stop( hcan );
	HAL_FDCAN_Start( hcan );
	
}
