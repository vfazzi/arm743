/********************************************************************************************/
/*	SYS_Functions.h		- Funzioni ausiliarie di sistema									*/

/********************************************************************************************/

#include "SYS_Include.h"
#include "APP_Include.h"


/********************************************************************************************/
/*	Funzioni del modulo																		*/
/*																							*/
/*	SYS_SystemLoopInit ( )				- Inizializzazione system loop						*/
/*	SYS_SystemLoopCallback ( )			- Callback del system loop							*/
/*	SYS_SystemTimerTick ( )				- Timer di sistema - Solo per processi critici		*/
/********************************************************************************************/
/*------------------------------------------------------------------------------------------*/
/*	SYS_SystemLoopInit ( )				- Inizializzazione system loop						*/
/*------------------------------------------------------------------------------------------*/
void inline	SYS_SystemLoopInit ( void )
{
	//	Comunicazione USB
	#ifdef	CON_USB_BUFFERED
	  SysTimerSet ( SYS_TIMER_USB_POLLING, COM_USB_POLLING_TIME );
	#endif
	//	Terminale USB
	#ifdef ASCII_TERM_ENABLED
	  SysTimerSet ( SYS_TIMER_TERM_USB, COM_USB_POLLING_TIME * 2 );
	#endif
	#ifdef USART6_INT_485_ENABLED
		SysTimerSet ( SYS_TIMER_485INT, INT_485_TIMEOUT_POLLING * 2 );
	#endif
}
void inline	SYS_SystemLoopCallback ( void )
{
	//	Gestione porta USB bufferizzata
	#ifdef	CON_USB_BUFFERED
		if ( isSysTimerExpired ( SYS_TIMER_USB_POLLING ) )
		{
			SysTimerSet ( SYS_TIMER_USB_POLLING, COM_USB_POLLING_TIME );
			if ( USB_IsUsbReady( ))
			{
				USB_BufSendToUsb();
			}
		}
	#endif
	//	Terminale USB
	#ifdef ASCII_TERM_ENABLED
		if ( isSysTimerExpired ( SYS_TIMER_TERM_USB ) )
		{
			TerminaleBuffered();
			SysTimerSet ( SYS_TIMER_TERM_USB, COM_USB_POLLING_TIME );
		}
	#endif
		//	Seriale interna
	#ifdef USART6_INT_485_ENABLED
		N485_IntPollingProc ( );
	#endif
	//	Seriale esterna
	#ifdef USART3_EXT_485_ENABLED
		N485_ExtPollingProc ( );
	#endif
	//	CAN
	#ifdef CAN1_ENABLED
		if ( ShortTimerIsExpired ( TIM_CAN ) )
		{
			SYS_CanPollingProc ( );
		}
	#endif
}
/*------------------------------------------------------------------------------------------*/
/*	SYS_SystemTimerTick ( )				- Timer di sistema - Solo per processi critici		*/
/*------------------------------------------------------------------------------------------*/
void inline SYS_SystemTimerTick ( void )
{

}

/********************************************************************************************/
