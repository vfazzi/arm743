
#ifndef __BUFFERCIRCOLARE_H
#define __BUFFERCIRCOLARE_H

	#include <string.h>




#ifndef CIR_BUFFER_SIZE
	#define CIR_BUFFER_SIZE	128
#endif
typedef struct {
	unsigned char * str;
	unsigned int len;
}_bufferstr;


typedef  int ( *p_CallBack_Send )( unsigned int wait );  /*!< pointer to */

typedef struct 
{
	unsigned char *first;			// puntatore all'inizio del buffer
	unsigned char *last;			// untatore alla fine del buffer
	unsigned char *pR;				// puntatore per la lettura
	unsigned char *pW;				// puntatore per la scrittura dei nuovi elementi
	unsigned char *last_str;
	unsigned char last_char;
	unsigned short last_esc;	
	unsigned short discard_esc	:1;	// se = 1 sostituisce la sequenza di caratteri
									// salvata in last_esc
	unsigned short do_overrun	:1;	// se = 1 sostituisce l'elemento pi� vecchio con
									// l'ultimo arrivato se = 0 una volta pieno non
									// vengono ineseriti elemnti fin quando non si crea posto
									// leggendo gli elementi pi� vechi
	unsigned short over_run		:1; // segnala che � avvenuto un over run che ha eliminato
									// l'elemento pi� vecchio
	
	unsigned short stop			:1;	// = 1 il buffer � boccato pieno
	
	unsigned int free;				// contatore posti liberi
	unsigned int size;	
  	unsigned char *buffer;
	p_CallBack_Send	p_CallBack_Send_char;

//  event_t *put_event;
  //event_t *get_event;
}_queue;

//	flash
typedef struct	{
	unsigned int first;		// indirizzo lineare inizio buffer in flash
	unsigned int last;		// indirizzo lineare fine buffer in flash
	unsigned int size;		// dimensione end - start
	unsigned int free;		// campi liberi in byte
	unsigned int read;		// indice di lettura
	unsigned int write;		// indice di scrittura
	unsigned short do_overrun	:1;	// se = 1 sostituisce l'elemento pi� vecchio con
									// l'ultimo arrivato se = 0 una volta pieno non
									// vengono ineseriti elemnti fin quando non si crea posto
									// leggendo gli elementi pi� vechi
	unsigned short over_run		:1; // segnala che � avvenuto un over run che ha eliminato
									// l'elemento pi� vecchio
	
	unsigned short stop			:1;	// = 1 il buffer � boccato pieno
}_fqueue;

	void BufferWritChar				( unsigned char c, _queue *q );
	void BufferInit					( _queue *q, unsigned int size, p_CallBack_Send pCallBack  );
	int BufferWrite					( _queue *q, unsigned char *p, unsigned int size );
	int BufferRead					( _queue *q, unsigned char *p, unsigned int size );
	int BufferGetLen				( _queue *q );
	int BufferGetFree				( _queue *q );
	void BufferFlush				( _queue *q );
	unsigned char * BufferFindStr	( _queue *q, unsigned char * p, int * len );
	unsigned char * Search_B2inB1	( _queue *q, unsigned char* p2, int b2_len );



#endif