/********************************************************************************************/
/*	LIB_Term.h					- Terminale USB (riga di comando)							*/

/********************************************************************************************/
#ifndef __LIB_TERM_DEFINED
#define __LIB_TERM_DEFINED

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <USB/buffercircolare.h>

/*------------------------------------------------------------------------------------------*/
/*	Costanti di configurazione																*/
/*------------------------------------------------------------------------------------------*/
#define TERMINAL_BUFFER_RX_SIZE		1024
#define TERMINAL_BUFFER_TX_SIZE		2048
#define MAX_ARGS		8			// Numero massimo di argomenti nella riga
#define INBUF_SIZE		256

/*------------------------------------------------------------------------------------------*/
/*	Struttura comando																		*/
/*------------------------------------------------------------------------------------------*/
typedef const struct
{
	char *	cmd;					// Comando
	char 	min_args;				// Numero minimo di argomenti
	char	max_args;				// Numero massimo di argomenti
	void	(*func)(int, char **);	// puntatore a Funzione da eseguire
	char *	Help;					// Descrizione help
	char *	syntax;					// Sintassi del comando
	char	flag;					// Bit 0 = 1 visualizza help della voce menu
} CMD;


/*------------------------------------------------------------------------------------------*/
/*	Funzioni del modulo TERM																*/
/*------------------------------------------------------------------------------------------*/
void Terminale				( char * pinBuff,int size );
void SetUpTerminale			( void );
void run_cmd 				( char *p );
void TerminaleBuffered		( void );


extern int	NUM_CMD;
extern CMD _CmdTab [];

#endif // Terminale.h
