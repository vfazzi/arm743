/********************************************************************************************/
/*	LIB_Term.c					- Terminale USB (riga di comando)							*/

/********************************************************************************************/
#include "SYS_Include.h"
#include "APP_Include.h"

/********************************************************************************************/
/*	Dichiarazione funzioni interne															*/
/********************************************************************************************/
void TERM_help (int argc, char **argv);
static int make_argv 	( char *cmdline, char *argv[] );
static const char msgInvalidCmd[] = "Comando non riconosciuto: %s\r\n>";


/********************************************************************************************/
/*	Dichiarazione variabili del modulo														*/
/********************************************************************************************/
//	Globali
_queue	pTermbRxBuf;
_queue	pTermTxBuf;
//	Private
char inBuff[INBUF_SIZE];
char * pRead = inBuff;
char * m_pRead = inBuff;
unsigned char usb_rx_buf[TERMINAL_BUFFER_RX_SIZE];
unsigned char usb_tx_buf[TERMINAL_BUFFER_TX_SIZE];

/********************************************************************************************/
/*	Funzioni del modulo TERM																*/
/*																							*/
/*	SetUpTerminale ( )						- Inizializzazione terminale					*/
/*	TerminaleBuffered ( )					- Gestione terminale (chiamata da timer)		*/
/*	TERM_help ( )							- Guida											*/
/*	run_cmd ( )								- Esecuzione comandi							*/
/********************************************************************************************/
/*------------------------------------------------------------------------------------------*/
/*	SetUpTerminale ( )						- Inizializzazione terminale					*/
/*------------------------------------------------------------------------------------------*/
void SetUpTerminale(void)
{
	pRead = inBuff;
	printf(HELP_TITLE);
	printf ("-Programma: %s del %s\r\n",_CODE_PROG,__DATE__);
	printf ("-Terminal start\r\n>");
}
/*------------------------------------------------------------------------------------------*/
/*	TerminaleBuffered ( )					- Gestione terminale (chiamata da timer)		*/
/*------------------------------------------------------------------------------------------*/
void TerminaleBuffered( void )
{
	int len = BufferGetLen( &pTermbRxBuf );
	// 	Non ci sono caratteri da processare
	if ( len  == 0 )	return ;

	do	{
		//	Legge un carattere dal buffer di ricezione e ne fa l'eco in trasmissione
		BufferRead( &pTermbRxBuf, (unsigned char *)m_pRead, 1);
		putchar ( *m_pRead );
		//	Gestione caratteri - Invio
		if(*m_pRead == 0x0d )
		{
			// Terminatore di stringa
			printf("\r\n");
			*(m_pRead)='\0';
			// Reset puntatore
			m_pRead = inBuff;
			run_cmd(inBuff);
			inBuff[0] = '\0';
		}
		// Back space
		else if(*m_pRead==0x08 && (m_pRead > inBuff ))
		{
			m_pRead--;
		}
		// Dato buono
		else if (*m_pRead != 0x0A)
		{
			//	Invia il carattere in trasmissione per fare l'eco
			fflush ( stdout );
			if((++m_pRead - inBuff) > ( INBUF_SIZE ))
			{
				// Sforato buffer resetta
				m_pRead = inBuff;
			}
		}
	}
	while ( --len );
}
/*------------------------------------------------------------------------------------------*/
/*	TERM_help ( )							- Guida											*/
/*------------------------------------------------------------------------------------------*/
void TERM_help (int argc, char **argv)
{
	//	Intestazione con nome del programma e data di compilazione
	printf("* * * programma: %s del %s * * *\r\n", _CODE_PROG,__DATE__);
	//	Scansione dei comandi
	for (int index = 0; index < NUM_CMD; index++)
	{
		//	Controlla se il comando viene visualizzato nella guida
		if ( _CmdTab [index].flag )
		{
			//	Scrive comando, descrizione e sintassi
			printf ("%s: %s\r\n", _CmdTab [index].cmd, _CmdTab [ index].Help );
			if ( _CmdTab [index].syntax [ 0 ] != 0 )
			{
				printf ("%s\r\n", _CmdTab [index].syntax );
			}
		}
	}
	printf("\r\n>");
}
/*------------------------------------------------------------------------------------------*/
/*	run_cmd ( )								- Esecuzione comandi							*/
/*------------------------------------------------------------------------------------------*/
void run_cmd (char *p)
{
    char i;
	CMD *pCmdTab = _CmdTab;
	char *arg_v[MAX_ARGS + 1];

	// inizializzo il puntatore alla tabella dei comandi
	int arg_c = make_argv ( p, arg_v );
	
	if (arg_c)		{
	
		for (i = 0; i < NUM_CMD; i++)	{
			if (strcasecmp((pCmdTab)->cmd,arg_v[0]) == 0)	{

				if (((arg_c-1)>=(pCmdTab)->min_args)&&((arg_c-1)<=(pCmdTab)->max_args))	
				{
					pCmdTab->func(arg_c -1, arg_v);
					return;
				}
				else	
				{
					printf("errore di sintassi %s\r\n%s\r\n>",arg_v[0],pCmdTab->syntax);
					return;
				}
			}
			pCmdTab++;
		}
		printf(msgInvalidCmd, arg_v[0]);
	}
	printf("\r\n>");
}

/********************************************************************************************/
/*	Funzioni private																		*/
/*																							*/
/*	__io_putchar ( )					- Redirect di STDOUT per inviare printf su USB		*/
/*	make_argv ( )						- Parser riga di comando							*/
/********************************************************************************************/
/*------------------------------------------------------------------------------------------*/
/*	__io_putchar ( )					- Redirect di STDOUT per inviare printf su USB		*/
/*------------------------------------------------------------------------------------------*/
int __io_putchar(int ch)
{
	BufferWritChar( ch, &pTermTxBuf);
	return(ch);
}
/*------------------------------------------------------------------------------------------*/
/*	make_argv ( )						- Parser riga di comando							*/
/*------------------------------------------------------------------------------------------*/
static int make_argv (char *cmdline, char *argv[])
{
	char in_text;
	int argc, i;
	argc = 0;
	i = 0;
	in_text = false;
	while (cmdline[i] != '\0')		{
		// spazio o tab o /
		if ( (cmdline[i] ==' ' ) || (cmdline[i] =='\t') || (cmdline[i] =='/' ) )	{
			if (in_text)
			{
				// terminatore per l'argomento
				cmdline[i] = '\0';
				in_text = false;
			}
			else 	{
			}
		}
		else	{
			if (in_text) {
			}
			else	{
				in_text = true;
				if (argc < MAX_ARGS)	{
					if (argv != NULL)	argv[argc] = &cmdline[i];
					argc++;
				}
				else
					//return argc
					break;
			}
		}
		i++;
	}
	if (argv != NULL)	argv[argc] = NULL;
	return argc;
}

/********************************************************************************************/
