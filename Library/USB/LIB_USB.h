/********************************************************************************************/
/*	LIB_USB.h					- Gestione comunicazione su USB								*/

/********************************************************************************************/


/*------------------------------------------------------------------------------------------*/
/*	Dichiarazione funzioni																	*/
/*------------------------------------------------------------------------------------------*/
void 	USB_InitUSBTerminal ( void );
void	USB_InitUSBDataBuffers ( void );
void 	USB_USBReceiveData ( uint8_t* rxBuffer, int rxCount );
uint8_t USB_BufSendToUsb ( void );
int		USB_UsbBufferGetCharCount ( void );
uint8_t USB_UsbBufferGetChar ( void );
bool 	USB_IsReady ( void );
uint8_t USB_SendData ( uint8_t* txData, int txSize );

#define	USB_IsUsbReady		CDC_USBIsReady
#define	USBD_DISCONNECTED					((uint8_t)0xFF)


/********************************************************************************************/
