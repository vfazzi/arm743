/********************************************************************************************/
/*	LIB_USB.c					- Gestione comunicazione su USB								*/

/********************************************************************************************/
#include "SYS_Include.h"

/********************************************************************************************/
/*	Funzioni del modulo																		*/
/*																							*/
/*	USB_InitUSBTerminal ( )				- Inizializzazione terminale USB					*/
/*	USB_InitUSBDataBuffers ( )			- Inizializzazione buffer per USB / COM Virtuale	*/
/*	USB_USBReceiveData ( )				- Inserisce nel buffer i dati ricevuti da USB		*/
/*	USB_BufSendToUsb ( )				- Invia su USB i dati presenti nel buffer TX		*/
/*	USB_UsbBufferGetCharCount ( )		- Numero di caratteri nel buffer di ricezione USB	*/
/*	USB_UsbBufferGetChar ( )			- Lettura carattere da buffer USB					*/
/* 	USB_IsReady ( )						- Controllo stato USB								*/
/* 	USB_SendData ( )					- Trasmissione dati su USB							*/
/********************************************************************************************/

/*------------------------------------------------------------------------------------------*/
/*	USB_InitUSBTerminal ( )				- Inizializzazione terminale USB					*/
/*------------------------------------------------------------------------------------------*/
void USB_InitUSBTerminal ( void )
{
#ifdef	CON_USB_BUFFERED
	USB_InitUSBDataBuffers ( );
#endif
#ifdef	ASCII_TERM_ENABLED
	SetUpTerminale ( );
#endif
}
/*------------------------------------------------------------------------------------------*/
/*	USB_InitUSBDataBuffers ( )			- Inizializzazione buffer per USB / COM Virtuale	*/
/*------------------------------------------------------------------------------------------*/
void USB_InitUSBDataBuffers ( void )
{
	pTermbRxBuf.buffer 	= usb_rx_buf;
	pTermTxBuf.buffer 	= usb_tx_buf;
	BufferInit(&pTermbRxBuf, sizeof(usb_rx_buf), NULL );
	BufferInit(&pTermTxBuf, sizeof(usb_tx_buf), NULL );
}
/*------------------------------------------------------------------------------------------*/
/*	USB_USBReceiveData ( )				- Inserisce nel buffer i dati ricevuti da USB		*/
/*	rxBuffer		= buffer dati ricevuti													*/
/*	rxCount			= numero di caratteri nel buffer										*/
/*------------------------------------------------------------------------------------------*/
void inline USB_USBReceiveData ( uint8_t* rxBuffer, int rxCount )
{
	if ( rxCount > 0)
	{
		for ( int i = 0; i < rxCount; i ++ )
		{
			BufferWritChar(rxBuffer[i], &pTermbRxBuf);
		}
	}

}
/*------------------------------------------------------------------------------------------*/
/*	USB_BufSendToUsb ( )				- Invia su USB i dati presenti nel buffer TX		*/
/*------------------------------------------------------------------------------------------*/
uint8_t USB_BufSendToUsb( void )
{
	int Len ;
	uint8_t result = USBD_OK;

	if (!CDC_USBIsReady() )
		return USBD_BUSY;

	if ( ( Len = BufferGetLen( &pTermTxBuf )) == 0 )
		return USBD_OK;

	if ( Len > sizeof(UserTxBufferFS) )	{
		Len = sizeof(UserTxBufferFS);
		BufferRead( &pTermTxBuf, UserTxBufferFS, sizeof(UserTxBufferFS) );
	}
	else {
		BufferRead( &pTermTxBuf, UserTxBufferFS, Len );
	}
	USBD_CDC_SetTxBuffer(&hUsbDeviceFS, UserTxBufferFS, Len);
	result = USBD_CDC_TransmitPacket(&hUsbDeviceFS);

	return result;
}
/*------------------------------------------------------------------------------------------*/
/*	USB_UsbBufferGetCharCount ( )		- Numero di caratteri nel buffer di ricezione USB	*/
/*------------------------------------------------------------------------------------------*/
int USB_UsbBufferGetCharCount ( void )
{
	return ( BufferGetLen( &pTermbRxBuf ) );
}
/*------------------------------------------------------------------------------------------*/
/*	USB_UsbBufferGetChar ( )			- Lettura carattere da buffer USB					*/
/*------------------------------------------------------------------------------------------*/
uint8_t USB_UsbBufferGetChar ( void )
{
	unsigned char chRead;
	BufferRead( &pTermbRxBuf, &chRead, 1);
	return ( chRead );
}
/*------------------------------------------------------------------------------------------*/
/* 	USB_IsReady ( )						- Controllo stato USB								*/
/*	Valore di ritorno:																		*/
/*	true	se USB connessa																	*/
/*	false	se USB non connessa																*/
/*------------------------------------------------------------------------------------------*/
bool USB_IsReady ( void )
{
	return (bool)CDC_USBIsReady ( );
}
/*------------------------------------------------------------------------------------------*/
/* 	USB_SendData ( )					- Trasmissione dati su USB							*/
/*	txData 	= buffer da trasmettere															*/
/*	txSize	= dimensione blocco dati														*/
/*	Valore di ritorno:																		*/
/*	USBD_OK			= OK																	*/
/*	USBD_FAIL		= errore trasmissione													*/
/*	USBD_BUSY		= occupato																*/
/*------------------------------------------------------------------------------------------*/
uint8_t USB_SendData ( uint8_t* txData, int txSize )
{
	if ( USB_IsReady ( ) == false ) return ( USBD_DISCONNECTED );
	return ( CDC_Transmit_FS ( txData, txSize ) );
}


/********************************************************************************************/
