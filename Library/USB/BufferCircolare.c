/**
 * \file            BufferCircolare.c
 * \brief           Gestione buffer circolare
 * @attention
 *
 * All rights reserved.</center></h2>
 *
 * @verbatim
 *
 * 2020/03/12  inserito retorno se non ci sono caratteri da leggere con  BufferRead
 * 2020/04/15  segnalato overrun e possibilit� di selezionare se sovrascrivere o no
 **
 * @endverbatim
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <USB/buffercircolare.h>

// PROTOTIPI FUNZIONE ESTERNE -------------------------------------------------
// VARIABILI ESTERNE ----------------------------------------------------------
// MACRO PRIVATE --------------------------------------------------------------
// VARIABILI GLOBALI ----------------------------------------------------------
// VARIABILI PRIVATE ----------------------------------------------------------
// PROTOTIPI FUNZIONE PRIVATE -------------------------------------------------

void SetReadPointer				( _queue *q, unsigned int j );

/** ---------------------------------------------------------------------------
  * @brief  restituisce la quantit� di dati nel buffer
  * @param  puntatore alla truttura del buffer
  * @retval quantit� di dati
  */
	int BufferGetLen( _queue	*q )
{
	if ( q->free == 0 )
		return q->size;
	
	if (q->pR == q->pW)
		return 0;
	if ( q->pR < q->pW)
		return ( q->pW - q->pR);
	
	return (q->pW - q->first) + ( q->last - q->pR );

}
/** ---------------------------------------------------------------------------
  * @brief  Restituisce quanti posti liberi ci sono nel buffer
  * @param  puntatore alla truttura del buffer
  * @retval posti liberi
  */
int BufferGetFree( _queue	*q )
{
	if (q->pR == q->pW)
		return 0;
	if ( q->pR < q->pW)
		return ( q->pW - q->pR);
	
	return (q->pR - q->pW);
}
/** ---------------------------------------------------------------------------
  * @brief  azzeramento totale del buffer
  * @param  puntatore alla truttura del buffer
  * @retval -
  */
void BufferFlush( _queue *q )
{
	q->pW		= q->buffer;
	q->pR		= q->buffer;
	q->free		= q->size;
	q->last_str = q->buffer;
	memset( q->buffer,0,q->size);
}
/** ---------------------------------------------------------------------------
  * @brief  Inizializzazione struttara di gestione del buffer
  * @param  puntatore alla truttura del buffer
  * @retval -
  */
void BufferInit(_queue	*q, unsigned int size, p_CallBack_Send pCallBack )
{
	q->first	= q->buffer;
	q->last		= q->buffer + size;
	q->pW		= q->buffer;
	q->pR		= q->buffer;
	q->last_str = q->buffer;
	q->free		= size;
	q->size		= size;
	q->p_CallBack_Send_char = pCallBack;
}
/** ---------------------------------------------------------------------------
  * @brief  Scrittura di una carattere nel buffer
  * @param  carattere da scrivere
  * @param  puntatore alla truttura del buffer
  * @retval -
  */
void BufferWritChar(unsigned char c, _queue	*q)
{

	if ( q->discard_esc )	{
		//sostituzione della sequenza \r\n con fine stringa
		q->last_esc = ( q->last_esc << 8 ) | c;
		//
		if ( q->last_esc == (  '\r'<<8 | '\n' ))	{
			// si mette solo se la sequenza � giusta 
			// e se non � la prima a buffer vu	oto
			if ( q->free != q->size && q->last_char != '\0')	{
				q->last_char = *q->pW++ = '\0';

			}
			else 
				return;
		}
		else if ( c == '\n' || c == '\r')  {
			// questi si scartano
			return;
		}
		else	{
			// caratteri normali
			q->last_char = *q->pW++ = c;
		}	
	}
	else {

		if ( q->free== 0 )	{
			// buffer pieno
			if ( q->do_overrun )	{
				// sovrascrivo il pi� vecchio spostando il cursore di lettura
				q->last_char = *q->pW++ = c;
				// overrun segnalato
				q->over_run = 1;
				// sposto la lettura
				if ( ++q->pR == q->last )	{
					q->pR = q->first;
				}
			}
			else	{
				// non devo sovrascrivere
				if ( q->p_CallBack_Send_char != NULL )	{
					// invio un carattere
					q->p_CallBack_Send_char( 1 );
					q->last_char = *q->pW++ = c;
					
					if ( q->free > 0 )	
						q->free--;
					
					if ( q->pW == q->last)	{
						q->pW = q->first;
					}
				}
				else {
					q->over_run = 1;
				}
			}
		}
		else {
			q->over_run = 0;
			q->last_char = *q->pW++ = c;
			if ( q->free > 0 )	
				q->free--;
			
			if ( q->pW == q->last)	{
				q->pW = q->first;
			}
		}
	}
}
/** ---------------------------------------------------------------------------
  * @brief  scrittura di un buffer di dati
  * @param  puntatore alla truttura del buffer
  * @param  puntatore al buffer di dati in ingresso
  * @param  dimensione dei dati da scrivere
  * @retval dati scritti nel buffer
  */
/*
int BufferWrite( _queue *q, unsigned char * p, unsigned int len)
{
	unsigned int m_size = len;
	unsigned int firstBloc = 0;
	
	if( m_size > CIR_BUFFER_SIZE)	{
		// troppi dati
		m_size = CIR_BUFFER_SIZE;
	}
	if ( q->pW >= q->pR)	{
		
		// controllo se devo spezzare il blocco perche sono in cima al buffer
		if ( m_size > (q->last - q->pW) )	{
			firstBloc =  q->size - ( q->pW - q->first);
			memcpy(q->pW,p,firstBloc);
			// resto
			memcpy(q->first,(p +  firstBloc),m_size - firstBloc);

			// aggiusto il puntatore di scrittura
			q->pW = q->first + (m_size - firstBloc);
			
		}
		else	{
			// blocco intero
			memcpy(q->pW, p, m_size);
			q->pW += m_size;
			// controllo il puntatore che non sfori
			if ((q->pW - q->buffer) >= q->size )
				q->pW = q->buffer + ((q->pW - q->buffer) % q->size);
		}
	}
	else	{
		memcpy(q->pW, p, m_size);
		q->pW += m_size;
		// controllo il puntatore che non sfori
		if ((q->pW - q->buffer) >= CIR_BUFFER_SIZE )
			q->pW = q->buffer + ((q->pW - q->buffer) % CIR_BUFFER_SIZE);
		if ( q->pR == q->pW)
			SetReadPointer(q, m_size);
			
	}
	if ( q->free > m_size )	{
		q->free = 0;
		// controllo la lettura
		if ( q->pW == q->pR )
			q->pR += 1;
		else 
			q->pR = q->pW + 1;
		
		if ( q->pR > q->last )
			q->pR = q->buffer +  ( q->pR - q->last );
		else 
			q->pR = q->first;
		// controllo se devo spostare il puntatore di lettura
		if ( q->pW > q->pR )	{
			// overrunn DA SEGNALARE -------------<<<
			// sposto la lettura
			q->pR = q->pW  + 1;
			
			if ( q->pR > q->last )	{
				q->pR = q->first;
			}
		}
	}
	 else {
		q->free -= m_size;
	}
	
	return m_size;
}
*/
/** ---------------------------------------------------------------------------
  * @brief  lettura contigua quantita size di dati presenti nel buffer
  * @param  puntatore alla truttura del buffer
  * @param  puntatore al buffer di destinazione dei dati
  * @param  dimensione del buffer di ricezione dei dati
  * @retval ritorna il numero dei dati letti <= size
  */
int BufferRead( _queue	*q, unsigned char *p, unsigned int size)
{
	unsigned int m_size = 0;
	unsigned int firstBloc = 0;
	
	unsigned int free = BufferGetLen( q );
	
	if ( free == 0 )
		return 0;
	// controllo quanta roba c'� da leggere
	if (q->pR < q->pW)	{
		if ( ( q->pW - q->pR) < size )
			m_size = q->pW - q->pR;
		else
			m_size = size;
		memcpy(p,q->pR,m_size);
		q->pR = q->pR + m_size;
	}
	else	{
		firstBloc = q->size - (q->pR - q->buffer);
		
		if ( firstBloc > size)	{
			firstBloc = size;
			memcpy( p, q->pR, firstBloc);
			q->pR = q->pR + size;
		}
		else	{
			memcpy( p, q->pR, firstBloc);
			if (firstBloc == size)	{
				SetReadPointer(q,firstBloc);
			}
			else if (( q->pW - q->buffer) < (size - firstBloc))	{
//@@@@� controllare				
				m_size = q->pW - q->buffer;
				memcpy( p + firstBloc, q->buffer, m_size);
				q->pR = q->pW;
			}
			else	{
				// i dati ancora da leggere sono di pi� di quanto chiesto
				m_size = size - firstBloc;
				memcpy( p + firstBloc, q->buffer, m_size);
				q->pR = q->buffer + m_size;
			}
		}	
	}
	q->free += (m_size + firstBloc);
	return (m_size + firstBloc);
}
// ----------------------------------------------------------------------------
//
//
/*
unsigned char * Search_B2inB1(_queue *q, unsigned char* p2, int b2_len)
{
	int cnt_Comp = 0;
	unsigned char * m_p2 = p2;
	unsigned char * p1 = q->pR;
	
	while ( p1 != q->pW)	{
		if (*p1 == *p2)	{
			cnt_Comp++; 
			p2++;
			if (cnt_Comp == b2_len)	{
				return p1-b2_len + 1;
			}
		}
		else	{
			cnt_Comp = 0;
			p2 = m_p2;
		}
		if (++p1 >= q->last )	{
			p1 = q->first;
		}
	}
	return NULL; 
}
*/
// ----------------------------------------------------------------------------
//	Cerca una stringa puntata da p nel buffer
//
/** ---------------------------------------------------------------------------
  * @brief  Cerca una stringa puntata da p nel buffer
  * @param  puntatore alla truttura del buffer
  * @param  puntatore alla tringa da ricercare
  * @param  lunghezza della stringa da ricercare
  * @retval puntatore alla stringa trovata nel buffer
  */
/*
unsigned char * BufferFindStr( _queue	*q, unsigned char * p,  int * len)
{
	unsigned char * m_p;
	
	int m_len = strlen((char *)p);
	if ( m_len < 0 )
		m_len = q->last - q->pR;
	
	m_p = Search_B2inB1(q, p, m_len);

	if ( m_p != NULL	)	{
		if ( m_p >= q->pR )
			*len = m_p - q->pR;
		else
			*len = q->size - ( q->pR - m_p );
	}

	return m_p;
}
*/
// ============================================================================
//		FUNZIONI LOCALI
// ============================================================================

/** ---------------------------------------------------------------------------
  * @brief  Imposta i puntatori di lettura
  * @param  puntatore alla truttura del buffer
  * @retval -
  */
void SetReadPointer( _queue	*q, unsigned int j)
{
	q->pR = q->pR + j;
	if ((q->pR - q->buffer) >= CIR_BUFFER_SIZE )
		q->pR = q->buffer + ((q->pR - q->buffer) % CIR_BUFFER_SIZE);
}
