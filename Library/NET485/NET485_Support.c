/********************************************************************************************/
/*	Net485_Support.c			- Funzioni di supporto per reti 485							*/

/********************************************************************************************/
#include "SYS_Include.h"
#include "APP_Include.h"

/*------------------------------------------------------------------------------------------*/
/*	CreaPacchetto ( )				- Costruisce il messaggio CEIABI da trasmettere			*/
/*------------------------------------------------------------------------------------------*/
void CreaPacchetto(_rs485* RS485, unsigned char dest, unsigned char op, unsigned char* pData, int lenData)
{

    for(int ind=0;ind<MAX_BUFF_TX485;ind++) RS485->tx_buffer[ind]=0;

    int indBuff=0,chk=0;
    RS485->tx_buffer[indBuff++] = STX;
    INS_DLE_CHK(lenData+2);
    RS485->tx_buffer[indBuff++]=0;
    RS485->tx_buffer[indBuff++]=0;
    INS_DLE_CHK(dest);
    INS_DLE_CHK(RS485->address); // Mitt
    INS_DLE_CHK(op);
    for(int ind=0;ind<lenData;ind++)
    {
        INS_DLE_CHK(*pData);
	    pData++;
    }
    INS_DLE((chk>>8));
    INS_DLE((chk&0xFF));
    RS485->tx_buffer[indBuff++] = ETX;
    RS485->tx_len = indBuff;

}
/*------------------------------------------------------------------------------------------*/
/*	TestCHK	( )					- Verifica checksum											*/
/*------------------------------------------------------------------------------------------*/
bool TestCHK(unsigned char* pBuff, int len)
{
	// Inizialmente pBuff punta alla LMSG
	int sum=0, sumRx;
	for (int ind=0;ind<len-3;ind++)
		sum+=(*pBuff++);
	sumRx=(*pBuff)*256+*(pBuff+1);
	return (sum==sumRx);

}
