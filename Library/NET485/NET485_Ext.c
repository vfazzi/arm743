/********************************************************************************************/
/*	Net485_Int.c			- Gestione seriale e rete 485 interna							*/

/********************************************************************************************/
#include "SYS_Include.h"
#include "APP_Include.h"


void N485_ExtStartTX(void);
extern void CreaPacchetto(_rs485* RS485, unsigned char dest, unsigned char op, unsigned char* pData, int lenData);



/********************************************************************************************/
/*	Funzioni del modulo																		*/
/*																							*/
/*	N485_ExtNetworkInit ( )			- Inizializzazione rete esterna							*/
/*	N485_ExtPollingProc ( )			- Gestione della comuncazione a polling					*/
/********************************************************************************************/
/*------------------------------------------------------------------------------------------*/
/*	N485_ExtNetworkInit ( )			- Inizializzazione rete esterna							*/
/*------------------------------------------------------------------------------------------*/
void N485_ExtNetworkInit ( void )
{
	DEREEXT_OFF;
	ext_485.address= sysCfg.m_extNetSlaveAddress;
	HAL_UART_MspInit ( &huart3 );
    //	Abilitazione interrupt
	__HAL_UART_DISABLE_IT( &huart3, UART_IT_TC);
	__HAL_UART_ENABLE_IT( &huart3, UART_IT_RXNE );
	__HAL_UART_CLEAR_IT( &huart3, UART_IT_RXNE );
}

/*------------------------------------------------------------------------------------------*/
/*	N485_ExtPollingProc ( )			- Gestione della comuncazione a polling					*/
/*------------------------------------------------------------------------------------------*/
void N485_ExtPollingProc()
{
	//	Struttura messaggio
    net485SendMsgData_t	msgTx;

    //	Controlla se c'è un messaggio nel buffer di ricezione
	if ( !ext_485.stato.received )
	{
		return;
	}
    //	MEssaggio ricevuto
    ext_485.stato.received=false;
	//	Controllo indirizzo di destinazione
	if ( ( ext_485.rx_buffer [PACC_DEST] !=ext_485.address ) && ( ext_485.rx_buffer [PACC_DEST] != IND_485_BROADCAST ) )
	{
		return;
	}
	//	Chiama la callback di gestione dei messaggi ricevuti
	msgTx.msgDataSize = 0;
	msgTx.destAddr = ext_485.rx_buffer [ PACC_MITT ];
	msgTx.msgCmd = ext_485.rx_buffer [ PACC_COMANDO ];
	memset(msgTx.msgData,0,sizeof(msgTx.msgData));
	if ( VARCO_485ExternalNetworkRcvMSgCallback ( ext_485.rx_buffer, &msgTx ) )
	{
		//	Trasmissione risposta
		CreaPacchetto( &ext_485, msgTx.destAddr, msgTx.msgCmd, msgTx.msgData, msgTx.msgDataSize);
		N485_ExtStartTX ( ); // Send
	}
}

/********************************************************************************************/
/*	Funzioni interne																		*/
/*																							*/
/*	N485_ExtCeiabiRX ( )				- Ricezione carattere CEIABI							*/
/*	N485_ExtStartTX ( )				- Avvia la trasmissione di un messaggio CEIABI			*/
/*	UART6_485Interrupt ( )			- Interrupt UART 										*/
/********************************************************************************************/
/*------------------------------------------------------------------------------------------*/
/*	N485_ExtCeiabiRX ( )				- Ricezione carattere CEIABI							*/
/*------------------------------------------------------------------------------------------*/
bool N485_ExtCeiabiRX(unsigned char sDato)
{
	static bool sInAttesa=true,sRiceve=false,sDle=false;
	bool sRicevuto=false;
    if(sInAttesa)
	{
		if(sDato==STX) // Inizio trasmissione il carattere STX è scartato
		{
			sRiceve=true;
			sDle=sInAttesa=false;
			ext_485.rx_index=0;
		}
		else // Errore nella sequenza di ricezione.
		{
			sInAttesa=true;
			sRiceve=sDle=false;
		}
	}
	else if(sRiceve) // Stato ricezione.
	{
		if(sDato==ETX)
		{
			ext_485.rx_buffer[ext_485.rx_index++]=sDato;
			sRicevuto=true;
			sInAttesa=true;
			sRiceve=sDle=false;
		}
		else if(sDato==STX) // Errore nella sequenza di ricezione.
		{
			sInAttesa=true;
			sRiceve=sDle=false;
		}
		else if(sDato==DLE) // Carattere da scartare prossimo buono.
		{
			sDle=true;
			sRiceve=sInAttesa=false;
		}
		else // Carattere normale da mettere nel Buffer.
		{
			if(!sRicevuto && ext_485.rx_index<MAX_BUFF_RX485-1)
			{
				ext_485.rx_buffer[ext_485.rx_index++]=sDato;
				sRiceve=true;
			}
			else
			{
				sInAttesa=true;
				sRiceve=sDle=false;
			}
		}
	}
	else if(sDle)// Stato analizza carattere dopo aver ricevuto un  DLE.
	{
		sDle=false;
		if(sDato==STX || sDato==ETX || sDato==DLE)
		{
			sInAttesa=false;
			if(!sRicevuto && ext_485.rx_index<MAX_BUFF_RX485-1)// Dato nel buffer e si ritorna allo stato ricezione.
			{
				ext_485.rx_buffer[ext_485.rx_index++]=sDato;
				sRiceve=true;
			}
			else
			{
				sInAttesa=true;
				sRiceve=false;
			}
		}
		else // Errore si ritorna in attesa.
		{
			sInAttesa=true;
			sRiceve=false;
		}
	}
	return sRicevuto;
}
/*------------------------------------------------------------------------------------------*/
/*	N485_ExtStartTX ( )				- Avvia la trasmissione di un messaggio CEIABI			*/
/*------------------------------------------------------------------------------------------*/
void N485_ExtStartTX(void)
{
	DEREEXT_ON;
	USART3->TDR = (uint8_t)ext_485.tx_buffer[0];
	ext_485.tx_cnt = 1;
	LL_USART_ClearFlag_TC(USART3);
	__HAL_UART_ENABLE_IT(&huart3, UART_IT_TC);
}
/*------------------------------------------------------------------------------------------*/
/*	UART6_485Interrupt ( )			- Interrupt UART 										*/
/*------------------------------------------------------------------------------------------*/
void UART3_485Interrupt ( void )
{

	// INTERRUPT DI RICEZIONE 485 rete esterna (UART3)
	if( LL_USART_IsActiveFlag_RXNE_RXFNE(USART3))  // Receiver
	{
		ext_485.stato.rx_on = true;
		//	Ricezione carattere
		if(N485_ExtCeiabiRX((uint8_t)(USART3->RDR & 0xFF)))
		{
			ext_485.stato.rx_on=false;
			if(TestCHK(ext_485.rx_buffer,ext_485.rx_index))
			{
				//	Messaggio valido
				ext_485.stato.received=true;
			}
			else
			{
				ext_485.stato.received=false;
				APP_485ExternalNetworkRcvMsgError ( );
			}
		}
		//	Buffer overrun
		if ( ext_485.rx_index > MAX_BUFF_RX485)	ext_485.rx_index = 0;
		//	Timer di controllo ricezione
	}


	// INTERRUPT DI TRASMISSIONE 485 rete esterna (UART3)
	if(LL_USART_IsActiveFlag_TC(USART3))
	{
		// 	Cancella l'interrupt
		LL_USART_ClearFlag_TC(USART3);
		//	Controlla se ci sono altri caratteri da trasmettere
		if (ext_485.tx_cnt < ext_485.tx_len)
		{
			//	Trasmette il carattere successivo e riattiva l'interrupt di trasmissione
			USART3->TDR = (uint8_t)ext_485.tx_buffer[ext_485.tx_cnt++];
			__HAL_UART_ENABLE_IT(&huart3, UART_IT_TC);
		}
		//	Trasmissione terminata
		else
		{
			//	Toglie il DE e abilita la ricezione
			DEREEXT_OFF;
			__HAL_UART_DISABLE_IT(&huart3, UART_IT_TC);
			__HAL_UART_ENABLE_IT(&huart3, UART_IT_RXNE);
		}
	}
 }

/********************************************************************************************/
