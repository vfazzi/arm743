/********************************************************************************************/
/*	Net485_Include.c			- Gestione UART6 e rete 485 interna							*/

/********************************************************************************************/


#ifndef	__SERIALI_DEFINED
#define	__SERIALI_DEFINED

//	Definizioni base protocollo CEIABI
#define DLE					0x10
#define STX					0x02
#define ETX					0x03
//	Macro
#define INS_DLE_CHK(x)         if((x==DLE)||(x==STX)||(x==ETX))RS485->tx_buffer[indBuff++]=DLE;RS485->tx_buffer[indBuff++]=x;chk+=x
#define INS_DLE(x)             if((x==DLE)||(x==STX)||(x==ETX))RS485->tx_buffer[indBuff++]=DLE;RS485->tx_buffer[indBuff++]=x

//	Dimensione massima buffers
#define MAX_BUFF_TX485	256
#define MAX_BUFF_RX485	256
#define MAX_BUFF_AZ		256


//	Indici campi del pacchetto CEIABI (indici dei vari campi nel pacchetto)
enum enmPaccCeiabi
{
    PACC_LMSG,			//	Lunghezza messaggio
	PACC_DEST=3,		//	Indirizzo di destinazione
	PACC_MITT,			//	Indirizzo mittente
	PACC_COMANDO,		//	Codice comando
    PACC_D0,			//	Primo byte di dati
	PACC_D1, PACC_D2, PACC_D3,
	PACC_D4, PACC_D5, PACC_D6, PACC_D7,
	PACC_D8, PACC_D9, PACC_D10,	PACC_D11,
	PACC_D12, PACC_D13, PACC_D14, PACC_D15,
	PACC_D16, PACC_D17, PACC_D18, PACC_D19,
    PACC_D20, PACC_D21, PACC_D22, PACC_D23,
	PACC_D24, PACC_D25, PACC_D26, PACC_D27,
	PACC_D28, PACC_D29, PACC_D30, PACC_D31,
	PACC_D32, PACC_D33, PACC_D34, PACC_D35,
	PACC_D36
};

typedef struct net485SendMsgData
{
	uint8_t	destAddr;
	uint8_t	msgCmd;
	uint8_t	msgData [ 64 ];
	uint8_t	msgDataSize;

} net485SendMsgData_t;


typedef struct	{
	struct{
		unsigned rx_on	:1;
		unsigned tx_on	:1;
		unsigned received 	:1;
	}stato;
	unsigned char rx_buffer[MAX_BUFF_RX485];
	unsigned char tx_buffer[MAX_BUFF_TX485];
	int rx_index;
	int tx_cnt;
	int tx_len;
	unsigned char address;
}_rs485;

bool N485_IntCeiabiRX(unsigned char sDato);
bool N485_ExtCeiabiRX(unsigned char sDato);
bool TestCHK(unsigned char* pBuff, int len);


// Funzioni
extern void N485_IntNetworkInit ( void );
extern void UART6_485Interrupt ( void );
extern void N485_IntPollingProc();
extern void N485_ExtNetworkInit ( void );
extern void UART3_485Interrupt ( void );
extern void N485_ExtPollingProc();

extern _rs485	int_485;
extern _rs485	ext_485;


#endif


