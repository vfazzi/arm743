/********************************************************************************************/
/*	Net485_Int.c				- Gestione UART6 e rete 485 interna							*/

/********************************************************************************************/
#include "SYS_Include.h"
#include "APP_Include.h"

#ifdef USART6_INT_485_ENABLED

_rs485	int_485;
_rs485	ext_485;

//	Dichiarazione funzioni interne
void N485_IntStartTX(void);
void CreaPacchetto(_rs485* RS485, unsigned char dest, unsigned char op, unsigned char* pData, int lenData);

/********************************************************************************************/
/*	Funzioni del modulo																		*/
/*																							*/
/*	N485_IntNetworkInit ( )			- Inizializzazione rete interna							*/
/*	UART6_485Interrupt ( )			- Interrupt UART 										*/
/*	SerialeInt ( )					- Gestione della comuncazione a polling					*/
/********************************************************************************************/
/*------------------------------------------------------------------------------------------*/
/*	IntNetworkInit ( )				- Inizializzazione rete interna							*/
/*------------------------------------------------------------------------------------------*/
void N485_IntNetworkInit ( void )
{
	DEREINT_OFF;
	int_485.address= sysCfg.m_intNetMasterAddress;
	HAL_UART_MspInit ( &huart6 );
    //	Abilitazione interrupt
	__HAL_UART_DISABLE_IT( &huart6, UART_IT_TC);
	__HAL_UART_ENABLE_IT( &huart6, UART_IT_RXNE );
	__HAL_UART_CLEAR_IT( &huart6, UART_IT_RXNE );
}
/*------------------------------------------------------------------------------------------*/
/*	N485_IntPollingProc ( )			- Gestione della comuncazione a polling					*/
/*------------------------------------------------------------------------------------------*/
void N485_IntPollingProc()
{
	//	Struttura messaggio
    net485SendMsgData_t	msgTx;

    if ( isSysTimerExpired ( SYS_TIMER_485INT ) )
    {
        //	Richiede il messaggio da trasmettere
    	msgTx.msgData [ 0 ] = 0;
    	msgTx.msgDataSize = 0;
        msgTx.destAddr = APP_485InternalNetworkGetNextScanAddress( msgTx.destAddr );
        //	Modifica del messaggio predefinito
        VARCO_485InternalNetworkSendMsgCallback ( &msgTx );
        CreaPacchetto( &int_485, msgTx.destAddr, msgTx.msgCmd, msgTx.msgData, msgTx.msgDataSize);
        N485_IntStartTX(); // Send
        SysTimerSet ( SYS_TIMER_485INT, INT_485_TIMEOUT_POLLING );
    }
    else if(int_485.stato.received)
    {
        // analizza pacchetto
        int_485.stato.received=false;
        VARCO_485InternalNetworkRcvMSgCallback ( );
    }
}


/********************************************************************************************/
/*	Funzioni interne																		*/
/*																							*/
/*	N485_IntCeiabiRX ( )			- Ricezione carattere CEIABI							*/
/*	N485_IntStartTX ( )				- Avvia la trasmissione di un messaggio CEIABI			*/
/*	UART6_485Interrupt ( )			- Interrupt UART 										*/
/********************************************************************************************/
/*------------------------------------------------------------------------------------------*/
/*	N485_IntCeiabiRX ( )				- Ricezione carattere CEIABI							*/
/*------------------------------------------------------------------------------------------*/
bool N485_IntCeiabiRX(unsigned char sDato)
{
	//dbg_printf("%X ",sDato);
    static bool sInAttesa=true,sRiceve=false,sDle=false;
	bool sRicevuto=false;
    if(sInAttesa)
	{
		if(sDato==STX) // Inizio trasmissione il carattere STX è scartato
		{
			sRiceve=true;
			sDle=sInAttesa=false;
			int_485.rx_index=0;
		}
		else // Errore nella sequenza di ricezione.
		{
			sInAttesa=true;
			sRiceve=sDle=false;
		}
	}
	else if(sRiceve) // Stato ricezione.
	{
		if(sDato==ETX)
		{
			int_485.rx_buffer[int_485.rx_index++]=sDato;
			sRicevuto=true;
			sInAttesa=true;
			sRiceve=sDle=false;
		}
		else if(sDato==STX) // Errore nella sequenza di ricezione.
		{
			sInAttesa=true;
			sRiceve=sDle=false;
		}
		else if(sDato==DLE) // Carattere da scartare prossimo buono.
		{
			sDle=true;
			sRiceve=sInAttesa=false;
		}
		else // Carattere normale da mettere nel Buffer.
		{
			if(!sRicevuto && int_485.rx_index<MAX_BUFF_RX485-1)
			{
				int_485.rx_buffer[int_485.rx_index++]=sDato;
				sRiceve=true;
			}
			else
			{
				sInAttesa=true;
				sRiceve=sDle=false;
			}
		}
	}
	else if(sDle)// Stato analizza carattere dopo aver ricevuto un  DLE.
	{
		sDle=false;
		if(sDato==STX || sDato==ETX || sDato==DLE)
		{
			sInAttesa=false;
			if(!sRicevuto && int_485.rx_index<MAX_BUFF_RX485-1)// Dato nel buffer e si ritorna allo stato ricezione.
			{
				int_485.rx_buffer[int_485.rx_index++]=sDato;
				sRiceve=true;
			}
			else
			{
				sInAttesa=true;
				sRiceve=false;
			}
		}
		else // Errore si ritorna in attesa.
		{
			sInAttesa=true;
			sRiceve=false;
		}
	}
	return sRicevuto;
}
/*------------------------------------------------------------------------------------------*/
/*	N485_IntStartTX ( )				- Avvia la trasmissione di un messaggio CEIABI			*/
/*------------------------------------------------------------------------------------------*/
void N485_IntStartTX(void)
{
	__HAL_UART_DISABLE_IT(&huart6, UART_IT_RXNE);
	DEREINT_ON;
	int_485.tx_cnt = 1;
	USART6->TDR = (uint8_t)int_485.tx_buffer[0];
	LL_USART_ClearFlag_TC(USART6);
	__HAL_UART_ENABLE_IT(&huart6, UART_IT_TC);
}
/*------------------------------------------------------------------------------------------*/
/*	UART6_485Interrupt ( )			- Interrupt UART 										*/
/*------------------------------------------------------------------------------------------*/
void UART6_485Interrupt ( void )
{

	// INTERRUPT DI RICEZIONE 485 rete interna (UART6)
	if( LL_USART_IsActiveFlag_RXNE_RXFNE(USART6))  // Receiver
	{
		int_485.stato.rx_on = true;
		//	Ricezione carattere
		if(N485_IntCeiabiRX((uint8_t)(USART6->RDR & 0xFF)))
		{
			int_485.stato.rx_on=false;
			if(TestCHK(int_485.rx_buffer,int_485.rx_index))
			{
				//	Messaggio valido
				int_485.stato.received=true;
			}
			else
			{
				int_485.stato.received=false;
				APP_485InternalNetworkRcvMsgError ( );
			}
		}
		//	Buffer overrun
		if ( int_485.rx_index > MAX_BUFF_RX485)	int_485.rx_index = 0;
		//	Timer di controllo ricezione
		SysTimerSet ( SYS_TIMER_485INT_RXTIMEOUT, mSEC(5));
	}


	// INTERRUPT DI TRASMISSIONE 485 rete interna (UART6)
	if(LL_USART_IsActiveFlag_TC(USART6))
	{
		// 	Cancella l'interrupt
		LL_USART_ClearFlag_TC(USART6);
		//	Controlla se ci sono altri caratteri da trasmettere
		if (int_485.tx_cnt < int_485.tx_len)
		{
			//	Trasmette il carattere successivo e riattiva l'interrupt di trasmissione
			USART6->TDR = (uint8_t)int_485.tx_buffer[int_485.tx_cnt++];
			__HAL_UART_ENABLE_IT(&huart6, UART_IT_TC);
		}
		//	Trasmissione terminata
		else
		{
			//	Toglie il DE e abilita la ricezione
			DEREINT_OFF;
			__HAL_UART_DISABLE_IT(&huart6, UART_IT_TC);
			__HAL_UART_ENABLE_IT(&huart6, UART_IT_RXNE);
		}
	}

	//	Fine ricezione (non arrivano caratteri da più di 5 ms)
	if ( isSysTimerExpired( SYS_TIMER_485INT_RXTIMEOUT ) )
	{
		SysTimerOff ( SYS_TIMER_485INT_RXTIMEOUT );
		int_485.stato.rx_on=false;
	}

}
#endif

/********************************************************************************************/
