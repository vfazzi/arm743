/********************************************************************************************/
/*	LIB_IO.C			- Gestione I/O della scheda tramite SPI								*/

/********************************************************************************************/
#include "SYS_include.h"
#include "APP_Include.h"

//	Maschera bit
const uint16_t mask_ON [] = {
	0x0001, 0x0002, 0x0004, 0x0008,
	0x0010, 0x0020, 0x0040, 0x0080,
	0x0100, 0x0200, 0x0400, 0x0800,
	0x1000, 0x2000, 0x4000, 0x8000
};

systemIO _sysIO;

/********************************************************************************************/
/*	Funzioni del modulo																		*/
/*																							*/
/* 	IO_SetOutput ( )				- Controllo uscita locale								*/
/*	IO_ReadOutput ( )				- Legge lo stato di un'uscita locale					*/
/*	IO_ReadInput ( )				- Legge lo stato di un ingresso	locale					*/
/* 	IO_SetOutputSlave ( )			- Controllo uscita in scheda slave						*/
/*	IO_ReadOutputSlave ( )			- Legge lo stato di un'uscita in scheda slave			*/
/*	IO_ReadInputSlave ( )			- Legge lo stato di un ingresso	ins cheda slave			*/
/*																							*/
/*	IO_Polling ( )					- Comunicazione col ssitema di I/O						*/
/********************************************************************************************/
/*------------------------------------------------------------------------------------------*/
/* 	IO_SetOutput ( )				- Controllo uscita locale								*/
/*	nOut	= numero uscita da controllare													*/
/*	bStatus	= stato uscita																	*/
/*------------------------------------------------------------------------------------------*/
void IO_SetOutput(unsigned short nOut, bool bStatus)
{
#ifdef IO_BASE_1
	nOut --;
#endif
    if(nOut < NUM_OUTPUT )
    {
    	_sysIO.out = bStatus ? _sysIO.out | mask_ON [ nOut ] : _sysIO.out & ~mask_ON [ nOut ];
    }
}
/*------------------------------------------------------------------------------------------*/
/*	IO_ReadOutput ( )				- Legge lo stato di un'uscita locale					*/
/*	nOut	= numero uscita da leggere														*/
/*------------------------------------------------------------------------------------------*/
bool IO_ReadOutput(unsigned short nOut )
{
#ifdef IO_BASE_1
	nOut --;
#endif
    if(nOut < NUM_OUTPUT )
    {
		return ( _sysIO.out & mask_ON [ nOut ] );
    }
    return(false);
}
/*------------------------------------------------------------------------------------------*/
/*	IO_ReadInput ( )				- Legge lo stato di un ingresso	locale					*/
/*	nIn		= numero ingresso da leggere													*/
/*------------------------------------------------------------------------------------------*/
bool IO_ReadInput(unsigned short nIn)
{
#ifdef IO_BASE_1
	nIn --;
#endif
	if ( nIn < NUM_INPUT )
	{
		return ( _sysIO.ing & mask_ON [ nIn ] );
	}
	return ( false );
}
/*------------------------------------------------------------------------------------------*/
/* 	IO_SetOutputSlave ( )			- Controllo uscita in scheda slave						*/
/*------------------------------------------------------------------------------------------*/
void IO_SetOutputSlave(unsigned short nOut, bool bStatus)
{
#ifdef IO_BASE_1
	nOut --;
#endif
    if(nOut < NUM_OUTPUT )
    {
    	_sysIO.outSlave = bStatus ? _sysIO.outSlave | mask_ON [ nOut ] : _sysIO.outSlave & ~mask_ON [ nOut ];
    }
}
/*------------------------------------------------------------------------------------------*/
/*	IO_ReadOutputSlave ( )			- Legge lo stato di un'uscita in scheda slave			*/
/*------------------------------------------------------------------------------------------*/
bool IO_ReadOutputSlave(unsigned short nOut)
{
#ifdef IO_BASE_1
	nOut --;
#endif
    if(nOut < NUM_OUTPUT )
    {
		return ( _sysIO.outSlave & mask_ON [ nOut ] );
    }
    return(false);
}
/*------------------------------------------------------------------------------------------*/
/*	IO_ReadInputSlave ( )			- Legge lo stato di un ingresso	ins cheda slave			*/
/*------------------------------------------------------------------------------------------*/
bool IO_ReadInputSlave(unsigned short nIn)
{
#ifdef IO_BASE_1
	nIn --;
#endif
	if ( nIn < NUM_INPUT )
	{
		return ( _sysIO.ingSlave & mask_ON [ nIn ] );
	}
	return ( false );
}

/*------------------------------------------------------------------------------------------*/
/*	IO_Polling ( )					- Comunicazione col ssitema di I/O						*/
/*------------------------------------------------------------------------------------------*/
void IO_Polling( void )
{
	unsigned short local_out, local_ing;
	local_out = _sysIO.out;
	PLI_LATCH;
	HAL_SPI_TransmitReceive( &hspi1,((unsigned char*)&local_out),((unsigned char*)&local_ing),2,1);
	PLO_LATCH;
	_sysIO.ing = ~local_ing;
}

/********************************************************************************************/
