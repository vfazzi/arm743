/********************************************************************************************/
/*	LIB_IO.h			- Gestione I/O della scheda tramite SPI								*/

/********************************************************************************************/

#ifndef	__LIB_IO_DEFINED
#define __LIB_IO_DEFINED


#define NUM_INPUT 16
#define NUM_OUTPUT 16

/*------------------------------------------------------------------------------------------*/
/*	Dichiarazione funzioni																	*/
/*------------------------------------------------------------------------------------------*/
void IO_SetOutput(unsigned short nOut, bool bStatus);
bool IO_ReadOutput(unsigned short nOut );
bool IO_ReadInput(unsigned short nIn);
void IO_SetOutputSlave(unsigned short nOut, bool bStatus);
bool IO_ReadOutputSlave(unsigned short nOut);
bool IO_ReadInputSlave(unsigned short nIn);

void IO_Polling( void );


/*------------------------------------------------------------------------------------------*/
/*	Dichiarazione struttura I/O																*/
/*------------------------------------------------------------------------------------------*/
typedef struct
{
	unsigned short out;			//	Uscite locali (scheda master)
	unsigned short outSlave;	//	Uscite scheda slave
	unsigned short ing;			//	Ingressi locali (scheda master)
	unsigned short ingSlave;	//	Ingressi scheda slave
} systemIO;


#endif
