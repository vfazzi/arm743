/********************************************************************************************/
/*	VARCO_ExtNetwork.c			- Applicazione VARCO - Comunicazione rete 485 esterna 		*/

/********************************************************************************************/
// Include di sistema e applicazione
#include "SYS_Include.h"
#include "APP_Include.h"


/********************************************************************************************/
/*	Funzioni del modulo																		*/
/*																							*/
/*	VARCO_485ExternalNetworkRcvMSgCallback ( )	- Comunicazione su rete esterna				*/
/********************************************************************************************/
bool inline VARCO_485ExternalNetworkRcvMSgCallback ( uint8_t* pRxBuff, net485SendMsgData_t* msgTx )
{
	int mask=1;

	//	Queste servono solo per semplificare il porting
	#define	txData		msgTx->msgData
	#define lenData		msgTx->msgDataSize

	//	Gestione comando ricevuto
	switch ( pRxBuff [ PACC_COMANDO ])
	{
		case COM485_AGR_STATE:
			//	Oggetto master
			if ( !isMaster ) return ( false );
			//	Controllo indirizzi
			if ( pRxBuff [ PACC_D0 ] != COM485_AGR_GETSTATE &&  pRxBuff [ PACC_D0 ] != COM485_AGR_GETSTATE_1 ) 
				return ( false );

			txData [ 0 ] = pRxBuff [ PACC_D0 ];
			// 	Per non far accendere il led ACCENSIONE sulla consolle di Iuppiter
			txData [ 2 ] = 1;
			if ( sEmergenza )
				txData[2]|=0x80;

			if ( PorteChiuse ( ) )
				txData [ 4 ] |= 0x03; // entrambi i bit attivi

			// presenza rete (OFF quando in batteria)
			if ( !IN_AZ_NO_RETE )
				txData [ 4 ] |= 0x10;

			txData [ 5 ]= azMaster.encoder >> 8;
			txData [ 6 ]= azMaster.encoder & 0xFF;
			txData [ 7 ]= azSlave.encoder >> 8;
			txData [ 8 ]= azSlave.encoder & 0xFF;

			if ( SEL1_MOD_ING != MOD_LIBERO && !(statoVarco >= ST_INIZIO_TR_I && statoVarco <= ST_FINE_TR_I) )
				txData [ 9 ] |= 0x04;

			if ( SEL2_MOD_USC !=MOD_LIBERO &&  !(statoVarco >= ST_INIZIO_TR_U && statoVarco <= ST_FINE_TR_U) )
				txData [ 9 ] |= 0x08;

			if ( statoVarco == ST_ALLARME )
				txData [ 9 ] |= 0x20;

			if ( SEL1_MOD_ING == MOD_LIBERO )
				txData [ 10 ] |= 0x01;
			else if ( SEL1_MOD_ING == MOD_BLOCCATO )
				txData[10] |= 0x40;

			if ( SEL2_MOD_USC == MOD_LIBERO )
				txData [ 10 ] |= 0x02;
			else if ( SEL2_MOD_USC == MOD_BLOCCATO )
				txData[10] |= 0x80;

			if(allarmi.loitering)		txData[11]|=0x01;
			if(allarmi.accodamento)		txData[11]|=0x02;
			if(allarmi.controflusso)	txData[11]|=0x04;
			if(allarmi.err_motore)		txData[11]|=0x08; // non gestito ancora
			if(allarmi.forzatura)		txData[11]|=0x10;
			if(allarmi.err_fotocellule)	txData[11]|=0x20; // non gestito ancora

			lenData=12;
		break;
		case COM_BROADCAST_AGR:
			if ( !isMaster ) return ( false );
			txData [ 0 ] = pRxBuff [ PACC_D0 ];
			lenData=1;
			switch ( pRxBuff [ PACC_D0 ] )
			{
				case COM485_AGR_DO_EMERG_ON:
					if ( !sEmergenza )
						sRichEm |= RICH_DA_SERIALE;
				break;
				case COM485_AGR_DO_EMERG_OFF:
					sRichEm &= ~RICH_DA_SERIALE;
				break;
				default:
					return (false);
			}
		break;
		case COM485_AGR_DO:
			if ( !isMaster ) return ( false );

			txData [ 0 ] = pRxBuff [ PACC_D0 ];
			lenData = 1;
			switch ( pRxBuff [ PACC_D0 ] )
			{
				case COM485_IUPPITER_EMERG:
					if ( pRxBuff [ PACC_D1 ] & 0x03 )
						sRichEm |= RICH_DA_SERIALE;
					else
						sRichEm &= ~RICH_DA_SERIALE;
				break;
				case COM485_IUPPITER_OPEN_PORTA:
					if ( pRxBuff [ PACC_D1 ] == 0x01 ) // cfr Apri Interna in Manuale per bussola
					{
						bRichUsc=true;
						sPrenotazUsc=1;
					}
					else if ( pRxBuff [ PACC_D1 ] == 0x02 ) // cfr Apri Esterna in Manuale per bussola
					{
						bRichIng=true;
						sPrenotazIng=1;
					}
				break;
				case COM485_AGR_DO_EMERG_ON:
					if(!sEmergenza)		
						sRichEm |= RICH_DA_SERIALE;
				break;
				case COM485_AGR_DO_EMERG_OFF:
					sRichEm &= ~RICH_DA_SERIALE;
				break;
				case COM485_AGR_DO_LIB_ING:
					SEL1_MOD_ING = MOD_LIBERO;
				break;
				case COM485_AGR_DO_LIB_USC:
					SEL2_MOD_USC = MOD_LIBERO;
				break;
				case COM485_AGR_DO_CONTROL_ING:
					SEL1_MOD_ING = MOD_CONTROLLATO;
				break;
				case COM485_AGR_DO_CONTROL_USC:
					SEL2_MOD_USC = MOD_CONTROLLATO;
				break;
				case COM485_AGR_DO_BLOC_ING:
					SEL1_MOD_ING = MOD_BLOCCATO;
				break;
				case COM485_AGR_DO_BLOC_USC:
					SEL2_MOD_USC = MOD_BLOCCATO;
				break;
				case COM485_AGR_DO_RICH_ING:
					bRichIng = true;
					sPrenotazIng = 1;
				break;
				case COM485_AGR_DO_RICH_USC:
					bRichUsc = true;
					sPrenotazUsc = 1;
				break;
				case COM485_AGR_CONTATORI:
					txData [ 1 ]= cntIngressi>>8;
					txData [ 2 ]= (unsigned char)cntIngressi & 0xFF;
					txData [ 3 ]= cntUscite>>8;
					txData [ 4 ]= (unsigned char)cntUscite & 0xFF;
					lenData=5;
					cntIngressi = cntUscite = 0;
				break;
				default:
					return (false );
			}
		break;	
		case COM485_EDSETUP_CHIEDI_FLAG:
			memcpy ( txData, &sysCfg.m_vttFlag [ 0 ], MAX_FLAG * sizeof(uint8_t) );
			lenData = MAX_FLAG * sizeof(uint8_t); // controllare che sia uguale a ind in debug
		break;	
		case COM485_EDSETUP_CHIEDI_PARAMETRI:
			// non posso usare memcpy perchè inverte MSB con LSB
			for(int ind=0;ind<MAX_PARAM;ind++)
			{
				txData[ind*2]=sysCfg.m_vttParam[ind]>>8;
				txData[ind*2+1]=(unsigned char)sysCfg.m_vttParam[ind]&0xFF;
			}
			lenData=28;
		break;
		case COM485_EDSETUP_CHIEDI_SELEZIONE:
			memcpy ( txData, &sysCfg.m_vttSel [ 0 ], MAX_SEL * sizeof (uint8_t));
			lenData = MAX_SEL * sizeof (uint8_t); // controllare che sia uguale a ind in debug
		break;	
		case COM485_EDSETUP_SCRIVI_FLAG:
			memcpy ( &sysCfg.m_vttFlag [ 0 ],  &pRxBuff [ PACC_D0 ], MAX_FLAG * sizeof(uint8_t) );
		break;	
		case COM485_EDSETUP_SCRIVI_PARAMETRI:
			// non posso usare memcpy perchè inverte MSB con LSB
			for(int ind=0;ind<MAX_PARAM;ind++)
				sysCfg.m_vttParam[ind]=pRxBuff[PACC_D0+ind*2]*256+pRxBuff[PACC_D1+ind*2];		
		break;
		case COM485_EDSETUP_SCRIVI_SELEZIONE:
			memcpy ( &sysCfg.m_vttSel [ 0 ],  &pRxBuff [ PACC_D0 ], MAX_SEL * sizeof(uint8_t) );
			if ( SEL3_TAILGATE_ACCURACY >= MAX_ACCURACY )	SEL3_TAILGATE_ACCURACY = MAX_ACCURACY-1;
		break;
		case COM485_GIRA_AZ:
			if ( pRxBuff [ PACC_D0 ] == DEFAULT_485_AZ_ADDRESS )	
			{
				if(giraAz.bufferFromAz[0]==pRxBuff[PACC_D1]) // se ho già nel buffer la risposta a questo messaggio la inoltro e svuoto il buffer
				{
					lenData=giraAz.nDatiFromAz+2; // tutto il campo dati che mi è arrivato + ind az + comando
					txData[0] = DEFAULT_485_AZ_ADDRESS;
					for ( int ind = 1; ind < lenData; ind++ )
					{
						txData [ ind ] = giraAz.bufferFromAz [ ind - 1 ];
					}
				}
				else if ( ( pRxBuff [ PACC_D1 ] == COM_AZION_CHIEDI_P2 || pRxBuff [ PACC_D1 ] == COM_AZION_IMPOSTA_P2 ) &&
							( !isMaster || isSlaveDead ) )
				{
					// risposta immediata in caso di mancanza slave o interrogazione diretta alla scheda slave
					lenData = ( pRxBuff [ PACC_D1 ] == COM_AZION_IMPOSTA_P2 ) ? 2 : 36;
					txData [ 0 ] = DEFAULT_485_AZ_ADDRESS;
					txData [ 1 ] = pRxBuff [ PACC_D1 ];
					for ( int ind = 2; ind < lenData; ind++ )
						txData [ ind ] = 0;
				}
				else if ( pRxBuff [ PACC_D1 ] == COM485_SAVEPARAM && (!isMaster || isSlaveDead || giraAz.paramSlaveSalvati) )
				{
					// in caso di collegamento con lo slave o master senza slave, inoltro giro solo la richiesta all'azionamento
					giraAz.comando = pRxBuff [ PACC_D1 ];
					giraAz.nDati2Az = pRxBuff [ PACC_LMSG ] - 4;
					giraAz.paramSlaveSalvati = false;
				}
				else if ( pRxBuff [ PACC_D1 ] == COM485_STATI_PC )
				{
					lenData = 11;
					txData[0] = DEFAULT_485_AZ_ADDRESS;
					txData[1] = pRxBuff [ PACC_D1 ];
					txData[2] = azMaster.encoder >> 8;
					txData[3] = azMaster.encoder & 0xFF;
					txData[4] = azSlave.encoder >> 8;
					txData[5] = azSlave.encoder & 0xFF;

					if ( azMaster.porta == STATO_CHIUSA )		txData [ 6 ] |= 0x01;
					if ( azSlave.porta == STATO_CHIUSA )		txData [ 6 ] |= 0x02;
					if ( azMaster.porta & mask_APERTA )			txData [ 6 ] |= 0x04;
					if ( azSlave.porta & mask_APERTA )			txData [ 6 ] |= 0x08;

					txData [ 7 ] = ( azSlave.ing << 3 ) + azMaster.ing;
					txData [ 7 ] = ~txData [ 7 ]; // iuppiter lo visualizza a logica invertita
					txData [ 8 ] = ( azSlave.out << 2 ) + azMaster.out;
				}
				else if ( pRxBuff [ PACC_D1 ] == COM_AZION_CHIEDI_IMPOSTAZIONI || pRxBuff [ PACC_D1 ] == COM_AZION_SCRIVI_IMPOSTAZIONI )
				{
					// riquadro Impostazioni della pag Azinamento di Iuppiter
					lenData = ( pRxBuff [ PACC_D1 ] == COM_AZION_SCRIVI_IMPOSTAZIONI ) ? 2 : 9;
					txData [ 0 ] = DEFAULT_485_AZ_ADDRESS;
					txData [ 1 ] = pRxBuff [ PACC_D1 ];
					for ( int ind = 2; ind < lenData; ind++ )
						txData [ ind ] = 0;
					
					if( pRxBuff [ PACC_D1 ] == COM_AZION_CHIEDI_IMPOSTAZIONI )
						txData[7]=30; // modello macchinario pr 449/466
				}
				else if ( pRxBuff [ PACC_D1 ] == COM_AZION_CHIEDI_P2 && sParamRicevuti&COM_CAN_PARAM_1 && sParamRicevuti&COM_CAN_PARAM_2 )
				{
					sParamRicevuti=0;
					lenData=36;
					// formatto il pacchetto come se fosse stato ricevuto dall'anello interno
					txData [ 0 ] = DEFAULT_485_AZ_ADDRESS;
					txData [ 1 ] = COM_AZION_CHIEDI_P2;
					txData [ 3 ] = azSlave.vel_wu;
					txData [ 5 ] = azSlave.vel_ap;
					txData [ 9 ] = azSlave.vel_ch;
					txData [ 14 ] = azSlave.diff_passi >> 8;
					txData [ 15 ] = azSlave.diff_passi & 0xFF;
					txData [ 16 ] = azSlave.passi_ap >> 8;
					txData [ 17 ] = azSlave.passi_ap & 0xFF;
					txData [ 19 ] = azSlave.kp;
					txData [ 31 ] = azSlave.rampa;
					txData [ 32 ] = azSlave.picco >> 8;
					txData [ 33 ] = azSlave.picco & 0xFF;
					txData [ 34 ] = azSlave.pos_forzatura >> 8;
					txData [ 35 ] = azSlave.pos_forzatura & 0xFF;
				}
				else if ( pRxBuff [ PACC_D1 ] == COM_AZION_IMPOSTA_P2 && sParamTrasmessi&COM_CAN_PARAM_1 && sParamTrasmessi&COM_CAN_PARAM_2 ) // && isTimerExpired(&stimer[TIM_TX_PARAM]))
				{
					ShortTimerSetOff(TIM_TX_PARAM);
					sParamTrasmessi = 0;
					lenData = 2;
					txData [ 0 ] = DEFAULT_485_AZ_ADDRESS;
					txData [ 1 ] = pRxBuff [ PACC_D1 ];
				}
				else // altrimenti inizio la procedura per girare il comando all'azionamento
				{
					if ( pRxBuff [ PACC_D1 ] == COM_AZION_CHIEDI_P2 || pRxBuff [ PACC_D1 ] == COM_AZION_IMPOSTA_P2 || pRxBuff [ PACC_D1 ] == COM485_SAVEPARAM )
						giraAz.gira2Slave = true;
					else
						giraAz.gira2Slave = false;

					giraAz.comando = pRxBuff [ PACC_D1 ];
					giraAz.nDati2Az = pRxBuff [ PACC_LMSG ] - 4;

					if ( giraAz.nDati2Az )
					{
						// se sto ricevendo i dati per impostare la porta 2 li salvo nelle variabili apposta
						if ( pRxBuff [ PACC_D1 ] == COM_AZION_IMPOSTA_P2 )
						{
							azSlave.vel_wu = pRxBuff [ PACC_D3 ];
							azSlave.vel_ap = pRxBuff [ PACC_D5 ];
							azSlave.vel_ch = pRxBuff [ PACC_D9 ];
							azSlave.diff_passi = ( pRxBuff [ PACC_D14 ] << 8 ) + pRxBuff [ PACC_D15 ];
							azSlave.passi_ap = ( pRxBuff [ PACC_D16 ] << 8 ) + pRxBuff [ PACC_D17 ];
							azSlave.kp = pRxBuff [ PACC_D19 ];
							azSlave.rampa = pRxBuff [ PACC_D31 ];
							azSlave.picco = ( pRxBuff [ PACC_D32 ] << 8 ) + pRxBuff [ PACC_D33 ];
							azSlave.pos_forzatura = ( pRxBuff [ PACC_D34 ] << 8 ) + pRxBuff [ PACC_D35 ];
							sComSlave = COM_CAN_PARAM_1;
						}
						else
						{
							for ( int ind = 0; ind < giraAz.nDati2Az; ind++ )
								giraAz.buffer2Az [ ind ] = pRxBuff [ PACC_D2 + ind ];
						}
					}
					//  non rispondo subito, attendo la risposta dell'azionamento (primo if del case)
					// per rispondere va modificato lenData e txData
				}
			}
			else if ( pRxBuff [ PACC_D0 ] == DEFAULT_485_PESO_ADDRESS )
			{
				uint16_t tmpData;
				if( pRxBuff [ PACC_D1 ] == COM485_LEGGI_ORA)
				{
					sysTime	tm;
					TM_GetSystemTime ( &tm );
					tmpData = tm.tm_Sec;
					tmpData += (tm.tm_min<<5);
					tmpData += (tm.tm_hour<<11);

					txData [ 0 ] = DEFAULT_485_PESO_ADDRESS;
					txData [ 1 ] = COM485_LEGGI_ORA;
					txData [ 2 ] = 0x3F;
					txData [ 3 ] = (tmpData>>8)&0xFF;
					txData [ 4 ] = tmpData&0xFF;

					lenData=5;
				}
				else if( pRxBuff [ PACC_D1 ] == COM485_LEGGI_DATA)
				{
					sysDate	dt;
					TM_GetSystemDate ( &dt );

					tmpData = dt.dt_day;
					tmpData += (dt.dt_month<<5); 
					tmpData += ((dt.dt_year-2000)<<9); 

					txData [ 0 ] = DEFAULT_485_PESO_ADDRESS;
					txData [ 1 ] = COM485_LEGGI_DATA;
					txData [ 2 ] = 0x3F;
					txData [ 3 ] = 0;		// giorno della settimana
					txData [ 4 ] = (tmpData>>8)&0xFF;
					txData [ 5 ] = tmpData&0xFF;

					lenData=6;
				}
				else if(pRxBuff [ PACC_D1 ] == COM485_SET_DATA_ORA)
				{
					sysDate date;
					sysTime tm;
					tm.tm_hour = (pRxBuff [ PACC_D2 ] / 0x10)*10 + (pRxBuff [ PACC_D2 ] % 0x10);
					tm.tm_min = (pRxBuff [ PACC_D3 ] / 0x10)*10 + (pRxBuff [ PACC_D3 ] % 0x10);
					date.dt_day = (pRxBuff [ PACC_D4 ] / 0x10)*10 + (pRxBuff [ PACC_D4 ] % 0x10);
					date.dt_month = (pRxBuff [ PACC_D5 ] / 0x10)*10 + (pRxBuff [ PACC_D5 ] % 0x10);
					date.dt_year = 2000 + (pRxBuff [ PACC_D6 ] / 0x10)*10 + (pRxBuff [ PACC_D6 ] % 0x10);
					TM_SetSystemDate(&date);
					TM_SetSystemTime(&tm);

					txData [ 0 ] = DEFAULT_485_PESO_ADDRESS;
					txData [ 1 ] = COM485_SET_DATA_ORA;

					lenData=2;
				}
			}	
			else
				return (false);
		break;
		case COM485_TRASM_I_O:
			// Trasmissione ingressi
			mask=1;
			for (int ind=1;ind<=8;ind++)
			{
				if ( IO_ReadInput ( ind ) ) 			txData [ 0 ] |= mask;
				if ( IO_ReadInput ( ind + 8 )) 			txData [ 1 ] |= mask;
				if ( IO_ReadInputSlave ( ind ) ) 		txData [ 2 ] |= mask;
				if ( IO_ReadInputSlave ( ind + 8 ) ) 	txData [ 3 ] |= mask;
				mask <<= 1;
			}
			// inviare a logica invertita
			for ( int ind = 0; ind <= 3; ind++ )
				txData [ ind ] = ~txData [ ind ];

			// Trasmissione uscite - mappate su OUT 1-4 di Iuppiter
			mask=1;
			for ( int ind = 1; ind <= 4; ind++ )
			{
				if ( IO_ReadOutput ( ind ) ) 			txData [ 18 ] |= mask;
				if ( IO_ReadOutput ( ind + 8 ) ) 		txData [ 19 ] |= mask;
				if ( IO_ReadOutputSlave ( ind ) ) 		txData [ 20 ] |= mask;
				if ( IO_ReadOutputSlave ( ind + 8 ) ) 	txData [ 21 ] |= mask;
				mask <<= 1;
			}

			// Trasmissione uscite - mappate su A B di Iuppiter - logica invertita rispetto a prima
			// non ci vuole mask=1; perchè continuo a modificare i bit successivi degli stessi byte
			for ( int ind = 5; ind <= 6; ind++ )
			{
				if ( !IO_ReadOutput ( ind ) ) 			txData [ 18 ] |= mask;
				if ( !IO_ReadOutput ( ind + 8 ) ) 		txData [ 19 ] |= mask;
				if ( !IO_ReadOutputSlave ( ind ) ) 	txData [ 20 ] |= mask;
				if ( !IO_ReadOutputSlave ( ind + 8 ) ) txData [ 21 ] |= mask;
				mask <<= 1;
			}

			// Trasmissione uscite - mappate su IV0 e IV1 di Iuppiter
			mask = 0x80;
			for ( int ind = 7; ind <= 8; ind++ )
			{
				if ( IO_ReadOutput ( ind ) ) 			txData [ 9 ] |= mask;
				if ( IO_ReadOutput ( ind + 8 ) ) 		txData [ 10 ] |= mask;
				if ( IO_ReadOutputSlave ( ind ) ) 		txData [ 11 ] |= mask;
				if ( IO_ReadOutputSlave ( ind + 8 ) ) 	txData [ 12 ] |= mask;
				mask >>= 1;
			}
			// inviare a logica invertita
			for ( int ind=18; ind <=21; ind++ )
				txData [ ind ] = ~txData [ ind ];

			txData [ 27 ] = 0x0F; // così da far visualizzare a Iuppiter fino a slave 3
			lenData = 29;
		break;
		case COM485_VERSIONE:
			strcpy ( (char*)txData, _CODE_PROG );
			lenData = 12;
		break;
		case COM485_RICHIESTA_VERSIONE:
			txData [ 0 ] = lAnagrafica >> 24;
			txData [ 1 ] = lAnagrafica >> 16;
			txData [ 2 ] = lAnagrafica >> 8;
			txData [ 3 ] = (unsigned char)lAnagrafica;
			lenData = 4;
		break;
		case COM485_SERIAL:
			txData [ 0 ] = 0x12;
			txData [ 1 ] = 0x23;
			lenData = 2;
		break;	
		case COM485_SAVEPARAM:
			APP_AsyncSaveSettings ( ); //TODO check
			lenData = 0;
		break;
		case COM485_GETNET:
			for(int ind=0;ind<4;ind++)
			{
				txData[ind]=sysCfg.m_ipAddress[ind];
				txData[ind+4]=sysCfg.m_subnetMask[ind];
				txData[ind+8]=sysCfg.m_gatewayAddress[ind];
			}
			for(int ind=0;ind<6;ind++)
				txData[ind+12]=sysCfg.m_macAddress[ind];
			
			lenData = 18;
		break;	
		case COM485_SETNET:
			if(pRxBuff[PACC_LMSG]==14)
				for(int ind=0;ind<4;ind++)
				{
					sysCfg.m_ipAddress[ind]=pRxBuff[PACC_D0+ind];
					sysCfg.m_subnetMask[ind]=pRxBuff[PACC_D4+ind];
					sysCfg.m_gatewayAddress[ind]=pRxBuff[PACC_D8+ind];
				}
		break;
		default:
			return (false); // comando sconosciuto, esco dalla funzione
	}

	if ( pRxBuff [ PACC_DEST ] == IND_485_BROADCAST) return (false);

	//	Trasmette la risposta
	return (true);
}

