/********************************************************************************************/
/*	APP_Can.c					- Funzioni gestione comunicazione tramite CAN				*/

/********************************************************************************************/
// Include di sistema e applicazione
#include "SYS_Include.h"
#include "APP_Include.h"


/********************************************************************************************/
/*	Funzioni del modulo																		*/
/*																							*/
/*	VARCO_TermOpenCommand ( )			- Comando apertura porta							*/
/*	VARCO_TermDebugInfoCommand ( )		- Visualizza le informazioni di debug				*/
/********************************************************************************************/
/*------------------------------------------------------------------------------------------*/
/*	VARCO_TermOpenCommand ( )			- Comando apertura porta							*/
/*------------------------------------------------------------------------------------------*/
void VARCO_TermOpenCommand ( int argc, char **argv )
{
	if ( ! strcmpi ( argv [ 1 ], "in" ) )
	{
		bRichIng = true;
		sPrenotazIng = 1;
		dbg_printf ( "Apriti sesamo Ingresso!\r\n>" );
	}
	else if ( ! strcmpi ( argv [ 1 ], "out" ) )
	{
		bRichUsc = true;
		sPrenotazUsc = 1;
		dbg_printf ( "Apriti sesamo Uscita!\r\n>" );
	}
}
/*------------------------------------------------------------------------------------------*/
/*	VARCO_TermDebugInfoCommand ( )		- Visualizza le informazioni di debug				*/
/*------------------------------------------------------------------------------------------*/
void VARCO_TermDebugInfoCommand ( int argc, char **argv )
{
	char str[32];
	if ( GetTextStatus ( statoVarco, str ) )
	{
		dbg_printf ( "Stato Varco: %s\r\n", str );
	}
	else
	{
		dbg_printf ( "Stato Varco: %d\r\n", statoVarco );
	}
	if ( statoVarco == ST_ALLARME )
	{
		dbg_printf ( "forzatura: %d   loitering: %d   controflusso: %d\r\n",
					 allarmi.forzatura,
					 allarmi.loitering,
					 allarmi.controflusso );
		dbg_printf ( "accodamento: %d azionamento: allarme master %d   allarme slave %d\r\n",
					 allarmi.accodamento,
					 azMaster.allarme,
					 azSlave.allarme );
	}
	dbg_printf("\r\n>");
}

