/********************************************************************************************/
/*	VARCO_Code.c				- Applicazione VARCO - Codice principale					*/

/********************************************************************************************/
// Include di sistema e applicazione
#include "SYS_Include.h"
#include "APP_Include.h"

//#define SEM_SEGUI_TRANSITO

/********************************************************************************************/
/*	Dichiarazione variabili globali															*/
/********************************************************************************************/
extern unsigned char net485InternalDevAddr [];
_azionamento 		azMaster;
_azionamento 		azSlave;
_giraAz 			giraAz={0};
enmStatoVarco_t 	statoVarco;
bool 				sEmergenza;
uint32_t			lAnagrafica=0;
_allarmi 			allarmi;
bool 				sBuzzer=false;
bool 				bRichIng=false;
bool 				bRichUsc=false;
unsigned char 		sRichEm=0;
unsigned char 		sPrenotazIng;
unsigned char 		sPrenotazUsc;
unsigned int 		cntIngressi;
unsigned int 		cntUscite;
unsigned char 		sParamRicevuti=0;
unsigned char 		sParamTrasmessi=0;
unsigned char 		sComSlave=COM_CAN_STATUS;
unsigned int 		counterCicliBreath=0;
_semafori 			semafori;

/********************************************************************************************/
/*	Funzioni del modulo																		*/
/*																							*/
/*	VARCO_Startup()						- Avvio applicazione							*/
/*	VARCO_ThreadCreate()					- Creazione thread applicazione					*/
/*	VARCO_InitMainThread()				- Inizializzazione thread primario				*/
/*	VARCO_MainLoopProc()					- Main loop										*/
/*	VARCO_SystemTimerCallback()			- Timer di sistema 1ms							*/
/********************************************************************************************/
/*------------------------------------------------------------------------------------------*/
/*	VARCO_Startup()						- Avvio applicazione							*/
/*------------------------------------------------------------------------------------------*/
void inline VARCO_Startup(void )
{
	//	UScite tutte OFF
	for(int i=1; i <= 16; i++) IO_SetOutput(i, false);

	//	Segnalazione LED - Avvio del sistema
	for(int i=0 ; i < 8; i++)
	{
		LEDR_ON;
		//LEDV_ON;
		HAL_Delay(200);
		// LEDR_ON;
		// LEDV_OFF;
		// HAL_Delay(100);
		LEDR_OFF;
		LEDV_OFF;
		HAL_Delay(100);
	}
	//	Imposta la modalità master/slave
	if(IN_VARCO_SLAVE)
	{
		isMaster=false;
		printf("Configurazione scheda: SLAVE\r\n>");
	}
	else
	{
		isMaster=true;
		printf( "Configurazione scheda: MASTER\r\n>");
	}

}
/*------------------------------------------------------------------------------------------*/
/*	VARCO_ThreadCreate()					- Creazione thread applicazione					*/
/*------------------------------------------------------------------------------------------*/
void inline VARCO_ThreadCreate()
{

}
/*------------------------------------------------------------------------------------------*/
/*	VARCO_InitMainThread()				- Inizializzazione thread primario				*/
/*------------------------------------------------------------------------------------------*/
void inline VARCO_InitMainThread(void )
{
	//	Inizializza i timer
}
/*------------------------------------------------------------------------------------------*/
/*	VARCO_MainLoopProc()					- Main loop										*/
/*------------------------------------------------------------------------------------------*/
void inline VARCO_MainLoopProc(void )
{
	// Inserire un piccolo ritardo ad ogni passaggio per non saturare la CPU (anche 1ms)
	osDelay(1);
	if(isMaster) gestSwingate();

}
/*------------------------------------------------------------------------------------------*/
/*	VARCO_HiSpeedTimerCallback()			- Timer hardware veloce							*/
/*------------------------------------------------------------------------------------------*/
void inline VARCO_HiSpeedTimerCallback(void )
{
	Breath(false);
}

/********************************************************************************************/
/*	Funzioni interne del modulo																*/
/*																							*/
/*	Breath()																				*/
/*	GetTextStatus	( )																		*/
/*	gestSemafori()																		*/
/*	IngressoOccupato()																	*/
/*	UscitaOccupata()																		*/
/*	gestSwingate()																		*/
/*	tailgate()																			*/
/*	controlli()																			*/
/*	ComandoPorta()																		*/
/********************************************************************************************/

/*------------------------------------------------------------------------------------------*/
/*	gestSwingate()																		*/
/*------------------------------------------------------------------------------------------*/
void gestSwingate()
{
    static unsigned char gestForzatura=0;
    static bool bVisteAperte=false; // per aprire le porte in emergenza e poi lasciarle libere
    //static enmStatoVarco_t oldStato=ST_WARMUP;
    static bool bTransNonEffettuato=false;

    gestSemafori();
    controlli();

    // if(oldStato!=statoVarco)
    // {
    //     char strOld[20],str[20];

    //     if(GetTextStatus(oldStato,strOld) && GetTextStatus(statoVarco,str))
    //         dbg_printf("\r\n[Cambio Stato] -> %s\r\n",str);
    //     else
    //         dbg_printf("\r\n[Cambio Stato] -> %d\r\n",statoVarco);
    //     oldStato=statoVarco;
    //     if(statoVarco==ST_ALLARME)
    //     {
    //         dbg_printf("forzatura: %d   loitering: %d   controflusso: %d\r\n",allarmi.forzatura,allarmi.loitering,allarmi.controflusso);
    //         dbg_printf("accodamento: %d azionamento: allarme master %d   allarme slave %d\r\n",allarmi.accodamento,azMaster.allarme,azSlave.allarme);
    //     }
    // }

    // Gestione antiaccodamento
    if(statoVarco>=ST_INIZIO_TR_I && statoVarco<=ST_FINE_TR_I)
    {
        if(tailgate(false,DIR_INGRESSO,&sPrenotazIng))
        {
            statoVarco=ST_ALLARME;
            allarmi.accodamento=true;
        }
    }
    else if(statoVarco>=ST_INIZIO_TR_U && statoVarco<=ST_FINE_TR_U)
    {
        if(tailgate(false,DIR_USCITA,&sPrenotazUsc))
        {
            statoVarco=ST_ALLARME;
            allarmi.accodamento=true;
        }
    }
    else
        tailgate(true,0,0);
    // Fine gestione antiaccodamento

    if(sRichEm && statoVarco!=ST_WARMUP)
        statoVarco=ST_EMERGENZA;


    switch(statoVarco)
    {
	case ST_WARMUP:
		SetSemafIng(SEM_GIALLO);
		SetSemafUsc(SEM_GIALLO);

		if(!IN_RICH_EMERGENZA)
		{
			ComandoPorta(MOT_CHIUDIPORTA);
		}
		if(PorteChiuse())
		{
			statoVarco=ST_QUIETE;
			bRichUsc=false;
			bRichIng=false;
			tailgate(true,0,0);
			OUT_BATTERIE_ON;
		}
	break;
	case ST_QUIETE:
		sBuzzer=false;
		bTransNonEffettuato=false;
		LongTimerSetOff(LTIM_TIMEOUT_TR);

		if(SEL1_MOD_ING==MOD_BLOCCATO)
		{
			SetSemafIng(SEM_ROSSO);
		}
		else if(SEL1_MOD_ING==MOD_LIBERO)
		{
			SetSemafIng(SEM_VERDE);
		}
		else
		{
			#ifndef SEM_TAILGATE
				SetSemafIng(SEM_BIANCO);
			#endif
		}

		if(SEL2_MOD_USC==MOD_BLOCCATO)
		{
			SetSemafUsc(SEM_ROSSO);
		}
		else if(SEL2_MOD_USC==MOD_LIBERO)
		{
			SetSemafUsc(SEM_VERDE);
		}
		else
		{
			#ifndef SEM_TAILGATE
				SetSemafUsc(SEM_BIANCO);
			#endif
		}

		if(allarmi.forzatura || allarmi.loitering)
		{
			statoVarco=ST_ALLARME;
			break;
		}

		if(((sPrenotazIng || bRichIng) && SEL1_MOD_ING!=MOD_BLOCCATO) || (IN_FOTO_3M && SEL1_MOD_ING==MOD_LIBERO)) 
        {
            if(!UscitaOccupata() && !(IN_FOTO_1S || IN_FOTO_2S || IN_FOTO_3S || IN_FOTO_10S))
                statoVarco=ST_INIZIO_TR_I;
            else
                statoVarco=ST_ATTESA_INIZIO_I;
            
            break;
        }
        else if(SEL1_MOD_ING==MOD_BLOCCATO)
            bRichIng=sPrenotazIng=false; // annullo eventuali richieste via seriale/prenotazioni

		if(((sPrenotazUsc || bRichUsc) && SEL2_MOD_USC!=MOD_BLOCCATO) || (IN_FOTO_3S && SEL2_MOD_USC==MOD_LIBERO)) 
        {
            if(!IngressoOccupato() && !(IN_FOTO_1M || IN_FOTO_2M || IN_FOTO_3M || IN_FOTO_10M))
                statoVarco=ST_INIZIO_TR_U;
            else
                statoVarco=ST_ATTESA_INIZIO_U;
            
            break;
        }
        else if(SEL2_MOD_USC==MOD_BLOCCATO)
            bRichUsc=sPrenotazUsc=false;  // annullo eventuali richieste via seriale/prenotazioni

	break;	
	// ******************* Gestione transito ingresso
	case ST_ATTESA_INIZIO_I:
		#ifndef SEM_TAILGATE
			SetSemafIng(SEM_VERDE);
		#endif
		if(FLAG2_SEMPRE_VERDE)
			SetSemafUsc(SEM_VERDE);
		else
			SetSemafUsc(SEM_ROSSO);

		if(LongTimerIsOff(LTIM_ATTESA_INIZIO)) LongTimerSet(LTIM_ATTESA_INIZIO, APP_LSEC(30)); 
		else if(LongTimerIsExpired(LTIM_ATTESA_INIZIO))
		{
			LongTimerSetOff(LTIM_ATTESA_INIZIO);
			statoVarco=ST_QUIETE;
			bRichIng=false;
		}
		if(!UscitaOccupata() && !(IN_FOTO_1S || IN_FOTO_2S || IN_FOTO_3S || IN_FOTO_10S))
		{
			statoVarco=ST_INIZIO_TR_I;
			LongTimerSetOff(LTIM_ATTESA_INIZIO);
		}
	break;
	case ST_INIZIO_TR_I:
		#ifndef SEM_TAILGATE
			SetSemafIng(SEM_VERDE);
		#endif

		if(FLAG2_SEMPRE_VERDE)
			SetSemafUsc(SEM_VERDE);
		else
			SetSemafUsc(SEM_ROSSO);
		ComandoPorta(MOT_APRIPORTA_1);

		if(LongTimerIsOff(LTIM_TIMEOUT_TR)) LongTimerSet(LTIM_TIMEOUT_TR,PAR1_TIMEOUT_TRANSITO); 
		else if(LongTimerIsExpired(LTIM_TIMEOUT_TR))
		{
			// così non suona il buzzer per segnalare transito prolungato
			LongTimerSetOff(LTIM_TIMEOUT_TR);
			dbg_printf( "      [ERR] transito non avvenuto!\r\n");
			statoVarco=ST_FINE_TR_I;
			sPrenotazIng=0;
			break;
		}

		if(IN_FOTO_1S || IN_FOTO_2S || IN_FOTO_3S)
		{
			allarmi.controflusso=true;
			statoVarco=ST_ALLARME;
		}
		else if(IN_FOTO_10M)
		{
			statoVarco=ST_TRANS_INC_I;
			break;
		}
	break;	
	case ST_TRANS_INC_I:
		#ifndef SEM_TAILGATE
			if(FLAG2_SEMPRE_VERDE)
				SetSemafUsc(SEM_VERDE);
			else
				SetSemafUsc(SEM_ROSSO);
		#endif
		#ifdef SEM_SEGUI_TRANSITO
			SetSemafIng(SEM_GIALLO); // per debug
			SetSemafUsc(SEM_GIALLO); // per debug
		#endif
		// controllo l'effettivo transito all'interno del varco
		// se nessuna fotocellula è attiva in questo momento vuol dire che non è entrato nessuno
		if(!(IN_FOTO_10M || IN_FOTO_4S || IN_FOTO_6S || IN_FOTO_9S))
		{
			// questo timer mi serve per tornare indietro nella macchina a stati del transito con un piccolo ritardo
			if(ShortTimerIsOff(TIM_BACK_STATO)) ShortTimerSet(TIM_BACK_STATO, mSEC(500));
			else if(ShortTimerIsExpired(TIM_BACK_STATO))
			{
				ShortTimerSetOff(TIM_BACK_STATO);
				// se il varco viene liberato dopo essere stato attraversato "per metà", deve andare in allarme (richiesta del cliente)
				statoVarco=bTransNonEffettuato ? ST_FINE_TR_I : ST_INIZIO_TR_I;

			}
		}
		// non faccio il controllo sul controflusso altrimenti con il trolley va in allarme
		// barriera oltrepassata
		else if(!IN_FOTO_10M && (IN_FOTO_4S || IN_FOTO_6S || IN_FOTO_9S)) // da controllare se 9S e 10S vegono occupate dal vetro
		{
			statoVarco=ST_PERSONA_OLTRE_I;
			ShortTimerSetOff(TIM_BACK_STATO);
		}
		else
			ShortTimerSetOff(TIM_BACK_STATO);

		// [IMPORTANTE] se vengono aggiunte altre istruzioni non legate all'if/else if di sopra, va aggiunto il break dopo il cambio stato!
		// e l'istruzione  *** stimerSetOff(TIM_AVANZAMENTO_STATO); ***
	break;
	case ST_PERSONA_OLTRE_I:
		#ifdef SEM_SEGUI_TRANSITO
			SetSemafIng(SEM_VIOLA); // per debug
			SetSemafUsc(SEM_VIOLA); // per debug
		#endif
		if((!UscitaOccupata() && !IN_FOTO_10M) || (SEL1_MOD_ING==MOD_LIBERO && IngressoOccupato()))
		{
			if(ShortTimerIsOff(TIM_BACK_STATO)) ShortTimerSet(TIM_BACK_STATO, mSEC(500));
			else if(ShortTimerIsExpired(TIM_BACK_STATO))
			{
				ShortTimerSetOff(TIM_AVANZAMENTO_STATO);
				ShortTimerSetOff(TIM_BACK_STATO);
				statoVarco=ST_TRANS_INC_I;

				// in questo caso se la persona torna indietro devo chiudere e dare allarme
				if(SEL1_MOD_ING!=MOD_LIBERO && FLAG3_ALLARME_INVALID_TRANS)
					bTransNonEffettuato=true;
			}
		}
		else if(!(IN_FOTO_10M /*|| IN_FOTO_6S*/) && (IN_FOTO_1S || IN_FOTO_2S || IN_FOTO_3S || IN_FOTO_4S))
		{
			// piccola attesa per evitare di richiudere troppo presto (trolley!)
			if(ShortTimerIsOff(TIM_AVANZAMENTO_STATO)) ShortTimerSet(TIM_AVANZAMENTO_STATO, mSEC(200));
			else if(ShortTimerIsExpired(TIM_AVANZAMENTO_STATO))
			{
				ShortTimerSetOff(TIM_BACK_STATO);
				ShortTimerSetOff(TIM_AVANZAMENTO_STATO);
				dbg_printf("      [OK] transito avvenuto!\r\n");

				if(LongTimerIsOn(LTIM_TIMEOUT_TR))
				{
					LongTimerSet(LTIM_TR_VALIDO_ING,APP_LSEC(2));
					if(cntIngressi<0xFFFF)
						cntIngressi++;
				}
				else
					dbg_printf("      [ERR] transito avvenuto oltre il tempo limite!\r\n");

				statoVarco=sPrenotazIng ? ST_TRANS_INC_I : ST_FINE_TR_I;
			}
		}
		else
		{
			ShortTimerSetOff(TIM_BACK_STATO);
			ShortTimerSetOff(TIM_AVANZAMENTO_STATO);
		}
	break;
	case ST_FINE_TR_I:
		if(!FLAG7_DISABILE || (!IN_FOTO_4S && ShortTimerIsExpired(TIM_ANTIF_DISABILE)))
			ComandoPorta(MOT_CHIUDIPORTA);
		else if(!IN_FOTO_4S && ShortTimerIsOff(TIM_ANTIF_DISABILE)) 
			ShortTimerSet(TIM_ANTIF_DISABILE,mSEC(500));
		else if(IN_FOTO_4S)
			ShortTimerSetOff(TIM_ANTIF_DISABILE);

		if(bTransNonEffettuato)
		{
			sBuzzer=true;
			SetSemafIng(SEM_ROSSO);
			SetSemafUsc(SEM_ROSSO);
		}
		else if(FLAG2_SEMPRE_VERDE)
		{
			SetSemafIng(SEM_VERDE);
			SetSemafUsc(SEM_VERDE);
		}
		else
		{
			SetSemafIng(SEM_ROSSO);
			SetSemafUsc(SEM_ROSSO);
		}

		// se mentre chiudo arriva una nuova prenotazione devo riaprire
		if(sPrenotazIng || IN_FOTO_10M || (SEL1_MOD_ING==MOD_LIBERO && (IngressoOccupato() || IN_FOTO_3M)))
		{
			ShortTimerSetOff(TIM_ANTIF_DISABILE);
			LongTimerSet(LTIM_TIMEOUT_TR,PAR1_TIMEOUT_TRANSITO);
			ComandoPorta(MOT_APRIPORTA_1);
			statoVarco=ST_TRANS_INC_I;
		}
		else if(PorteChiuse())
		{
			ShortTimerSetOff(TIM_ANTIF_DISABILE);
			statoVarco=ST_QUIETE;
			bRichIng=false;
		}
	break; 
// ******************* Fine gestione transito ingresso
// ******************* Gestione transito uscita
	case ST_ATTESA_INIZIO_U:
		#ifndef SEM_TAILGATE
			SetSemafUsc(SEM_VERDE);
		#endif

		if(FLAG2_SEMPRE_VERDE )
			SetSemafIng(SEM_VERDE);
		else
			SetSemafIng(SEM_ROSSO);

		if(LongTimerIsOff(LTIM_ATTESA_INIZIO)) LongTimerSet(LTIM_ATTESA_INIZIO, APP_LSEC(30));
		else if(LongTimerIsExpired(LTIM_ATTESA_INIZIO))
		{
			LongTimerSetOff(LTIM_ATTESA_INIZIO);
			statoVarco=ST_QUIETE;
			bRichUsc=false;
		}
		if(!IngressoOccupato() && !(IN_FOTO_1M || IN_FOTO_2M || IN_FOTO_3M || IN_FOTO_10M))
		{
			statoVarco=ST_INIZIO_TR_U;
			LongTimerSetOff(LTIM_ATTESA_INIZIO);
		}
	break;	
	case ST_INIZIO_TR_U:
		#ifndef SEM_TAILGATE
			SetSemafUsc(SEM_VERDE);
		#endif

		if(FLAG2_SEMPRE_VERDE )
			SetSemafIng(SEM_VERDE);
		else
			SetSemafIng(SEM_ROSSO);
		ComandoPorta(MOT_APRIPORTA_2);

		if(LongTimerIsOff(LTIM_TIMEOUT_TR)) LongTimerSet(LTIM_TIMEOUT_TR, PAR1_TIMEOUT_TRANSITO);
		else if(LongTimerIsExpired(LTIM_TIMEOUT_TR))
		{
			LongTimerSetOff(LTIM_TIMEOUT_TR); // così non suona il buzzer per segnalare transito prolungato
			dbg_printf("      [ERR] transito non avvenuto!\r\n");
			statoVarco=ST_FINE_TR_U;
			sPrenotazUsc=0;
			break;
		}

		if(IN_FOTO_1M || IN_FOTO_2M || IN_FOTO_3M)
		{
			allarmi.controflusso=true;
			statoVarco=ST_ALLARME;
		}
		else if(IN_FOTO_10S)
		{
			statoVarco=ST_TRANS_INC_U;
			break;
		}
	break;	
	case ST_TRANS_INC_U:
		#ifndef SEM_TAILGATE
			if(FLAG2_SEMPRE_VERDE)
				SetSemafIng(SEM_VERDE);
			else
				SetSemafIng(SEM_ROSSO);
		#endif
		#ifdef SEM_SEGUI_TRANSITO
			SetSemafIng(SEM_GIALLO); // per debug
			SetSemafUsc(SEM_GIALLO); // per debug
		#endif
		// controllo l'effettivo transito all'interno del varco
		// se nessuna fotocellula è attiva in questo momento vuol dire che non è entrato nessuno
		if(!(IN_FOTO_10S || IN_FOTO_4M || IN_FOTO_6M || IN_FOTO_9M))
		{
			// questo timer mi serve per tornare indietro nella macchina a stati del transito con un piccolo ritardo
			if(ShortTimerIsOff(TIM_BACK_STATO)) ShortTimerSet(TIM_BACK_STATO, mSEC(500));
			else if(ShortTimerIsExpired(TIM_BACK_STATO))
			{
				ShortTimerSetOff(TIM_BACK_STATO);
				// se il varco viene liberato dopo essere stato attraversato "per metà", deve andare in allarme (richiesta del cliente)
				statoVarco=bTransNonEffettuato ? ST_FINE_TR_U : ST_INIZIO_TR_U;
			}
		}
		// non faccio il controllo sul controflusso altrimenti con il trolley va in allarme
		// barriera oltrepassata
		else if(!IN_FOTO_10S && (IN_FOTO_4M || IN_FOTO_6M || IN_FOTO_9M))
		{
			statoVarco=ST_PERSONA_OLTRE_U;
			ShortTimerSetOff(TIM_BACK_STATO);
		}
		else
			ShortTimerSetOff(TIM_BACK_STATO);
		// [IMPORTANTE] se vengono aggiunte altre istruzioni non legate all'if/else if di sopra, va aggiunto il break dopo il cambio stato!
		// e l'istruzione  *** stimerSetOff(TIM_AVANZAMENTO_STATO); ***
	break;	
	case ST_PERSONA_OLTRE_U:
		#ifdef SEM_SEGUI_TRANSITO
			SetSemafIng(SEM_VIOLA); // per debug
			SetSemafUsc(SEM_VIOLA); // per debug
		#endif
		if((!IngressoOccupato() && !IN_FOTO_10S) ||	(SEL2_MOD_USC==MOD_LIBERO && UscitaOccupata()))
		{
			if(ShortTimerIsOff(TIM_BACK_STATO)) ShortTimerSet(TIM_BACK_STATO, mSEC(500));
			else if(ShortTimerIsExpired(TIM_BACK_STATO))
			{
				ShortTimerSetOff(TIM_AVANZAMENTO_STATO);
				ShortTimerSetOff(TIM_BACK_STATO);
				statoVarco=ST_TRANS_INC_U;

				// in questo caso se la persona torna indietro devo chiudere e dare allarme
				if(SEL2_MOD_USC!=MOD_LIBERO && FLAG3_ALLARME_INVALID_TRANS)
					bTransNonEffettuato=true;
			}
		}
		else if(!(IN_FOTO_10S /*|| IN_FOTO_6M*/) && (IN_FOTO_1M || IN_FOTO_2M || IN_FOTO_3M || IN_FOTO_4M))
		{
			// piccola attesa per evitare di richiudere troppo presto (trolley!)
			if(ShortTimerIsOff(TIM_AVANZAMENTO_STATO)) ShortTimerSet(TIM_AVANZAMENTO_STATO, mSEC(200));
			else if(ShortTimerIsExpired(TIM_AVANZAMENTO_STATO))
			{
				ShortTimerSetOff(TIM_BACK_STATO);
				ShortTimerSetOff(TIM_AVANZAMENTO_STATO);
				if(LongTimerIsOn(LTIM_TIMEOUT_TR))
				{
					dbg_printf("      [OK] transito avvenuto!\r\n");
					LongTimerSet(LTIM_TR_VALIDO_USC, APP_LSEC(2));
					if(cntUscite<0xFFFF) cntUscite++;
				}
				else
				{
					dbg_printf("      [ERR] transito avvenuto oltre il tempo limite!\r\n");
				}
				statoVarco=sPrenotazUsc ? ST_TRANS_INC_U : ST_FINE_TR_U;
			}
		}
		else
		{
			ShortTimerSetOff(TIM_BACK_STATO);
			ShortTimerSetOff(TIM_AVANZAMENTO_STATO);
		}
	break;	
	case ST_FINE_TR_U:
		if(!FLAG7_DISABILE || (!IN_FOTO_4M && ShortTimerIsExpired(TIM_ANTIF_DISABILE)))
			ComandoPorta(MOT_CHIUDIPORTA);
		else if(!IN_FOTO_4M && ShortTimerIsOff(TIM_ANTIF_DISABILE ))
			ShortTimerSet(TIM_ANTIF_DISABILE, mSEC(500));
		else if(IN_FOTO_4M )
			ShortTimerSetOff(TIM_ANTIF_DISABILE);

		if(bTransNonEffettuato)
		{
			sBuzzer=true;
			SetSemafIng(SEM_ROSSO);
			SetSemafUsc(SEM_ROSSO);
		}
		else if(FLAG2_SEMPRE_VERDE)
		{
			SetSemafIng(SEM_VERDE);
			SetSemafUsc(SEM_VERDE);
		}
		else
		{
			SetSemafIng(SEM_ROSSO);
			SetSemafUsc(SEM_ROSSO);
		}
		// se mentre chiudo arriva una nuova prenotazione devo riaprire
		if(sPrenotazUsc || IN_FOTO_10S || (SEL2_MOD_USC==MOD_LIBERO && (UscitaOccupata() || IN_FOTO_3S)))
		{
			ShortTimerSetOff(TIM_ANTIF_DISABILE);
			LongTimerSet(LTIM_TIMEOUT_TR, PAR1_TIMEOUT_TRANSITO);
			ComandoPorta(MOT_APRIPORTA_2);
			statoVarco=ST_TRANS_INC_U;
		}
		else if(PorteChiuse())
		{
			ShortTimerSetOff(TIM_ANTIF_DISABILE);
			statoVarco=ST_QUIETE;
			bRichUsc=false;
		}
	break;	
// ******************* Fine gestione transito uscita
	case ST_ALLARME:
		if(FLAG1_ALLARME_ROSSO)
		{
			SetSemafIng(SEM_ROSSO);
			SetSemafUsc(SEM_ROSSO);
		}
		else
		{
			SetSemafIng(SEM_BLU);
			SetSemafUsc(SEM_BLU);
		}
		sBuzzer=true;

		if(LongTimerIsExpired(LTIM_FINE_ALLARME))
		{
			ComandoPorta(MOT_CHIUDIPORTA);
			if(PorteChiuse())
			{
				LongTimerSetOff(LTIM_FINE_ALLARME);
				statoVarco=ST_QUIETE;
				// con il flag attivo in caso di allarme controflusso, non annullo le prenotazioni e riattivo il timer per il "vecchio" transito
				if(FLAG6_SALVA_PRENOTAZIONE && allarmi.controflusso) LongTimerSet(LTIM_TIMEOUT_TR,PAR1_TIMEOUT_TRANSITO);
				else
				{
					bRichIng=bRichUsc=false;
					sPrenotazUsc=sPrenotazIng=0;
				}
				allarmi.loitering=false;
				allarmi.accodamento=false;
				allarmi.controflusso=false;
				break;
			}
		}

		if(allarmi.forzatura)
		{
			switch(gestForzatura)
			{
			case 0:
				if(LongTimerIsOff(LTIM_TEMPO_FRENO))
				{
					LongTimerSet(LTIM_TEMPO_FRENO, PAR4_TEMPO_FRENO);
					ComandoPorta(MOT_FERMAPORTA);
					//dbg_printf("Mandato Ferma!\r\n");
				}
				else if(LongTimerIsExpired(LTIM_TEMPO_FRENO))
				{
					ComandoPorta(MOT_CHIUDIPORTA);
					LongTimerSetOff(LTIM_TEMPO_FRENO);
					gestForzatura=1;
					//dbg_printf("Mandato Chiudi!\r\n");
				}
			break;
			case 1:
				// faccio partire un timer che serve all'azionamento per decidere se rimanere in allarme o no
				if(ShortTimerIsOff(TIM_DOPO_SFONDAMENTO))
					ShortTimerSet(TIM_DOPO_SFONDAMENTO,mSEC(800));

				if(PorteChiuse() && !IngressoOccupato() && !UscitaOccupata()) // da testare, ho aggiunto il controllo sul varco libero per resettare la funzione tailgate
				{
					//dbg_printf("Fine Allarme!\r\n");
					allarmi.forzatura=false;
					allarmi.loitering=false;
					gestForzatura=0;
					statoVarco=ST_QUIETE;
					ShortTimerSetOff(TIM_DOPO_SFONDAMENTO);
				}
				else if(ShortTimerIsExpired(TIM_DOPO_SFONDAMENTO))
				{
					ShortTimerSetOff(TIM_DOPO_SFONDAMENTO);
					if(azMaster.statoMot==STMOT_PORTAFORZATA || azSlave.statoMot==STMOT_PORTAFORZATA)
						gestForzatura=0;
				}
			break;
			}
		}
		else if(allarmi.loitering)
		{
			if(!IngressoOccupato() && !UscitaOccupata())
			{
				if(LongTimerIsOff(LTIM_FINE_ALLARME))
					LongTimerSet(LTIM_FINE_ALLARME, PAR3_TIMEOUT_FINE_ALL);
			}
			else
				LongTimerSetOff(LTIM_FINE_ALLARME);
		}
		else if(allarmi.controflusso)
		{
			ComandoPorta(MOT_CHIUDIPORTA);

			if(!UscitaOccupata() && PorteChiuse())
			{
				if(LongTimerIsOff(LTIM_FINE_ALLARME))
					LongTimerSet(LTIM_FINE_ALLARME, PAR3_TIMEOUT_FINE_ALL);
			}
		}
		else if(allarmi.accodamento)
		{
			if(!(IN_FOTO_6S || IN_FOTO_9S || IN_FOTO_6M || IN_FOTO_9M))
			{
				if(ShortTimerIsOff(TIM_ANTIF_ACCODAMENTO))
					ShortTimerSet(TIM_ANTIF_ACCODAMENTO, mSEC(1000));
				else if(ShortTimerIsExpired(TIM_ANTIF_ACCODAMENTO))
					ComandoPorta(MOT_CHIUDIPORTA);
			}
			else
				ShortTimerSetOff(TIM_ANTIF_ACCODAMENTO);

			// Liberato il varco per un tempo impostabile torno in quiete
			if(!(IngressoOccupato() || UscitaOccupata() || IN_FOTO_10M || IN_FOTO_10S))
			{
				if(LongTimerIsOff(LTIM_FINE_ALLARME))
					LongTimerSet(LTIM_FINE_ALLARME, PAR3_TIMEOUT_FINE_ALL);
			}
			else
				LongTimerSetOff(LTIM_FINE_ALLARME);
		}
	break;	
	case ST_EMERGENZA:
		sEmergenza=true;
		sBuzzer=false;
		SetSemafIng(SEM_GIALLO);
		SetSemafUsc(SEM_GIALLO);
		if(sRichEm)
		{
			// in caso venga dato l'emergenza quando qualcuno ha forzato l'anta - da testare
			if(allarmi.forzatura)
			{
				ComandoPorta(MOT_CHIUDIPORTA);
				if(ShortTimerIsOff(TIM_EMERG_ALLARME))
					ShortTimerSet(TIM_EMERG_ALLARME, mSEC(500));
				else if(ShortTimerIsExpired(TIM_EMERG_ALLARME))
				{
					ShortTimerSetOff(TIM_EMERG_ALLARME);
					allarmi.forzatura=false;
				}
			}
			else if(PorteAperte1() && !bVisteAperte)
				bVisteAperte=true;
			else if(!bVisteAperte)
			{
				if(FLAG4_INVERTI_DIR_EMERG)
					ComandoPorta(MOT_APRIPORTA_1); // direzione ingresso
				else
					ComandoPorta(MOT_APRIPORTA_2); // direzione uscita
			}
		}
		else
		{
			bVisteAperte=false;
			ComandoPorta(MOT_CHIUDIPORTA);
			if(PorteChiuse())
			{
				sEmergenza=false;
				statoVarco=ST_QUIETE;
				bRichIng=false;
				bRichUsc=false;
				allarmi.loitering=false;
				allarmi.accodamento=false;
				allarmi.controflusso=false;
			}
		}
	break;	
	case ST_ULTIMA_APERTURA:
		// semafori spenti per risparmiare
		SetSemafIng(SEM_OFF);
		SetSemafUsc(SEM_OFF);
		sBuzzer=false;

		if(LongTimerIsOff( LTIM_ULTIMA_APERTURA))
			LongTimerSet(LTIM_ULTIMA_APERTURA, APP_LSEC(60));

		if(allarmi.forzatura)
		{
			ComandoPorta(MOT_CHIUDIPORTA);
			if(ShortTimerIsOff(TIM_EMERG_ALLARME))
				ShortTimerSet(TIM_EMERG_ALLARME, mSEC(500));
			else if(ShortTimerIsExpired(TIM_EMERG_ALLARME))
			{
				ShortTimerSetOff(TIM_EMERG_ALLARME);
				allarmi.forzatura=false;
			}
		}
		else
		{
			if(FLAG4_INVERTI_DIR_EMERG)
				ComandoPorta(MOT_APRIPORTA_1); // direzione ingresso
			else
				ComandoPorta(MOT_APRIPORTA_2); // direzione uscita
		}

		if(PorteAperte1() || PorteAperte2() || LongTimerIsExpired(LTIM_ULTIMA_APERTURA))
		{
			LongTimerSetOff(LTIM_ULTIMA_APERTURA);
			OUT_BATTERIE_OFF;
		}
	break;
	}
}
/*------------------------------------------------------------------------------------------*/
/*	Breath()																				*/
/*------------------------------------------------------------------------------------------*/
#define PERIODO_BREATH		100
#define LIMITE_SUPERIORE	100
#define LIMITE_INFERIORE	1 // VALORI VALIDI 1-100, SE 0 NON SI ACCENDE
#define TEMPO_PAUSA			50
#define MAX_SLOWDOWN		0
void Breath(bool reset)
{
	static unsigned int counterON=0;
	static unsigned int limit=1; // IMPORTANTE SIA 1 ALL'INIZIO, NON DEVE ESSERE 0
	static unsigned int counterPeriodo=0;
	static unsigned int pausa=0;
	static bool incrementaLimite=true, accendiLed=true;
	static unsigned char slowdown=0; // serve per rallentare il respiro, diminuendo la frequenza con cui aumenta il limite da raggiungere

	if(reset)
	{
		counterON=0;
		counterPeriodo=0;
		counterCicliBreath=0;
		limit=1;
		pausa=0;
		incrementaLimite=true;
		accendiLed=true;
		slowdown=0;
		return;
	}
	if((counterPeriodo++)<100) // contatore per il periodo (1 periodo=12.5ms)
	{
		if(accendiLed)
		{
			if((++counterON)==limit)
			{
				accendiLed=false;
				if(pausa)
				{
					if((--pausa)==0)
					{
						if(isMaster)
						{
							counterCicliBreath++;
							//dbg_printf("Finito ciclo Breath n %d\r\n",counterCicliBreath);
						}
						counterPeriodo=counterON=0;
					}
				}
				else if(incrementaLimite)
				{
					if(limit==(LIMITE_SUPERIORE*PERIODO_BREATH)/100)
					{
						//pausa=TEMPO_PAUSA; //on - non funziona! fa un lampo ogni fine pausa
						incrementaLimite=false;
					}
					else
					{
						if(slowdown==MAX_SLOWDOWN)
						{
							slowdown=0;
							++limit;
						}
						else
							slowdown++;
					}
				}
				else
				{
					if(limit==(LIMITE_INFERIORE*PERIODO_BREATH)/100)
					{
					  	// finito il ciclo di respiro, imposto una temporizzazione tra uno e l'altro
						pausa=TEMPO_PAUSA; //off
						incrementaLimite=true;
					}
					else
					{
						if(slowdown==MAX_SLOWDOWN)
						{
							slowdown=0;
							--limit;
						}
						else
							slowdown++;
					}
				}
			}
		}
	}
	else
	{
		counterPeriodo=0;
		counterON=0;
		accendiLed=true;
		return;
	}

	if(accendiLed)
		OUT_BREATH_ON;
	else
		OUT_BREATH_OFF;

}

/*------------------------------------------------------------------------------------------*/
/*	GetTextStatus	( )																		*/
/*------------------------------------------------------------------------------------------*/
bool GetTextStatus(enmStatoVarco_t stato, char* str)
{
    bool ret=true;
    switch(stato)
    {
    case ST_WARMUP:
        sprintf(str,"WARMUP");
    break;
    case ST_QUIETE:
        sprintf(str,"QUIETE");
    break;
    case ST_INIZIO_TR_I:
        sprintf(str,"INIZIO TRANSITO ING");
    break;
    case ST_ATTESA_INIZIO_I:
        sprintf(str,"ATTESA INIZIO ING");
    break;
    case ST_TRANS_INC_I:
        sprintf(str,"TRANSITO IN CORSO ING");
    break;
    case ST_PERSONA_OLTRE_I:
        sprintf(str,"PERSONA OLTRE ING");
    break;
    case ST_FINE_TR_I:
        sprintf(str,"FINE TRANSITO ING");
    break;
    case ST_INIZIO_TR_U:
        sprintf(str,"INIZIO TRANSITO USC");
    break;
    case ST_ATTESA_INIZIO_U:
        sprintf(str,"ATTESA INIZIO USC");
    break;
    case ST_TRANS_INC_U:
        sprintf(str,"TRANSITO IN CORSO USC");
    break;
    case ST_PERSONA_OLTRE_U:
        sprintf(str,"PERSONA OLTRE USC");
    break;
    case ST_FINE_TR_U:
        sprintf(str,"FINE TRANSITO USC");
    break;
    case ST_ALLARME:
        sprintf(str,"ALLARME");
    break;
    case ST_EMERGENZA:
        sprintf(str,"EMERGENZA");
    break;
    case ST_ULTIMA_APERTURA:
        sprintf(str,"ULTIMA APERTURA");
    break;
    default:
        ret=false;
    }
    return ret;
}
/*------------------------------------------------------------------------------------------*/
/*	gestSemafori()																		*/
/*------------------------------------------------------------------------------------------*/
void gestSemafori()
{
/*
    static enmSemafori_t oldsem;
    if(semafori.inMaster!=oldsem) dbg_printf("sem master: %d\r\n", semafori.inMaster);
    oldsem=semafori.inMaster;
*/
switch(semafori.inMaster)
    {
    case SEM_OFF:
        OUT_LED_IN_M_RED_OFF;
        OUT_LED_IN_M_GREEN_OFF;
        OUT_LED_IN_M_BLUE_OFF;
    break;
    case SEM_ROSSO:
        OUT_LED_IN_M_RED_ON;
        OUT_LED_IN_M_GREEN_OFF;
        OUT_LED_IN_M_BLUE_OFF;
    break;
    case SEM_VERDE:
        OUT_LED_IN_M_GREEN_ON;
        OUT_LED_IN_M_RED_OFF;
        OUT_LED_IN_M_BLUE_OFF;
    break;
    case SEM_GIALLO:
        OUT_LED_IN_M_RED_ON;
        OUT_LED_IN_M_GREEN_ON;
        OUT_LED_IN_M_BLUE_OFF;
    break;
    case SEM_BLU:
        OUT_LED_IN_M_BLUE_ON;
        OUT_LED_IN_M_GREEN_OFF;
        OUT_LED_IN_M_RED_OFF;
    break;
    case SEM_VIOLA:
        OUT_LED_IN_M_RED_ON;
        OUT_LED_IN_M_BLUE_ON;
        OUT_LED_IN_M_GREEN_OFF;
    break;
    case SEM_CIANO:
        OUT_LED_IN_M_GREEN_ON;
        OUT_LED_IN_M_BLUE_ON;
        OUT_LED_IN_M_RED_OFF;
    break;
    case SEM_BIANCO:
        OUT_LED_IN_M_RED_ON;
        OUT_LED_IN_M_GREEN_ON;
        OUT_LED_IN_M_BLUE_ON;
    break;
    }

    switch(semafori.inSlave)
    {
    case SEM_OFF:
        OUT_LED_IN_S_RED_OFF;
        OUT_LED_IN_S_GREEN_OFF;
        OUT_LED_IN_S_BLUE_OFF;
    break;
    case SEM_ROSSO:
        OUT_LED_IN_S_RED_ON;
        OUT_LED_IN_S_GREEN_OFF;
        OUT_LED_IN_S_BLUE_OFF;
    break;
    case SEM_VERDE:
        OUT_LED_IN_S_GREEN_ON;
        OUT_LED_IN_S_RED_OFF;
        OUT_LED_IN_S_BLUE_OFF;
    break;
    case SEM_GIALLO:
        OUT_LED_IN_S_RED_ON;
        OUT_LED_IN_S_GREEN_ON;
        OUT_LED_IN_S_BLUE_OFF;
    break;
    case SEM_BLU:
        OUT_LED_IN_S_BLUE_ON;
        OUT_LED_IN_S_GREEN_OFF;
        OUT_LED_IN_S_RED_OFF;
    break;
    case SEM_VIOLA:
        OUT_LED_IN_S_RED_ON;
        OUT_LED_IN_S_BLUE_ON;
        OUT_LED_IN_S_GREEN_OFF;
    break;
    case SEM_CIANO:
        OUT_LED_IN_S_GREEN_ON;
        OUT_LED_IN_S_BLUE_ON;
        OUT_LED_IN_S_RED_OFF;
    break;
    case SEM_BIANCO:
        OUT_LED_IN_S_RED_ON;
        OUT_LED_IN_S_GREEN_ON;
        OUT_LED_IN_S_BLUE_ON;
    break;
    }

    switch(semafori.uscMaster)
    {
    case SEM_OFF:
        OUT_LED_USC_M_RED_OFF;
        OUT_LED_USC_M_GREEN_OFF;
        OUT_LED_USC_M_BLUE_OFF;
    break;
    case SEM_ROSSO:
        OUT_LED_USC_M_RED_ON;
        OUT_LED_USC_M_GREEN_OFF;
        OUT_LED_USC_M_BLUE_OFF;
    break;
    case SEM_VERDE:
        OUT_LED_USC_M_GREEN_ON;
        OUT_LED_USC_M_RED_OFF;
        OUT_LED_USC_M_BLUE_OFF;
    break;
    case SEM_GIALLO:
        OUT_LED_USC_M_RED_ON;
        OUT_LED_USC_M_GREEN_ON;
        OUT_LED_USC_M_BLUE_OFF;
    break;
    case SEM_BLU:
        OUT_LED_USC_M_BLUE_ON;
        OUT_LED_USC_M_GREEN_OFF;
        OUT_LED_USC_M_RED_OFF;
    break;
    case SEM_VIOLA:
        OUT_LED_USC_M_RED_ON;
        OUT_LED_USC_M_BLUE_ON;
        OUT_LED_USC_M_GREEN_OFF;
    break;
    case SEM_CIANO:
        OUT_LED_USC_M_GREEN_ON;
        OUT_LED_USC_M_BLUE_ON;
        OUT_LED_USC_M_RED_OFF;
    break;
    case SEM_BIANCO:
        OUT_LED_USC_M_RED_ON;
        OUT_LED_USC_M_GREEN_ON;
        OUT_LED_USC_M_BLUE_ON;
    break;
    }

    switch(semafori.uscSlave)
    {
    case SEM_OFF:
        OUT_LED_USC_S_RED_OFF;
        OUT_LED_USC_S_GREEN_OFF;
        OUT_LED_USC_S_BLUE_OFF;
    break;
    case SEM_ROSSO:
        OUT_LED_USC_S_RED_ON;
        OUT_LED_USC_S_GREEN_OFF;
        OUT_LED_USC_S_BLUE_OFF;
    break;
    case SEM_VERDE:
        OUT_LED_USC_S_GREEN_ON;
        OUT_LED_USC_S_RED_OFF;
        OUT_LED_USC_S_BLUE_OFF;
    break;
    case SEM_GIALLO:
        OUT_LED_USC_S_RED_ON;
        OUT_LED_USC_S_GREEN_ON;
        OUT_LED_USC_S_BLUE_OFF;
    break;
    case SEM_BLU:
        OUT_LED_USC_S_BLUE_ON;
        OUT_LED_USC_S_GREEN_OFF;
        OUT_LED_USC_S_RED_OFF;
    break;
    case SEM_VIOLA:
        OUT_LED_USC_S_RED_ON;
        OUT_LED_USC_S_BLUE_ON;
        OUT_LED_USC_S_GREEN_OFF;
    break;
    case SEM_CIANO:
        OUT_LED_USC_S_GREEN_ON;
        OUT_LED_USC_S_BLUE_ON;
        OUT_LED_USC_S_RED_OFF;
    break;
    case SEM_BIANCO:
        OUT_LED_USC_S_RED_ON;
        OUT_LED_USC_S_GREEN_ON;
        OUT_LED_USC_S_BLUE_ON;
    break;
    }
}
/*------------------------------------------------------------------------------------------*/
/*	IngressoOccupato()																	*/
/*------------------------------------------------------------------------------------------*/
bool IngressoOccupato()
{
    return (IN_FOTO_4M || IN_FOTO_6M);
}
/*------------------------------------------------------------------------------------------*/
/*	UscitaOccupata()																		*/
/*------------------------------------------------------------------------------------------*/
bool UscitaOccupata()
{
    return (IN_FOTO_4S || IN_FOTO_6S);
}


#define  TEMPO_MINIMO   150

/*------------------------------------------------------------------------------------------*/
/*	tailgate()																			*/
/*------------------------------------------------------------------------------------------*/
bool tailgate(bool bReset,unsigned char dir, unsigned char* ptrPrenot )
{
    static bool oldF8, oldF9, f9Attraversata;
    static bool bRilevato=false;
    static bool printed2=false, printed3=false;
    static unsigned char sFaseAcc=0;
    bool IN_FOTO_8,IN_FOTO_9;

    if(bReset || (statoVarco==ST_QUIETE && !IngressoOccupato()) || ((SEL1_MOD_ING==MOD_LIBERO && dir==DIR_INGRESSO) || (SEL2_MOD_USC==MOD_LIBERO && dir==DIR_USCITA)))
    {
        sFaseAcc=0;
        oldF8=oldF9=f9Attraversata=bRilevato=false;
        ShortTimerSetOff(TIM_f8);
        ShortTimerSetOff(TIM_f9);
        ShortTimerSetOff(TIM_DISTANZA_ACC);
        printed2=printed3=false;
        return(bRilevato);
    }
    else if(bRilevato)
        return(bRilevato);

    if(dir==DIR_INGRESSO)
    {
        IN_FOTO_8=IN_FOTO_8M;
        IN_FOTO_9=IN_FOTO_9M;
    }
    else if(dir==DIR_USCITA)
    {
        IN_FOTO_8=IN_FOTO_8S;
        IN_FOTO_9=IN_FOTO_9S;
    }

	#ifdef SEM_TAILGATE
		switch(sFaseAcc )
		{
			case 0:
				SetSemafIng(SEM_OFF);
			break;
			case 1:
				SetSemafIng(SEM_VERDE);
			break;
			case 2:
				SetSemafIng(SEM_GIALLO);
			break;
			case 3:
				SetSemafIng(SEM_VIOLA);
			break;
			case 4:
				SetSemafIng(SEM_CIANO);
			break;
		}
	#endif

    if(ShortTimerIsExpired(TIM_DISTANZA_ACC) && !printed2)
    {
        dbg_printf("> Scaduto timer TIM_DISTANZA_ACC\r\n");
        printed2=true;
    }

	// inizio gestione contatto proveniente da spyfloor
	if(IN_SPYFLOOR_ALARM && ShortTimerIsOff(TIM_SPYFLOOR_ALARM))
		ShortTimerSet(TIM_SPYFLOOR_ALARM,mSEC(50));
	else if(IN_SPYFLOOR_ALARM && ShortTimerIsExpired(TIM_SPYFLOOR_ALARM))
	{
		dbg_printf(" [SPYFLOOR] Allarme rilevato!\r\n");
        bRilevato=true;
        return bRilevato;
	}
	else if(!IN_SPYFLOOR_ALARM)
		ShortTimerSetOff(TIM_SPYFLOOR_ALARM);
	// fine gestione spyfloor

    switch(sFaseAcc)
    {
	case 0:
		if(IN_FOTO_8 && !oldF8)
		{
			oldF8=true;
			ShortTimerSet(TIM_f8, NONSIGN - 1); // TODO controllare valore timer incrementale
		}
		else if(!IN_FOTO_8 && oldF8)
		{
			oldF8=false;
			uint32_t el_time=NONSIGN-1-ShortTimerGetValue(TIM_f8);
			if(el_time>TEMPO_MINIMO)
			{
				sFaseAcc=1;
				switch(SEL3_TAILGATE_ACCURACY)
				{
					case ACC_STRINGENTE:
						ShortTimerSet(TIM_DISTANZA_ACC, 0);
					break;
					case ACC_MEDIO:
						ShortTimerSet(TIM_DISTANZA_ACC, mSEC(500));
					break;
					case ACC_BASSO:
						ShortTimerSet(TIM_DISTANZA_ACC, mSEC(800));
					break;
				}
				dbg_printf("\r\n (fase0) Prima persona passata da F8: %d\r\n> Caricato TIM_DISTANZA_ACC a %d\r\n",
								(int)el_time,
								(int)ShortTimerGetValue(TIM_DISTANZA_ACC));
			}
			else
			{
				dbg_printf(" (fase0) Annullato intercettamento F8: %d\r\n", (int)el_time);
			}
			ShortTimerSetOff(TIM_f8);
		}
	break;
	case 1:
		if(IN_FOTO_9 && !oldF9)
		{
			oldF9=true;
			ShortTimerSet(TIM_f9, NONSIGN-1);
		}
		else if(!IN_FOTO_9 && oldF9)
		{
			oldF9=false;
			uint32_t el_time=NONSIGN-1-ShortTimerGetValue(TIM_f9);
			if(el_time>TEMPO_MINIMO)
			{
				dbg_printf(" (fase1) Prima persona passata da F9: %d\r\n", (int)el_time);
				if(*ptrPrenot )
				{
					f9Attraversata=true;
					(*ptrPrenot)--;
					dbg_printf(" (fase1) Scalata prenotazione, %d rimasti\r\n", *ptrPrenot);
				}
				else
				{
					dbg_printf(" (fase1) [ACCODAMENTO] Nessuna prenotazione, transito rilevato\r\n");
					bRilevato=true;
					return bRilevato;
				}
			}
			else
			{
				dbg_printf(" (fase1) Annullato intercettamento F9: %d\r\n", (int)el_time);
			}
			ShortTimerSetOff(TIM_f9);
		}

		// va analizzato anche la prima fotocellula in questo punto
		if(IN_FOTO_8 && ShortTimerIsOff(TIM_f8))
			ShortTimerSet(TIM_f8, TEMPO_MINIMO);
		// se è stato attivo per troppo tempo vado in allarme subito
		else if(IN_FOTO_8 && ShortTimerIsExpired(TIM_f8))
		{
			if(ShortTimerIsExpired(TIM_DISTANZA_ACC))
			{
				// se ho ancora prenotazioni non segnalo allarme e mi sposto nella fase di rilevamento seconda persona tramite F9
				if(*ptrPrenot)
				{
					ShortTimerSetOff(TIM_f8);
					sFaseAcc=3;
					break;
				}
				else
				{
					dbg_printf( " (fase1) [ACCODAMENTO] Seconda persona sta passando da F8\r\n");
					bRilevato=true;
					return bRilevato;
				}
			}
			else
			{
				ShortTimerSetOff(TIM_f8); // così forzo il cambio fase
				if(!printed3)
				{
					printed3=true;
					dbg_printf(">> F8 occupata ma TIM_DISTANZA_ACC=%d\r\n", (int)ShortTimerGetValue(TIM_DISTANZA_ACC));
				}
			}
		}
		else if(!IN_FOTO_8)
			ShortTimerSetOff(TIM_f8);
		if(f9Attraversata && ShortTimerIsOff(TIM_f8))
			sFaseAcc=2;
	break;
	case 2:
		if(IN_FOTO_8 && !oldF8)
		{
			oldF8=true;
			ShortTimerSet(TIM_f8, NONSIGN-1);
		}
		else if(!IN_FOTO_8 && oldF8)
		{
			oldF8=false;
			uint32_t el_time=NONSIGN-1-ShortTimerGetValue(TIM_f8);
			ShortTimerSetOff(TIM_f8);
			if(el_time > TEMPO_MINIMO && ShortTimerIsExpired(TIM_DISTANZA_ACC))
			{
				dbg_printf(" (fase2) Seconda persona passata da F8: %d\r\n", (int)el_time);
				sFaseAcc=3;
				break;
			}
			else
			{
				dbg_printf(" (fase2) Annullato intercettamento F8: %d, mancano %d\r\n",
								(int)el_time,
								(int)ShortTimerGetValue(TIM_DISTANZA_ACC));
			}
		}

		// Controllo anche la seconda fotocellula per capire se la prima persona torna indietro
		if(IN_FOTO_9 && !oldF9)
		{
			oldF9=true;
			ShortTimerSet(TIM_f9_i, NONSIGN-1);
		}
		else if(!IN_FOTO_9 && oldF9) /* iMPORTANTE DA CAMBIARE ANCHE NEL PROGRAMMA VERO!!!!!*/
		{
			oldF9=false;
			uint32_t el_time=NONSIGN-1-ShortTimerGetValue(TIM_f9_i);
			ShortTimerSetOff(TIM_f9_i);
			if(el_time>TEMPO_MINIMO )
			{
				dbg_printf(" (fase2) Prima persona forse torna indietro da F9: %d\r\n", (int)el_time);
				sFaseAcc=4;
			}
			else
				dbg_printf(" (fase2) Annullato intercettamento F9: %d\r\n", (int)el_time);
		}
	break;
	case 3:
		if(IN_FOTO_9 && ShortTimerIsOff(TIM_f9))
			ShortTimerSet(TIM_f9, TEMPO_MINIMO);
		else if(IN_FOTO_9 && ShortTimerIsExpired(TIM_f9)) // se è stato attivo per troppo tempo vado in allarme subito
		{
			if(*ptrPrenot)
			{
				(*ptrPrenot)--;
				dbg_printf(" (fase3) Scalata prenotazione, %d rimasti\r\n", *ptrPrenot);
				tailgate(true, 0, 0);
			}
			else
			{
				dbg_printf(" (fase3) [ACCODAMENTO] Seconda persona sta passando da F9\r\n");
				bRilevato=true;
				return bRilevato;
			}
		}
		else if(!IN_FOTO_9)
			ShortTimerSetOff(TIM_f9);
	break;
	case 4:
		if(IN_FOTO_8 && !oldF8)
		{
			oldF8=true;
			ShortTimerSet(TIM_f8, NONSIGN-1);
		}
		else if(!IN_FOTO_8 && oldF8)
		{
			oldF8=false;
			uint32_t el_time=NONSIGN-1-ShortTimerGetValue(TIM_f8);
			if(el_time>TEMPO_MINIMO)
			{
				dbg_printf(" (fase4) Prima persona torna indietro da F8: %d\r\nRESET!\r\n", (int)el_time);
				tailgate(true, 0, 0);
			}
		}
	break;
    }
    return bRilevato;
}
/*------------------------------------------------------------------------------------------*/
/*	controlli()										 										*/
/*------------------------------------------------------------------------------------------*/
// funzione chiamata ogni iterazione per gestire i controlli generali sul varco
void controlli()
{
    static bool sBuzzerOnOff=false;

    if(IN_AZ_NO_RETE )
    {
		if(statoVarco!=ST_WARMUP && statoVarco!=ST_ULTIMA_APERTURA && statoVarco!=ST_EMERGENZA)
		{
			if(ShortTimerIsOff(TIM_NO_RETE))
				ShortTimerSet(TIM_NO_RETE, mSEC(300));
			else if(ShortTimerIsExpired(TIM_NO_RETE))
				statoVarco=ST_ULTIMA_APERTURA;
		}
		else if(statoVarco!=ST_ULTIMA_APERTURA)
			OUT_BATTERIE_OFF; // spengo subito gnicosa
    }
    else
        ShortTimerSetOff(TIM_NO_RETE);

    // Gestione delle prenotazioni
    if(statoVarco!=ST_WARMUP && statoVarco!=ST_EMERGENZA &&  statoVarco!=ST_ALLARME) 
    {
    	if(IN_RICH_TRANSITO_ING && ShortTimerIsOff(TIM_DEB_ING))
    		ShortTimerSet(TIM_DEB_ING, mSEC(50));
        else if(!IN_RICH_TRANSITO_ING)
        {
            if(ShortTimerIsExpired(TIM_DEB_ING ))
            {
                LongTimerSet(LTIM_TIMEOUT_TR, PAR1_TIMEOUT_TRANSITO);
                sPrenotazIng++;
            }
            ShortTimerSetOff(TIM_DEB_ING);
        }

        if(IN_RICH_TRANSITO_USC && ShortTimerIsOff(TIM_DEB_USC))
        	ShortTimerSet(TIM_DEB_USC, mSEC(50));
        else if(!IN_RICH_TRANSITO_USC)
        {
            if(ShortTimerIsExpired(TIM_DEB_USC))
            {
            	LongTimerSet(LTIM_TIMEOUT_TR, PAR1_TIMEOUT_TRANSITO);
                sPrenotazUsc++;
            }
            ShortTimerSetOff(TIM_DEB_USC);
        }
    }

    // Lo slave non risponde, dichiaro la sua porta in uno stato sconosciuto per non procedere con il programma
    if(isSlaveDead)
        azSlave.porta=STATO_SCONOSCIUTO;

    // Allarme loitering
    if(statoVarco==ST_QUIETE && (IngressoOccupato() || UscitaOccupata()))
    {
        if(LongTimerIsOff(LTIM_PERMANENZA_QUIETE))
        	LongTimerSet(LTIM_PERMANENZA_QUIETE, PAR2_TIMEOUT_PERMANENZA);
        else if(LongTimerIsExpired(LTIM_PERMANENZA_QUIETE))
            allarmi.loitering=true;
    }
    else
        LongTimerSetOff(LTIM_PERMANENZA_QUIETE);

    // Allarme forzatura
    if(azMaster.allarme || azSlave.allarme)
        allarmi.forzatura=true;

    if(((statoVarco>ST_INIZIO_TR_I && statoVarco<=ST_FINE_TR_I) || (statoVarco>ST_INIZIO_TR_U && statoVarco<=ST_FINE_TR_U)) && LongTimerIsExpired(LTIM_TIMEOUT_TR)) //estremo inferiore non incluso volutamente nell'intervallo
        sBuzzer=true;

    // Gestione Puls Emergenza
    if(PULS_EM_IMPULSIVO) // DEFINITO SEMPRE false
    {
        // NOTA BENE - SE ANDRA' UTILIZZATO VA RITESTATO BENE LA GESTIONE ATTIVAZIONE DA PULSANTE E RIMOZIONE DA SERIALE E VICEVERSA
        if(IN_RICH_EMERGENZA && LongTimerIsOff(LTIM_PULS_EM))
            LongTimerSet(LTIM_PULS_EM,APP_LSEC(2));
        else if(!IN_RICH_EMERGENZA)
        {
            if(LongTimerIsExpired(LTIM_PULS_EM))
                sRichEm=sEmergenza ? false : true;
            LongTimerSetOff(LTIM_PULS_EM);
        }
    }
    else
    {
        // L'attivazione del pulsante di emergenza deve sempre attivare il relativo bit
        if(IN_RICH_EMERGENZA)
        {
        	LongTimerSetOff(LTIM_RIMUOVI_EM);
            if(LongTimerIsOff(LTIM_PULS_EM))
            {
            	LongTimerSet(LTIM_PULS_EM,2); // 200 msec
            }
            else if(LongTimerIsExpired(LTIM_PULS_EM))
            {
                sRichEm|=RICH_DA_ING;
                LongTimerSetOff(LTIM_PULS_EM);
            }
        }
        // se il pulsante è stato premuto disattivo l'emergenza anche se è stata attivata da seriale
        else if(!IN_RICH_EMERGENZA &&(sRichEm & RICH_DA_ING))
        {
        	LongTimerSetOff(LTIM_PULS_EM);
            if(LongTimerIsOff(LTIM_RIMUOVI_EM))
            	LongTimerSet(LTIM_RIMUOVI_EM,APP_LSEC(2));
            else if(LongTimerIsExpired(LTIM_RIMUOVI_EM))
            {
                sRichEm&=~RICH_DA_ING;
                sRichEm&=~RICH_DA_SERIALE;
                LongTimerSetOff(LTIM_RIMUOVI_EM);
            }
        }
    }

    if(FLAG8_MOD_CONTATTI)
    {
        if(IN_BLOCCO_ING) SEL1_MOD_ING=MOD_BLOCCATO;
        else if(IN_LIBERO_ING) SEL1_MOD_ING=MOD_LIBERO;
        else SEL1_MOD_ING=MOD_CONTROLLATO;

        if(IN_BLOCCO_USC) SEL2_MOD_USC=MOD_BLOCCATO;
        else if(IN_LIBERO_USC) SEL2_MOD_USC=MOD_LIBERO;
        else SEL2_MOD_USC=MOD_CONTROLLATO;
    }

	if(statoVarco==ST_ALLARME)
		OUT_ALLARME_ON;
	else
		OUT_ALLARME_OFF;

	if(PorteChiuse())
		OUT_ANTE_CHIUSE_ON;
	else
		OUT_ANTE_CHIUSE_OFF;

	// Validazione transito
	if(LongTimerIsOn(LTIM_TR_VALIDO_ING))
		OUT_VALIDAZ_ING_ON;
	else
		OUT_VALIDAZ_ING_OFF;

	if(LongTimerIsOn(LTIM_TR_VALIDO_USC))
	{
		OUT_VALIDAZ_USC_ON; // tra {} perchè la define contiene due funzioni
	}
	else
	{
		OUT_VALIDAZ_USC_OFF; // tra {} perchè la define contiene due funzioni
	}
	// Gestione Buzzer
	if(sBuzzer)
	{
		if(ShortTimerIsOff(TIM_BUZZER))
			ShortTimerSet(TIM_BUZZER, mSEC(500));
		else if(ShortTimerIsExpired(TIM_BUZZER))
		{
			ShortTimerSetOff(TIM_BUZZER);
			if(sBuzzerOnOff)
				OUT_BUZZER_OFF;
			else
				OUT_BUZZER_ON;
			sBuzzerOnOff=!sBuzzerOnOff;
		}
	}
	else
	{
		sBuzzerOnOff=false;
		ShortTimerSetOff(TIM_BUZZER);
		OUT_BUZZER_OFF;
	}

}
/*------------------------------------------------------------------------------------------*/
/*	ComandoPorta()																		*/
/*------------------------------------------------------------------------------------------*/
void ComandoPorta(enmComandiPorte_t cmd)
{
    static enmComandiPorte_t oldCmd;
    if(cmd!=oldCmd)
    {
        // in caso di nuovo comando forzo il timer a scadere dopo 1 ms per migliorare la sincronia tra porte
        if(ShortTimerGetValue(TIM_CAN)>20)
            ShortTimerSet(TIM_CAN,1);
        
		oldCmd=cmd;
    }
    azMaster.comandoPorta=azSlave.comandoPorta=cmd;
}

