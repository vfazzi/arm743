/********************************************************************************************/
/*	VARCO_Can.c					- Applicazione VARCO - Gestione CAN							*/

/********************************************************************************************/
// Include di sistema e applicazione
#include "SYS_Include.h"
#include "APP_Include.h"

#define POSIZIONE_CMD_PARTICOLARE   4

/********************************************************************************************/
/*	Funzioni del modulo																		*/
/*																							*/
/*	VARCO_CanReceiveCallbackMaster ( )		- Ricezione CAN modo master						*/
/*	VARCO_CanSendCallbackMaster ( )			- Trasmissione CAN modo master					*/
/*	VARCO_CanReceiveCallbackSlave ( )		- Ricezione CAN modo slave						*/
/*	VARCO_CanSendCallbackSlave ( )			- Trasmissione CAN modo slave					*/
/********************************************************************************************/
/*------------------------------------------------------------------------------------------*/
/*	VARCO_CanReceiveCallbackMaster ( )		- Ricezione CAN modo master						*/
/*------------------------------------------------------------------------------------------*/
void VARCO_CanReceiveCallbackMaster ( void *hcan, can_packet packet )
{
	ShortTimerSet ( TIM_SLAVE_ALIVE, CAN_SLAVE_ALIVE_TIME );

	if( packet.dlc == 8 && packet.data [ 7 ] == COM_CAN_STATUS )
    {
    	_sysIO.ingSlave 	= ( packet.data [ 0 ] << 8 ) + packet.data [ 1 ];
    	azSlave.porta 		= packet.data [ 2 ];
    	azSlave.encoder 	= ( packet.data [ 3 ] << 8 ) + packet.data [ 4 ];
    	azSlave.allarme 	= (packet.data [ 5 ] & 0x01) ? true : false;
    	azSlave.ing 		= 0;
    	if ( packet.data [ 5 ] &0x02 )	azSlave.ing |= 0x01;
    	if ( packet.data [ 5 ] &0x04 )	azSlave.ing |= 0x02;
    	if ( packet.data [ 5 ] &0x08 )	azSlave.ing |= 0x04;
		
    	azSlave.out			= 0;
    	if ( packet.data [ 5 ] &0x10 )	azSlave.out |= 0x01;
    	if ( packet.data [ 5 ] &0x20 )	azSlave.out |= 0x02;
		
    	azSlave.statoMot 	= packet.data [ 6 ];
    }
    else if ( packet.dlc == 8  && packet.data [ 7 ] == COM_CAN_PARAM_1 )
    {
    	sParamRicevuti |= COM_CAN_PARAM_1;
    	azSlave.vel_ap 		= packet.data [ 0 ];
    	azSlave.vel_ch		= packet.data [ 1 ];
    	azSlave.vel_wu		= packet.data [ 2 ];
    	azSlave.passi_ap	= ( packet.data [ 3 ] <<8 ) + packet.data [ 4 ];
    	azSlave.kp			= packet.data [ 5 ];
    	//dbg_printf("vel_ap: %d vel_ch: %d vel_wu: %d passi_ap: %d kp: %d\r\n",azSlave.vel_ap,azSlave.vel_ch,azSlave.vel_wu,azSlave.passi_ap,azSlave.kp);
    }
    else if ( packet.dlc == 8 && packet.data [ 7 ] == COM_CAN_PARAM_2 )
    {
    	sParamRicevuti |= COM_CAN_PARAM_2;
    	azSlave.rampa			= packet.data [ 0 ];
    	azSlave.picco			= ( packet.data [ 1 ] << 8 ) + packet.data [ 2 ];
    	azSlave.diff_passi		= ( packet.data [ 3 ] << 8 ) + packet.data [ 4 ];
    	azSlave.pos_forzatura	= ( packet.data [ 5 ] << 8 ) + packet.data [ 6 ];
      //dbg_printf("rampa: %d picco: %d pos_forz: %d\r\n",azSlave.rampa,azSlave.picco,azSlave.pos_forzatura);
    }
}
/*------------------------------------------------------------------------------------------*/
/*	VARCO_CanSendCallbackMaster ( )			- Trasmissione CAN modo master					*/
/*------------------------------------------------------------------------------------------*/
void VARCO_CanSendCallbackMaster ( can_packet* outPacket )
{
	int lmsg = 0;
	
	#ifdef	CAN_LED_ENABLED
		LEDR_TOGGLE;
	#endif

	if ( sComSlave == COM_CAN_PARAM_1 )
    {
    	sParamTrasmessi |= COM_CAN_PARAM_1;
    	outPacket->data [ 0 ] = azSlave.vel_ap;
    	outPacket->data [ 1 ] = azSlave.vel_ch;
    	outPacket->data [ 2 ] = azSlave.vel_wu;
    	outPacket->data [ 3 ] = azSlave.passi_ap >> 8;
    	outPacket->data [ 4 ] = azSlave.passi_ap & 0xFF;
    	outPacket->data [ 5 ] = azSlave.kp;
    	outPacket->data [ 6 ] = 0;
    	outPacket->data [ 7 ] = COM_CAN_PARAM_1;
    	lmsg = 8;
    	sComSlave = COM_CAN_PARAM_2;
    }
    else if ( sComSlave == COM_CAN_PARAM_2 )
    {
    	sParamTrasmessi |= COM_CAN_PARAM_2;
    	outPacket->data [ 0 ] = azSlave.rampa;
    	outPacket->data [ 1 ] = azSlave.picco >> 8;
    	outPacket->data [ 2 ] = azSlave.picco & 0xFF;
    	outPacket->data [ 3 ] = azSlave.diff_passi >> 8;
    	outPacket->data [ 4 ] = azSlave.diff_passi & 0xFF;
    	outPacket->data [ 5 ] = azSlave.pos_forzatura >> 8;
    	outPacket->data [ 6 ] = azSlave.pos_forzatura & 0xFF;
    	outPacket->data [ 7 ] = COM_CAN_PARAM_2;
    	lmsg = 8;
    	sComSlave = COM_CAN_STATUS;

    	// tempo di attesa per sincronizzazione di Iuppiter
    	ShortTimerSet (TIM_TX_PARAM, mSEC (1000) );
    }
    else
    {
    	outPacket->data [ lmsg ++ ] = azSlave.comandoPorta;          		// data [0]
    	outPacket->data [ lmsg ++ ] = _sysIO.outSlave >> 8;                 // data [1]
    	outPacket->data [ lmsg ++ ] = _sysIO.outSlave & 0xFF;               // data [2]
    	outPacket->data [ lmsg ] = 0;                                       // data [3]
    	if ( FLAG5_APRI_ANTIF )		
			outPacket->data [ lmsg ] |= 0x01;

    	if ( counterCicliBreath == 200 )
    	{
    		outPacket->data [lmsg] |= 0x02;
    		counterCicliBreath = 0;
    		Breath ( true );
    	}

    	// inserire prima di questa istruzione altre impostazioni on/off da girare allo slave e/o all'azionamento dello slave
    	lmsg++;

    	// IL COMANDO PARTICOLARE DEVE ESSERE SEMPRE L'ULTIMO
    	if ( giraAz.gira2Slave && ( giraAz.comando == COM_AZION_CHIEDI_P2 || giraAz.comando == COM485_SAVEPARAM ) ) 
    		outPacket->data [ lmsg++ ] = giraAz.comando;                  // data[POSIZIONE_CMD_PARTICOLARE]

    	if ( giraAz.comando == COM485_SAVEPARAM )	giraAz.paramSlaveSalvati = true;
        // *********** FONDAMENTALE *********************
        // se vengono aggiunti byte va modificata la define POSIZIONE_CMD_PARTICOLARE
    }
    // azzero le variabili per girare il comando una volta sola
    giraAz.comando = 0;
    giraAz.gira2Slave = false;
    outPacket->sid = CAN_ADDR_SLAVE;
    outPacket->dlc= ( lmsg < 8 ) ? lmsg : 8;
}
/*------------------------------------------------------------------------------------------*/
/*	VARCO_CanReceiveCallbackSlave ( )		- Ricezione CAN modo slave						*/
/*------------------------------------------------------------------------------------------*/
void VARCO_CanReceiveCallbackSlave ( void *hcan, can_packet packet )
{
    if ( packet.dlc == 8 && packet.data [ 7 ] == COM_CAN_PARAM_1 )
    {
    	sParamRicevuti |= COM_CAN_PARAM_1;
    	azSlave.vel_ap		=packet.data [ 0 ];
    	azSlave.vel_ch		=packet.data [ 1 ];
    	azSlave.vel_wu		=packet.data [ 2 ];
    	azSlave.passi_ap	=( packet.data [ 3 ] << 8 ) + packet.data [ 4 ];
    	azSlave.kp			=packet.data [ 5 ];
    }
    else if ( packet.dlc == 8 && packet.data [ 7 ] == COM_CAN_PARAM_2 )
    {
    	sParamRicevuti |= COM_CAN_PARAM_2;
    	azSlave.rampa			= packet.data [ 0 ];
    	azSlave.picco			= ( packet.data [ 1 ] << 8 ) + packet.data [ 2 ];
    	azSlave.diff_passi		= ( packet.data [ 3 ] << 8 ) + packet.data [ 4 ];
    	azSlave.pos_forzatura	= ( packet.data [ 5 ] << 8 ) + packet.data [ 6 ];
    }
    else
    {
		azMaster.comandoPorta=packet.data[0];
		_sysIO.out = ( packet.data [ 1 ] << 8 ) + packet.data [ 2 ];

		if ( packet.data [ 3 ] &0x01 )
			SET_FLAG5_APRI_ANTIF_ON;
		else
			SET_FLAG5_APRI_ANTIF_OFF;

		if(packet.data[3]&0x02)
		{
			Breath(true); // sincronismo breath
		}
		if ( packet.data [ POSIZIONE_CMD_PARTICOLARE ] == COM_AZION_CHIEDI_P2 && packet.dlc == POSIZIONE_CMD_PARTICOLARE + 1 )
		{
			// modifico il comando per avere i param della porta dello slave
			giraAz.comando = COM_AZION_CHIEDI_P2 - 1;
			giraAz.nDati2Az = 0;
		}
		else if ( packet.data [ POSIZIONE_CMD_PARTICOLARE ] == COM485_SAVEPARAM && packet.dlc==POSIZIONE_CMD_PARTICOLARE + 1 ) 
		{
			giraAz.comando=COM485_SAVEPARAM;
			giraAz.nDati2Az=0;
		}
    }

    if ( sParamRicevuti & COM_CAN_PARAM_1 && sParamRicevuti & COM_CAN_PARAM_2 )
    {
    	sParamRicevuti = 0;
    	// modifico il comando perchè all'azionamento deve arrivare imposta_p1
    	giraAz.comando = COM_AZION_IMPOSTA_P2 - 1;
		giraAz.nDati2Az = 34;
		giraAz.buffer2Az [ 1 ] = azSlave.vel_wu;
		giraAz.buffer2Az [ 3 ] = azSlave.vel_ap;
		giraAz.buffer2Az [ 7 ] = azSlave.vel_ch;
		giraAz.buffer2Az [ 12 ] = azSlave.diff_passi >> 8;
		giraAz.buffer2Az [ 13 ] = azSlave.diff_passi & 0xFF;
		giraAz.buffer2Az [ 14 ] = azSlave.passi_ap >> 8;
		giraAz.buffer2Az [ 15 ] = azSlave.passi_ap & 0xFF;
		giraAz.buffer2Az [ 17 ] = azSlave.kp;
		giraAz.buffer2Az [ 29 ] = azSlave.rampa;
		giraAz.buffer2Az [ 30 ] = azSlave.picco >> 8;
		giraAz.buffer2Az [ 31 ] = azSlave.picco & 0xFF;
		giraAz.buffer2Az [ 32 ] = azSlave.pos_forzatura >> 8;
		giraAz.buffer2Az [ 33 ] = azSlave.pos_forzatura & 0xFF;
    }

}
/*------------------------------------------------------------------------------------------*/
/*	VARCO_CanSendCallbackSlave ( )			- Trasmissione CAN modo slave					*/
/*------------------------------------------------------------------------------------------*/
void VARCO_CanSendCallbackSlave ( can_packet* outPacket )
{
	int lmsg = 0;

	// questa istruzione serve per gestire l'invio diviso in due msg dello slave
    if ( sComSlave == COM_CAN_STATUS ) sComSlave = COM_CAN_PARAM_1;

    if ( giraAz.comando == giraAz.bufferFromAz [ 0 ] && giraAz.comando == COM_AZION_CHIEDI_P2-1 )// && giraAz.comando!=COM485_SAVEPARAM) // se ho della roba pronta da mandare
    {
    	if ( sComSlave == COM_CAN_PARAM_1 )
    	{
    		// di alcuni parametri mando solo la parte LO per risparmiare byte, tanto sono valori sotto i 255
			outPacket->data [ 0 ] = /*(giraAz.bufferFromAz[3]<<8)+*/giraAz.bufferFromAz [ 4 ]; // vel apertura
			outPacket->data [ 1 ] = /*(giraAz.bufferFromAz[7]<<8)+*/giraAz.bufferFromAz [ 8 ]; // vel chiusura
			outPacket->data [ 2 ] = giraAz.bufferFromAz [ 2 ]; // vel warmup
			outPacket->data [ 3 ] = giraAz.bufferFromAz [ 15 ];// passi apertura HI
			outPacket->data [ 4 ] = giraAz.bufferFromAz [ 16 ]; // passi apertura LO
			outPacket->data [ 5 ] = /*(giraAz.bufferFromAz[17]<<8)+*/giraAz.bufferFromAz [ 18 ]; // kp
			outPacket->data [ 6 ] = 0; // vuoto
			outPacket->data [ 7 ] = COM_CAN_PARAM_1;
			lmsg = 8;
			sComSlave=COM_CAN_PARAM_2;
      }
      else
      {
    	  outPacket->data [ 0 ] =/*(giraAz.bufferFromAz[29]<<8)+*/giraAz.bufferFromAz [ 30 ]; // rampa
    	  outPacket->data [ 1 ] = giraAz.bufferFromAz [ 31 ]; // picco corrente HI
    	  outPacket->data [ 2 ] = giraAz.bufferFromAz [ 32 ]; // picco corrente LO
    	  outPacket->data [ 3 ] = giraAz.bufferFromAz [ 13 ]; // diff passi HI - visualizzato come frenata chiusura
    	  outPacket->data [ 4 ] = giraAz.bufferFromAz [ 14 ]; // diff passi LO - visualizzato come frenata chiusura
    	  outPacket->data [ 5 ] = giraAz.bufferFromAz [ 33 ]; // pos forzatura HI
    	  outPacket->data [ 6 ] = giraAz.bufferFromAz [ 34 ]; // pos forzatura LO
    	  outPacket->data [ 7 ] = COM_CAN_PARAM_2;
    	  lmsg = 8;
    	  sComSlave = COM_CAN_PARAM_1;
    	  giraAz.comando = 0;
    	  giraAz.bufferFromAz [ 0 ] = 0;
      }
    }
    else // STATUS slave -> master
    {
    	outPacket->data [ lmsg++ ] = _sysIO.ing >> 8;                 // data [0]
    	outPacket->data [ lmsg++ ] = _sysIO.ing & 0xFF;               // data [1]
    	outPacket->data [ lmsg++ ] = azMaster.porta;          	      // data [2]
    	outPacket->data [ lmsg++ ] = azMaster.encoder >> 8;           // data [3]
    	outPacket->data [ lmsg++ ] = azMaster.encoder & 0xFF;         // data [4]

    	outPacket->data [ lmsg ] = 0;                                // data [5]
		if ( azMaster.allarme )	  outPacket->data [ lmsg ] |= 0x01;
		if ( azMaster.ing&0x01 )  outPacket->data [ lmsg ] |= 0x02;
		if ( azMaster.ing&0x02 )  outPacket->data [ lmsg ] |= 0x04;
		if ( azMaster.ing&0x04 )  outPacket->data [ lmsg ] |= 0x08;
		if ( azMaster.out&0x01 )  outPacket->data [ lmsg ] |= 0x10;
		if ( azMaster.out&0x02 )  outPacket->data [ lmsg ] |= 0x20;
		lmsg++;
		outPacket->data[lmsg++] = azMaster.statoMot;               // data [6]
		outPacket->data[lmsg++] = COM_CAN_STATUS;                 // data [7] ultimo byte inviabile - serve al master per capire il contenuto del msg
    }

	outPacket->sid = CAN_ADDR_MASTER;
    outPacket->dlc= ( lmsg < 8 ) ? lmsg : 8;

}


/********************************************************************************************/
