/********************************************************************************************/
/*	VARCO_IntNetwork.c			- Applicazione VARCO - Comunicazione rete 485 interna 		*/

/********************************************************************************************/
// Include di sistema e applicazione
#include "SYS_Include.h"
#include "APP_Include.h"

/********************************************************************************************/
/*	Funzioni del modulo																		*/
/*																							*/
/* 	VARCO_485InternalNetworkRcvMSgCallback ( )	- Ricezione da rete interna					*/
/* 	VARCO_485InternalNetworkSendMsgCallback ( )	- Trasmissione verso rete interna			*/
/********************************************************************************************/
void inline VARCO_485InternalNetworkRcvMSgCallback ( void )
{
	//	Per i dati del messaggio ricevuto fare riferimento alla variabile sys
	//	Buffer messaggio ricevuto
    unsigned char* pRxBuff=int_485.rx_buffer;
    //	Messaggio destinato al master
    if(pRxBuff [ PACC_DEST ] != int_485.address )
        return;

    //	Messaggio proveniente dall'azionamento
    if ( pRxBuff [ PACC_MITT ] == DEFAULT_485_AZ_ADDRESS )
    {
        //	Comando di richiesta stato
        if(pRxBuff [ PACC_COMANDO ] == COM_CHIEDI_STATO )
        {
            azMaster.porta 			= pRxBuff [ PACC_D0 ];
            azMaster.allarme		= ( pRxBuff [ PACC_D1 ] & 0x01 ) ? true : false;
            azMaster.costaChiusura	= ( pRxBuff [ PACC_D1 ] & 0x02 ) ? true : false;
            azMaster.ing			= pRxBuff [ PACC_D2 ] & 0x07;
            azMaster.out			= ( pRxBuff [ PACC_D2 ] >> 3 ) & 0x03;
            azMaster.encoder		= pRxBuff [ PACC_D3 ] * 256 + pRxBuff [ PACC_D4 ];
            azMaster.statoMot		= pRxBuff [ PACC_D5 ];
            if ( azMaster.encoder & 0x80 ) azMaster.encoder |= 0xFFFF0000;
        }
        //	Altro comando
        else
        {
            giraAz.nDatiFromAz			= pRxBuff [ PACC_LMSG ] - 2;
            giraAz.bufferFromAz [ 0 ] 	= pRxBuff [ PACC_COMANDO ];
            for ( int ind = 1; ind < giraAz.nDatiFromAz + 1; ind++ )
                giraAz.bufferFromAz [ ind ] = pRxBuff [ PACC_D0 + ind - 1 ];

            // modificato altrimenti lo slave continua a mandare il comando salva all'azionamento
            if ( giraAz.comando == COM485_SAVEPARAM ) giraAz.comando=0;
        }
    }

}
/*------------------------------------------------------------------------------------------*/
/* 	VARCO_485InternalNetworkSendMsgCallback ( )	- Trasmissione verso rete interna			*/
/*------------------------------------------------------------------------------------------*/
bool inline VARCO_485InternalNetworkSendMsgCallback ( net485SendMsgData_t* msgTx )
{
	//	Messaggi diretti all'azionamento
	if ( msgTx->destAddr == DEFAULT_485_AZ_ADDRESS )
	{
        // se devo mandare un comando all'azionamento e non devo mandarlo allo slave via canbus (chiedi/imposta P2)
        // oppure devo mandarlo allo slave via canbus ma è il comando SALVA_PARAM, quindi va mandato sia allo slave che all'azionamento
        if(giraAz.comando && !giraAz.gira2Slave)
        {
            msgTx->msgCmd = giraAz.comando;
            msgTx->msgDataSize = giraAz.nDati2Az;
            memcpy ( msgTx->msgData, giraAz.buffer2Az, giraAz.nDati2Az );
            if ( isMaster ) giraAz.comando = 0;
        }
        else
        {
        	msgTx->msgCmd = COM_CHIEDI_STATO;
        	msgTx->msgData [0] = azMaster.comandoPorta;
        	msgTx->msgData [1] = 0;
            if ( isMaster ) 	
                msgTx->msgData [1] |= 0x01;
            if ( FLAG5_APRI_ANTIF )	
                msgTx->msgData [1] |= 0x02;
            
			msgTx->msgDataSize = 2;
        }
	}
	return ( false );
}

