/********************************************************************************************/
/*	VARCO.h					- Applicazione VARCO - File include principale			 		*/
/*	Da aggiungere in APP_Include.h															*/

/********************************************************************************************/

#ifndef APP_USERCODE_VARCO_H_
#define APP_USERCODE_VARCO_H_
#include "limits.h"

/*------------------------------------------------------------------------------------------*/
/*	Funzioni di connessione al framework													*/
/*------------------------------------------------------------------------------------------*/
void VARCO_Startup ( void );
void VARCO_ThreadCreate ( );
void VARCO_InitMainThread ( void );
void VARCO_MainLoopProc ( void );
void VARCO_HiSpeedTimerCallback ( void );
void VARCO_CanReceiveCallbackMaster ( void *hcan, can_packet packet );
void VARCO_CanReceiveCallbackSlave ( void *hcan, can_packet packet );
void VARCO_CanSendCallbackMaster ( can_packet* outPacket );
void VARCO_CanSendCallbackSlave ( can_packet* outPacket );
void VARCO_485InternalNetworkRcvMSgCallback ( void );
bool VARCO_485InternalNetworkSendMsgCallback ( net485SendMsgData_t* msgTx );
bool VARCO_485ExternalNetworkRcvMSgCallback ( uint8_t* pRxBuff, net485SendMsgData_t* msgTx );

void VARCO_TermDebugInfoCommand ( int argc, char **argv );
void VARCO_TermOpenCommand ( int argc, char **argv );

// COMANDI PER AGR CONSOLLE
	#define COM485_AGR_STATE           		0x24
    #define COM485_AGR_GETSTATE            	0x24 // retrocompatibilita' con TP Consolle
    #define COM485_AGR_GETSTATE_1          	0x3F
	#define COM485_AGR_DO               	0x81
    #define COM485_IUPPITER_EMERG           0x04
    #define COM485_IUPPITER_OPEN_PORTA		0x08	// D[0] = 1 porta interna D[0] = 2 esterna
    #define COM485_AGR_DO_EMERG_ON          0x37
    #define COM485_AGR_DO_EMERG_OFF         0x43
    #define COM485_AGR_DO_RICH_ING          0x14
    #define COM485_AGR_DO_RICH_USC          0x15
    #define COM485_AGR_DO_LIB_ING           0x16
    #define COM485_AGR_DO_LIB_USC           0x17
    #define COM485_AGR_DO_CONTROL_ING       0x18
    #define COM485_AGR_DO_CONTROL_USC       0x19
    #define COM485_AGR_DO_BLOC_ING          0x20
    #define COM485_AGR_DO_BLOC_USC          0x21
    #define COM485_AGR_CONTATORI            0x22


// COMANDI ANELLO INTERNO
	#define COM_BROADCAST_AGR               0x01
	#define COM_CHIEDI_STATO                0x04
	#define COM485_RICHIESTA_VERSIONE       0x05
	#define COM485_SAVEPARAM				0x20
	#define COM485_SET_DATA_ORA				0x01	
	#define COM485_LEGGI_ORA				0x22
	#define COM485_LEGGI_DATA				0x23
	#define COM485_GIRA_AZ                  0xA0
	#define COM485_STATI_PC                 0x51
    #define COM_AZION_CHIEDI_P2			    0x53
    #define COM_AZION_IMPOSTA_P2			0x55
    #define COM_AZION_CHIEDI_IMPOSTAZIONI	0x56
    #define COM_AZION_SCRIVI_IMPOSTAZIONI	0x57
	#define COM485_TRASM_I_O                0x29
	#define COM485_VERSIONE                 0xf0
	#define COM485_SERIAL                   0xf1
	#define COM485_PROT_EXT                 0xf7
    #define PROT_EXT_RICH_STATO             0x53 // 'S'
    #define PROT_EXT_AVVIA_TR               0x41 // 'A'
    #define PROT_EXT_ATTIVA_EM              0x45 // 'E'
    #define PROT_EXT_DISATTIVA_EM           0x65 // 'e'
    #define PROT_EXT_RIFIUTA_TR             0x58 // 'X'
	#define COM485_EDSETUP_CHIEDI_FLAG		0x72
	#define COM485_EDSETUP_CHIEDI_PARAMETRI	0x73
	#define COM485_EDSETUP_CHIEDI_SELEZIONE	0x74
	#define COM485_EDSETUP_SCRIVI_FLAG		0x75
	#define COM485_EDSETUP_SCRIVI_PARAMETRI	0x76
	#define COM485_EDSETUP_SCRIVI_SELEZIONE	0x77
	#define COM485_GETNET					0x98
	#define COM485_SETNET					0x99

// Flag
#define FLAG1_ALLARME_ROSSO         (sysCfg.m_vttFlag[0]&0x01) // durante l'allarme i led sono rossi
#define FLAG2_SEMPRE_VERDE          (sysCfg.m_vttFlag[0]&0x02) // durante il transito i led sono verdi (sia lato ingresso sia lato uscita), senza sono rosso/versi
#define FLAG3_ALLARME_INVALID_TRANS (sysCfg.m_vttFlag[0]&0x04) // se durante il transito la persona torna indietro, col flag la porta si chiuse subito e segna allarme
#define FLAG4_INVERTI_DIR_EMERG     (sysCfg.m_vttFlag[0]&0x08) // invertire la direzione di emergenza
#define FLAG5_APRI_ANTIF            (sysCfg.m_vttFlag[0]&0x10) // in caso di costa in corrente, col flag riapre, senza blocca la porta con inn a denti
	#define SET_FLAG5_APRI_ANTIF_ON     sysCfg.m_vttFlag[0]|=0x10
	#define SET_FLAG5_APRI_ANTIF_OFF    sysCfg.m_vttFlag[0]&=~0x10
#define FLAG6_SALVA_PRENOTAZIONE    (sysCfg.m_vttFlag[0]&0x20) // in caso di controflusso, viene salvata la prenotazione e riapre il varco, senza si annullano
#define FLAG7_DISABILE              (sysCfg.m_vttFlag[0]&0x40) // varco 900, considera una fotocellula in più per l'antiinf
#define FLAG8_MOD_CONTATTI          (sysCfg.m_vttFlag[0]&0x80) // per impostare le modalità (libero/bloccato/controllato) tramite contatti ingorando la seriale
#define FLAG9_                      (sysCfg.m_vttFlag[1]&0x01)
#define FLAG10_                     (sysCfg.m_vttFlag[1]&0x02)
#define FLAG11_                     (sysCfg.m_vttFlag[1]&0x04)
#define FLAG12_                     (sysCfg.m_vttFlag[1]&0x08)
#define FLAG13_                     (sysCfg.m_vttFlag[1]&0x10)
#define FLAG14_                     (sysCfg.m_vttFlag[1]&0x20)
#define FLAG15_                     (sysCfg.m_vttFlag[1]&0x40)
#define FLAG16_                     (sysCfg.m_vttFlag[1]&0x80)
#define FLAG17_                     (sysCfg.m_vttFlag[2]&0x01)


#define PAR1_TIMEOUT_TRANSITO       APP_LSEC(sysCfg.m_vttParam[0]) // tempo dopo il quale la porta si richiude se non passa nessuno o viene segnalato tramite buzzer trans non valido
#define PAR2_TIMEOUT_PERMANENZA     APP_LSEC(sysCfg.m_vttParam[1]) // tempo dopo il quale scatta l'allarme loitering in quiete se occupate le fotocellule
#define PAR3_TIMEOUT_FINE_ALL       APP_LSEC(sysCfg.m_vttParam[2]) // tempo di ritardo dopo la fine dell'allarme
#define PAR4_TEMPO_FRENO            APP_LSEC(sysCfg.m_vttParam[3]) // tempo di attivazione del freno in caso di allarme forzatura
#define PAR5_                       sysCfg.m_vttParam[4]
#define PAR6_                       sysCfg.m_vttParam[5]
#define PAR7_                       sysCfg.m_vttParam[6]
#define PAR8_                       sysCfg.m_vttParam[7]
#define PAR9_                       sysCfg.m_vttParam[8]
#define PAR10_                      sysCfg.m_vttParam[9]
#define PAR11_                      sysCfg.m_vttParam[10]
#define PAR12_                      sysCfg.m_vttParam[11]
#define PAR13_                      sysCfg.m_vttParam[12]
#define PAR14_                      sysCfg.m_vttParam[13]

// Ingressi Master
#define IN_FOTO_1M             IO_ReadInput(1)         // entrata varco verticale
#define IN_FOTO_2M             IO_ReadInput(2)         // entrata varco verticale
#define IN_FOTO_3M             IO_ReadInput(3)         // entrata varco verticale
#define IN_FOTO_4M             IO_ReadInput(4)         // pavimento lato ingresso
#define IN_RICH_TRANSITO_USC_M IO_ReadInput(5)
#define IN_FOTO_6M             IO_ReadInput(6)         // pavimento lato ingresso
#define IN_BLOCCO_USC          IO_ReadInput(7)
#define IN_FOTO_8M             IO_ReadInput(8)         // alto lato ingresso
#define IN_FOTO_9M             IO_ReadInput(9)         // alto lato ingresso
#define IN_FOTO_10M            !IO_ReadInput(10)       // barriera verticale porta (NC)
#define IN_LIBERO_ING          IO_ReadInput(11)
#define IN_LIBERO_USC          IO_ReadInput(12)
#define IN_RICH_TRANSITO_ING   IO_ReadInput(13)
#define IN_BLOCCO_ING          IO_ReadInput(14)
#define IN_RICH_EMERGENZA_M    IO_ReadInput(15)
#define IN_VARCO_SLAVE         IO_ReadInput(16)
#define IN_AZ_NO_RETE          (azMaster.ing&0x01)

// Ingressi Slave
#define IN_FOTO_1S                  IO_ReadInputSlave(1)
#define IN_FOTO_2S                  IO_ReadInputSlave(2)
#define IN_FOTO_3S                  IO_ReadInputSlave(3)
#define IN_FOTO_4S                  IO_ReadInputSlave(4)
#define IN_RICH_TRANSITO_USC_SLV    IO_ReadInputSlave(5)
#define IN_FOTO_6S                  IO_ReadInputSlave(6)
#define IN_FOTO_7S                  IO_ReadInputSlave(7)
#define IN_FOTO_8S                  IO_ReadInputSlave(8)
#define IN_FOTO_9S                  IO_ReadInputSlave(9)
#define IN_FOTO_10S                 !IO_ReadInputSlave(10)
#define ing11s_libero               IO_ReadInputSlave(11)
#define ing12s_libero               IO_ReadInputSlave(12)
#define ing13s_libero               IO_ReadInputSlave(13)
#define IN_SPYFLOOR_ALARM           IO_ReadInputSlave(14)
#define IN_RICH_EMERGENZA_SLV       IO_ReadInputSlave(15)

// Uscite Master
#define OUT_LED_IN_M_BLUE_ON         IO_SetOutput(1,true)
#define OUT_LED_IN_M_BLUE_OFF        IO_SetOutput(1,false)
#define OUT_LED_IN_M_GREEN_ON        IO_SetOutput(2,true)
#define OUT_LED_IN_M_GREEN_OFF       IO_SetOutput(2,false)
#define OUT_LED_IN_M_RED_ON          IO_SetOutput(3,true)
#define OUT_LED_IN_M_RED_OFF         IO_SetOutput(3,false)
#define OUT_BUZZER_ON                IO_SetOutput(4,true)
#define OUT_BUZZER_OFF               IO_SetOutput(4,false)
#define OUT_LED_USC_M_BLUE_ON        IO_SetOutput(5,true)
#define OUT_LED_USC_M_BLUE_OFF       IO_SetOutput(5,false)
#define OUT_LED_USC_M_GREEN_ON       IO_SetOutput(6,true)
#define OUT_LED_USC_M_GREEN_OFF      IO_SetOutput(6,false)
#define OUT_LED_USC_M_RED_ON         IO_SetOutput(7,true)
#define OUT_LED_USC_M_RED_OFF        IO_SetOutput(7,false)
#define OUT_BREATH_ON                IO_SetOutput(8,true)
#define OUT_BREATH_OFF               IO_SetOutput(8,false)
#define OUT_ANTE_CHIUSE_ON           IO_SetOutput(11,true)
#define OUT_ANTE_CHIUSE_OFF          IO_SetOutput(11,false)
#define OUT_GUASTO_ON                IO_SetOutput(12,true)
#define OUT_GUASTO_OFF               IO_SetOutput(12,false)
#define OUT_VALIDAZ_ING_ON           IO_SetOutput(13,true)
#define OUT_VALIDAZ_ING_OFF          IO_SetOutput(13,false)
#define OUT_VALIDAZ_USC_ON           IO_SetOutput(14,true);IO_SetOutputSlave(14,true)
#define OUT_VALIDAZ_USC_OFF          IO_SetOutput(14,false);IO_SetOutputSlave(14,false)
#define OUT_ALLARME_ON               IO_SetOutput(15,true)
#define OUT_ALLARME_OFF              IO_SetOutput(15,false)
#define OUT_BATTERIE_ON              IO_SetOutput(16,true)
#define OUT_BATTERIE_OFF             IO_SetOutput(16,false)

#define SEL1_MOD_ING                sysCfg.m_vttSel[0]
#define SEL2_MOD_USC                sysCfg.m_vttSel[1]
#define SEL3_TAILGATE_ACCURACY      sysCfg.m_vttSel[2]

#define mask_APERTA 0x06

// Uscite Slave
#define OUT_LED_IN_S_BLUE_ON        IO_SetOutputSlave(1,true)
#define OUT_LED_IN_S_BLUE_OFF       IO_SetOutputSlave(1,false)
#define OUT_LED_IN_S_GREEN_ON       IO_SetOutputSlave(2,true)
#define OUT_LED_IN_S_GREEN_OFF      IO_SetOutputSlave(2,false)
#define OUT_LED_IN_S_RED_ON         IO_SetOutputSlave(3,true)
#define OUT_LED_IN_S_RED_OFF        IO_SetOutputSlave(3,false)

#define OUT_LED_USC_S_BLUE_ON        IO_SetOutputSlave(5,true)
#define OUT_LED_USC_S_BLUE_OFF       IO_SetOutputSlave(5,false)
#define OUT_LED_USC_S_GREEN_ON       IO_SetOutputSlave(6,true)
#define OUT_LED_USC_S_GREEN_OFF      IO_SetOutputSlave(6,false)
#define OUT_LED_USC_S_RED_ON         IO_SetOutputSlave(7,true)
#define OUT_LED_USC_S_RED_OFF        IO_SetOutputSlave(7,false)



#define IN_RICH_EMERGENZA    (IN_RICH_EMERGENZA_M||IN_RICH_EMERGENZA_SLV)
#define IN_RICH_TRANSITO_USC (IN_RICH_TRANSITO_USC_M||IN_RICH_TRANSITO_USC_SLV)

// Selezioni
enum enmMod
{
	MOD_CONTROLLATO,
	MOD_LIBERO,
	MOD_BLOCCATO
};
enum enmAccuracy
{
	ACC_STRINGENTE,
	ACC_MEDIO,
	ACC_BASSO,
	MAX_ACCURACY
};

enum enmDir
{
	DIR_INGRESSO=1,
	DIR_USCITA
};

enum enmRichEmerg
{
	RICH_DA_ING=0x01,
	RICH_DA_SERIALE=0x02
};

//	Codici di stato del varco
typedef enum enmStatoVarco
{
	ST_WARMUP,
	ST_QUIETE,
	// stati per direzione ingresso
	ST_INIZIO_TR_I,
	ST_ATTESA_INIZIO_I,
	ST_TRANS_INC_I,
	ST_PERSONA_OLTRE_I,
	ST_FINE_TR_I,
	// stati per direzione uscita
	ST_INIZIO_TR_U,
	ST_ATTESA_INIZIO_U,
	ST_TRANS_INC_U,
	ST_PERSONA_OLTRE_U,
	ST_FINE_TR_U,

	ST_ALLARME,
	ST_EMERGENZA,
	ST_ULTIMA_APERTURA
} enmStatoVarco_t;

//	Codici di stato della porta
typedef enum enmStatoPorta
{
	STATO_SCONOSCIUTO,
	STATO_CHIUSA,
	STATO_APERTA_O = 0x02,
	STATO_APERTA_AO =0x04,
	STATO_WU = 0xFF
} enmStatoPorta_t;

//	Codici comandi porte
typedef enum enmComandiPorte
{
	MOT_FERMAPORTA,
	MOT_CHIUDIPORTA,
	MOT_APRIPORTA_1,
	MOT_APRIPORTA_2
} enmComandiPorte_t;

//	Codici comandi semafori
typedef enum enmSemafori
{
	SEM_OFF,
	SEM_ROSSO,
	SEM_VERDE,
	SEM_GIALLO,
	SEM_BLU,
	SEM_VIOLA,
	SEM_CIANO,
	SEM_BIANCO
} enmSemafori_t;

//	Codici stato motori
typedef enum enmStatiMot
{
	STMOT_QUIETE=0,
	STMOT_APRIPORTA,
	STMOT_CHIUDIPORTA,
	STMOT_WARMUP,
	STMOT_EMERGENZA,
	STMOT_FORZATURA_INC,
	STMOT_PORTAFORZATA
} enmStatiMot_t;

//	Struttura parametri azionamento
typedef struct	{
	enmStatoPorta_t porta;
	enmComandiPorte_t comandoPorta;
	bool allarme;
	bool costaChiusura;
	int encoder;
	unsigned char ing;
	unsigned char out;
	enmStatiMot_t statoMot;
	unsigned char vel_ap;
	unsigned char vel_ch;
	unsigned char vel_wu;
	unsigned int passi_ap;
	unsigned char kp;
	unsigned char rampa;
	unsigned int picco;
	unsigned int pos_forzatura;
	unsigned int diff_passi;
} _azionamento;

//	Struttura comandi azionamento
typedef struct
{
	unsigned short comando;
	unsigned short nDatiFromAz;
	unsigned short nDati2Az;
	unsigned char bufferFromAz[MAX_BUFF_AZ];
	unsigned char buffer2Az[MAX_BUFF_AZ];
	bool gira2Slave;
	bool paramSlaveSalvati;
}_giraAz;

//	Struttura comandi semafori
typedef struct
{
	enmSemafori_t inMaster;
	enmSemafori_t inSlave;
	enmSemafori_t uscMaster;
	enmSemafori_t uscSlave;
}_semafori;

//	Struttura allarmi
typedef struct
{
	unsigned char loitering:1;
	unsigned char accodamento:1;
	unsigned char controflusso:1;
	unsigned char err_motore:1;
	unsigned char forzatura:1;
	unsigned char err_fotocellule:1;
	unsigned char bit_libero1:1;
	unsigned char bit_libero2:1;
} _allarmi;


#define PorteChiuse()   (azMaster.porta==STATO_CHIUSA&&azSlave.porta==STATO_CHIUSA)
#define PorteAperte1()   (azMaster.porta==STATO_APERTA_O&&azSlave.porta==STATO_APERTA_AO) // DIR INGRESSO
#define PorteAperte2()   (azMaster.porta==STATO_APERTA_AO&&azSlave.porta==STATO_APERTA_O) // DIR USCITA
#define SetSemafIng(color)  semafori.inMaster=semafori.inSlave=color
#define SetSemafUsc(color)  semafori.uscMaster=semafori.uscSlave=color

#define	isSlaveDead		ShortTimerIsExpired(TIM_SLAVE_ALIVE)

#ifndef NONSIGN
	#define NONSIGN			0x7FFFFFFF //ULONG_MAX
#endif

#define PULS_EM_IMPULSIVO           false      // NOTA BENE - SE ANDRA' UTILIZZATO VA RITESTATA BENE LA GESTIONE ATTIVAZIONE DA PULSANTE E RIMOZIONE DA SERIALE E VICEVERSA

//	Dichiarazione generale variabili
extern uint32_t			lAnagrafica;
extern _azionamento 	azMaster;
extern _azionamento 	azSlave;
extern _giraAz 			giraAz;
extern enmStatoVarco_t 	statoVarco;
extern bool 			isMaster;
extern bool 			sEmergenza;
extern _allarmi 		allarmi;
extern bool 			sBuzzer;
extern bool 			bRichIng;
extern bool 			bRichUsc;
extern unsigned char 	sRichEm;
extern unsigned char 	sPrenotazIng;
extern unsigned char 	sPrenotazUsc;
extern unsigned int 	cntIngressi;
extern unsigned int 	cntUscite;
extern unsigned char 	sParamRicevuti;
extern unsigned char 	sParamTrasmessi;
extern unsigned char	sComSlave;
extern unsigned int 	counterCicliBreath;
extern _semafori 		semafori;



// QUESTE SONO ANCORA DA DEFINIRE
void Breath ( bool b );
void gestSwingate();
void gestSemafori();
void controlli();
void ComandoPorta(enmComandiPorte_t cmd);
bool tailgate(bool bReset,unsigned char dir, unsigned char* ptrPrenot);
bool IngressoOccupato();
bool UscitaOccupata();
bool ReadInput(unsigned short ING);
bool ReadInputSlave(unsigned short ING);
void SetOutput(unsigned short OUT, bool status);
void SetOutputSlave(unsigned short OUT, bool status);
bool ReadOutput(unsigned short OUT);
bool ReadOutputSlave(unsigned short OUT);
bool GetTextStatus(enmStatoVarco_t stato, char* str);



#endif /* APP_USERCODE_VARCO_H_ */


/********************************************************************************************/
