/********************************************************************************************/
/*	APP_CodeConfig.h			- Configurazione generale applicazione						*/

/********************************************************************************************/

#ifndef	__APP_CODE_CONFIG_DEFINED
#define	__APP_CODE_CONFIG_DEFINED	1



/*------------------------------------------------------------------------------------------*/
/*	Configurazione moduli APP																*/
/*------------------------------------------------------------------------------------------*/
//	Abilita il test della flash QSPI all'avvio del demo (output su terminale)
//#define APP_DEMO_TEST_FLASH

/*------------------------------------------------------------------------------------------*/
/*	Codice del programma																	*/
/*------------------------------------------------------------------------------------------*/
//	Nome firmware demo
#define _CODE_PROG	(const char*)"5244015pARMd"
#define	APP_BOARD_ID	"01234567"


/*------------------------------------------------------------------------------------------*/
/*	Clock di sistema																		*/
/*------------------------------------------------------------------------------------------*/
//#define	MCUSPEED_480 (1)

/*------------------------------------------------------------------------------------------*/
/*	Configurazione moduli di sistema e liberie												*/
/*------------------------------------------------------------------------------------------*/
//	Se definito effettua il polling di tutti i moduli di sistema da un thread separato dal main
#define	SYS_POLLING_PRIVATE_THREAD	(1)
//	Timing in millisecondi del thread di polling di sistema
#define SYS_POLLING_THREAD_SLEEPTIME	mSEC(1)
//	Se definito abilita la bufferizzazione della porta USB / COM Virtuale
#define	CON_USB_BUFFERED	(1)
//	Se definito abilita il terminale ASCII
#define	ASCII_TERM_ENABLED	(1)
//	Tempo di polling buffers della porta USB
#define COM_USB_POLLING_TIME			mSEC(50)
//	Intervallo di polling rete 485 interna
#define	INT_485_TIMEOUT_POLLING			mSEC(80)
//	Definizioni della base per gli indici di ingressi e uscite
//#define	IO_BASE_0		(1)				//	Primo I/O ha indice 0
#define	IO_BASE_1		(1)			//	Primo I/O ha indice 1


//	Long timer tick (default = 100ms)
#define	LTIMERS_TICK		mSEC(100)

//	Utilizzo HISPEEDTIMER (timer hardware veloce)
#define	HISPEED_TIMER_ENABLED	(1)
// 	Dividendo per calcolo frequenza del timer
#define HISPEED_TIMER_CONST	240
// 	Selezione frequenza timer hardware veloce
#ifdef HISPEED_TIMER_ENABLED
	//	Frequenze ammesse per HSTIMER (in KHz): 20, 16, 12, 10, 8, 6, 4. 2
	#define	HSTIMER_FREQ_20KHZ	20
	#define	HSTIMER_FREQ_16KHZ	16
	#define	HSTIMER_FREQ_12KHZ	12
	#define	HSTIMER_FREQ_10KHZ	10
	#define	HSTIMER_FREQ_8KHZ	8
	#define	HSTIMER_FREQ_6KHZ	6
	#define	HSTIMER_FREQ_4KHZ	4
	#define	HSTIMER_FREQ_2KHZ	2
	//	Frequenza selezionata per il timer HS
	#define HISPEED_TIMER_FREQ_KHZ	HSTIMER_FREQ_8KHZ
#endif

#ifdef HISPEED_TIMER_ENABLED
	// Se definito connette la gestione degli I/O al timer hardwaare veloce
	//	Richiede l'abilitazione precedente del timer hardware veloce
	#define IO_HIISPEED				(1)
#endif
//	Tempo di polling I/O su timer normale di sistema
//  Non usato se definito I/O veloce
#define IO_POLLING_TIME					mSEC(5)


//	Abilita gestione rete interna 485 su USART6
#define	USART6_INT_485_ENABLED	(1)
//	Abilita gestione rete esterna 485 su USART3
#define USART3_EXT_485_ENABLED 	(1)

//	Baud rate seriali
#define	APP_DEFAULT_BAUDRATE_UART3	(uint32_t)19200
#define	APP_DEFAULT_BAUDRATE_UART6	(uint32_t)57600

//	Abilita la gestione della CAN
#define	CAN1_ENABLED			(1)
//	LED indicatori comunicazione (R=TX, V=RX)
#define	CAN_LED_ENABLED			(1)
//	Debug con messaggi su terminale USB
//#define DEBUG_TX_CAN				(1)
//#define DEBUG_RX_CAN				(1)

//	Visualizza il tempo di sistema nell'help (da verificare se funziona)
#define	MY_SYSTIME				(1)

//	Il terminale ASCII richiede la bufferizzazione della porta USB
#ifdef ASCII_TERM_ENABLED
	#ifndef CON_USB_BUFFERED
#define	CON_USB_BUFFERED	(1)
	#endif
#endif


/*------------------------------------------------------------------------------------------*/
/*	Indirizzi di rete																		*/
/*------------------------------------------------------------------------------------------*/

//	Rete 485
//	Indrizzo master rete 485 interna
#define	APP_DEFAULT_INT_MASTER_ADDR	0x4B
//	Indirizzo slave rete 485 esterna
#define	APP_DEFAULT_EXT_SLAVE_ADDR	0x04
//	Indirizzo azionamento on-board
#define	DEFAULT_485_AZ_ADDRESS		0xC5
//  Indirizzo scheda peso per pagina Varie  di Iuppiter(orologio)
#define	DEFAULT_485_PESO_ADDRESS	0x0A
//	Indirizzo broadcast per rete esterna
#define IND_485_BROADCAST       0x0A

//	Rete CAN
#define CAN_ADDR_MASTER   	100
#define CAN_ADDR_SLAVE    	CAN_ADDR_MASTER-1
#define CAN_SLAVE_MASK  	(0x0FF)
#define	CAN_POLLING_TIME		mSEC(100)
#define	CAN_SLAVE_ALIVE_TIME	mSEC(1000)

//	Rete Ethernet
#define	ETH_ENABLED			(1)
//	Indirizzo IP predefinito
#define	APP_DEFAULT_IP_0	192
#define	APP_DEFAULT_IP_1	168
#define	APP_DEFAULT_IP_2	1
#define	APP_DEFAULT_IP_3	25
//	Gateway predefinito
#define	APP_DEFAULT_GWAY_0	192
#define	APP_DEFAULT_GWAY_1	168
#define	APP_DEFAULT_GWAY_2	1
#define	APP_DEFAULT_GWAY_3	1
//	Subnet mask predefinita
#define	APP_DEFAULT_MASK_0	255
#define	APP_DEFAULT_MASK_1	255
#define	APP_DEFAULT_MASK_2	255
#define	APP_DEFAULT_MASK_3	0
//	MAC address predefinito
#define APP_HW_MAC_0		0x20
#define APP_HW_MAC_1		0x45 	// era 0x80
#define APP_HW_MAC_2		0xA5	// era 0xE1
#define APP_HW_MAC_3		0x00
#define APP_HW_MAC_4		0x00
#define APP_HW_MAC_5		0x00

//	Credenziali di accesso al sito web
#define	APP_USERNAME		"admin"
#define	APP_USERPWD			"admin"

//	Server TCP/IP
// #define TCP_SERVER_DEBUG	(1)
#define	TCP_MAX_SERVER		8
#ifdef ETH_ENABLED
#define	WEB_SERVER_ENABLED		(1)
	#define FWUPDATE_SERVER_ENABLED (1)
#endif
#define	HTTP_PORT			80
#define RAWDATA_PORT 		1001
#define	FWUPDATE_PORT		1200
#define FWUPDATE_TIMEOUT_MS		(10000)
//#define	FWUPDATE_DEBUG_MSG		(1)


/*------------------------------------------------------------------------------------------*/
/* 	Definzione dei timer dell'applicazione													*/
/*	Elencare nella enum i nomi dei timers dell'applicazione	e chiudere con APP_MAX_TIMER	*/
/* 	P.es. definire due tumers a 32 bit chiamati TIMER_0 e TIMER_!							*/
/* 	enum AppTimers {																		*/
/*		TIMER_0,																			*/
/*		TIMER_1,																			*/
/*		APP_MAX_TIMER																		*/
/*	};																						*/
/*																							*/
/*	Definizione dei Long timers dell'applicazione		100 ms ogni Tick					*/
/*	Elencare nella enum i nomi dei long timers e chiudere con APP_MAX_LTIMER				*/
/* 	P.es. definire due tumers a 32 bit chiamati L_TIMER_0 e L_TIMER_!						*/
/*	enum AppLongTimers {																	*/
/*		L_TIMER_0,																			*/
/*		L_TIMER_1,																			*/
/*		APP_MAX_LTIMER																		*/
/*	};																						*/
/*------------------------------------------------------------------------------------------*/
enum AppTimers
{
	//	Timer definiti per tutte le applicazioni (usati dalla CAN)
	TIM_CAN,
	TIM_SLAVE_ALIVE,
	TIM_TX_PARAM,
	TIM_DOPO_SFONDAMENTO,
	TIM_AVANZAMENTO_STATO,
	TIM_BACK_STATO,
	TIM_f8,TIM_f9,TIM_f9_i,TIM_DISTANZA_ACC,
	TIM_BUZZER,
	TIM_EMERG_ALLARME,
	TIM_ANTIF_ACCODAMENTO,
	TIM_NO_RETE,
	TIM_DEB_ING,TIM_DEB_USC,
	TIM_ANTIF_DISABILE,
	TIM_SPYFLOOR_ALARM,
	APP_MAX_TIMER
};

enum AppLongTimers
{
	LTIM_TEMPO_FRENO,
	LTIM_TIMEOUT_TR,
	LTIM_PERMANENZA_QUIETE,
	LTIM_FINE_ALLARME,
	LTIM_PULS_EM,
	LTIM_RIMUOVI_EM,
	LTIM_TR_VALIDO_ING,
	LTIM_TR_VALIDO_USC,
	LTIM_ULTIMA_APERTURA,
	LTIM_ATTESA_INIZIO,
	APP_MAX_LTIMER
};

#endif

