/********************************************************************************************/
/* 	APP_Include.h				- File di dichiarazioni principale del modulo APP			*/

/********************************************************************************************/
#include "APP_Terminal.h"
#include "APP_ConfigData.h"
#include "APP_User.h"

#define LWIP_DEBUG


//	Applicazione VARCO
#include "Varco.h"

/*------------------------------------------------------------------------------------------*/
/*	Struttura di configurazione																*/
/*------------------------------------------------------------------------------------------*/
extern struct 	APP_SystemConfigData sysCfg;
extern unsigned char net485InternalDevAddr [];

/*------------------------------------------------------------------------------------------*/
/*	Funzioni varie di supporto																*/
/*------------------------------------------------------------------------------------------*/
#define	SYSTEM_RESET	HAL_NVIC_SystemReset ( );

/*------------------------------------------------------------------------------------------*/
/*	Funzioni in APP_Settings.c																*/
/*------------------------------------------------------------------------------------------*/
bool APP_LoadDefaultSettings ( void );
bool APP_LoadSettingsFromFlashMemory ( void );
bool APP_SaveSettingsToFlashMemory ( void );
void APP_AsyncSaveSettings ( void );
void APP_AsyncSaveSettingsProc ( void );

/*------------------------------------------------------------------------------------------*/
/*	Funzioni in APP_Main.c																	*/
/*------------------------------------------------------------------------------------------*/
void APP_Bootstrap ( void );
void APP_SystemTimerCallback ( uint64_t nTicks );

/*------------------------------------------------------------------------------------------*/
/*	Funzioni in APP_Uart.c																	*/
/*------------------------------------------------------------------------------------------*/
#ifdef USART6_INT_485_ENABLED
void APP_485InternalNetworkRcvMsgError ( void );
uint8_t APP_485InternalNetworkGetNextScanAddress ( uint8_t prevAddress );
#endif
#ifdef USART3_EXT_485_ENABLED
void APP_485ExternalNetworkRcvMsgError ( void );
#endif

#ifndef USART6_INT_485_ENABLED
void APP_UART6RxCharCallback ( uint8_t rxChar );
bool APP_UART6TxNextCharRequest ( uint8_t* nextChar );
void APP_UART6StartSendChar ( uint8_t frstChar );
#endif
#ifndef USART3_EXT_485_ENABLED
void APP_UART3RxCharCallback ( uint8_t rxChar );
bool APP_UART3TxNextCharRequest ( uint8_t* nextChar );
void APP_UART3StartSendChar ( uint8_t frstChar );
#endif

/*------------------------------------------------------------------------------------------*/
/*	Funzioni in APP_Can.c																	*/
/*------------------------------------------------------------------------------------------*/
//	void canbus_send(void *hcan, can_packet packet )
#define APP_CanSend			canbus_send

/*------------------------------------------------------------------------------------------*/
/*	Macro gestione timers																	*/
/*	Chiamano funzioni dichiarate in LIB_Timers.h											*/
/*------------------------------------------------------------------------------------------*/

#define	APP_LSEC(x)							10*x	// era (mSEC(x)/LTIMERS_TICK) ma non funzionava

/*------------------------------------------------------------------------------------------*/
/*	Funzioni gestione flash memory															*/
/*------------------------------------------------------------------------------------------*/

//uint8_t QSPI_Read 				( uint8_t* pData, uint32_t ReadAddr, uint32_t Size );
#define APP_FlashRead 			QSPI_Read
//uint8_t QSPI_Write      		( uint8_t* pData, uint32_t WriteAddr, uint32_t Size );
#define	APP_FlashWrite			QSPI_Write
//uint8_t QSPI_Erase_Sector		( uint32_t Address );
#define	APP_FlashEraseSector	QSPI_Erase_Sector
//uint8_t QSPI_Erase_Block32K 	( uint32_t Address );
#define	APP_FlashEraseBlock32K	QSPI_Erase_Block32K
//uint8_t QSPI_Erase_Block64K 	( uint32_t Address );
#define	APP_FlashEraseBlock64K	QSPI_Erase_Block64K
//uint8_t QSPI_Erase_Chip 		( void );
#define	APP_FlashEraseChip		QSPI_Erase_Chip
//uint8_t QSPI_GetInfo    		( QSPI_Info* pInfo );
#define	APP_FlashGetInfo		QSPI_GetInfo
//	Indirizzi della flash
#define	FLASH_BASE_ADDR			0x10000
#define	FLASH_CONFIG_ADDR		FLASH_BASE_ADDR
#define	FLASH_CONFIG_SIZE		0x1000
#define	FLASH_BASE_USER			(FLASH_BASE_ADDR+FLASH_CONFIG_SIZE)

/*------------------------------------------------------------------------------------------*/
/*	Funzioni comunicazione su USB / COM Virtuale in APP_USB.c								*/
/*------------------------------------------------------------------------------------------*/
void 	APP_USBReceiveCallback 				( uint8_t* rxBuffer, int rxCount );
void	APP_USBSendCompleteCallback 		( void );
void 	APP_USBConnectCallback 				( void );
//uint8_t	APP_USBIsReady 					( uint8_t* txData, int txSize );
#define APP_USBIsReady		USB_IsReady
//uint8_t APP_USBSendData ( uint8_t* txData, int txSize )
#define	APP_USBSendData		USB_SendData
// int APP_USBGetRxBufferCharCount ( void )
#define	APP_USBGetRxBufferCharCount			USB_UsbBufferGetCharCount
//	uint8_t APP_USBGetRxBufferChar ( void )
#define	APP_USBGetRxBufferChar				USB_UsbBufferGetChar
//	bool APP_isUSBConnexted ( void )
#define	APP_isUSBConnexted					CDC_USBIsReady


/*------------------------------------------------------------------------------------------*/
/*	Funzioni gestione rete e server TCP/IP													*/
/*------------------------------------------------------------------------------------------*/
void APP_TcpStartAppServer 		( void );
bool APP_TcpReceiveCallback 	( TCPServerHandle* hServer, char* pData, uint16_t dataLen );
void APP_TcpServerConnected 	( TCPServerHandle* hServer );
void APP_TcpServerDisconnected 	( TCPServerHandle* hServer );
void APP_EthCableConnected 		( void );
void APP_EthCableDisconnected 	( void );
// bool APP_TcpSendData ( TCPServerHandle* hServer, unsigned char* pData, unsigned int dataSize );
#define APP_TcpSendData 		SRV_SendData
// bool APP_TcpBufferedSendByte ( TCPServerHandle* hServer, uint8_t ch, bool forceSend );
#define APP_TcpBufferedSendByte SRV_BufferedSendByte
// void APP_ServerDisconnect ( TCPServerHandle* hServer );
#define APP_ServerDisconnect	SRV_Disconnect
// bool APP_IsEthCableConnected ( void );
#define	APP_IsEthCableConnected	SRV_IsEthCableConnected
// bool APP_ServerStart ( TCPServerHandle* hServer );
#define APP_ServerStart			SRV_ServerInit
// bool APP_ServerIsConnected ( TCPServerHandle* hServer );
#define	APP_ServerIsConnected 	SRV_IsConnected
// uint8_t APP_GetServerCount ( void );
#define	APP_GetServerCount 		SRV_GetServerCount
// TCPServerHandle* APP_GetServerHandle ( uint8_t nServer );
#define APP_GetServerHandle 	SRV_GetServerHandle


/*------------------------------------------------------------------------------------------*/
/*	Funzioni web server																		*/
/*------------------------------------------------------------------------------------------*/
//bool APP_StartWebServer ( void );
#define	APP_StartWebServer		WSRV_Start
//bool APP_RegisterWebPage ( hWebPage* newPage );
#define	APP_RegisterWebPage		WSRV_RegisterWebPage
//bool APP_RegisterWebResource ( resType rType, int16_t resId, char* res );
#define	APP_RegisterWebResource	WSRV_RegisterResource

/*------------------------------------------------------------------------------------------*/
/*	Funzioni in APP_Utility.c																*/
/*------------------------------------------------------------------------------------------*/
bool APP_utiParseIPAddress ( char* ipAddr, uint8_t* pOutput );
bool APP_utiParseMacAddress ( char* macAddr, uint8_t* pOutput );

/*------------------------------------------------------------------------------------------*/
/*	Funzioni in LIB_DateTime.c																*/
/*------------------------------------------------------------------------------------------*/
//void APP_GetSystemDate ( sysDate* dt );
#define	APP_GetSystemDate	TM_GetSystemDate
//void APP_GetSystemTime ( sysTime* tm );
#define	APP_GetSystemTime	TM_SetSystemTime
//void APP_SetSystemDate ( sysDate* dt );
#define	APP_SetSystemDate	TM_SetSystemDate
//void APP_SetSystemTime ( sysTime* tm );
#define	APP_SetSystemTime	TM_SetSystemTime
//void APP_GetSystemDateString ( char* strDate );
#define	APP_GetSystemDateString		TM_GetSystemDateString
//void APP_GetSystemTimeString ( char* strTime );
#define APP_GetSystemTimeString		TM_GetSystemTimeString
//void APP_SetSystemDateString ( char* strDate );
#define	APP_SetSystemDateString		TM_SetSystemDateString
//void APP_GetSystemTimeString ( char* strTime );
#define APP_SetSystemTimeString		TM_SetSystemTimeString

/*------------------------------------------------------------------------------------------*/
/*	Funzioni in SYS_Functions.c																*/
/*------------------------------------------------------------------------------------------*/
/*
void 	SYS_SystemTimerTick ( void );
void 	SYS_SystemLoopInit ( void );
void 	SYS_SystemLoopCallback ( void );
*/

/*------------------------------------------------------------------------------------------*/
/*	Macro controllo LED																		*/
/*------------------------------------------------------------------------------------------*/
//	LED verde
#define LEDV_OFF	  		HAL_GPIO_WritePin(LED_V_GPIO_Port, LED_V_Pin, GPIO_PIN_SET)
#define LEDV_ON				HAL_GPIO_WritePin(LED_V_GPIO_Port, LED_V_Pin, GPIO_PIN_RESET)
#define LEDV_TOGGLE			HAL_GPIO_TogglePin(LED_V_GPIO_Port, LED_V_Pin)
// 	LED rosso
#define LEDR_OFF	  		HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_SET)
#define LEDR_ON				HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_RESET)
#define LEDR_TOGGLE			HAL_GPIO_TogglePin(LED_R_GPIO_Port, LED_R_Pin)


/*------------------------------------------------------------------------------------------*/
/*	Varie																					*/
/*------------------------------------------------------------------------------------------*/
#define dbg_printf printf
