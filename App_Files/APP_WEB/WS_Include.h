/********************************************************************************************/
/* 	WS_Include.h				- File include del sito web									*/

/********************************************************************************************/

#ifndef	__WEBISTE__DEFINED
#define __WEBISTE__DEFINED

#define	INCLUDE_LOGO_BITMAP		(1)
//#define INCLUDE_LED_OFF			(1)
//#define INCLUDE_LED_RED			(1)
//#define INCLUDE_LED_GREEN		(1)


/********************************************************************************************/
/*	Funzioni del sito																		*/
/********************************************************************************************/
void WS_RegisterWebPages ( void );
void WS_RegisterWebResources ( void );
void WS_GetGlobalParamCallback ( uint8_t pageId, uint16_t paramId, char* paramValue );


/********************************************************************************************/
/*	Prototipi delle pagine																	*/
/********************************************************************************************/
//	Templates
/*
void WS_WebPageOpenCallback ( uint8_t nPageId );
void WS_WebPageGetParamCallback ( uint8_t pageId, uint16_t paramId, char* paramValue );
void WS_WebPagePostParamCallback ( uint8_t pageId, uint16_t paramId, char* paramValue );
extern const char WS_WebPageData [];
*/
//	Home page
extern const char WS_HomepageData [];
void WS_HomepageOpenCallback ( uint8_t nPageId );
void WS_HomepageGetParamCallback ( uint8_t pageId, uint16_t paramId, char* paramValue );
void WS_HomepagePostParamCallback ( uint8_t pageId, uint16_t paramId, char* paramValue );

//	Menu principale
extern const char WS_MainMenuPageData [];
void WS_MainMenuPageOpenCallback ( uint8_t nPageId );
void WS_MainMenuPageGetParamCallback ( uint8_t pageId, uint16_t paramId, char* paramValue );
void WS_MainMenuPagePostParamCallback ( uint8_t pageId, uint16_t paramId, char* paramValue );

//	PAgina di configurazione
extern const char WS_ConfigPageData [];
void WS_ConfigPageOpenCallback ( uint8_t nPageId );
void WS_ConfigPageGetParamCallback ( uint8_t pageId, uint16_t paramId, char* paramValue );
void WS_ConfigPagePostParamCallback ( uint8_t pageId, uint16_t paramId, char* paramValue );

//	Pagina 1
extern const char WS_Page1Data [];
void WS_Page1OpenCallback ( uint8_t nPageId );
void WS_Page1GetParamCallback ( uint8_t pageId, uint16_t paramId, char* paramValue );
void WS_Page1PostParamCallback ( uint8_t pageId, uint16_t paramId, char* paramValue );

//	Pagina 2
extern const char WS_Page2Data [];
void WS_Page2OpenCallback ( uint8_t nPageId );
void WS_Page2GetParamCallback ( uint8_t pageId, uint16_t paramId, char* paramValue );
void WS_Page2PostParamCallback ( uint8_t pageId, uint16_t paramId, char* paramValue );

/********************************************************************************************/
/*	Risorse																					*/
/********************************************************************************************/
extern const char htmlPageHeader [];
extern const char htmlPageFooter [];
extern const char pageTimerScript [];
extern const char srvSendAndRcvDataScript [];

#ifdef INCLUDE_LOGO_BITMAP
	extern const char logoBitmap [];
#endif
#ifdef INCLUDE_LED_OFF
	extern const char ledOffImg [];
#endif
#ifdef INCLUDE_LED_RED
	extern const char ledRedImg [];
#endif
#ifdef INCLUDE_LED_GREEN
	extern const char ledGreenImg [];
#endif

#endif

/********************************************************************************************/
