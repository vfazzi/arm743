/********************************************************************************************/
/* 	WS_Page1.c					- Pagina 1 del sito web										*/

/********************************************************************************************/
#include "SYS_Include.h"
#include "APP_Include.h"

/********************************************************************************************/
/*	Definizione della pagina																*/
/********************************************************************************************/
const char WS_ConfigPageData [] =
"<html>"
	//	Header
	"<head>"
		"<meta charset=\x22UTF-8\x22>"
		"<title>ARM STM32H743Zi board - Configurazione di sistema</title>"
		//	Javascript per l'aggiornamento periodico dell'orologio
        "<script type=text/javascript>"
			//	Inserisce gli script per la gestione del timer di pagina
			//	Contiene le funzioni pageTimerStart () e pageTimerStop ()
			"<#S1>"
			//	Inserisce gli script con le funzioni getDataFromServer ( )
			//	e sendDataToServer ( )
			"<#S2>"
			//	Funzioni utente definite nella pagina
			//	Callback ricezione risposta da getDataFromServer ()
			"function onRcvData () {"
				//	La proprietà this.responseText contiene i dati ricevuti
				"timeBox=document.getElementById('I9');"
				//	L'aggiornamento dell'orario viene fatto solo se non si sta scrivendo nel box
				"if( timeBox!=document.activeElement){"
					"timeBox.value=this.responseText.slice(3,11);"
				"}"
			"}"
			//	Callback del timer di pagina
			"function onPageTimerTick() {"
				"getDataFromServer('F9');"
			"}"
			//	Funzione update data e ora
			"function dateTimeUpdate () {"
				"dateBox=document.getElementById('I8');"
				"timeBox=document.getElementById('I9');"
				"sendData = 'F8=' + dateBox.value;"
				"sendData+='&';"
				"sendData += 'F9=' + timeBox.value;"
				"sendData+='&';"
				"sendDataToServer(sendData);"
			"}"
		"</script>"
	"</head>"
	//	Corpo pagina
	"<body onLoad='pageTimerStart(2000);' onUnload='pageTimerStop();>"
		"<div style='position:relative'>"
			//	Intestazione della pagina (risorsa HTML n.1)
			"<#H1>"
			// Colonna sinistra menu
			"<div align=center style='float:left; width:180px; height:600px; background-color:teal' >"
				// 	Ritorno al menu principale
				"<p><input type=button value='Main menu' style=width:160px onClick=\x22location.href='Pag1'\x22></p>"
			"</div>"
			//*************************************************************
			// Area utile della pagina
			"<div style='height:600px; background-color:#EEEEEE'>"
				"<br><br>"
				"<center>"
					// Impostazioni rete e sistema
					"<form method=post>"
						"<table align=center width=400px frame=box>"
							"<tr>"
								"<td>Nome utente</td>"
								"<td><input maxlength=8 name=F1 type=text value=<#F1>></td>"
								"</tr>"
								"<tr>"
								"<td>Password</td>"
								"<td><input maxlength=8 name=F2 type=password value=<#F2>></td>"
							"</tr>"
								"<tr><td>&nbsp</td></tr>"
							"<tr>"
								"<td>Nome dispositivo</td>"
								"<td><input maxlength=8 name=F3 type=text value=<#F3>></td>"
							"</tr>"
							"<tr>"
								"<td>MAC Address</td>"
								"<td><input maxlength=18 name=F4 type=text value=<#F4>></td>"
							"</tr>"
							"<tr>"
								"<td>Indirizzo IP</td>"
								"<td><input maxlength=15 name=F5 type=text value=<#F5>></td>"
							"</tr>"
							"<tr>"
								"<td>Subnet mask</td>"
								"<td><input maxlength=15 name=F6 type=text value=<#F6>></td>"
							"</tr>"
							"<tr>"
								"<td>Gateway predefinito</td>"
								"<td><input maxlength=15 name=F7 type=text value=<#F7>></td>"
							"</tr>"
							"</tr>"
								"<tr><td>&nbsp</td></tr>"
							"<tr>"
						"</table>"
						"<p><input type=submit value='Salva' style=width:160px></p>"
					"</form>"
					"<br><br>"
					//	Data e ora di sistema
					"<table align=center width=400px frame=box>"
						"<tr>"
							"<td>Data</td>"
							"<td><input maxlength=10 name=F8 id=I8 type=text value=<#F8>></td>"
						"</tr>"
						"<tr>"
							"<td>Ora</td>"
							"<td><input maxlength=8 name=F9 id=I9 type=text value=<#F9>></td>"
						"</tr>"
						"</tr>"
							"<tr><td>&nbsp</td></tr>"
						"<tr>"
					"</table>"
					"<p><input type=button value='Salva' style=width:160px onClick='dateTimeUpdate();'></p>"
				"</center>"
			"</div>"
			// Fine area utile della pagina
			//*************************************************************
			//	Base della pagina (risorsa HTML n.2)
			"<#H2>"
		"</div>"
	"/<body>"
"/<html>";


/********************************************************************************************/
/*	Callback di pagina																		*/
/*																							*/
/*	WS_ConfigPageOpenCallback ( )			- Apertura della pagina							*/
/*	WS_ConfigPageGetParamCallback ( )		- GET di un parametro							*/
/*	WS_ConfigPagePostParamCallback ( )		- POST di un parametro							*/
/********************************************************************************************/
/*------------------------------------------------------------------------------------------*/
/*	WS_ConfigPageOpenCallback ( )			- Apertura della pagina							*/
/*------------------------------------------------------------------------------------------*/
void WS_ConfigPageOpenCallback ( uint8_t nPageId )
{

}
/*------------------------------------------------------------------------------------------*/
/*	WS_ConfigPageGetParamCallback ( )		- GET di un parametro							*/
/*------------------------------------------------------------------------------------------*/
void WS_ConfigPageGetParamCallback ( uint8_t pageId, uint16_t paramId, char* paramValue )
{
	switch ( paramId )
	{
		case 1:		//	Nome utente
			strcpy ( paramValue, sysCfg.m_userName );
		break;
		case 2:		//	Password
			strcpy ( paramValue, sysCfg.m_userPwd );
		break;
		case 3:		//	ID scheda
			strcpy ( paramValue, sysCfg.m_boardID );
		break;
		case 4:		//	MAC Address
			sprintf ( paramValue,
					  "%02x:%02x:%02X:%02X:%02X:%02X",
					  sysCfg.m_macAddress [ 0 ],
					  sysCfg.m_macAddress [ 1 ],
					  sysCfg.m_macAddress [ 2 ],
					  sysCfg.m_macAddress [ 3 ],
					  sysCfg.m_macAddress [ 4 ],
					  sysCfg.m_macAddress [ 5 ] );
		break;
		case 5:		//	IP address
			sprintf ( paramValue,
					  "%d.%d.%d.%d",
					  sysCfg.m_ipAddress [ 0 ],
					  sysCfg.m_ipAddress [ 1 ],
					  sysCfg.m_ipAddress [ 2 ],
					  sysCfg.m_ipAddress [ 3 ] );
		break;
		case 6:		//	Subnet mask
			sprintf ( paramValue,
					  "%d.%d.%d.%d",
					  sysCfg.m_subnetMask [ 0 ],
					  sysCfg.m_subnetMask [ 1 ],
					  sysCfg.m_subnetMask [ 2 ],
					  sysCfg.m_subnetMask [ 3 ] );
		break;
		case 7:		//	Gateway
			sprintf ( paramValue,
					  "%d.%d.%d.%d",
					  sysCfg.m_gatewayAddress [ 0 ],
					  sysCfg.m_gatewayAddress [ 1 ],
					  sysCfg.m_gatewayAddress [ 2 ],
					  sysCfg.m_gatewayAddress [ 3 ] );
		break;
		case 8:		//	Data
			APP_GetSystemDateString ( paramValue );
		break;
		case 9:		//	Ora
			APP_GetSystemTimeString ( paramValue );
		break;
	}
}
/*------------------------------------------------------------------------------------------*/
/*	WS_ConfigPagePostParamCallback ( )		- POST di un parametro							*/
/*------------------------------------------------------------------------------------------*/
void WS_ConfigPagePostParamCallback ( uint8_t pageId, uint16_t paramId, char* paramValue )
{
	switch ( paramId )
	{
		case 1:		//	Nome utente
			strcpy ( sysCfg.m_userName, paramValue );
		break;
		case 2:		//	Password
			strcpy ( sysCfg.m_userPwd, paramValue );
		break;
		case 3:		//	ID scheda
			strcpy ( sysCfg.m_boardID, paramValue );
		break;
		case 4:		//	MAC Address
			APP_utiParseMacAddress ( paramValue, sysCfg.m_macAddress );
		break;
		case 5:		//	IP address
			APP_utiParseIPAddress ( paramValue, sysCfg.m_ipAddress );
		break;
		case 6:		//	Subnet mask
			APP_utiParseIPAddress ( paramValue, sysCfg.m_subnetMask );
		break;
		case 7:		//	Gateway
			APP_utiParseIPAddress ( paramValue, sysCfg.m_gatewayAddress );
		break;
		case 8:		//	Data
			APP_SetSystemDateString ( paramValue );
		break;
		case 9:		//	Ora
			APP_SetSystemTimeString ( paramValue );
		break;
		//	Chiamato al termine della sequenza di parametri per concludere il POST
		case 0:
			APP_AsyncSaveSettings ( );
		break;


	}
}






/********************************************************************************************/
