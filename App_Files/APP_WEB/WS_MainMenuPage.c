/********************************************************************************************/
/* 	WS_MainMenuPage.c			- Pagina menu principale del sito web						*/

/********************************************************************************************/
#include "SYS_Include.h"
#include "APP_Include.h"

/********************************************************************************************/
/*	Definizione della pagina																*/
/********************************************************************************************/
const char WS_MainMenuPageData [] =
"<html>"
	//	Header
	"<head>"
		"<meta charset=\x22UTF-8\x22>"
		"<title>ARM STM32H743Zi board - Main menu</title>"
	"</head>"
	//	Corpo pagina
	"<body>"
		"<div style='position:relative'>"
			//	Intestazione della pagina (risorsa HTML n.1)
			"<#H1>"
			// Colonna sinistra menu
			"<div align=center style='float:left; width:180px; height:600px; background-color:teal' >"
				// Pagina di configurazione
				"<p><input type=button style=width:160px value='Configurazione' onClick=\x22location.href='Pag2'\x22></p>"
				// Pagina numero 1
				"<p><input type=button style=width:160px value='Pagina 1' onClick=\x22location.href='Pag10'\x22></p>"
				// Pagina numero 2
				"<p><input type=button style=width:160px value='Pagina 2' onClick=\x22location.href='Pag11'\x22></p>"
				// Reset scheda
				"<p><input type=button value='Reset' style=width:160px onClick=\x22location.href='Pag999'\x22></p>"
			"</div>"
	        //*************************************************************
	        // Area utile della pagina
			"<div style='height:600px; background-color:#EEEEEE'>"
				"<center>"
					//	Testo della pagina
					"<br>Scheda SAIMA 524415r3<br>"
					"<br>Kernel per ARM STM32H743Zi versione 0.94<br>"
					//	Logo
					"<#I1>"
				"</center>"
	        "</div>"
	        // Fine area utile della pagina
	        //*************************************************************
			//	Base della pagina (risorsa HTML n.2)
			"<#H2>"
		"</div>"
	"/<body>"
"/<html>";

/********************************************************************************************/
/*	Callback di pagina																		*/
/*																							*/
/*	WS_MainMenuPageOpenCallback ( )			- Apertura della pagina							*/
/*	WS_MainMenuPageGetParamCallback ( )		- GET di un parametro							*/
/*	WS_MainMenuPagePostParamCallback ( )	- POST di un parametro							*/
/********************************************************************************************/
/*------------------------------------------------------------------------------------------*/
/*	WS_MainMenuPageOpenCallback ( )			- Apertura della pagina							*/
/*------------------------------------------------------------------------------------------*/
void WS_MainMenuPageOpenCallback ( uint8_t nPageId )
{

}
/*------------------------------------------------------------------------------------------*/
/*	WS_MainMenuGetParamCallback ( )			- GET di un parametro							*/
/*------------------------------------------------------------------------------------------*/
void WS_MainMenuPageGetParamCallback ( uint8_t pageId, uint16_t paramId, char* paramValue )
{

}
/*------------------------------------------------------------------------------------------*/
/*	WS_MainMenuPostParamCallback ( )		- POST di un parametro							*/
/*------------------------------------------------------------------------------------------*/
void WS_MainMenuPagePostParamCallback ( uint8_t pageId, uint16_t paramId, char* paramValue )
{

}


/********************************************************************************************/
