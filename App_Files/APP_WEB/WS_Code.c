/********************************************************************************************/
/* 	WS_Code.c					- Codice comune del sito web								*/

/********************************************************************************************/
#include "SYS_Include.h"
#include "APP_Include.h"

/********************************************************************************************/
/*	Funzioni del modulo																		*/
/*																							*/
/*	WS_RegisterWebPages ( )				- Registra le pagine web sul server					*/
/*	WS_RegisterWebResources ( )			- Registra le risorse web sul server				*/
/*	WS_GetGlobalParamCallback ( )		- Callback di richiesta parametri globali			*/
/********************************************************************************************/
/*------------------------------------------------------------------------------------------*/
/*	WS_RegisterWebPages ( )				- Registra le pagine web sul server					*/
/*------------------------------------------------------------------------------------------*/
void WS_RegisterWebPages ( void )
{
	hWebPage newPage;
	//	Home page
	newPage.p_pageAuthUserOnly = false;
	newPage.p_pageId = 0;
	newPage.p_pagePtr = (char*)WS_HomepageData;
	newPage.p_pageOpenCallback = WS_HomepageOpenCallback;
	newPage.p_pageGetParamCallback = WS_HomepageGetParamCallback;
	newPage.p_pagePostParamCallback = WS_HomepagePostParamCallback;
	APP_RegisterWebPage ( &newPage );

	//	Menu principale
	newPage.p_pageAuthUserOnly = true;
	newPage.p_pageId = 1;
	newPage.p_pagePtr = (char*)WS_MainMenuPageData;
	newPage.p_pageOpenCallback = WS_MainMenuPageOpenCallback;
	newPage.p_pageGetParamCallback = WS_MainMenuPageGetParamCallback;
	newPage.p_pagePostParamCallback = WS_MainMenuPagePostParamCallback;
	APP_RegisterWebPage ( &newPage );

	//	Configurazione
	newPage.p_pageAuthUserOnly = true;
	newPage.p_pageId = 2;
	newPage.p_pagePtr = (char*)WS_ConfigPageData;
	newPage.p_pageOpenCallback = WS_ConfigPageOpenCallback;
	newPage.p_pageGetParamCallback = WS_ConfigPageGetParamCallback;
	newPage.p_pagePostParamCallback = WS_ConfigPagePostParamCallback;
	APP_RegisterWebPage ( &newPage );

	//	Pagina 1
	newPage.p_pageAuthUserOnly = true;
	newPage.p_pageId = 10;
	newPage.p_pagePtr = (char*)WS_Page1Data;
	newPage.p_pageOpenCallback = WS_Page1OpenCallback;
	newPage.p_pageGetParamCallback = WS_Page1GetParamCallback;
	newPage.p_pagePostParamCallback = WS_Page1PostParamCallback;
	APP_RegisterWebPage ( &newPage );

	//	Pagina 2
	newPage.p_pageAuthUserOnly = true;
	newPage.p_pageId = 11;
	newPage.p_pagePtr = (char*)WS_Page2Data;
	newPage.p_pageOpenCallback = WS_Page2OpenCallback;
	newPage.p_pageGetParamCallback = WS_Page2GetParamCallback;
	newPage.p_pagePostParamCallback = WS_Page2PostParamCallback;
	APP_RegisterWebPage ( &newPage );
}

/*------------------------------------------------------------------------------------------*/
/*	WS_RegisterWebResources ( )			- Registra le risorse web sul server				*/
/*------------------------------------------------------------------------------------------*/
void WS_RegisterWebResources ( void )
{
	//	Registra i blocchi comuni di codice HTML
	APP_RegisterWebResource ( RES_HTML, 1, (char*)htmlPageHeader );
	APP_RegisterWebResource ( RES_HTML, 2, (char*)htmlPageFooter );
	APP_RegisterWebResource ( RES_SCRIPT, 1, (char*)pageTimerScript );
	APP_RegisterWebResource ( RES_SCRIPT, 2, (char*)srvSendAndRcvDataScript );


	#ifdef INCLUDE_LOGO_BITMAP
	APP_RegisterWebResource ( RES_IMAGE, 1, (char*)logoBitmap );
	#endif
	#ifdef INCLUDE_LED_OFF
		APP_RegisterWebResource ( RES_IMAGE, 1001, (char*)ledOffImg );
	#endif
	#ifdef INCLUDE_LED_RED
		APP_RegisterWebResource ( RES_IMAGE, 1002, (char*)ledRedImg );
	#endif
	#ifdef INCLUDE_LED_GREEN
		APP_RegisterWebResource ( RES_IMAGE, 1003, (char*)ledGreenImg );
	#endif

}
/*------------------------------------------------------------------------------------------*/
/*	WS_GetGlobalParamCallback ( )		- Callback di richiesta parametri globali			*/
/*	pageId		= pagina che richiede il parametro											*/
/*	paramId		= ID del parametro (>= 1000)												*/
/*	paramValue	= buffer di ritorno valore del parametro									*/
/*------------------------------------------------------------------------------------------*/
void WS_GetGlobalParamCallback ( uint8_t pageId, uint16_t paramId, char* paramValue )
{
	switch ( paramId )
	{
		// ID della scheda
		case 1000:
			strcpy ( paramValue, sysCfg.m_boardID );
		break;
		//	Indirizzo IP
		case 1001:
			sprintf ( 	paramValue,
						"%d.%d.%d.%d",
						sysCfg.m_ipAddress[0],
						sysCfg.m_ipAddress[1],
						sysCfg.m_ipAddress[2],
						sysCfg.m_ipAddress[3] );
		break;
	}
}

/********************************************************************************************/
