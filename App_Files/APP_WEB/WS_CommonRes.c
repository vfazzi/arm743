/********************************************************************************************/
/* 	WS_CommonRes.c				- Definizione risorse comuni del sito web					*/

/********************************************************************************************/
#include "SYS_Include.h"
#include "APP_Include.h"

/********************************************************************************************/
/*	Parti comuni di codice HTML																*/
/********************************************************************************************/
//	Intestazione
const char htmlPageHeader [] = {
	"<div align=center style='height:50px; background-color:SkyBlue; border:thin'>"
		"<font size=4>ARM STM32H743ZI Card</font><br>"
		"<font size=3>Firmware versione 0.94</font>"
		"<font size=3>(C)2022 SAIMA Sicurezza S.p.a.</font>"
	"</div>"
};
//	Piede di pagina
const char htmlPageFooter [] = {
   "<div style='clear:both; background-color:SkyBlue; height:30px;'>" \
   	   "ID scheda: <#F1000> - Indirizzo IP: <#F1001>"
   "</div>"
};


/********************************************************************************************/
/*	Script																					*/
/********************************************************************************************/
const char pageTimerScript [] = {
	"ck=null;ptTime=1000;"
	//	Avvio timer di pagina
	//	tmTime = intervallo del timer in millisecondi
	"function pageTimerStart(tmTime) {"
		"ptTime=tmTime;"
		"ck=setTimeout('clkUpd()',ptTime);"
	"}"
	//	Arresto timer di pagina
	"function pageTimerStop() {"
		"clearTimeout(ck);"
	"}"
	//	Callback del timer di pagina
	"function clkUpd() {"
		"ck=setTimeout('clkUpd()',ptTime);"
		"if(onPageTimerTick) onPageTimerTick ( );"
	"}"
};

const char srvSendAndRcvDataScript [] = {
	//	Richiesta dati dal server
	//	Datalist = 'F1&F2;..Fm dove n=nome del campo da richiedere
	//	Es. getDataFromServer('F1&F2') richiede i campi F1 e F2
	"function getDataFromServer (dataList) {"
		"if(onRcvData){"
			"myReq=new XMLHttpRequest();"
			"myReq.onload=onRcvData;"
			"myReq.open('GETDATA','/ ' + dataList,true);"
			"myReq.send(null);"
		"}"
	"}"
	//	Invio dati al server
	//	dataList = F1=dati1&f2=dati2&...
	//	dove F1 = nome del dato, dati1 = contenuto
	"function sendDataToServer (dataList) {"
		"if(onRcvData){"
			"myReq=new XMLHttpRequest();"
			"myReq.onload=onRcvData;"
			"myReq.open('POSTDATA','/ ' + dataList,true);"
			"myReq.send(null);"
		"}"
	"}"

};

/********************************************************************************************/
/*	Immagini																				*/
/********************************************************************************************/
// LED rosso acceso
#ifdef INCLUDE_LED_OFF
  // Immagine LED spento
  const char ledOffImg [ ] = {
    "data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQBAMAAADt3e"
    "JSAAAAB3RJTUUH4wUFCQwMCeiEZgAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAARnQU1BAACxjwv8YQUAAA"
    "AJUExURf///wAAAICAgElfqbYAAAA0SURBVHjaY2BgYBQUFGAAASElJUUQLagEBIIQAbAQoxIYCEAEQE"
    "IkMeDa4QYirIBbCnUGAFGEC9MvopUxAAAAAElFTkSuQmCC"
  };
#endif

#ifdef INCLUDE_LED_RED
  // Immagine LED rosso
  const char ledRedImg [ ] = {
    "data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQBAMAAADt3"
    "eJSAAAAB3RJTUUH4wUFCQgT4IxMlwAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAARnQU1BAACxjwv8YQUAA"
    "AAJUExURf///wAAAP8AAMDJnbAAAAA0SURBVHjaY2BgYBQUFGAAASElJUUQLagEBIIQAbAQoxIYCEAEQ"
    "EIkMeDa4QYirIBbCnUGAFGEC9MvopUxAAAAAElFTkSuQmCC"
  };
#endif

#ifdef INCLUDE_LED_GREEN
  // Immagine LED verde
  const char ledGreenImg [ ] = {
    "data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQBAMAAADt3"
    "eJSAAAAB3RJTUUH4wUFCQgwQus95QAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAARnQU1BAACxjwv8YQUAA"
    "AAJUExURf///wAAAAD/AO11VC8AAAA0SURBVHjaY2BgYBQUFGAAASElJUUQLagEBIIQAbAQoxIYCEAEQ"
    "EIkMeDa4QYirIBbCnUGAFGEC9MvopUxAAAAAElFTkSuQmCC"
  };
#endif

//	Logo SAIMA
#ifdef INCLUDE_LOGO_BITMAP
	const char logoBitmap [] = {
		"data:image/gif;base64,"
		"R0lGODlhUQE9APf/AHYAAX0CAH4IBX4KCX0QDX4SEgB1swF8uACEvQCKvgCQvwCNwQCTxACVyACZxgCcyg2axwqbyhSezACgxwCh"
		"zAqjzQ6ozgOi0BOkzRWpzhqkzRupzRKk0BWp0Rin0Rup0CakzSSqzi2rziKm0SOr0yin0Cqs0iew1DCmzjOt0jqs0jOy1TSz2jqz"
		"1ju02D242kGnz0Ks00Kz1UK22US42Um01Eq22k+41ky72la32FS921m22Fq82mC/3VjA3FzE4WTD3WrD3nHF33fI3XnF32PE4GXI"
		"4GvF4W3I4XTG4nLK4XzN44IDAYQGCIUKBIQLCYgGAYgMBYkOCYMPEIwWDYUUEoUWGYcZGI0WEY0YEYwcGpAWDpAYEo0fIJAdIIsh"
		"HZIiHI0hII8yK480MZEjIZEnKZIpJZMsKpUuMZYyLZg4NqA8PplBPaJAP5xIRJ5WU6BEQ6NJRKRMSqlPT6VSTalZV65eYathXrFj"
		"XqplY6lwbap5drFkY7NqZbJrarhta7ZxbLp3dq2CfbmAfcOEf7SIh7KQjLSZl7egnbunpb61rL+1s4HN443P5YrP6IbR5pLU5pLU"
		"6JbZ65nV6Zza6r/EwKTb7Kvd7bLd7bve8a/g77Lh7rXj8Lzl8r7p8sGDg8OJhcOKiMmPjcqPkcWRjciRj8aSkcmVkcqWmsuZlcyc"
		"m9Cens2eoM2kntGhn8qop8K6uNKkpNKmqNOppdWsq9isq9eusdaxr9aystu1s9u3udq5tty8uuG7usW+wMTBvd3AveDBvsXFxsrO"
		"0cnQzcvW1t3EwdDb28/e49He4c3g3cDk787h4sXn88Pp8srn8svq9NLh4dDs9NPt+Nnu9tvv+Nby9trx9t3x+OLEwuHEyOPIxOXL"
		"y+jNyujSzubT0ufY0+fY1enV1O3X2erY1e3c3PDd3O3e4e3g3fLg3u7j4uTy9uTz+eb4++j29uv2+uz4+vHk4/Lp5vTq6fjt7Pju"
		"8Pbw7fX19PX1+fT5/Pr08/v59v7+/gAAAAAAAAAAACwAAAAAUQE9AAAI/wD5CRxIsKDBgwgTKlzIsKHDhxAjSpxIsaLFixgzatzI"
		"saPHjyBDihxJsqTJkyhTqlzJsqXLlzBjyvzI7hIjHBtIVIBQIYIFGkssYZtJtKjRo0bXRWqx4IHTEwxOFJmxhEeHpgxSNKKGtKvX"
		"r2AxPuvxYAEDR5dUIIGXAAc/ENP4wYjUSUiDBA5uLGs5b9uuWapKhTo1S9c4fWETK1488BoPBk0fNMjnLAYNdglI5FtAidKBfNCC"
		"PKBQlkGNuCbl1fIjh0yVJwICMJkdYMCVNHMC3ZLHuLdvmY0aNKVAfAEkfkRo8LP0SFoDaEtI8GukwgFxChMWNBhCMhsfM7KZSP+h"
		"Qp48FizlnTCRTabOrX0f05k7h84cOvro8utHN67exXkABijggPIghhA9AyaYoDz+PbSPPApGCKA88FU0D4QQSigPbzK1I8MC1l1H"
		"AQM+XDNJCyHoUIFwFjTggSWQmBDiBMQ9kIAI1oDECxwEiEdFFlxkgUUWRBYp5JBAUiEFEwOgoYqBGtFSABdDYqEFkkYSOcUdFsmD"
		"hhZggEFGmGSWCUYVpSCUzpdmtmkmFmzY81AdVbhpZ5hW2FIRL2DeWaYVoMQEjQQLiHjdBAw08EAFHJyAgw9GrGCCBww41cAFho7o"
		"wCYdqVMHAE78CGQWomZpqpFbRBGAGtlsVEcASkr/IeustMoaxRPiWKiFeLX2KgUAnyBUzhW8RuGrrVEY60QAuTgUD7HHHquqLBXd"
		"EYCxUmB7LBNl0PPSJgwwgKmIZTmQQhLPJCQNIy2UNZqhDTBAyUbclAFrkERieaqp+nLBBAGpZDROFT/iS+W+XARAioVkSLFvlgGE"
		"gpA5Xzj8sJFcBBlAIA7dwgS+FxuJRQC1UBRPGBYHqS/GVDDRbEucJIrdzCM+wIM0D1UTRARNjUucA5xllE0VTmRc5I8rZ6nyeUgG"
		"GaQUAZiCkSoBnEcklUlf7YQaFUpED8pElnpqAIEeVE7FIVNB5RZDPrGGQ9YefPTD6DFBC0VUF4new1tk/8EEHS1JI7OIQNeAs0Tr"
		"PNYAvAtwahE3VjxxsMFIZm01v0T2TUUAsVzEhuQhH73FAMFQNA/YQoJsKtkIoVNx1nrrS8UT2zC0TxpFGx06eUyUPFEbRQsZOqlS"
		"qLNSPhEwcB2miU5ykSYQiCtiAw0MRVE9anwMO+z5UoGlqLpLUYU3FXkTapFUqj52H6Y3rP7YEpvdhcXcq11lFgGMwlAvT/TNfZZ1"
		"u5tEsvEE75HKe+9DVQCCpRKmjAtTC9DA4S7CjhIswGcUUMAHKjKIAAhJVEwjlfCqVB4Dxq5IfctcwuRQEUB40Gog3FffpPAFDkXk"
		"dFIAWeVCiL+yGYRi47kYev/2NiQmyKFrCBkEEwAYsvMEQIARCUQA5Ca3U1lta0gsSSQSQKPrMEAD8OBICgpFHBolAAkTqYcWLKY2"
		"JqZwSQOYwhSewIQ2mtCKWBAAL9J4hijsq35MmMVEcDi8iE1sfsIbXtiqYI6FwMEJijRSy6D4EBz+L5FqI08TgIESdzggRBS4AAMk"
		"0A6PmEB5xLnAAxiAGohQzWCqg6HfwjCIXWQjG7kIRBU+BqT/BYAPE8lFE8SGuSNlgW34Y6FECBk6Qx7EHIi8JACfqJBuFDCBF5uk"
		"RIBRtQSqbFRXw4K/OHYSHJBxAhdwQAOu8RF4JM9nCjiBROigvSyBED1NoEM8flj/hyU2bV9bk1NE+gk73YmqYFmQAhbIsUzUhcyZ"
		"P4xmJInEhDooJBVTnGjY7CYRPvCyiXojUhTSAKWRXKNSIgoaSDzBgC6SZgGdgIg+xgBJfGGpclI4g7cQIgcmbKGEQCWPFAbQjYjI"
		"Y43YRFjqAkCIhlrsofGLqMOkmaUokKGkBZnDRyeqTYjMA21/NFUKhxQAQZZEB2QkjgJeMJIfFKqLClgBRORRhvFgiXJACsAfFCKO"
		"2Mzmr4D9KwCoBZFQvLCJlxsiF5qgBqwyhJlQPeRUNUqFAezxIOO4wmS5ylGIoKJqE23aFpgwhyy20ynXeYAD1kGSCCzOi1xxSDnW"
		"SCqs/1lNd05QZkJ4gYtc7CIXwA2ucGthPAe1Yav8kluVxLlRTnrVoReDaEGgudmJMmGvB5nFvTRKqs4+BA5NsOLVkmvAKgwgVyOZ"
		"RFopsAAfJCQf1JiGNOZL3/rad77TaOVAlrDeBSjhIekA613BecwBBGKnJ9lGAS+m0Fh+MK+AuCF0HyZdglA3kRPF4kH84E/udtUh"
		"3Fjww8ZTxb0VSWEkYQEqSVO9g0AjBxhwioxnTGMaVyqMBMkHaa7jgAxUUgxRGJKD74c/NJCiHKYFSQeTSoU0XMF7yNTb08Jgw4ZA"
		"NrpRna5EQzs78hWEHtEk5u682xApVtGeYEik7oTMBSekYf8k70AtcRggV4NA4y4OqLGea6w8HBPEnKl1QGxtx4aaPviPy8ICGvgQ"
		"i214Ax0g0Ucf85UkuQnAFnFwQuVGBcNAPvepWJYshotpqoT5cCDZ8Kk4xZw2Mi+kHrgTng7FKQBVzAGS4xUZFmgnkk4or4sLeMRB"
		"VnBB7Kg2zzL+pLKX/clKvcMgl1CAiBRgiYd8IqO9DBkXpKAe8VyBDHD4BC3QkWSK3EIAzBWbOKWgBX24EEglFnJuy32QK1M4yxbe"
		"8h//eTXSGkSKGCO1qT7MEFtUbUgxDNsT5mFmSks5YXYQSSNW7JRBDwQeO6aAAyrA8Y57/OMdjwAG/DyQfFTgXez/DcJDyiHipOar"
		"aUJdkmyqAIdA5ILeD8GD9hCeaycAbhcCSOSmFd5Ih9h7XxUeyIWpOmoqXGEcBMFHrEVG9YcRfCH9xPCmncAGftQCkkJGX+qkcIV9"
		"gsQHK3ZABw7CDtQyQAnvgIfc5073utMdIStYMQPYOqcpIvx/4gwhkbk9mzQEohwVOYdmxX5TJizMHmaIApVK5TSNMdDKE0Y6vpWu"
		"74Fn+0gG5BxBxFGFKoAz8Iq8ekLMcc2wRzlhHNMHGKq7OlWExAWgbEALDvKOdGZwghnRQUtrZAKIcGMAVeCC9wpa6iCZJ19baEIA"
		"pHAHxEtkFfXM18HQcwXrExTDyr8i/9eMnnn4iVqaqkMmE4A5EFIsMXUjpDSTXZ0Qw4Zz4FOoHT96yoUoS9JvcBASJEAjzKMDbLdj"
		"DYAEnLAJm8AJDriADBiBEsgJmNAMCKEEafUAHhBFABA2Lhc295M+pVIwm3MFthcRhWY03yNOTLAGFaJd5BF/tUUlT+Bcj1V+q7N5"
		"ArF0F3Nml8NuNkQHAoA1NnVAozZw9HcQ+qAG56NDIuRmFYJ9YSNCcyM+6OURHCAiDGAEvDcB7xIv1BOGYjiGYYgAG3QQjrBeFRAR"
		"+gAHGcVqR3I5IRV/5yEqUNMHOFcQ3kAw8JZtVgNR6rBGQ+RwVxMAfkB+oHZv55dNBjSCyv/HBL0gEPOAVNq3Q4SIhJSUEMEwAPe0"
		"XEHCBBEmEAOTiKtzeR1RASn1XwbBDid3KKmFcpniRXVmEJEwfMRhARJRD3IAAG00POGna2I2MuxXZh0GTqKyBU/wDQQRB8V4hFJA"
		"BvOAeaRofs/UeQCkBlqQQpwmTgEQiq8kQt8kBWqgBlFwR5jYEBzWPVo3O9wwEPpQaFigjfa0NY6VEVnoRUdwEO6gSj8TL8zmAAzw"
		"j590F9KBhuvFAROxD58wAHW0BUszhcaUOlZzHt6UMIPQEPqQBpI3QgiVBVGABkj0SiY2NqsgjWdGjVJ1hEbSBH9wB8EzVt7jBGcg"
		"J30AWqbCBIH/YAfIdY4LoQ9mQD+gVyRRIAYlBQoe9DAa8zIdsQHvIkoGuIo7NgEfcAJUWZVWeZVVCQLudRBDsF4+RhHcIAcDcC32"
		"k2v3d1NyiDFUIgDawBC48DGl0i9McGr84A3DFDKklYf8gEPKF1nVSHsQ8wm4cJQ8t0jeIGlPNZEt0wt1gGtWl4QFIQvoNmpNc10F"
		"AQzv90cQ9xEs8FojwncFsQ6+xwDVthE6oADj8gDyVBH7sA2BcAZPIBvlyETgU3W6tli6lRAeFXCCJwA2KBD7UGgqeTRT4GUKIQ/u"
		"k5Y5uIj7gpPl0ARbMFZYwgSq8A0+lXBK8gXl8EhUeCqqZxCvgjVU/+g0VOAEv8kP9RB5SIkFUmAF1scRaDczDXCGBfEOrQhTHJF3"
		"8ukCGbEPwRAIcpAGUwBYCoU0zKWcVEcFTWAMCqEOaDORauZmCDYQRjmcQLIFAZAmC0EPP0kqRLacf2mhfmMHwfkEECk8AuAHUihm"
		"FcUPa+CY+/KdBEEOVZCI4plQZDChAgEImUlhp+ARjJCBD8AOq4haDrACjJCkSrqkTJqkj7AEwnYQFQBKC1AEHTEP3TALoRAIc9AF"
		"sdmEz/dQoYgQsQArQjc33XgQuyBiCOMEaMAQ9HAGbGShSbeD1mgkTBBxhECY4aR8ZNChlIYkAeAKLgqj3gmZAyEK2IZhCP+kV9kV"
		"APJYdRrGEZygKNexAJEgpSEiHAvQqZ76qaDaqQgwGQYRMy6lAM4jEvKQC6CgBrHZRi43JFvTIAchB+ElhyS0ObeghGhgqCKjfAJQ"
		"OgoRp34kovijg/zAg81pUcDApiC0bTl0W8JDBVOAeMKZTYjKD7djoieEJBiqlAShDoh0KviioO3IEW2Hcmt1EEqAAGUUi/BaRj2W"
		"DwbhA2+FHQ0AfCKhD94QCMTiNCHzjGZnEOJQQJXTl0OyBVJgBjo6EH5wlBfDBHiwoZNmrHWarHdaJEzAJfvAhEfifN+kXMKTW9r6"
		"oi4nowIRDJN5R/cjBWEwsFkFsQPHjcO4Eaf/FGjPVhD5YAOdGi4++7NAC7QI4AD0WhBTeh0NgIsNsQ/4sA/64LRP67RSuw9OyxDk"
		"wIS/OGJfkA4IcW2TR655mhC1sKhMlFBhEI0JEac5ZLHIqqynsrEC8bAlBoIRmS8otg8mC0iZWBAPG1LLRVY1WxCGhU1IEgVmUI8W"
		"8Qj9xYUHsQlVoQOQG7mSO7mRmwNWWhCR0F/52BCqQAZp8LmgG7qgSwZ5wBDx4AUKRTfsSQYw6469GjIC0DkHop5+mznHNKjDWrEi"
		"erHlADbmiKdcwg+84FN0k6C8FpzB85h7OxDPMo22GwCskBDlYFd4JCTZOhHtkGdd5BRFKxIcsGIj/wINDmEKAAAFUeAE6Ju+6gsF"
		"TFJUC4EHZMsvUpAGDcsPwMCJSCkFYIC2CHEH2Qd+ahMAc6AQ9tBHPGdbTONEyBpgDhOpeGpR/FDAxbpv99OC8IG8JylJiKpd3MOe"
		"XcC/BtGxkGRA3/RgcNsRNNBfTxkSGHgB6DQiBdkQ3DAF9+RNTQDBCnGtMeo3OFwQNZnBGpubB8EK97I94qOMCFHADpMx3HOxDCyi"
		"YSsQWdeDBrMxwPmirba8AgEHUFC8LaoQUvS3QvcjVKAFRbcRgvMAPrMAnBAS0xCQKaUJCkG1+VAhWLsyzJVCGKo/wEm1VCsQpRB0"
		"VkdRP5pF9DB7HRwArf+grX3MyAIhD7N3hIEXwKbIyPAxD13ABE4ABZsMBZ78yZ/MVAPxxwLhOs6rsREnEIMJyqzcylAQAAyqrTrs"
		"nTL6DTSMrSUJnLrMyNsgBX3jf5V4rI2cES6wXgwQAaX0ERpgizCsEMRADMMwDMIgDNHgv7pziUfDBHowDMeQDMfwzclQDHmAbujB"
		"ZFCQCMcAzc8sDMdQCDtpT1LwC8UgDM+8zvT8zMkgnAd6Qgl1BsVQz/R8z8LwBmzgBgZ90Ah90GyQB8NADNMsDA1dDL8giL9bJFAg"
		"B8nQ0MLgBgWd0AYNBx9t0GzwBg1NDMUwBhP8R0xgCMdwz9JMDNGQBz3qnVf/END1fNMQTQxg9Xkv55H+rNEQfc/EQBHYgFKptAAj"
		"8BFMEUpltADOoBAPPc2/MAyKYLDdg8dDxARY4AaFcAiGIAh1sCsOfCoL69C+UAnCgNbFwAZQ8D9bM8/TXAlo/dByfQx70MXkqn1M"
		"cAjFMNdRDdHFENiCPdiE7dBzLdeVQAy/sG2FaSpO4AZwLQyEPdmUndb0rJEfCAWFUAy+ENdoTQwNEzJv/ddo7dfFQAdQQLh7wwSI"
		"INHC0Nl+XRFLkACGogAp0BE3sF7stZUJ8dfTnAxvMNPlPJGXQwWe3ARNwARQkJg3pTRMIAjJ8NfFsAh2xT1QIAiR7dvTPAzUjTT8"
		"/8JcTPAGyqDd5F3e5D0ME526D/PYLW3e7r3dmB0ymp3dwqAMhqDVvcgvz93e5X0M922bCZwFUCDe5G0RJJBWECQCyXwR+fAhqcQ8"
		"a7gQDv3X0qwFkodMJ+mEAS5030PGQMIEafDPf80Md619+/IEikDfvh3NP+k/lSM2CuULE/7eNB7Vw+ALtHUxUOAG/F3j2k0MYwCY"
		"kjTff50Mttp/RrwIKk7hixCDuUZMVFAFQe3bFsEO6mQoo9TGFvEMI3BOP8MA4rsQ5H0MiNAEDlMw9wM7YpNwlSYqUaAFvqDixNBH"
		"zvehQTLa760MetBh2xd4QsYEhdDjPv7e6C2IOi4Hgv8+6A89DPFtdUS+6LCwNyUsTk4TBWqw5L6tDGzgRzz0toKQ6MJwEZwAIljO"
		"AD1Acg+RD0EQLphCgDYiCQ1R3shwCMvdnXQLh3/0rOKzCIleDInQBJYoQlajzczg1+Xt63g9dFV16Ype4+idUC6346Cu6MSQBmsb"
		"o8ZtCNmtDHtQjKiXSEywB9P+13bd1iS0L5YO6hhBCcNBLqMKBAvOEPDACBJg1KmZAFHKEObNDIlANHU7OfYkeCDj3UACBVeQCMyg"
		"3cdQB6l9NBPpfFDQ2jTu0A5F3JIUBUre7O9NDL6wbSeaJeyt8X9NDGIQRLkOBdoe1cWgkdN6pvHoBIkgze//XQxVTQU/lU1SkOIj"
		"nxHs3gAu5QBFsDN0Ngk5exDwYAk+sErt1S5Ii+8P4d7KAAvZ48ucFocImlh31TJp4AvH0NkjTwyCWEXl6tMiTuPJcAdQYEAvniVM"
		"kAfjLvL0nN7YbNE8DveLbu29lMCCh/KRjewVjSUjVQwyP/Ox9qGm0gR5kPB/rRGbEC/E0QBA8Agt4AMVsAKfVAE4UARK8AhKgAQ4"
		"wAEVwAAHQAJDoARLEGM0Ei6w/vTuXQzHIAhfwATl+IFKEzZUEAXLvQeBPeaGkLxU1Ev+UgfjXeP+vUTpM4dCSQZlb/cUjuNUj025"
		"9fbOXvLHRDePXt950MV4jKd6/zD87/0LzJD9KpNU86viaFwCCTAaS1AWJAAJO6ADLbACCRCQDPC9DWACCzAJPaADOKACmEACAPGA"
		"wgINy/gdRJhQ4UFhDR0+fHhM2B4yTqBEoZJFYxYuGzVyAQlSikUnZPb8UibMF8RKDYuxYUJF5syZUqhIYYKo2EqIPRsS+3VFylCi"
		"RYlGCXComE+mTR9WGuYLC0aaNWVCcbPU6dafYpxUBYuzkNZhxLpgtGmVZhOdXBv+KnYIilG6RJmMhbhQ7969QhgkwIEEBZIiBl5c"
		"gsagiCUdNHTokPbABydGQRDg+JGAAY98fPm6/XmMGKI3YnAGYJKaiRPWUaCkRp1lzP+bRMSUDXM6bJGUKFPq2iVDTFjLnsQdtjwm"
		"hwmWjBy1aOkSJgwZM2XCvNEK2i3QK09+282qvSkxNAK+Ew2AV1gxQ0zO22QiJjtoYmOuPMefP38V7Hk9/9/LGRUWMGEEHo5IAQgc"
		"KPEhkh6KeAwJR5BoQQcZnrFAMxEyAXCvez4EMUR8PhxRn3sQsmecWz4B5A8+8KhjDhnrwMOPUHiR50R+8gkRxBFB3Mceb8gRRxxy"
		"jkQyHCK/SWcfEnvs0UQS5yEHHXTUiSceeeaZxx4v6/HSHnx+lBJKM8/0kUhxwEGyzSO/UYdHNOd8Ep9yvmHTTTe9mcfJe/JBx5s8"
		"2wQHHCOiwYGTnx99nBMfe+Sxp0suJ510S3nkWfTDDjdV6JIQElhgBSM0WCGJTZbQIQgjIliihSWAIGEBBxoQgRJOb8U1V1135bVX"
		"X38FNlhhh+0QmiMk+KuBBzI4oYUVLnjgA1AdYEADHgwiNlttt+W2W2+/BZdXaSzpYQUPLKggXQ88mKEHSKDpLFx556W3XnvvxTdf"
		"ffflt19//wU4YIEHJrjgYQMCADs="
	};
#endif

/********************************************************************************************/
