/********************************************************************************************/
/* 	WS_Homepage.c				- Definizione home page del sito web						*/

/********************************************************************************************/
#include "SYS_Include.h"
#include "APP_Include.h"

/********************************************************************************************/
/*	Definizione della pagina																*/
/********************************************************************************************/
const char WS_HomepageData [] =
 "<html>"
	"<head>"
		"<meta charset=\x22UTF-8\x22>"
		"<title>ARM STM32H743Zi board - Home page</title>"
	"</head>"
	"<body>"
		//	Intestazione della pagina (risorsa HTML n.1)
		"<#H1>"
		//	Contenuto della pagina
		"<center>"
			"<br><br><br>"
			//	Logo (risorsa immagnie n.1)
			"<#I1>"
			"<br><br><br>"
			//	Pulsante di accesso
			"<p><input type=button value='Login' onClick=\x22location.href='Pag1'\x22></p>"
		"</center>"
		//	Base della pagina (risorsa HTML n.2)
		"<#H2>"
	"/<body>"
"/<html>";




/********************************************************************************************/
/*	Callback di pagina																		*/
/*																							*/
/*	WS_HomepageOpenCallback ( )				- Apertura della pagina							*/
/*	WS_HomepageGetParamCallback ( )			- GET di un parametro							*/
/*	WS_HomepagePostParamCallback ( )		- POST di un parametro							*/
/********************************************************************************************/
/*------------------------------------------------------------------------------------------*/
/*	WS_HomepageOpenCallback ( )				- Apertura della pagina							*/
/*------------------------------------------------------------------------------------------*/
void WS_HomepageOpenCallback ( uint8_t nPageId )
{

}
/*------------------------------------------------------------------------------------------*/
/*	WS_HomepageGetParamCallback ( )			- GET di un parametro							*/
/*------------------------------------------------------------------------------------------*/
void WS_HomepageGetParamCallback ( uint8_t pageId, uint16_t paramId, char* paramValue )
{

}
/*------------------------------------------------------------------------------------------*/
/*	WS_HomepagePostParamCallback ( )		- POST di un parametro							*/
/*------------------------------------------------------------------------------------------*/
void WS_HomepagePostParamCallback ( uint8_t pageId, uint16_t paramId, char* paramValue )
{

}


/********************************************************************************************/
