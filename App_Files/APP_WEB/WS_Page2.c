/********************************************************************************************/
/* 	WS_Page2.c					- Pagina 2 del sito web										*/

/********************************************************************************************/
#include "SYS_Include.h"
#include "APP_Include.h"

/********************************************************************************************/
/*	Definizione della pagina																*/
/********************************************************************************************/
const char WS_Page2Data [] =
"<html>"
	//	Header
	"<head>"
		"<meta charset=\x22UTF-8\x22>"
		"<title>ARM STM32H743Zi board - Pagina utemte numero 2</title>"
	"</head>"
	//	Corpo pagina
	"<body>"
		"<div style='position:relative'>"
			//	Intestazione della pagina (risorsa HTML n.1)
			"<#H1>"
			// Colonna sinistra menu
			"<div align=center style='float:left; width:180px; height:600px; background-color:teal' >"
				//	Inserire qui eventuali altri pulsanti
				// 	Ritorno al menu principale
				"<p><input type=button value='Main menu' style=width:160px onClick=\x22location.href='Pag1'\x22></p>"
			"</div>"
			//*************************************************************
			// Area utile della pagina
			"<div style='height:600px; background-color:#EEEEEE'>"
				"<center>"
					//	Testo della pagina
					"<br>Scheda SAIMA 524415r3<br>"
					"<br>Kernel per ARM STM32H743Zi versione 0.94<br>"
					"<br><br> * PAGINA UTENTE NUMERO 2 * <br><br>"
				"</center>"
			"</div>"
			// Fine area utile della pagina
			//*************************************************************
			//	Base della pagina (risorsa HTML n.2)
			"<#H2>"
		"</div>"
	"/<body>"
"/<html>";


/********************************************************************************************/
/*	Callback di pagina																		*/
/*																							*/
/*	WS_Page2OpenCallback ( )				- Apertura della pagina							*/
/*	WS_Page2GetParamCallback ( )			- GET di un parametro							*/
/*	WS_Page2PostParamCallback ( )			- POST di un parametro							*/
/********************************************************************************************/
/*------------------------------------------------------------------------------------------*/
/*	WS_Page2OpenCallback ( )				- Apertura della pagina							*/
/*------------------------------------------------------------------------------------------*/
void WS_Page2OpenCallback ( uint8_t nPageId )
{

}
/*------------------------------------------------------------------------------------------*/
/*	WS_Page2GetParamCallback ( )			- GET di un parametro							*/
/*------------------------------------------------------------------------------------------*/
void WS_Page2GetParamCallback ( uint8_t pageId, uint16_t paramId, char* paramValue )
{

}
/*------------------------------------------------------------------------------------------*/
/*	WS_Page2PostParamCallback ( )			- POST di un parametro							*/
/*------------------------------------------------------------------------------------------*/
void WS_Page2PostParamCallback ( uint8_t pageId, uint16_t paramId, char* paramValue )
{

}



/********************************************************************************************/
