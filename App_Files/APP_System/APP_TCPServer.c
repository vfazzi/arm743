/********************************************************************************************/
/*	APP_TcpServer.c			- Funzioni server TCP/IP (escluso HTTP)							*/

/********************************************************************************************/
// Include di sistema e applicazione
#include "SYS_Include.h"
#include "APP_Include.h"


TCPServerHandle	p_rawServer;
unsigned char p_RawBuffRx[MAX_BUFF_RX485];
unsigned char p_RawBuffTx[MAX_BUFF_TX485];


bool RawDataRCV ( unsigned char* pData, uint16_t dataLen );
bool RawDataDecode(int dataLen);
void RawDataAnalyze();

/********************************************************************************************/
/*	Funzioni del modulo																		*/
/*																							*/
/*	APP_TcpStartAppServer ( )		- Avvia i server TCP specifici dell'applicazione		*/
/*	APP_ServerInit ( )				- Inizializzazione di un server							*/
/*	APP_SendData ( )				- Invio dati a un client connesso						*/
/*	APP_ServerDisconnect ( )		- Chiusura connessione al client						*/
/*	APP_IsEthCableConnected ( )		- Stato connessione cavo Ethernet						*/
/*	APP_ServerIsConnected ( )		- Indica se il server ha un client collegato			*/
/*	APP_GetServerCount ( )			- Numero di server attivi								*/
/*	APP_GetServerHandle ( )			- Recupero handle di un server 							*/
/*																							*/
/********************************************************************************************/
/*------------------------------------------------------------------------------------------*/
/*	APP_TcpStartAppServer ( )		- Avvia i server TCP specifici dell'applicazione		*/
/*	N.B. il server HTTP e il server di programmazione remota sono già attivati nel SYS		*/
/*------------------------------------------------------------------------------------------*/
void APP_TcpStartAppServer ( void )
{
	p_rawServer.connPort = RAWDATA_PORT;
	p_rawServer.txBufferSize = 1024;
	SRV_StartServer (&p_rawServer);

}
/*------------------------------------------------------------------------------------------*/
/*	APP_ServerInit ( )				- Inizializzazione di un server							*/
/*	hServer		= struttura dati del server (settare solo la porta)							*/
/*------------------------------------------------------------------------------------------*/
// bool APP_ServerInit ( TCPServerHandle* hServer );

/*------------------------------------------------------------------------------------------*/
/*	APP_SendData ( )				- Invio dati a un client connesso						*/
/*	hServer		= handle del server															*/
/*	pData		= buffer dati																*/
/*	dataSize	= dimensione buffer dati													*/
/*------------------------------------------------------------------------------------------*/
// bool APP_SendData ( TCPServerHandle* hServer, unsigned char* pData, unsigned int dataSize );

/*------------------------------------------------------------------------------------------*/
/*	SRV_BufferedSendByte ( )		- Trasmissione di un byte in modo bufferizzato			*/
/*	hServer		= handle del server															*/
/*	ch			= carattere da inserire nel buffer											*/
/*	forceSend	= forza la trasmissione e lo svuotamento del buffer							*/
/*------------------------------------------------------------------------------------------*/
// bool APP_TcpBufferedSendByte ( TCPServerHandle* hServer, uint8_t ch, bool forceSend );

/*------------------------------------------------------------------------------------------*/
/*	APP_ServerDisconnect ( )		- Chiusura connessione al client						*/
/*	hServer		= handle del server															*/
/*------------------------------------------------------------------------------------------*/
// void APP_ServerDisconnect ( TCPServerHandle* hServer );

/*------------------------------------------------------------------------------------------*/
/*	APP_IsEthCableConnected ( )		- Stato connessione cavo Ethernet						*/
/*------------------------------------------------------------------------------------------*/
// bool APP_IsEthCableConnected ( void );

/*------------------------------------------------------------------------------------------*/
/*	APP_ServerIsConnected ( )		- Indica se il server ha un client collegato			*/
/*	hServer		= handle del server															*/
/*------------------------------------------------------------------------------------------*/
// bool APP_ServerIsConnected ( TCPServerHandle* hServer );

/*------------------------------------------------------------------------------------------*/
/*	APP_GetServerCount ( )			- Numero di server attivi								*/
/*------------------------------------------------------------------------------------------*/
// uint8_t APP_GetServerCount ( void );

/*------------------------------------------------------------------------------------------*/
/*	APP_GetServerHandle ( )			- Recupero handle di un server 							*/
/*	nServer	= numero del server																*/
/*------------------------------------------------------------------------------------------*/
// TCPServerHandle* APP_GetServerHandle ( uint8_t nServer );

/********************************************************************************************/
/*	Callback																				*/
/*																							*/
/*	APP_TcpServerConnected ( )		- Notifica connessione di un client a un server			*/
/*	APP_TcpServerDisconnected ( )	- Notifica disconnessione di un client connesso			*/
/*	APP_TcpReceiveCallback ( )		- Notifica ricezione dati da un client					*/
/*	APP_EthCableConnected  ( )		- Notifica connessione cavo di rete						*/
/*	APP_EthCableDisconnected ( )	- Notifica disconnessione cavo di rete					*/
/********************************************************************************************/
/*------------------------------------------------------------------------------------------*/
/*	APP_TcpServerConnected ( )		- Notifica connessione di un client a un server			*/
/*	hServer		= handle del server															*/
/*------------------------------------------------------------------------------------------*/
void APP_TcpServerConnected ( TCPServerHandle* hServer )
{

}
/*------------------------------------------------------------------------------------------*/
/*	APP_TcpServerDisconnected ( )	- Notifica disconnessione di un client connesso			*/
/*	hServer		= handle del server															*/
/*------------------------------------------------------------------------------------------*/
void APP_TcpServerDisconnected ( TCPServerHandle* hServer )
{

}
/*------------------------------------------------------------------------------------------*/
/*	APP_TcpReceiveCallback ( )		- Notifica ricezione dati da un client					*/
/*	hServer		= handle del server															*/
/*	pData		= buffer dati																*/
/*	dataSize	= dimensione buffer dati													*/
/*	Uscire con return(false) se la connessione deve rimanere aperta, return (true) se 		*/
/*	deve essere chiusa.																		*/
/*------------------------------------------------------------------------------------------*/
bool APP_TcpReceiveCallback ( TCPServerHandle* hServer, char* pData, uint16_t dataLen )
{
	// printf("hServer->connPort: %d len: %d\r\n",hServer->connPort,dataLen);
	// for(int ind=0;ind<dataLen;ind++) printf("%x ",pData[ind]);
	// printf("\r\n");
	if ( hServer->connPort == FWUPDATE_PORT )
	{
		FWU_RcvData ( (unsigned char*)pData, dataLen );
	}
	else if(hServer->connPort == RAWDATA_PORT)
	{
		if(RawDataRCV((unsigned char*)pData,dataLen))
			RawDataAnalyze();
	}
	return ( false );
}
/*------------------------------------------------------------------------------------------*/
/*	APP_EthCableConnected  ( )		- Notifica connessione cavo di rete						*/
/*------------------------------------------------------------------------------------------*/
void APP_EthCableConnected ( )
{

}
/*------------------------------------------------------------------------------------------*/
/*	APP_EthCableDisconnected ( )	- Notifica disconnessione cavo di rete					*/
/*------------------------------------------------------------------------------------------*/
void APP_EthCableDisconnected ( )
{

}
	
void RawDataAnalyze()
{
	net485SendMsgData_t	msgTx;
	_rs485	fake_485; // uso la struttura delle routine della 485 per sfruttare le stesse funzioni dopo
	fake_485.address=ext_485.address;

	if(p_RawBuffRx[PACC_DEST]!=ext_485.address && p_RawBuffRx[PACC_DEST]!=IND_485_BROADCAST)
		return;
	
	//	Chiama la callback di gestione dei messaggi ricevuti
	msgTx.msgDataSize = 0;
	msgTx.destAddr = p_RawBuffRx [ PACC_MITT ];
	msgTx.msgCmd = p_RawBuffRx [ PACC_COMANDO ];
	memset(msgTx.msgData,0,sizeof(msgTx.msgData));
	if(VARCO_485ExternalNetworkRcvMSgCallback(p_RawBuffRx,&msgTx))
	{
		CreaPacchetto(&fake_485,msgTx.destAddr,msgTx.msgCmd,msgTx.msgData, msgTx.msgDataSize);
		//printf(">> ");
		for(int ind=0;ind<fake_485.tx_len;ind++)
		{
			//printf("%x ",fake_485.tx_buffer[ind]);
			SRV_BufferedSendByte(&p_rawServer,fake_485.tx_buffer[ind],false);
		}
		SRV_FlushBuffer ( &p_rawServer );
		//printf("\r\n");
	}
}

bool RawDataRCV ( unsigned char* pData, uint16_t dataLen )
{
	static unsigned char p_rcvStatus=0,p_rcvCount=0;
	unsigned char* 	p_rcvPtr;
	static bool p_rcvFlag;
	//	Analisi flusso dati ricevuti
	for ( int i = 0; i < dataLen; i ++ )
	{
		unsigned char b = pData [i];
		//	Macchina a stati decoder
		switch ( p_rcvStatus )
		{
			//	Stato di attesa
			case 0:
				if ( b == STX )
				{
					p_rcvFlag = false;
					p_rcvStatus = 1;
					p_rcvCount = 0;
					memset ( p_RawBuffRx, 0, MAX_BUFF_RX485);
					p_rcvPtr = p_RawBuffRx;
				}
			break;
			//	Stato di ricezione
			case 1:
				//	Eliminatore DLE
				if ( b == DLE )
				{
					p_rcvStatus = 10;
				}
				//	ETX - Fine messaggio
				else if ( b == ETX )
				{
					//	Controllo correttezza messaggio
					if(RawDataDecode(p_rcvCount))
					{
						p_rcvFlag = true;
					}
					p_rcvStatus = 0;
				}
				//	Errore
				else if ( b == STX )
				{
					p_rcvCount = 0;
					p_rcvStatus = 0;
				}
				//	Salva carattere generico
				else
				{
					*p_rcvPtr = b;
					p_rcvPtr ++;
					p_rcvCount ++;
				}
			break;
			//	Carattere successivo al DLE
			case 10:
				//	Accetta solo STX, ETX o DLE
				if ( ( b == STX ) || ( b == DLE ) || ( b == ETX ) )
				{
					*p_rcvPtr = b;
					p_rcvPtr ++;
					p_rcvCount ++;
					p_rcvStatus = 1;
				}
				//	Altri caratteri = ERRORE di sequenza
				else
				{
					p_rcvStatus = 0;
					p_rcvCount = 0;
				}
			break;
		}
		//	Controllo buffer overrun
		if ( p_rcvCount>MAX_BUFF_RX485 )
		{
			p_rcvStatus = 0;
			p_rcvCount = 0;
		}
	}
	return p_rcvFlag;
}

bool RawDataDecode(int dataLen)
{
	uint16_t ind, chk=0;

	for(ind=0;ind<dataLen-2;ind++)
		chk+=p_RawBuffRx[ind];

	return (chk==p_RawBuffRx[ind]*256+p_RawBuffRx[ind+1]);
}
/********************************************************************************************/
