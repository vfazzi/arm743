/********************************************************************************************/
/*	APP_Terminal.h				- Funzioni del terminale									*/

/********************************************************************************************/

#ifndef	__APP_TERM_DEFINED
#define __APP_TERM_DEFINED

#define HELP_TITLE			"*Saima Sicurezza SPA*\r\n"

/********************************************************************************************/
/*	Dichiarazione funzioni di gestione comandi (file APP_TermCmd.c)							*/
/********************************************************************************************/
extern void TERM_SetCommand 	(int argc, char **argv);
extern void TERM_GetCommand 	(int argc, char **argv);
extern void TERM_SaveCommand	(int argc, char **argv);
extern void TERM_Reboot   		(int argc, char **argv);
extern void TERM_help 			(int argc, char **argv);



#endif

/********************************************************************************************/

