/********************************************************************************************/
/* 	APP_ConfigData.c			- Gestione configurazione del sistema						*/

/********************************************************************************************/
// Include di sistema e applicazione
#include "SYS_Include.h"
#include "APP_Include.h"

/********************************************************************************************/
/*	Struttura di configurazione di sistema													*/
/********************************************************************************************/
struct 	APP_SystemConfigData sysCfg;
bool 	APP_saveConfigAsync = false;


/********************************************************************************************/
/*	Funzioni private																		*/
/********************************************************************************************/
void CFG_AddChecksum ( struct APP_SystemConfigData* pCfg );
bool CFG_VerifyChecksum ( struct APP_SystemConfigData* pCfg );

/********************************************************************************************/
/*	Funzioni del modulo																		*/
/*																							*/
/*	APP_LoadDefaultSettings ( )			- Ripristina le impostazioni predefinite			*/
/*	APP_LoadSettingsFromFlashMemory ( )	- Carica le impostazioni dall'area flash			*/
/*	APP_SaveSettingsToFlashMemory ( )	- Salva le impostazioni nell'area flash				*/
/*	APP_AsyncSaveSettings ( )			- Salva in modo asincrono dal main loop				*/
/*	APP_AsyncSaveSettingsProc ( )		- Chiamata dal main loop per salvare i dati			*/
/********************************************************************************************/
/*------------------------------------------------------------------------------------------*/
/*	APP_LoadDefaultSettings ( )			- Ripristina le impostazioni predefinite			*/
/*------------------------------------------------------------------------------------------*/
bool APP_LoadDefaultSettings ( void )
{
	//	Set zero
	memset ( &sysCfg, 0, sizeof(sysCfg));
	//	ID struttura
	sysCfg.m_cfgID = CFG_ID;
	sysCfg.m_Checksum = 0;
	sysCfg.m_structSize = sizeof(struct APP_SystemConfigData);
	//	ID scheda
	strcpy ( sysCfg.m_boardID, CFG_BOARD_ID );
	//	Indirizzo IP predefinit
	sysCfg.m_ipAddress [ 0 ] = CFG_DEFAULT_IP_0;
	sysCfg.m_ipAddress [ 1 ] = CFG_DEFAULT_IP_1;
	sysCfg.m_ipAddress [ 2 ] = CFG_DEFAULT_IP_2;
	sysCfg.m_ipAddress [ 3 ] = CFG_DEFAULT_IP_3;
	//	Gateway predefinito
	sysCfg.m_gatewayAddress [ 0 ] = CFG_DEFAULT_GWAY_0;
	sysCfg.m_gatewayAddress [ 1 ] = CFG_DEFAULT_GWAY_1;
	sysCfg.m_gatewayAddress [ 2 ] = CFG_DEFAULT_GWAY_2;
	sysCfg.m_gatewayAddress [ 3 ] = CFG_DEFAULT_GWAY_3;
	//	Subnet mask predefinita
	sysCfg.m_subnetMask [ 0 ] = CFG_DEFAULT_MASK_0;
	sysCfg.m_subnetMask [ 1 ] = CFG_DEFAULT_MASK_1;
	sysCfg.m_subnetMask [ 2 ] = CFG_DEFAULT_MASK_2;
	sysCfg.m_subnetMask [ 3 ] = CFG_DEFAULT_MASK_3;
	//	MAC address random
	sysCfg.m_macAddress [ 0 ] = CFG_HW_MAC_0;
	sysCfg.m_macAddress [ 1 ] = CFG_HW_MAC_1;
	sysCfg.m_macAddress [ 2 ] = CFG_HW_MAC_2;
	// sysCfg.m_macAddress [ 3 ] = CFG_HW_MAC_3;
	// sysCfg.m_macAddress [ 4 ] = CFG_HW_MAC_4;
	// sysCfg.m_macAddress [ 5 ] = CFG_HW_MAC_5;
	sysCfg.m_macAddress [ 3 ] = rand()&0xff;
	sysCfg.m_macAddress [ 4 ] = rand()&0xff;
	sysCfg.m_macAddress [ 5 ] = rand()&0xff;



	//	Credenziali di accesso al sito web
	memset ( sysCfg.m_userName, 0, 16 );
	memset ( sysCfg.m_userPwd, 0, 16 );
	strcpy ( sysCfg.m_userName, CFG_USERNAME );
	strcpy ( sysCfg.m_userPwd, CFG_USERPWD );
	//	Baud rate seriali
	sysCfg.m_uartExtBaudRate = CFG_DEFAULT_BAUDRATE_UART_EXT;
	sysCfg.m_uartIntBaudRate = CFG_DEFAULT_BAUDRATE_UART_INT;
	//	Reti 485
	sysCfg.m_intNetMasterAddress = CFG_DEFAULT_INT_MASTER_ADDR;
	sysCfg.m_extNetSlaveAddress = CFG_DEFAULT_EXT_SLAVE_ADDR;

	//	Parametri di configurazione dal codice originale
	SET_PARAM(1,10); // timeout porta aperta
    SET_PARAM(2,10); // timeout allarme in quiete fotocelluleù
    SET_PARAM(3,2);  // fine allarme
    SET_PARAM(4,5);  // tempo freno attivo

	SET_FLAG5_APRI_ANTIF_ON;
	//	Costruzione checksum
	CFG_AddChecksum ( &sysCfg );
	return(true);
};
/*------------------------------------------------------------------------------------------*/
/*	APP_LoadSettingsFromFlashMemory ( )	- Carica le impostazioni dall'area flash			*/
/*------------------------------------------------------------------------------------------*/
bool APP_LoadSettingsFromFlashMemory ( void )
{
	struct APP_SystemConfigData tempCfg;
	//	Carica la struttura di configurazione dalla flash
	if ( APP_FlashRead  ( (uint8_t*)&tempCfg, FLASH_CONFIG_ADDR, sizeof(sysCfg) ) == QSPI_OK )
	{
		//	Verifica la checksum dei dati caricati
		if ( CFG_VerifyChecksum ( &tempCfg ) )
		{
			//	Dati validi, copia nella struttura di configurazione
			memcpy ( &sysCfg, &tempCfg, sizeof(sysCfg) );
			return ( true );
		}
	}
	//	Verifica la checksum della configurazione in memoria
	if( ! CFG_VerifyChecksum ( &sysCfg ) )
	{
		APP_LoadDefaultSettings ( );
		APP_SaveSettingsToFlashMemory ( );
	}
	//	Errore di caricamento configurazione
	return (false);

}
/*------------------------------------------------------------------------------------------*/
/*	APP_SaveSettingsToFlashMemory ( )	- Salva le impostazioni nell'area flash				*/
/*------------------------------------------------------------------------------------------*/
bool APP_SaveSettingsToFlashMemory ( void )
{
	//	Cancella il settore della flash dove registra la configurazione
	if ( APP_FlashEraseSector ( FLASH_CONFIG_ADDR ) == QSPI_OK )
	{
		//	Calcola la checksum del blocco
		CFG_AddChecksum ( &sysCfg );
		//	Salva i dati
		if ( APP_FlashWrite ( (uint8_t*)&sysCfg, FLASH_CONFIG_ADDR, sizeof(sysCfg) ) == QSPI_OK )
		{
			return ( true);
		}
	}
	return ( false);
}
/*------------------------------------------------------------------------------------------*/
/*	APP_AsyncSaveSettings ( )			- Salva in modo asincrono dal main loop				*/
/*------------------------------------------------------------------------------------------*/
void inline APP_AsyncSaveSettings ( void )
{
	APP_saveConfigAsync = true;
}
/*------------------------------------------------------------------------------------------*/
/*	APP_AsyncSaveSettingsProc ( )		- Chiamata dal main loop per salvare i dati			*/
/*------------------------------------------------------------------------------------------*/
void APP_AsyncSaveSettingsProc ( void )
{
	if ( APP_saveConfigAsync )
	{
		APP_saveConfigAsync = false;
		APP_SaveSettingsToFlashMemory ( );
	}
}



/********************************************************************************************/
/*	Funzioni private del modulo																*/
/*																							*/
/*	CFG_AddChecksum ( )				- Calcola la checksum della struttra di configurazione	*/
/*	CFG_VerifyChecksum ( )			- Verifica la checksum della struttra di configurazione	*/
/********************************************************************************************/
/*------------------------------------------------------------------------------------------*/
/*	CFG_AddChecksum ( )				- Calcola la checksum della struttra di configurazione	*/
/*------------------------------------------------------------------------------------------*/
void CFG_AddChecksum ( struct APP_SystemConfigData* pCfg )
{
	uint8_t*	pData = (uint8_t*)&sysCfg;
	pCfg->m_Checksum = 0;
	for ( int i = 0; i < sizeof(struct APP_SystemConfigData); i ++)
	{
		pCfg->m_Checksum += *pData;
	}

}
/*------------------------------------------------------------------------------------------*/
/*	CFG_VerifyChecksum ( )			- Verifica la checksum della struttra di configurazione	*/
/*------------------------------------------------------------------------------------------*/
bool CFG_VerifyChecksum ( struct APP_SystemConfigData* pCfg )
{
	uint8_t*	pData = (uint8_t*)&sysCfg;
	int32_t		chkTemp = pCfg->m_Checksum;
	//	Pre-verifica di ID e size
	if ( ( pCfg->m_cfgID != CFG_ID ) || ( pCfg->m_structSize != sizeof(sysCfg) ) )
	{
		return(false);
	}
	//	Ricalcola la checksum
	sysCfg.m_Checksum = 0;
	for ( int i = 0; i < sizeof(struct APP_SystemConfigData); i ++)
	{
		pCfg->m_Checksum += *pData;
	}
	//	Confronta il valore originale con quello calcolato
	return ( chkTemp == pCfg->m_Checksum );
}

/********************************************************************************************/
