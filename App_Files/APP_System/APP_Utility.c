/********************************************************************************************/
/*	APP_Utility.c.c				- Funzioni varie											*/

/********************************************************************************************/
// Include di sistema e applicazione
#include "SYS_Include.h"
#include "APP_Include.h"

/********************************************************************************************/
/*	Funzioni																				*/
/*																							*/
/*	APP_utiParseIPAddress ( )		- Effettua il parsing di un indirizzo IP in stringa		*/
/*	APP_utiParseMacAddress ( )		- Effettua il parsing di un indirizzo MAC in stringa	*/
/********************************************************************************************/
/*------------------------------------------------------------------------------------------*/
/*	APP_utiParseIPAddress ( )		- Effettua il parsing di un indirizzo IP in stringa		*/
/*	ipAddr 		= indirizzo IP in forma xxx.xxx.xxx.xxx (xxx decimale da 1 a 3 cifre)		*/
/*	pOutput		= array di bytes contenete i valori separati								*/
/*	Valore di ritorno:																		*/
/*	true se riesce																			*/
/*	false se fallisce per formato errato della stringa										*/
/*------------------------------------------------------------------------------------------*/
bool APP_utiParseIPAddress ( char* ipAddr, uint8_t* pOutput )
{
	char* 	pScan;
	int		tmp;
	int 	i = 0;
	pScan = strtok ( ipAddr, ".");
	while ( ( pScan != (char*)0 ) && ( i < 4 ) )
	{
		tmp = strtol ( pScan, NULL, 0 );
		if ( ( tmp >= 0 ) && ( tmp <= 255 ) )
		{
			pOutput [ i ] = (uint8_t)tmp;
			pScan = strtok ( (char*)0, ".");
			i ++;
		}
		else
		{
			return (false);
		}
	}
	return ( i == 4 );
}
/*------------------------------------------------------------------------------------------*/
/*	APP_utiParseMacAddress ( )		- Effettua il parsing di un indirizzo MAC in stringa	*/
/*	macAddr 	= indirizzo MAC in forma hh:hh:hh:hh:hh:hh (hh esadecimale 1 o 2 cifre)		*/
/*	pOutput		= array di bytes contenete i valori separati								*/
/*	Valore di ritorno:																		*/
/*	true se riesce																			*/
/*	false se fallisce per formato errato della stringa										*/
/*------------------------------------------------------------------------------------------*/
bool APP_utiParseMacAddress ( char* macAddr, uint8_t* pOutput )
{
	char* 	pScan;
	int		tmp;
	int 	i = 0;
	pScan = strtok ( macAddr, ":");
	while ( ( pScan != (char*)0 ) && ( i < 6 ) )
	{
		tmp = strtol ( pScan, NULL, 16 );
		if ( ( tmp >= 0 ) && ( tmp <= 255 ) )
		{
			pOutput [ i ] = (uint8_t)tmp;
			pScan = strtok ( (char*)0, ":");
			i ++;
		}
		else
		{
			return (false);
		}
	}
	return ( i == 6 );
}


/********************************************************************************************/
