/********************************************************************************************/
/* 	APP_ConfigData.h			- File di dichiarazioni settaggi applicazione				*/

/********************************************************************************************/

#include "stdint.h"
#include <stdbool.h>

#ifndef __APP_SETTINGS_DEFINED
#define __APP_SETTINGS_DEFINED

//	ID della struttura
#define	CFG_ID				0x37A5C91D

//	Credenziali di accesso al sito web
#ifndef	APP_BOARD_ID
	#define	CFG_BOARD_ID		"00FF00FF"
#else
	#define	CFG_BOARD_ID		APP_BOARD_ID
#endif

//	Indirizzo IP predefinito
#ifndef APP_DEFAULT_IP_0
	#define	CFG_DEFAULT_IP_0	192
#else
	#define	CFG_DEFAULT_IP_0	APP_DEFAULT_IP_0
#endif
#ifndef APP_DEFAULT_IP_1
	#define	CFG_DEFAULT_IP_1	168
#else
	#define	CFG_DEFAULT_IP_1	APP_DEFAULT_IP_1
#endif
#ifndef APP_DEFAULT_IP_2
	#define	CFG_DEFAULT_IP_2	1
#else
	#define	CFG_DEFAULT_IP_2	APP_DEFAULT_IP_2
#endif
#ifndef APP_DEFAULT_IP_3
	#define	CFG_DEFAULT_IP_3	10
#else
	#define	CFG_DEFAULT_IP_3	APP_DEFAULT_IP_3
#endif

//	Gateway predefinito
#ifndef APP_DEFAULT_GWAY_0
	#define	CFG_DEFAULT_GWAY_0	192
#else
	#define	CFG_DEFAULT_GWAY_0	APP_DEFAULT_GWAY_0
#endif
#ifndef APP_DEFAULT_GWAY_1
	#define	CFG_DEFAULT_GWAY_1	168
#else
	#define	CFG_DEFAULT_GWAY_1	APP_DEFAULT_GWAY_1
#endif
#ifndef APP_DEFAULT_GWAY_2
	#define	CFG_DEFAULT_GWAY_2	1
#else
	#define	CFG_DEFAULT_GWAY_2	APP_DEFAULT_GWAY_2
#endif
#ifndef APP_DEFAULT_GWAY_3
	#define	CFG_DEFAULT_GWAY_3	1
#else
	#define	CFG_DEFAULT_GWAY_3	APP_DEFAULT_GWAY_3
#endif

//	Subnet mask predefinita
#ifndef APP_DEFAULT_MASK_0
	#define	CFG_DEFAULT_MASK_0	255
#else
	#define	CFG_DEFAULT_MASK_0	APP_DEFAULT_MASK_0
#endif
#ifndef APP_DEFAULT_MASK_1
	#define	CFG_DEFAULT_MASK_1	255
#else
	#define	CFG_DEFAULT_MASK_1	APP_DEFAULT_MASK_1
#endif
#ifndef APP_DEFAULT_MASK_2
	#define	CFG_DEFAULT_MASK_2	255
#else
	#define	CFG_DEFAULT_MASK_2	APP_DEFAULT_MASK_2
#endif
#ifndef APP_DEFAULT_MASK_3
	#define	CFG_DEFAULT_MASK_3	0
#else
	#define	CFG_DEFAULT_MASK_3	APP_DEFAULT_MASK_3
#endif

//	MAC address predefinito
#ifndef APP_HW_MAC_0
	#define CFG_HW_MAC_0		0x00
#else
	#define CFG_HW_MAC_0		APP_HW_MAC_0
#endif
#ifndef APP_HW_MAC_1
	#define CFG_HW_MAC_1		0x80
#else
	#define CFG_HW_MAC_1		APP_HW_MAC_1
#endif
#ifndef APP_HW_MAC_2
	#define CFG_HW_MAC_2		0xE1
#else
	#define CFG_HW_MAC_2		APP_HW_MAC_2
#endif
#ifndef APP_HW_MAC_3
	#define CFG_HW_MAC_3		0x00
#else
	#define CFG_HW_MAC_3		APP_HW_MAC_3
#endif
#ifndef APP_HW_MAC_4
	#define CFG_HW_MAC_4		0x00
#else
	#define CFG_HW_MAC_4		APP_HW_MAC_4
#endif
#ifndef APP_HW_MAC_5
	#define CFG_HW_MAC_5		0x00
#else
	#define CFG_HW_MAC_5		APP_HW_MAC_5
#endif

//	Credenziali di accesso al sito web
#ifndef	APP_USERNAME
	#define	CFG_USERNAME		"admin"
#else
	#define	CFG_USERNAME		APP_USERNAME
#endif
#ifndef	APP_USERPWD
	#define	CFG_USERPWD		"admin"
#else
	#define	CFG_USERPWD		APP_USERPWD
#endif

//	Baud rate seriali
#ifndef APP_DEFAULT_BAUDRATE_UART3
	#define	CFG_DEFAULT_BAUDRATE_UART_EXT	(uint32_t)57600
#else
	#define	CFG_DEFAULT_BAUDRATE_UART_EXT	APP_DEFAULT_BAUDRATE_UART3
#endif
#ifndef APP_DEFAULT_BAUDRATE_UART6
	#define	CFG_DEFAULT_BAUDRATE_UART_INT	(uint32_t)57600
#else
	#define	CFG_DEFAULT_BAUDRATE_UART_INT	APP_DEFAULT_BAUDRATE_UART6
#endif

//	Reti 485
#ifndef APP_DEFAULT_INT_MASTER_ADDR
	#define	CFG_DEFAULT_INT_MASTER_ADDR	0x4B
#else
	#define	CFG_DEFAULT_INT_MASTER_ADDR APP_DEFAULT_INT_MASTER_ADDR
#endif
#ifndef APP_DEFAULT_EXT_SLAVE_ADDR
	#define	CFG_DEFAULT_EXT_SLAVE_ADDR	0x04
#else
	#define	CFG_DEFAULT_EXT_SLAVE_ADDR APP_DEFAULT_EXT_SLAVE_ADDR
#endif

//	Numero di parametri di configurazione del sistema
#define SET_PARAM(x,y)              sysCfg.m_vttParam[x-1]=y
#define MAX_PARAM	14
#define MAX_FLAG	6
#define MAX_SEL		8


typedef struct APP_SystemConfigData
{
	//	Intestazione
	int32_t		m_Checksum;
	int32_t		m_structSize;
	int32_t		m_cfgID;
	//	ID della scheda
	char 		m_boardID [ 10 ];
	//	Configurazione Ethernet
	uint8_t		m_ipAddress [ 4 ];
	uint8_t		m_gatewayAddress [ 4 ];
	uint8_t		m_subnetMask [ 4 ];
	uint8_t		m_macAddress [ 6 ];
	//	Configurazione accesso al sito web
	char		m_userName [ 16 ];
	char		m_userPwd [ 16 ];
	//	Configurazione porte seriali
	uint32_t	m_uartExtBaudRate;
	uint32_t	m_uartIntBaudRate;
	uint8_t		m_intNetMasterAddress;
	uint8_t		m_extNetSlaveAddress;
	//	Parametri di configurazione del sistema (dal codice originale)
	uint16_t 	m_vttParam 	[ MAX_PARAM ];
	uint8_t 	m_vttFlag 	[ MAX_FLAG ];
	uint8_t 	m_vttSel 	[ MAX_SEL ];

} APP_SystemConfigData_t;

extern struct 	APP_SystemConfigData sysCfg;

#endif


/********************************************************************************************/
