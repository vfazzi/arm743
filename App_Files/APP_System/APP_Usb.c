/********************************************************************************************/
/*	APP_Usb.c					- Funzioni gestione comunicazione su USB					*/

/********************************************************************************************/
// Include di sistema e applicazione
#include "SYS_Include.h"
#include "APP_Include.h"

/********************************************************************************************/
/* 	Comunicazione tramite seriale virtuale su USB											*/
/*																							*/
/*	APP_USBReceiveCallback ( )		- Ricezione dati da USB									*/
/*	APP_USBSendCompleteCallback ( )	- Termine trasmissione su USB							*/
/*	APP_USBConnectCallback ( )		- Notifica connessione USB								*/
/* 	APP_USBIsReady ( )				- Controllo stato USB									*/
/* 	APP_USBSendData ( )				- Trasmissione dati su USB								*/
/********************************************************************************************/
/*------------------------------------------------------------------------------------------*/
/*	APP_USBReceiveCallback ( )	- Ricezione dati da USB										*/
/*	rxBuffer	= buffer dati ricevuti														*/
/*	rxCount		= dimensione dati ricevuti													*/
/*------------------------------------------------------------------------------------------*/
void inline APP_USBReceiveCallback ( uint8_t* rxBuffer, int rxCount )
{

}
/*------------------------------------------------------------------------------------------*/
/*	APP_USBSendCompleteCallback ( )	- Termine trasmissione su USB							*/
/*------------------------------------------------------------------------------------------*/
void inline APP_USBSendCompleteCallback ( void )
{

}
/*------------------------------------------------------------------------------------------*/
/*	APP_USBConnectCallback ( )		- Notifica connessione USB								*/
/*------------------------------------------------------------------------------------------*/
void inline APP_USBConnectCallback ( void )
{

}
/*------------------------------------------------------------------------------------------*/
/* 	APP_USBIsReady ( )				- Controllo stato USB									*/
/*	Valore di ritorno:																		*/
/*	true	se USB connessa																	*/
/*	false	se USB non connessa																*/
/*------------------------------------------------------------------------------------------*/
//bool APP_USBIsReady ( void );

/*------------------------------------------------------------------------------------------*/
/* 	APP_USBSendData ( )		- Trasmissione dati su USB										*/
/*	txData 	= buffer da trasmettere															*/
/*	txSize	= dimensione blocco dati														*/
/*	Valore di ritorno:																		*/
/*	USBD_OK			= OK																	*/
/*	USBD_FAIL		= errore trasmissione													*/
/*	USBD_BUSY		= occupato																*/
/*------------------------------------------------------------------------------------------*/
//uint8_t APP_USBSendData ( uint8_t* txData, int txSize );


/********************************************************************************************/
