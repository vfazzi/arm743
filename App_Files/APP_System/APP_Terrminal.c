/********************************************************************************************/
/*	APP_Terminal.c				- Gestione terminale										*/

/********************************************************************************************/
// Include di sistema e applicazione
#include <APP_Terminal.h>
#include "SYS_Include.h"
#include "APP_Include.h"



const char msgSaveAndRestart [] = "OK - Salvare la configurazione di sistema (save cfg).\r\n>";
const char msgRestart [] = "Configurazione salvata. Eseguire un reboot\r\n>";
const char msgError [] = "Parametri non corretti o errore comando\r\n>";


/*------------------------------------------------------------------------------------------*/
/*	Dichiarazione funzioni di supporto														*/
/*------------------------------------------------------------------------------------------*/
void inline printNetConfig ( void );
void inline printParamConfig ( void );
void inline printFlagsConfig ( void );
void inline printSelConfig ( void );
void inline printDateTime ( void );
char inline setIpAddress ( char* ip );
char inline setGatewayAddress ( char* ip );
char inline setNetMask ( char* ip );
char inline setMacAddress ( char* mac );
char inline setParam ( int nParam, int parVal );
char inline setFlag ( int nParam, int parVal );
char inline setSel ( int nParam, int parVal );
char inline setSystemTime ( char* newTime );
char inline setSystemDate ( char* newDate );

#define		FLGERROR_OK					0
#define		FLGERROR_SAVE_AND_RESTART	1
#define		FLGERROR_ERROR				100

/********************************************************************************************/
/*	Funzioni gestione comandi																*/
/*																							*/
/*	TERM_GetCommand ( )				- Gestione comando set									*/
/*	TERM_SetCommand ( )				- Gestione comando set									*/
/*	TERM_SaveSettings ( )				- Salva la configurazione							*/
/*	TERM_Reboot ( )					- Reset scheda										*/
/********************************************************************************************/
/*------------------------------------------------------------------------------------------*/
/*	TERM_GetCommand ( )				- Gestione comando set									*/
/*------------------------------------------------------------------------------------------*/
void TERM_GetCommand (int argc, char **argv)
{
	//	Il comando accetta un solo parametro
	if ( argc == 1 )
	{
		//	Get configurazione rete
		if ( !strcmpi ( argv [1], "net") ) 			printNetConfig ( );
		//	Get parametri azionamento
		else if ( !strcmpi ( argv [1], "param") )	printParamConfig ( );
		//	Get flags azionamento
		else if ( !strcmpi ( argv [1], "flags") )	printFlagsConfig ( );
		//	Get selezioni azionamento
		else if ( !strcmpi ( argv [1], "sel") )		printSelConfig ( );
		//	Get orologio di sistema
		else if ( !strcmpi ( argv [1], "time") )	printDateTime ( );
		//	Get orologio di sistema
		else if ( !strcmpi ( argv [1], "date") )	printDateTime ( );
	}
	printf ( ">");
}

/*------------------------------------------------------------------------------------------*/
/*	TERM_SetCommand ( )				- Gestione comando set									*/
/*------------------------------------------------------------------------------------------*/
void TERM_SetCommand ( int argc, char **argv)
{
	char flgError = FLGERROR_ERROR;

	//	Comandi SET con 1 solo parametro
	if ( argc == 1 )
	{
		//	Set valori predefiniti
		if ( !strcmpi ( argv [1], "default") )
		{
			APP_LoadDefaultSettings ( );
			printf ( "OK\r\n" );
		}
	}
	//	Comandi SET con 2 parametri:
	else if ( argc == 2 )
	{
		//	Set indirizzo IP
		if ( !strcmpi ( argv [1], "ip") )			flgError = setIpAddress ( argv [2] );
		//	Set indirizzo gateway
		else if ( !strcmpi ( argv [1], "gateway") )	flgError = setGatewayAddress ( argv [2] );
		//	Set netmask
		else if ( !strcmpi ( argv [1], "netmask") )	flgError = setNetMask ( argv [2] );
		//	Set MAC address
		else if ( !strcmpi ( argv [1], "mac") )		flgError = setMacAddress ( argv [2] );
		//	Set time
		else if ( !strcmpi ( argv [1], "time") )
		{
			flgError = APP_SetSystemTimeString ( argv [2] ) ? FLGERROR_OK : FLGERROR_ERROR;
		}
		//	Set date
		else if ( !strcmpi ( argv [1], "date") )
		{
			flgError = APP_SetSystemDateString ( argv [2] ) ? FLGERROR_OK : FLGERROR_ERROR;
		}

	}
	//	Comandi SET con 3 parametri
	else if ( argc == 3 )
	{
		int nParam = strtol ( argv [ 2 ], NULL, 0 );
		int parVal = strtol ( argv [ 3 ], NULL, 0  );
		//	Set parametri azionamento (?)
		if ( !strcmpi ( argv [1], "param") )		flgError = setParam ( nParam, parVal );
		//	Set flag azionamento (?)
		else if ( !strcmpi ( argv [1], "flag") )	flgError = setFlag ( nParam, parVal );
		//	Set selezioni azionamento  (?)
		else if ( !strcmpi ( argv [1], "sel") )		flgError = setSel ( nParam, parVal );
	}
	//	Visualizza messaggio
	switch ( flgError )
	{
		case FLGERROR_OK:
			printf ( "OK\r\n>" );
		break;
		case FLGERROR_SAVE_AND_RESTART:
			printf ( msgSaveAndRestart );
		break;
		case FLGERROR_ERROR:
			printf ( msgError );
		break;
	}
	printf ( ">");
}
/*------------------------------------------------------------------------------------------*/
/*	TERM_SaveCommand ( )			- Gestione comando save									*/
/*------------------------------------------------------------------------------------------*/
void TERM_SaveCommand (int argc, char **argv)
{
	if ( argc == 1 )
	{
		//	Salva la configurazione di sistema
		if ( !strcmpi ( argv [1], "cfg") )
		{
			APP_AsyncSaveSettings ( );
			printf ( msgRestart );
		}
	}
}
/*------------------------------------------------------------------------------------------*/
/*	TERM_Reboot ( )						- Reset scheda										*/
/*------------------------------------------------------------------------------------------*/
void TERM_Reboot (int argc, char **argv)
{
	SYSTEM_RESET;
}


/********************************************************************************************/
/*	Funzioni di supporto																	*/
/*																							*/
/*	printNetConfig ( )					- Visualizza la configurazione di rete				*/
/*	printParamConfig ( )				- Visualizza i parametri dell'azionamento			*/
/*	printFlagsConfig ( )				- Visualizza i flags dell'azionamento				*/
/*	printSelConfig ( )					- Visualizza le selezioni dell'azionamento			*/
/*	setIpAddress ( )					- Imposta l'indirizzo IP							*/
/*	setGatewayAddress ( )				- Imposta l'indirizzo del gateway					*/
/*	setNetMask ( )						- Imposta la subnet mask							*/
/*	setMacAddress ( )					- Imposta il MAC address							*/
/*	setParam ( )						- Imposta un parametro dell'azionamento				*/
/*	setFlag ( )							- Imposta un flag dell'azionamento					*/
/*	setSel ( )							- Imposta una selezione dell'azionamento			*/
/*	printDebugInfo ( )					- Visualizza le informazioni di debug				*/
/********************************************************************************************/
/*------------------------------------------------------------------------------------------*/
/*	printNetConfig ( )					- Visualizza la configurazione di rete				*/
/*------------------------------------------------------------------------------------------*/
void printNetConfig ( void )
{
	#ifdef ETH_ENABLED
		//	Indirizzi di rete
		printf ( "IP address: %d.%d.%d.%d\r\n",
				 sysCfg.m_ipAddress [ 0 ],
				 sysCfg.m_ipAddress [ 1 ],
				 sysCfg.m_ipAddress [ 2 ],
				 sysCfg.m_ipAddress [ 3 ]
			);
		printf ( "Gateway address: %d.%d.%d.%d\r\n",
				 sysCfg.m_gatewayAddress [ 0 ],
				 sysCfg.m_gatewayAddress [ 1 ],
				 sysCfg.m_gatewayAddress [ 2 ],
				 sysCfg.m_gatewayAddress [ 3 ]
			);
		printf ( "Subnet mask: %d.%d.%d.%d\r\n",
				 sysCfg.m_subnetMask [ 0 ],
				 sysCfg.m_subnetMask [ 1 ],
				 sysCfg.m_subnetMask [ 2 ],
				 sysCfg.m_subnetMask [ 3 ]
			);
		printf ( "MAC address: %02x:%02x:%02x:%02x:%02x:%02x\r\n",
				 sysCfg.m_macAddress [ 0 ],
				 sysCfg.m_macAddress [ 1 ],
				 sysCfg.m_macAddress [ 2 ],
				 sysCfg.m_macAddress [ 3 ],
				 sysCfg.m_macAddress [ 4 ],
				 sysCfg.m_macAddress [ 5 ]
			);
		//	Stato connessione
		if ( APP_IsEthCableConnected ( ) )
		{
			printf ( "Cavo di rete connesso\r\n" );
		}
		else
		{
			printf ( "Cavo di rete non connesso\r\n" );
		}
		//	Stato dei server
		for ( uint8_t i = 0; i < APP_GetServerCount ( ); i ++ )
		{
			TCPServerHandle* h = SRV_GetServerHandle ( i );
			printf ( "Server attivo su porta %d - %s\r\n",
					 h->connPort,
					 h->isConnected ? "Connesso" : "Non connesso"
				   );
		}
	#else
		printf ( "Interfaccia Ethernet non attiva.\r\n");
	#endif
	//	Clock CPU
	printf ( "CPU Clock = %ld\r\n", HAL_RCC_GetSysClockFreq ( ) );
}
/*------------------------------------------------------------------------------------------*/
/*	printParamConfig ( )				- Visualizza i parametri dell'azionamento			*/
/*------------------------------------------------------------------------------------------*/
void printParamConfig ( void )
{
	for ( int i = 0; i < MAX_PARAM; i ++ )
	{
		printf ( "Parametro n.%d = %d [0x%X]\r\n", i, sysCfg.m_vttParam[i], (unsigned int)sysCfg.m_vttParam[i]);
	}
}
/*------------------------------------------------------------------------------------------*/
/*	printFlagsConfig ( )				- Visualizza i flags dell'azionamento				*/
/*------------------------------------------------------------------------------------------*/
void printFlagsConfig ( void )
{
	for ( int i = 0; i < MAX_FLAG; i ++ )
	{
		printf ( "Flag n.%d = %d [0x%X]\r\n", i, sysCfg.m_vttFlag[i], (unsigned int)sysCfg.m_vttFlag[i]);
	}
}
/*------------------------------------------------------------------------------------------*/
/*	printSelConfig ( )					- Visualizza le selezioni dell'azionamento			*/
/*------------------------------------------------------------------------------------------*/
void printSelConfig ( void )
{
	for ( int i = 0; i < MAX_SEL; i ++ )
	{
		printf ( "Selezione n.%d = %d [0x%X]\r\n", i, sysCfg.m_vttSel[i], (unsigned int)sysCfg.m_vttSel[i]);
	}

}
/*------------------------------------------------------------------------------------------*/
/*	printDateTime ( )					- Visualizza data e ora di sistema					*/
/*------------------------------------------------------------------------------------------*/
void printDateTime ( void )
{
	char strDateBuffer [ 12 ];
	char strTimeBuffer [ 10 ];
	APP_GetSystemDateString ( strDateBuffer );
	APP_GetSystemTimeString ( strTimeBuffer );
	printf ( "Data: %s\r\nOra: %s\r\n>", strDateBuffer, strTimeBuffer );
}
/*------------------------------------------------------------------------------------------*/
/*	setIpAddress ( )					- Imposta l'indirizzo IP							*/
/*------------------------------------------------------------------------------------------*/
char setIpAddress ( char* ip )
{
	uint8_t  ipAddr [ 4 ];
	if ( APP_utiParseIPAddress ( ip, ipAddr ) )
	{
		sysCfg.m_ipAddress [ 0 ] = ipAddr [ 0 ];
		sysCfg.m_ipAddress [ 1 ] = ipAddr [ 1 ];
		sysCfg.m_ipAddress [ 2 ] = ipAddr [ 2 ];
		sysCfg.m_ipAddress [ 3 ] = ipAddr [ 3 ];
		return (FLGERROR_SAVE_AND_RESTART);
	}
	return(FLGERROR_ERROR);
}
/*------------------------------------------------------------------------------------------*/
/*	setGatewayAddress ( )				- Imposta l'indirizzo del gateway					*/
/*------------------------------------------------------------------------------------------*/
char setGatewayAddress ( char* ip )
{
	uint8_t  ipAddr [ 4 ];
	if ( APP_utiParseIPAddress ( ip, ipAddr ) )
	{
		sysCfg.m_gatewayAddress [ 0 ] = ipAddr [ 0 ];
		sysCfg.m_gatewayAddress [ 1 ] = ipAddr [ 1 ];
		sysCfg.m_gatewayAddress [ 2 ] = ipAddr [ 2 ];
		sysCfg.m_gatewayAddress [ 3 ] = ipAddr [ 3 ];
		return (FLGERROR_SAVE_AND_RESTART);
	}
	return(FLGERROR_ERROR);
}
/*------------------------------------------------------------------------------------------*/
/*	setNetMask ( )						- Imposta la subnet mask							*/
/*------------------------------------------------------------------------------------------*/
char setNetMask ( char* ip )
{
	uint8_t  ipAddr [ 4 ];
	if ( APP_utiParseIPAddress ( ip, ipAddr ) )
	{
		sysCfg.m_subnetMask [ 0 ] = ipAddr [ 0 ];
		sysCfg.m_subnetMask [ 1 ] = ipAddr [ 1 ];
		sysCfg.m_subnetMask [ 2 ] = ipAddr [ 2 ];
		sysCfg.m_subnetMask [ 3 ] = ipAddr [ 3 ];
		return (FLGERROR_SAVE_AND_RESTART);
	}
	return(FLGERROR_ERROR);
}
/*------------------------------------------------------------------------------------------*/
/*	setMacAddress ( )					- Imposta il MAC address							*/
/*------------------------------------------------------------------------------------------*/
char setMacAddress ( char* mac )
{
	uint8_t macAddr [ 6 ];
	if ( APP_utiParseMacAddress ( mac, macAddr ) )
	{
		sysCfg.m_macAddress [ 0 ] = macAddr [ 0 ];
		sysCfg.m_macAddress [ 1 ] = macAddr [ 1 ];
		sysCfg.m_macAddress [ 2 ] = macAddr [ 2 ];
		sysCfg.m_macAddress [ 3 ] = macAddr [ 3 ];
		sysCfg.m_macAddress [ 4 ] = macAddr [ 4 ];
		sysCfg.m_macAddress [ 5 ] = macAddr [ 5 ];
		return (FLGERROR_SAVE_AND_RESTART);
	}
	return(FLGERROR_ERROR);
}
/*------------------------------------------------------------------------------------------*/
/*	setParam ( )						- Imposta un parametro dell'azionamento				*/
/*------------------------------------------------------------------------------------------*/
char setParam ( int nParam, int parVal )
{
	if ( nParam < MAX_PARAM )
	{
		sysCfg.m_vttParam [ nParam ] = parVal;
		return (FLGERROR_SAVE_AND_RESTART);
	}
	return ( FLGERROR_ERROR );
}
/*------------------------------------------------------------------------------------------*/
/*	setFlag ( )							- Imposta un flag dell'azionamento					*/
/*------------------------------------------------------------------------------------------*/
char setFlag ( int nParam, int parVal )
{
	if ( nParam < MAX_FLAG )
	{
		sysCfg.m_vttFlag [ nParam ] = parVal;
		return (FLGERROR_SAVE_AND_RESTART);
	}
	return ( FLGERROR_ERROR );
}
/*------------------------------------------------------------------------------------------*/
/*	setSel ( )							- Imposta una selezione dell'azionamento			*/
/*------------------------------------------------------------------------------------------*/
char setSel ( int nParam, int parVal )
{
	if ( nParam < MAX_SEL )
	{
		sysCfg.m_vttSel [ nParam ] = parVal;
		return (FLGERROR_SAVE_AND_RESTART);
	}
	return ( FLGERROR_ERROR );
}
/*------------------------------------------------------------------------------------------*/
/*	setSystemTime ( )					- Impostazione orario di sistema					*/
/*------------------------------------------------------------------------------------------*/
char setSystemTime ( char* newTime )
{
	if ( strlen ( newTime ) == 8 )
	{
		sysTime	time;
		sscanf ( newTime, "%02d:%02d:%02d", &time.tm_hour, &time.tm_min, &time.tm_Sec );
		APP_SetSystemTime ( &time );
	}
	return ( FLGERROR_ERROR );
}
/*------------------------------------------------------------------------------------------*/
/*	setSystemDate ( )					- Impostazione data di sistema						*/
/*------------------------------------------------------------------------------------------*/
char setSystemDate ( char* newDate )
{
	if ( strlen ( newDate ) == 10 )
	{
		sysDate date;
		sscanf ( newDate, "%02d-%02d-%04d", &date.dt_day, &date.dt_month, &date.dt_year );
		APP_SetSystemDate ( &date );
	}
	return ( FLGERROR_ERROR );
}

