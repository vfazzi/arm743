/********************************************************************************************/
/*	APP_Uart.c					- Funzioni gestione comunicazione tramite porte serialo		*/

/********************************************************************************************/
// Include di sistema e applicazione
#include "SYS_Include.h"
#include "APP_Include.h"


/********************************************************************************************/
/*	Indirizzi di rete																		*/
/********************************************************************************************/
//	Rete interna
unsigned char net485InternalDevAddr [] =
{
	DEFAULT_485_AZ_ADDRESS,				//	E' presente solo l'azionamento
};


/********************************************************************************************/
/*	Funzioni del modulo																		*/
/*																							*/
/*	APP_485InternalNetworkRcvMsgError ( )	- Errore messaggio ricevuto da rete interna		*/
/*	APP_485InternalNetworkGetNextScanAddress () - Indirizzi scansione rete interna			*/
/*																							*/
/*	APP_485ExternalNetworkRcvMsgError ( )	- Errore messaggio ricevuto da rete esterna		*/
/*																							*/
/*	APP_UART6StartSendChar ( )		- Avvio trasmissione su UART6							*/
/*	APP_UART6RxCharCallback ( )		- Callback ricezione di un carattere da UART6			*/
/*	APP_UART6TxNextCharRequest ( )	- Richiesta prossimo carattere da trasmettere su UART6	*/
/*																							*/
/*	APP_UART3StartSendChar ( )		- Avvio trasmissione su UART3							*/
/*	APP_UART3RxCharCallback ( )		- Callback ricezione di un carattere da UART3			*/
/*	APP_UART3TxNextCharRequest ( )	- Richiesta prossimo carattere da trasmettere su UART3	*/
/********************************************************************************************/


/********************************************************************************************/
/*	CEIAIBI UART6 - Master su rete interna													*/
/*																							*/
/*	APP_485InternalNetworkRcvMsgError ( )	- Errore messaggio ricevuto da rete interna		*/
/*	APP_485InternalNetworkGetNextScanAddress () - Indirizzi scansione rete interna			*/
/********************************************************************************************/
#ifdef	USART6_INT_485_ENABLED
/*------------------------------------------------------------------------------------------*/
/*	APP_485InternalNetworkRcvMsgError ( )	- Errore messaggio ricevuto da rete interna		*/
/*------------------------------------------------------------------------------------------*/
void inline APP_485InternalNetworkRcvMsgError ( void )
{
	
}
/*------------------------------------------------------------------------------------------*/
/*	APP_485InternalNetworkGetNextScanAddress () - Indirizzi scansione rete interna			*/
/*	prevAddress 	= indirizzo precedente													*/
/*------------------------------------------------------------------------------------------*/
uint8_t inline APP_485InternalNetworkGetNextScanAddress ( uint8_t prevAddress )
{
	//	In questo caso c'è una sola periferica (azionamento) con indirizzo IND_AZ.
	//	Se ci sono altri oggetti in rete, ad ogni chiamata la funzione deve ritornare
	//	l'indrizzo CEIABI del prossimo oggetto interrogato, in modo ciclico.
	return ( net485InternalDevAddr [ 0 ] );
}
#endif


/********************************************************************************************/
/*	CEIAIBI UART3 - Slave su rete esterna													*/
/*																							*/
/*	APP_485ExternalNetworkRcvMsgError ( )	- Errore messaggio ricevuto da rete esterna		*/
/********************************************************************************************/
#ifdef	USART3_EXT_485_ENABLED
/*------------------------------------------------------------------------------------------*/
/*	APP_485ExternalNetworkRcvMsgError ( )	- Errore messaggio ricevuto da rete esterna		*/
/*------------------------------------------------------------------------------------------*/
void inline APP_485ExternalNetworkRcvMsgError ( void )
{
	
}
#endif


/********************************************************************************************/
/*	Gestione diretta UART 6																	*/
/*																							*/
/*	APP_UART6StartSendChar ( )		- Avvio trasmissione su UART6							*/
/*	APP_UART6RxCharCallback ( )		- Callback ricezione di un carattere da UART6			*/
/*	APP_UART6TxNextCharRequest ( )	- Richiesta prossimo carattere da trasmettere su UART6	*/
/********************************************************************************************/
#ifndef	USART6_INT_485_ENABLED
/*------------------------------------------------------------------------------------------*/
/*	APP_UART6StartSendChar ( )		- Avvio trasmissione su UART6							*/
/*	frstChar	= carattere da trasmettere													*/
/*------------------------------------------------------------------------------------------*/
void APP_UART6StartSendChar ( uint8_t frstChar )
{
	__HAL_UART_DISABLE_IT(&huart6, UART_IT_RXNE);
	DEREINT_ON;
	USART6->TDR = frstChar;
	LL_USART_ClearFlag_TC(USART6);
	__HAL_UART_ENABLE_IT(&huart6, UART_IT_TC);

}
/*------------------------------------------------------------------------------------------*/
/*	APP_UART6RxCharCallback ( )		- Callback ricezione di un carattere da UART6			*/
/*	rxChar		= carattere ricevuto														*/
/*------------------------------------------------------------------------------------------*/
void inline APP_UART6RxCharCallback ( uint8_t rxChar )
{


}
/*------------------------------------------------------------------------------------------*/
/*	APP_UART6TxNextCharRequest ( )	- Richiesta prossimo carattere da trasmettere su UART6	*/
/*	nextChar	= carattere da trasmettere													*/
/*	Valore di ritorno: true se ci sono altri caratteri da trasmettere, altrimenti false		*/
/*------------------------------------------------------------------------------------------*/
bool inline APP_UART6TxNextCharRequest ( uint8_t* nextChar )
{

	return ( false );
}
#endif


/********************************************************************************************/
/*	Gestione diretta UART 3																	*/
/*																							*/
/*	APP_UART3StartSendChar ( )		- Avvio trasmissione su UART3							*/
/*	APP_UART3RxCharCallback ( )		- Callback ricezione di un carattere da UART3			*/
/*	APP_UART3TxNextCharRequest ( )	- Richiesta prossimo carattere da trasmettere su UART3	*/
/********************************************************************************************/
#ifndef	USART3_EXT_485_ENABLED
/*------------------------------------------------------------------------------------------*/
/*	APP_UART3StartSendChar ( )		- Avvio trasmissione su UART3							*/
/*	frstChar	= carattere da trasmettere													*/
/*------------------------------------------------------------------------------------------*/
void APP_UART3StartSendChar ( uint8_t frstChar )
{
	__HAL_UART_DISABLE_IT(&huart3, UART_IT_RXNE);
	DEREINT_ON;
	USART3->TDR = frstChar;
	LL_USART_ClearFlag_TC(USART3);
	__HAL_UART_ENABLE_IT(&huart3, UART_IT_TC);
}
/*------------------------------------------------------------------------------------------*/
/*	APP_UART3RxCharCallback ( )		- Callback ricezione di un carattere da UART3			*/
/*	rxChar		= carattere ricevuto														*/
/*------------------------------------------------------------------------------------------*/
void inline APP_UART3RxCharCallback ( uint8_t rxChar )
{


}
/*------------------------------------------------------------------------------------------*/
/*	APP_UART3TxNextCharRequest ( )	- Richiesta prossimo carattere da trasmettere su UART3	*/
/*	nextChar	= carattere da trasmettere													*/
/*	Valore di ritorno: true se ci sono altri caratteri da trasmettere, altrimenti false		*/
/*------------------------------------------------------------------------------------------*/
bool inline APP_UART3TxNextCharRequest ( uint8_t* nextChar )
{

	return ( false );
}
#endif

/********************************************************************************************/
