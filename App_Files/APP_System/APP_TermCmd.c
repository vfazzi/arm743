/********************************************************************************************/
/*	APP_TermCmd.h			- Definizione comandi del terminale								*/

/********************************************************************************************/
// Include di sistema e applicazione
#include "SYS_Include.h"
#include "APP_Include.h"


/********************************************************************************************/
/*	Tabella definizione dei comandi	riconosciuti dal terminale								*/
/*																							*/
/*	Ogni comando riconosciuto dal terimnale viene definito tramite una struttura:			*/
/*	typedef const struct																	*/
/*	{																						*/
/*		char *	cmd;					// Comando											*/
/*		char 	min_args;				// Numero minimo di argomenti						*/
/*		char	max_args;				// Numero massimo di argomenti						*/
/*		void	(*func)(int, char **);	// puntatore a Funzione da eseguire 				*/
/*		char *	Help;					// Descrizione help									*/
/*		char *	syntax;					// Sintassi del comando								*/
/*		char	flag;					// Bit 0 = 1 visualizza help della voce menu		*/
/*	} CMD;																					*/
/*																							*/
/********************************************************************************************/
const CMD _CmdTab[]=
{
	// Comando help (sempre presente)
	{
		"help",								//	Nome comando
		0,									//	Può avere 0 o 1 parametro
		1,
		TERM_help,							//	Handler
		"visualizza l'elenco dei comandi",	//	Descrizione
		"",									//	Sintassi
		1
	},
	//	Comando set = imposta un parametro
	{
			"set",
			1,
			3,
			TERM_SetCommand,
			"Imposta un parametro di configurazione",
			"    set ip      xxx.xxx.xxx.xxx    [indirizzo IP]\n\r"
			"    set gateway xxx.xxx.xxx.xxx    [indirizzo gateway]\r\n"
			"    set netmask xxx.xxx.xxx.xxx    [netmask]\r\n"
			"    set mac     hh:hh:hh:hh:hh:hh  [indirizzo MAC]\r\n"
			"    set time    hh:mm:ss\r\n"
			"    set date    GG-MM-AAAA\r\n"
			"    set param   p [numero parametro] v [valore parametro]\r\n"
			"    set flag    f [numero flag]      v [valore flag]\r\n"
			"    set sel     s [numero selezione] v [valore selezione]\r\n"
			"    set default   (ripristino configurazione predefinita)",
			1
	},
	//	Comando get - Visualizza la configurazione
	{
			"get",
			1,
			1,
			TERM_GetCommand,
			"Legge un gruppo di parametri di configurazione",
			"    get net (lettura parametri rete)\r\n"
			"    get param (lettura parametri )\r\n"
			"    get flag (lettura flag )\r\n"
			"    get time / date (lettura data e ora di sistema)\r\n"
			"    get sel (lettura selezioni azionamento)",
			1

	},
	//	Comando save - Salva le configurazioni
	{
			"save",
			1,
			1,
			TERM_SaveCommand,
			"Salva le impostazioni",
			"    save cfg (salva tutta la configurazione di sistema)",
			1
	},
	//	reboot = reset della scheda
	{
		"reboot",
		0,
		0,
		TERM_Reboot,
		"Riavvio del sistema (reset)",
		"",
		1
	},
	//	Comandi definiti dall'applicazione
	//	Comandi VARCO
	//	debug = mostra info di debug
	{
		"debug",
		0,
		0,
		VARCO_TermDebugInfoCommand,
		"Mostra informazioni di debug",
		"",
		1
	},
	//	open = apre porta
	{
		"open",
		1,
		1,
		VARCO_TermOpenCommand,
		"Apre una porta",
		"    open in  (apre porta ingresso)\r\n"
		"    open out (apre porta uscita)",
		1
	}
};

//	Calcola il numero di comandi totale
int	NUM_CMD = (sizeof(_CmdTab)/sizeof(CMD));

/********************************************************************************************/

