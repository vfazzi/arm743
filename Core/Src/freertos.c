/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "SYS_Include.h"
#include "APP_Include.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
/*------------------------------------------------------------------------------------------*/
/*	Definizione del thread di polling delle periferiche di sistema							*/
/*	Solo se abilitata la gestione dal thread separato										*/
/*------------------------------------------------------------------------------------------*/
#ifdef SYS_POLLING_PRIVATE_THREAD
	//	Struttura di definizione del thread
	osThreadId_t sysPollingTaskHandle;;
	const osThreadAttr_t sysPollingTask_attributes = {
	  .name = "sysPollingtTask",
	  .stack_size = 256 * 4,
	  .priority = (osPriority_t) osPriorityNormal,
	};
	//	Funzione del thread
	void StartSysPollingTask(void *argument);
#endif
/* USER CODE END Variables */
/* Definitions for defaultTask */
osThreadId_t defaultTaskHandle;
const osThreadAttr_t defaultTask_attributes = {
  .name = "defaultTask",
  .stack_size = 256 * 4,
  .priority = (osPriority_t) osPriorityNormal,
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void *argument);

extern void MX_LWIP_Init(void);
extern void MX_USB_DEVICE_Init(void);
void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of defaultTask */
  defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  #ifdef SYS_POLLING_PRIVATE_THREAD
		sysPollingTaskHandle = osThreadNew(StartSysPollingTask, NULL, &sysPollingTask_attributes);
  #endif
  /* add threads, ... */
  //	Creazione thread dell'applicazione
  VARCO_ThreadCreate ( );
  //	Avvio web server
  #ifdef WEB_SERVER_ENABLED
  	  WSRV_Start ( );
  #endif
  	//	Avvio server TCPIP dell'applicazione
	#ifdef ETH_ENABLED
  	  APP_TcpStartAppServer ( );
	#endif
  	//	Avvio server di aggiornamento firmware
  	#ifdef FWUPDATE_SERVER_ENABLED
  	  	FWU_StartSystem ( );
  	#endif

  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
  /* USER CODE END RTOS_EVENTS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* init code for LWIP */
#ifdef ETH_ENABLED
  MX_LWIP_Init();
#endif

  /* init code for USB_DEVICE */
  MX_USB_DEVICE_Init();
  /* USER CODE BEGIN StartDefaultTask */
#ifndef SYS_POLLING_PRIVATE_THREAD
  SYS_SystemLoopInit ( );
#endif
  VARCO_InitMainThread ( );
  /* Infinite loop */
  for(;;)
  {
#ifndef SYS_POLLING_PRIVATE_THREAD
	  //	Polling periferiche dal thread principale
	  SYS_SystemLoopCallback ( );
#endif
	  //	Applicazione
	  VARCO_MainLoopProc ( );
	  //	Gestione salvataggio configurazione in modo asincrono
	  APP_AsyncSaveSettingsProc ( );
  }
  /* USER CODE END StartDefaultTask */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
#ifdef	SYS_POLLING_PRIVATE_THREAD
	//	Se non è definito il tempo di pollong imposta 1ms
	#ifndef SYS_POLLING_THREAD_SLEEPTIME
		#define	SYS_POLLING_THREAD_SLEEPTIME mSEC(1)
	#endif
/*------------------------------------------------------------------------------------------*/
/*	StartSysPollingTask ( )			- Thread per il polling delle periferiche di sistema	*/
/*	Solo se abilitata la gestione dal thread separato										*/
/*------------------------------------------------------------------------------------------*/
	void StartSysPollingTask(void *argument)
	{
	  SYS_SystemLoopInit ( );
	  /* Infinite loop */
	  for(;;)
	  {
		  SYS_SystemLoopCallback ( );
		  osDelay ( SYS_POLLING_THREAD_SLEEPTIME);
	  }
	}
#endif

/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
